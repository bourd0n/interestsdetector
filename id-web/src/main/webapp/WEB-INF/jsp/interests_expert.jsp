<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<head>
    <title>Interests Detector</title>
    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <script type="text/javascript" src="<c:url value="/resources/js/index.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-2.1.3.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/listgroup.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/notify.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/index.js" />"></script>
</head>
<body>
<div class="container">

    <div class="header clearfix">
        <h3 class="text-muted">Interests Detector</h3>
    </div>

    <!-- Jumbotron -->
    <div class="jumbotron">

        <p class="lead">Interests for user: <a href="https://twitter.com/${screen_name}"> ${screen_name} </a></p>

        <p>User Name: ${name} </p>

        <p>User BIO: ${bio} </p>
    </div>

    <!-- Example row of columns -->
    <div class="row">
        <div class="col-sm-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Tweets</h3>
                </div>
                <ul class="list-group pre-scrollable">
                    <c:forEach var="tweet" items="${tweets}">
                        <li class="list-group-item">
                            <p>${tweet.text}</p>
                            <small class="text-muted">${tweet.createdAt}</small>
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
        <div class="col-sm-5">
            <form id="mainform" method="post" action="${pageContext.request.contextPath}/saveExpertChoice">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Detected Interests</h3>
                    </div>
                    <div id="interests_div" class="pre-scrollable">
                        <select class="list-group pre-scrollable" form="mainform" multiple>
                            <c:forEach var="interest" items="${interests}">
                                <option class="myselect" value="${interest.value}">
                                        ${interest.key}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <input type="hidden" id="userId" name="userId" value="${userId}">
                </div>
                <button type="submit" id="submit_relevant" name="submit_relevant" value="submit_relevant" class="btn btn-default">
                    Submit Relevant
                </button>
                <button type="button" id="calc_map" name="calc_map" value="calc_map" class="btn btn-default">
                    Calculate MAP
                </button>
            </form>
        </div>
    </div>

    <footer class="footer">
        <p>&copy; Nikita Aleksandrov 2015</p>
    </footer>

</div>

</body>
</html>
