<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<head>
  <title>Interests Detector</title>
  <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
  <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
  <script type="text/javascript" src="<c:url value="/resources/js/index.js" />"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/jquery-2.1.3.js" />"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/notify.min.js" />"></script>
  <script type="text/javascript" src="<c:url value="/resources/js/index.js" />"></script>
</head>
<body>
<div class="container">

  <div class="header clearfix">
    <h3 class="text-muted">Interests Detector</h3>
  </div>

  <!-- Jumbotron -->
  <div class="jumbotron">

    <p class="lead">Calculated mean avg precision for user with id: ${userId} </p>

  </div>

  <!-- Example row of columns -->
  <div class="row">
    <div class="col-sm-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Mean Average Precision</h3>
        </div>
        <ul class="list-group">
          <c:forEach var="entry" items="${meanAvgPrec}">
            <li class="list-group-item">
              <p>${entry.key} = ${entry.value}</p>
            </li>
          </c:forEach>
        </ul>
      </div>
    </div>

  <footer class="footer">
    <p>&copy; Nikita Aleksandrov 2015</p>
  </footer>

</div>

</body>
</html>
