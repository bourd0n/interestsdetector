function fillusers() {
    $.ajax({
        url: 'getloadedusers',
        method: 'get',
        ContentType: 'json',
        success: function (response) {
            var options = '';
            if (response != null) {
                $.each(response, function (index, value) {
                    options = options + '<option value=' + index + '>' + value + '</option>';
                });
                $('#loadedUsers').html(options);
            }
        }
    });
}

$(function () {
    $("#mainform").on("submit", function (e) {
        e.preventDefault();
        var selectedCategories = $('#interests_div').find('ul.list-group').find('.active');
        var userId = $('#userId').attr('value');
        console.log(userId);
        var selectedCategoriesIds = [];
        $.each(selectedCategories, function (index, value) {
            var t = $(value).attr('data-value');
            console.log(t);
            var ids = t.substr(1, t.length-2).split(",");
            $.each(ids, function (i, v){
                v = $.trim(v);
                selectedCategoriesIds.push(v);
            });
        });
        console.log(selectedCategoriesIds);
        $.ajax({
            url: $(this).attr("action"),
            type: 'POST',
            data: {selectedCategoriesIds : selectedCategoriesIds, userId : userId},
            success: function (data) {
                $("#submit_relevant").notify("Thanks!", "success");
            }
        });
    });
});

$(function () {
    $("#calc_map").on("click", function (e) {
        e.preventDefault();
        var userId = $('#userId').attr('value');
        window.location.href = '/id-web/calculatemap?userId='+userId;
        return false;
        //$.ajax({
        //    url: "calculatemap",
        //    type: 'GET',
        //    data: {userId : userId}
        //});
    });
});
