<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Interests Detector</title>
    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <script type="text/javascript" src="<c:url value="/resources/js/index.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-2.1.3.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
</head>
<body>
<div>
    <div class="container">
        <div class="header clearfix">
            <h3 class="text-muted">Interests Detector</h3>
        </div>

        <div class="jumbotron">
            <p class="lead">Select user from already downloaded or enter screen name</p>

            <form id="mainform" method="post" action="${pageContext.request.contextPath}/interests">
                <div class="form-group">
                    <label for="loadedUsers">Already loaded users</label>
                    <select id="loadedUsers" onfocus="fillusers()" class="form-control"
                            title="Select already downloaded user" name="loadedUsers"></select>
                </div>
                <div class="form-group">
                    <label for="screen_name">Enter screen name</label>
                    <input type="text" class="form-control" id="screen_name" placeholder="Screen Name without @"
                           name="screen_name">
                </div>
                <label>
                    <input type="checkbox" name="expertMode">
                    Expert User mode
                </label>
                <br>
                <button type="submit" class="btn btn-default" form="mainform">Detect interests</button>
            </form>
        </div>

        <footer class="footer">
            <p>&copy; Nikita Aleksandrov 2015</p>
        </footer>

    </div>
</div>
</body>
</html>