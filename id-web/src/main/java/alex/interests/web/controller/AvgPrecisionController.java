package alex.interests.web.controller;

import alex.interests.test.AveragePrecision;
import alex.interests.test.Method;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class AvgPrecisionController {

    public static final Logger log = LoggerFactory.getLogger(AvgPrecisionController.class);

    @RequestMapping(value = "/calculatemap", method = RequestMethod.GET)
    public ModelAndView calculateMeanAvgPrecision(@RequestParam(value = "userId", required = false) String userId) {
        log.debug("Start calc map for user {}", userId);
        final Map<Method, Double> meanAvgPrecision = StringUtils.isNotEmpty(userId) ?
                new AveragePrecision(userId, 20).calculateMeanAvgPrecision()
                : AveragePrecision.calculateMeanAvgPrecisionForAllUsers(20);

        ModelAndView view = new ModelAndView("calculatemap");
        view.addObject("userId", String.valueOf(userId));
        view.addObject("meanAvgPrec", meanAvgPrecision);
        return view;
    }
}
