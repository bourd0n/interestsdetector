package alex.interests.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Deprecated
@Controller
public class AlreadyLoadedUsersController {

    @RequestMapping(value = "/welcome", method = RequestMethod.POST)
    public ModelAndView helloWorld() {

        String message = "<br><div style='text-align:center;'>"
                + "<h3>********** Hello World, Spring MVC Tutorial</h3>This message is coming from CrunchifyHelloWorld.java **********</div><br><br>";
        return new ModelAndView("welcome", "message", message);
    }

    @RequestMapping(value = {"/index.jsp", "/", "/index"}, method = RequestMethod.POST)
    public ModelAndView helloWorld2(/*@RequestParam(value = "screen_name") String screenName*/) {

        String message = "Hello " /*+ screenName*/;
        return new ModelAndView("welcome", "message", message);
    }


}
