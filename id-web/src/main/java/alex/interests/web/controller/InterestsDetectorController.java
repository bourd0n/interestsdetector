package alex.interests.web.controller;

import alex.interests.data.extractor.LocalMongoExtractor;
import alex.interests.data.extractor.twitter.TwitterUtil;
import alex.interests.data.model.Interest;
import alex.interests.data.model.Profile;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.UserInterestsInferencer;
import alex.interests.process.mapping.entities.Category;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.params.InterestsMapperParams;
import alex.interests.process.params.ReferenceType;
import alex.interests.process.params.RelationType;
import alex.interests.test.BaselineMethod;
import alex.interests.test.InterestsMappingTestHelper;
import alex.interests.test.Method;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;
import java.util.stream.Collectors;

import static alex.interests.test.InterestsMappingTestHelper.*;

@Controller
public class InterestsDetectorController {

    private static final Logger log = LoggerFactory.getLogger(InterestsDetectorController.class);

    @RequestMapping(value = {"/interests"}, method = RequestMethod.POST)
    public ModelAndView getInterests(@RequestParam(value = "screen_name", required = false) String screenName,
                                     @RequestParam(value = "loadedUsers", required = false) String loadedUserId,
                                     @RequestParam(value = "expertMode", required = false) String expertMode) {
        long userId;
        if (StringUtils.isNotEmpty(screenName)) {
            userId = TwitterUtil.getUserIdByScreenName(screenName);
        } else if (StringUtils.isNotEmpty(loadedUserId)) {
            userId = Long.valueOf(loadedUserId);
        } else {
            throw new IllegalArgumentException("Either screen name or loaded user id should not be empty");
        }

        TwitterUtil.loadUserById(userId);

        final boolean isExpertMode = "on".equalsIgnoreCase(expertMode);
        final String userIdStr = String.valueOf(userId);

        final User user = LocalMongoExtractor.getInstance().getUserById(userIdStr);

        List<Interest> interestList;
        if (isExpertMode){
            List<Interest> interestsComplex20 = loadSimpleInterests(userIdStr, Method.wiki_complex_20);
            if (interestsComplex20 == null) {
                LinkedHashMap<Category, Double> categories = LocalMongoExtractor.getInstance().loadInterestsForUser(userId);
                if (categories == null) {
                    final AlgorithmParams algorithmParams = AlgorithmParams.DEFAULT_ALGORITHM_PARAMS;
                    categories = new UserInterestsInferencer(LocalMongoExtractor.getInstance(), algorithmParams)
                            .inferenceCategories(user, RelationType.FOLLOWING, ReferenceType.ALL);
                    try {
                        LocalMongoExtractor.getInstance().saveCategoriesForUser(userId, categories);
                    } catch (Exception e) {
                        log.error("Exception during saving interestsComplex20", e);
                    }
                }
                interestsComplex20 = categoriesToInterests(categories);
                saveSimpleInterests(userIdStr, Method.wiki_complex_20, interestsComplex20);
            }
            List<Interest> interestsBaseline = loadSimpleInterests(userIdStr, Method.baseline);
            if (interestsBaseline == null){
                BaselineMethod baselineMethod = new BaselineMethod();
                interestsBaseline = baselineMethod.getInterestsForUser(user);
                saveSimpleInterests(user.getId(), Method.baseline, interestsBaseline);
            }
            List<Interest> interestsComplexL3 = loadSimpleInterests(userIdStr, Method.wiki_complex_20_l3);
            if (interestsComplexL3 == null) {
                InterestsMapperParams mapperParams = new InterestsMapperParams(10, 0.3, 3);
                final AlgorithmParams algorithmParams = new AlgorithmParams.AlgorithmParamsBuilder()
                        .interestsMapperParams(mapperParams).build();
                LinkedHashMap<Category, Double> categories = new UserInterestsInferencer(LocalMongoExtractor.getInstance(), algorithmParams)
                        .inferenceCategories(user, RelationType.FOLLOWING, ReferenceType.ALL);
                interestsComplexL3 = categoriesToInterests(categories);
                saveSimpleInterests(userIdStr, Method.wiki_complex_20_l3, interestsComplexL3);
            }
            interestList = interestsComplex20.parallelStream().limit(20).collect(Collectors.toList());
            interestList.addAll(interestsBaseline.parallelStream().limit(20).collect(Collectors.toList()));
            interestList.addAll(interestsComplexL3.parallelStream().limit(20).collect(Collectors.toList()));
        } else {
            List<Interest> interests = loadSimpleInterests(userIdStr, Method.wiki_complex_20);
            if (interests == null) {
                LinkedHashMap<Category, Double> categories = LocalMongoExtractor.getInstance().loadInterestsForUser(userId);
                if (categories == null) {
                    final AlgorithmParams algorithmParams = AlgorithmParams.DEFAULT_ALGORITHM_PARAMS;
                    categories = new UserInterestsInferencer(LocalMongoExtractor.getInstance(), algorithmParams)
                            .inferenceCategories(user, RelationType.FOLLOWING, ReferenceType.ALL);
                    try {
                        LocalMongoExtractor.getInstance().saveCategoriesForUser(userId, categories);
                    } catch (Exception e) {
                        log.error("Exception during saving interests", e);
                    }
                }
                interests = categoriesToInterests(categories);
                saveSimpleInterests(userIdStr, Method.wiki_complex_20, interests);
            }
            interestList = interests.parallelStream().limit(40).collect(Collectors.toList());
        }

        LinkedListMultimap<String, Integer> interestsTitles = LinkedListMultimap.create();

        interestList.forEach(interest -> interestsTitles.put(interest.getTitle(), interest.getId()));

        LinkedListMultimap<String, Integer> finalInterestsTitles;
        if (isExpertMode){
            finalInterestsTitles = LinkedListMultimap.create();
            final List<String> interestsTitlesList = new ArrayList<>(interestsTitles.keySet());
            Collections.shuffle(interestsTitlesList);
            interestsTitlesList.forEach(title -> finalInterestsTitles.putAll(title, interestsTitles.get(title)));
        } else {
            finalInterestsTitles = interestsTitles;
        }

        final List<Tweet> tweets = user.getTweets(true);
        final Profile profile = LocalMongoExtractor.getInstance().loadUserProfile(userIdStr);

        ModelAndView modelAndView = isExpertMode
                ? new ModelAndView("interests_expert")
                : new ModelAndView("interests");
        modelAndView.addObject("userId", user.getId());
        modelAndView.addObject("name", profile.getName());
        modelAndView.addObject("bio", profile.getDescription());
        modelAndView.addObject("screen_name", profile.getScreenName());
        modelAndView.addObject("tweets", tweets);
        modelAndView.addObject("interests", finalInterestsTitles.asMap());
        return modelAndView;
    }
}
