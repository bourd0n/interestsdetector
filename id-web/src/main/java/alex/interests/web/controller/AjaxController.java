package alex.interests.web.controller;

import alex.interests.data.extractor.twitter.TwitterUtil;
import alex.interests.test.InterestsMappingTestHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class AjaxController {

    @RequestMapping(value = "/getloadedusers", method = RequestMethod.GET)
    public
    @ResponseBody
    Map<Long, String> getLoadedUsers() {
        return TwitterUtil.getLoadedUsers();
    }

    @RequestMapping(value = "/saveExpertChoice", method = RequestMethod.POST)
    public
    @ResponseBody
    boolean saveExpertChoice(@RequestParam("userId") String userId,
                             @RequestParam("selectedCategoriesIds[]") String[] selectedCategoriesIds) {
        final List<String> catIdsStr = Arrays.asList(selectedCategoriesIds);
        final List<Integer> catIdsInt = catIdsStr.parallelStream().map(Integer::valueOf).collect(Collectors.toList());
        InterestsMappingTestHelper.saveRelevantInterests(userId, catIdsInt);
        return true;
    }
}
