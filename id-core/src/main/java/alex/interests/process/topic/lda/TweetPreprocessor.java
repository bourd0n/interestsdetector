package alex.interests.process.topic.lda;

public class TweetPreprocessor {

    public static final String emoticon_string = "(?:" +
            "[<>]?" +
            "[:;=8]" +
            "[\\-o\\*']?" +
            "[\\*\\)\\]\\(\\[3dDpP/:\\}\\{@\\|\\\\]" +
            "|" +
            "[\\)\\]\\(\\[CdDpP/:\\}\\{@\\|\\\\]" +
            "[\\-o\\*']?" +
            "[:;=8]" +
            "[<>]?" +
            "|" +
            "[\\)]+" +
            "|" +
            "[\\(]+" +
            "|" +
            "[\\^][_\\-]*[\\^]" +
            "|" +
            "[\\.\\*][_\\-]+[\\.\\*]" +
            "|" +
            "[\\-][\\.][\\-]" +
            "|" +
            "[<]+[3]+" +
            ")";

    public static final String exclamation = "([!]{2,})";
    public static final String puzzled_punctuation = "(?:[!]{1,}[?]+[!?]*|[?]{1,}[!]+[!?]*|[!]{2,})";

    public static final String hyperlinks_re = "(https?|ftp|file|htttps?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    public static final String twitter_pic_re = "pic\\.twitter\\.com/[\\w]+";

    public static final String phones = "(?:\n" +
            "(?:" +
            "\\+?[01]\n" +
            "[\\-\\s.]*\n" +
            ")?\n" +
            "(?:" +
            "[\\(]?\n" +
            "\\d{3}\n" +
            "[\\-\\s.\\)]*\n" +
            ")?\n" +
            "\\d{3}" +
            "[\\-\\s.]*\n" +
            "\\d{4}" +
            ")";

    public static final String html_tags = "(?:<[^>]+>)";
    public static final String ellipsis = "(?:\\.(?:\\s*\\.){1,})";

    public static final String finalRegex =
            "(" + hyperlinks_re + ")|" +
                    "(" + twitter_pic_re + ")|" +
                    "(" + phones + ")|" +
                    "(" + html_tags + ")|" +
                    "(" + ellipsis + ")|" +
                    "(" + emoticon_string + ")|" +
                    "(" + puzzled_punctuation + ")";

    private static final String ngram_regex =
            "(" + TweetPreprocessor.hyperlinks_re + ")|" +
                    "(" + TweetPreprocessor.twitter_pic_re + ")|" +
                    "(" + TweetPreprocessor.phones + ")|" +
                    "(" + TweetPreprocessor.html_tags + ")";

    public TweetPreprocessor() {
    }

    public static String deletePunctuation(String tweet) {
        tweet = tweet.replaceAll("&amp;", "&").replaceAll("&lt;", "<").replaceAll("&gt;", ">");
        return tweet.replaceAll(finalRegex, "");
    }
}
