package alex.interests.process.topic.lda;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.model.Topic;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.params.TopicInferenceParams;
import alex.interests.process.topic.TopicExtractor;
import alex.interests.process.topic.TopicModel;
import cc.mallet.pipe.iterator.ArrayIterator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.ThreadSafe;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

@ThreadSafe
public class LDAExtractor implements TopicExtractor {
    private static final Logger log = LoggerFactory.getLogger(LDAExtractor.class);

    private final AlgorithmParams algorithmParams;
    private final DataExtractor extractor;
    private static final ConcurrentMap<String, String> locksForTopicCalculation = new ConcurrentHashMap<String, String>();

    public LDAExtractor(AlgorithmParams algorithmParams) {
        this.algorithmParams = algorithmParams;
        this.extractor = MongoExtractor.getInstance();
    }

    public LDAExtractor(AlgorithmParams algorithmParams, DataExtractor extractor) {
        this.algorithmParams = algorithmParams;
        this.extractor = extractor;
    }

    @Override
    public TopicModel extractUserOwnTopics(User user) {
        if (user.isTopicsCalculated())
            return user.getOwnTopicModel();
        //to not allow multiple calculation for same user
        synchronized (getSyncObject(user.getId())) {
            if (user.isTopicsCalculated())
                return user.getOwnTopicModel();
            List<Tweet> tweets = collectAllTweets(user);
            log.debug("Run LDA for {}", user);
            TopicModel topicModel = runLDA(user, tweets);
            user.setOwnTopicModel(topicModel);
            return topicModel;
        }
    }

    private List<Tweet> collectAllTweets(User user) {
        final List<Tweet> tweets = new ArrayList<>(user.getTweets(true));
        final Set<String> usersToExtractTweets = new HashSet<>();
        usersToExtractTweets.addAll(user.getFollowers(true));
        usersToExtractTweets.addAll(user.getFriends(true));
        log.debug("Users to extract tweets size {}", usersToExtractTweets.size());
        usersToExtractTweets.forEach(userId -> tweets.addAll(extractor.loadUserTweets(userId).parallelStream().limit(500).collect(Collectors.toList())));
        log.debug("Tweets size: {}", tweets.size());
        return tweets;
    }


    private Object getSyncObject(final String id) {
        locksForTopicCalculation.putIfAbsent(id, id);
        return locksForTopicCalculation.get(id);
    }

    private TopicModel runLDA(User user, List<Tweet> tweets) {
        try {
            int numIterations = algorithmParams.getNumIterations();
            int numThreads = algorithmParams.getNumThreads();
            double alpha = algorithmParams.getAlpha();
            double beta = algorithmParams.getBeta();
            int numTopics = algorithmParams.getNumTopics();
            TopicInferenceParams topicInferenceParams = algorithmParams.getTopicInferenceParams();
            InstanceList instances = LDAHelper.thruTrainPipe(new ArrayIterator(tweets));
            ParallelTopicModel model = new ParallelTopicModel(numTopics, alpha, beta);
            model.addInstances(instances);
            // Use numThreads parallel samplers, which each look at one half the corpus and combine
            // statistics after every iteration.
            model.setNumThreads(numThreads);
            // Run the model for numIterations iterations and stop
            model.setNumIterations(numIterations);
            log.debug("Start model estimate for {} tweets", tweets.size());
            model.estimate();
            log.debug("Finish model estimate for {} tweets", tweets.size());
            Alphabet dataAlphabet = instances.getDataAlphabet();
            ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();
            List<Topic> topics = new ArrayList<>(numTopics);
            for (int topic = 0; topic < numTopics; topic++) {
                Map<String, Double> termsDistribution = new HashMap<>();
                Iterator<IDSorter> iterator = topicSortedWords.get(topic).iterator();
                int rank = 0;
                while (iterator.hasNext() && rank < 10) {
                    IDSorter idCountPair = iterator.next();
                    String term = (String) dataAlphabet.lookupObject(idCountPair.getID());
                    Double weight = idCountPair.getWeight();
                    termsDistribution.put(term, weight);
                    rank++;
                }
                topics.add(new Topic(termsDistribution));
            }

            if (user == null) {
                return new TopicModel(topics, Collections.emptyList(), model);
            }

            final List<Tweet> sourceUserTweets = user.getTweets(true);
            Instance instanceOneLine = LDAHelper.allTweetsToOneInstance(sourceUserTweets);
            TopicInferencer inferencer = model.getInferencer();
            log.debug("Start inference for {} tweets", sourceUserTweets.size());
            double[] sampledDistribution = inferencer.getSampledDistribution(instanceOneLine,
                    topicInferenceParams.numIterations,
                    topicInferenceParams.thinning,
                    topicInferenceParams.burnIn);
            log.debug("Finish inference for {} tweets", sourceUserTweets.size());
            List<Double> topicsDistribution = Arrays.asList(ArrayUtils.toObject(sampledDistribution));
            TopicModel topicModel = new TopicModel(topics, topicsDistribution, model);
            log.debug("LDA return own user {}", topicModel.toStringSortedByDistribution());
            return topicModel;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public TopicModel extractTopics(List<Tweet> tweets) {
        return runLDA(null, tweets);
    }
}