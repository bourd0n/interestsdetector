package alex.interests.process.topic.lda;

import alex.interests.data.model.Tweet;
import cc.mallet.pipe.Pipe;
import cc.mallet.types.Instance;

import java.io.Serializable;

public class ExtractTextFromTweetPipe extends Pipe implements Serializable {

    public Instance pipe(Instance carrier) {
        if (carrier.getData() instanceof Tweet) {
            Tweet data = (Tweet) carrier.getData();
            carrier.setData(data.getText());
        } if (!(carrier.getData() instanceof String)) {
            //if it is not a string already
            throw new IllegalArgumentException("DeletePunctuationPipe expects a String, found a " + carrier.getData().getClass());
        }
        return carrier;
    }
}
