package alex.interests.process.topic;

import alex.interests.data.model.Topic;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;

import java.util.List;

public interface TopicExtractor {

	/**
	 * 
	 * @param user
	 */
	TopicModel extractUserOwnTopics(User user);

	/**
	 * 
	 * @param tweets
	 */
    TopicModel extractTopics(List<Tweet> tweets);

}