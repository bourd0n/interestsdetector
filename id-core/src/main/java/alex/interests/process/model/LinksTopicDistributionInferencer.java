package alex.interests.process.model;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.topic.lda.LDAHelper;
import alex.interests.process.params.TopicInferenceParams;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Instance;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.ThreadSafe;
import java.util.Arrays;
import java.util.List;

@ThreadSafe
public class LinksTopicDistributionInferencer {
    private static final Logger log = LoggerFactory.getLogger(LinksTopicDistributionInferencer.class);

    private final DataExtractor extractor;
    private final AlgorithmParams algorithmParams;

    public LinksTopicDistributionInferencer(DataExtractor extractor, AlgorithmParams algorithmParams) {
        this.extractor = extractor;
        this.algorithmParams = algorithmParams;
    }

    public Pair<List<Double>, Integer> inferenceDistributionOverUserTopics(String targetUserId, TopicInferencer inferencer) {
        //User targetUser = extractor.loadUserTweets(targetUserId);
        //if (targetUser == null) {
        //    user not found
            //return null;
        //}
        log.trace("Start inference distribution over topics for user {}", targetUserId);
        List<Tweet> tweets = extractor.loadUserTweets(targetUserId);
        double[] sampledDistribution;
        if (CollectionUtils.isEmpty(tweets)) {
            log.trace("Tweets empty for user {}. Return distribution with 0.0", targetUserId);
            sampledDistribution = new double[algorithmParams.getNumTopics()];
            Arrays.fill(sampledDistribution, 0.0);
        } else {
            Instance instanceOneLine = LDAHelper.allTweetsToOneInstance(tweets);
            final TopicInferenceParams topicInferenceParams = algorithmParams.getTopicInferenceParams();
            log.trace("Call inferencer for user {} with {} tweets", targetUserId, tweets.size());
            sampledDistribution = inferencer.getSampledDistribution(instanceOneLine,
                    topicInferenceParams.numIterations,
                    topicInferenceParams.thinning,
                    topicInferenceParams.burnIn);
        }
        log.trace("Finish topic distribution inferencing for user {}", targetUserId);
        final List<Double> sampledDistributionList = Arrays.asList(ArrayUtils.toObject(sampledDistribution));
        return Pair.of(sampledDistributionList, tweets.size());
    }

}
