package alex.interests.process.mapping.entities;

public class CategoryWeight {

    private Category category;
    private Double weight;

    public CategoryWeight() {
    }

    public CategoryWeight(Category category, Double weight) {
        this.category = category;
        this.weight = weight;
    }

    public Category getCategory() {
        return category;
    }

    public Double getWeight() {
        return weight;
    }
}
