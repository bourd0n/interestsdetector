package alex.interests.process.mapping;


import alex.interests.data.model.Topic;
import alex.interests.process.mapping.entities.Category;

import java.util.LinkedHashMap;
import java.util.Map;

public interface TopicCategoryMapper {

	//void mapTopic(Topic topic);

	//void mapTopicsForUser(User user);

	LinkedHashMap<Category, Double> mapTopicsToInterests(Map<Topic, Double> topics);

}