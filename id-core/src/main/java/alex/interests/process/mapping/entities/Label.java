
package alex.interests.process.mapping.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Label {

    private String text;
    private int linkDocCount;
    private int linkOccCount;
    private int docCount;
    private int occCount;
    private double linkProbability;
    private boolean isStopword;
    private List<Sense> senses = new ArrayList<>();

    /**
     * @return The text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return The linkDocCount
     */
    public int getLinkDocCount() {
        return linkDocCount;
    }

    /**
     * @param linkDocCount The linkDocCount
     */
    public void setLinkDocCount(int linkDocCount) {
        this.linkDocCount = linkDocCount;
    }

    /**
     * @return The linkOccCount
     */
    public int getLinkOccCount() {
        return linkOccCount;
    }

    /**
     * @param linkOccCount The linkOccCount
     */
    public void setLinkOccCount(int linkOccCount) {
        this.linkOccCount = linkOccCount;
    }

    /**
     * @return The docCount
     */
    public int getDocCount() {
        return docCount;
    }

    /**
     * @param docCount The docCount
     */
    public void setDocCount(int docCount) {
        this.docCount = docCount;
    }

    /**
     * @return The occCount
     */
    public int getOccCount() {
        return occCount;
    }

    /**
     * @param occCount The occCount
     */
    public void setOccCount(int occCount) {
        this.occCount = occCount;
    }

    /**
     * @return The linkProbability
     */
    public double getLinkProbability() {
        return linkProbability;
    }

    /**
     * @param linkProbability The linkProbability
     */
    public void setLinkProbability(double linkProbability) {
        this.linkProbability = linkProbability;
    }

    /**
     * @return The senses
     */
    public List<Sense> getSenses() {
        return senses;
    }

    /**
     * @param senses The senses
     */
    public void setSenses(List<Sense> senses) {
        this.senses = senses;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    public boolean isStopword() {
        return isStopword;
    }

    public void setIsStopword(boolean isStopword) {
        this.isStopword = isStopword;
    }
}
