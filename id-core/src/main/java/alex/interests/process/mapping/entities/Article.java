
package alex.interests.process.mapping.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Article {

    private int id;
    private String title;
    private List<ArticleCategory> parentCategories = new ArrayList<>();
    private int totalParentCategories;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The parentCategories
     */
    public List<ArticleCategory> getParentCategories() {
        return parentCategories;
    }

    /**
     * @param parentCategories The parentCategories
     */
    public void setParentCategories(List<ArticleCategory> parentCategories) {
        this.parentCategories = parentCategories;
    }

    /**
     * @return The totalParentCategories
     */
    public int getTotalParentCategories() {
        return totalParentCategories;
    }

    /**
     * @param totalParentCategories The totalParentCategories
     */
    public void setTotalParentCategories(int totalParentCategories) {
        this.totalParentCategories = totalParentCategories;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
