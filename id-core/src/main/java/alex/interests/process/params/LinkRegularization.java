package alex.interests.process.params;

public enum LinkRegularization {
    IN, OUT, INOUT, NONE
}
