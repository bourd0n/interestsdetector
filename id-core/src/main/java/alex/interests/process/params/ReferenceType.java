package alex.interests.process.params;

/**
 * Followers is back references, following - references.
 * Same for retweets
 */
public enum ReferenceType {
    BACK_REFERENCES, REFERENCES, ALL
}
