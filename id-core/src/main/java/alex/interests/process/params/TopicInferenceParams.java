package alex.interests.process.params;

public class TopicInferenceParams {

    public static final TopicInferenceParams DEFAULT_INFERENCE_PARAMS = new TopicInferenceParams(100, 1, 5);

    public final int numIterations;
    public final int thinning;
    public final int burnIn;

    public TopicInferenceParams(int numIterations, int thinning, int burnIn) {
        this.numIterations = numIterations;
        this.thinning = thinning;
        this.burnIn = burnIn;
    }
}
