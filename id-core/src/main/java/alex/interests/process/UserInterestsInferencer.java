package alex.interests.process;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.data.model.ReTweet;
import alex.interests.data.model.Topic;
import alex.interests.data.model.User;
import alex.interests.process.mapping.WikiTopicMapper;
import alex.interests.process.mapping.entities.Category;
import alex.interests.process.model.LinksTopicDistributionInferencer;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.params.ReferenceType;
import alex.interests.process.params.RelationType;
import alex.interests.process.topic.TopicModel;
import alex.interests.process.topic.lda.LDAExtractor;
import alex.interests.process.weight.FollowerWeightsCalculator;
import alex.interests.process.weight.FriendWeightsCalculator;
import alex.interests.process.weight.ImplicitFollowingLinkWeightsCalculator;
import alex.interests.process.weight.RetweetedWeightsCalculator;
import com.google.common.base.*;
import com.google.common.base.Objects;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.ThreadSafe;
import java.util.*;

public class UserInterestsInferencer {

    private static final Logger log = LoggerFactory.getLogger(UserInterestsInferencer.class);

    private final DataExtractor dataExtractor;
    private AlgorithmParams algorithmParams;
    private final LinksTopicDistributionInferencer linksTopicDistributionInferencer;
    private final FollowerWeightsCalculator followerWeightsCalculator;
    private final FriendWeightsCalculator friendWeightsCalculator;
    private final RetweetedWeightsCalculator retweetedWeightsCalculator;
    private final ImplicitFollowingLinkWeightsCalculator implicitFollowingLinkWeightsCalculator;


    public UserInterestsInferencer(DataExtractor dataExtractor, AlgorithmParams algorithmParams) {
        Preconditions.checkArgument(algorithmParams.getCoefInLinks() + algorithmParams.getCoefOutLinks() + algorithmParams.getCoefOwn() == 1,
                "Regularization coefficients should be sum 1");
        this.dataExtractor = dataExtractor;
        this.algorithmParams = algorithmParams;
        this.linksTopicDistributionInferencer = new LinksTopicDistributionInferencer(dataExtractor, algorithmParams);
        this.followerWeightsCalculator = new FollowerWeightsCalculator(dataExtractor, algorithmParams);
        this.implicitFollowingLinkWeightsCalculator = new ImplicitFollowingLinkWeightsCalculator(dataExtractor, algorithmParams);
        this.friendWeightsCalculator = new FriendWeightsCalculator(dataExtractor, algorithmParams);
        this.retweetedWeightsCalculator = new RetweetedWeightsCalculator(dataExtractor, algorithmParams);
    }

    public UserInterestsInferencer(DataExtractor dataExtractor,
                                   AlgorithmParams algorithmParams,
                                   FollowerWeightsCalculator followerWeightsCalculator,
                                   FriendWeightsCalculator friendWeightsCalculator,
                                   ImplicitFollowingLinkWeightsCalculator implicitFollowingLinkWeightsCalculator,
                                   RetweetedWeightsCalculator retweetedWeightsCalculator) {
        this.dataExtractor = dataExtractor;
        this.algorithmParams = algorithmParams;
        this.linksTopicDistributionInferencer = new LinksTopicDistributionInferencer(dataExtractor, algorithmParams);
        this.followerWeightsCalculator = followerWeightsCalculator;
        this.friendWeightsCalculator = friendWeightsCalculator;
        this.implicitFollowingLinkWeightsCalculator = implicitFollowingLinkWeightsCalculator;
        this.retweetedWeightsCalculator = retweetedWeightsCalculator;
    }

    public void setAlgorithmParams(AlgorithmParams algorithmParams) {
        this.algorithmParams = algorithmParams;
    }

    public LinkedHashMap<Category, Double> inferenceCategories(User user,
                                                               RelationType relationType,
                                                               ReferenceType referenceType) {
        Map<Topic, Double> inferencedInterests = inferenceTopics(user, relationType, referenceType);
        WikiTopicMapper wikiTopicMapper = new WikiTopicMapper(algorithmParams.getInterestsMapperParams());
        return wikiTopicMapper.mapTopicsToInterests(inferencedInterests);
    }

    public LinkedHashMap<Topic, Double> inferenceTopics(User user,
                                                        RelationType relationType,
                                                        ReferenceType referenceType) {
        LDAExtractor ldaExtractor = new LDAExtractor(algorithmParams);
        TopicModel ownTopicModel = ldaExtractor.extractUserOwnTopics(user);
        return inferenceTopics(user, relationType, referenceType, ownTopicModel);
    }

    public LinkedHashMap<Topic, Double> inferenceTopics(User user,
                                                        RelationType relationType,
                                                        ReferenceType referenceType,
                                                        TopicModel ownTopicModel) {
        //out-links
        ListMultimap<Key, Double> referenceId2TopicsDist = null;
        Map<String, Double> referenceOutRegWeights = null;
        Map<String, Double> referenceInRegWeights = null;
        //in-links
        ListMultimap<Key, Double> backReferenceId2TopicsDist = null;
        Map<String, Double> backReferenceOutRegWeights = null;
        Map<String, Double> backReferenceInRegWeights = null;

        if (relationType == RelationType.FOLLOWING) {
            if (referenceType == ReferenceType.BACK_REFERENCES || referenceType == ReferenceType.ALL) {
                if (algorithmParams.getCoefInLinks() != 0 || algorithmParams.getCoefOutLinks() != 0) {
                    int recursiveLevel = 1;
                    Set<String> followers = user.getFollowers(true);
                    log.debug("Start topic inference for followers");
                    backReferenceId2TopicsDist = inferenceTopicDistributionsForLinks(ownTopicModel, followers, recursiveLevel);
                    if (algorithmParams.getCoefOutLinks() != 0) {
                        log.debug("Start followers out-link regularization");
                        backReferenceOutRegWeights = followerWeightsCalculator.calculateOutLinkWeights(user);
                    }
                    if (algorithmParams.getCoefInLinks() != 0) {
                        log.debug("Start followers in-link regularization");
                        backReferenceInRegWeights = followerWeightsCalculator.calculateInLinkWeights(user);
                    }
                }
            }
            if (referenceType == ReferenceType.REFERENCES || referenceType == ReferenceType.ALL) {
                if (algorithmParams.getCoefInLinks() != 0 || algorithmParams.getCoefOutLinks() != 0) {
                    int recursiveLevel = 1;
                    Set<String> friends = user.getFriends(true);
                    log.debug("Start topic inference for friends");
                    referenceId2TopicsDist = inferenceTopicDistributionsForLinks(ownTopicModel, friends, recursiveLevel);
                    if (algorithmParams.getCoefOutLinks() != 0) {
                        log.debug("Start friends out-link regularization");
                        referenceOutRegWeights = friendWeightsCalculator.calculateOutLinkWeights(user);
                    }
                    if (algorithmParams.getCoefInLinks() != 0) {
                        log.debug("Start friends in-link regularization");
                        referenceInRegWeights = friendWeightsCalculator.calculateInLinkWeights(user);
                    }
                }
            }
        }
        if (relationType == RelationType.RETWEETS) {
            if (referenceType == ReferenceType.REFERENCES) {
                if (algorithmParams.getCoefInLinks() != 0 || algorithmParams.getCoefOutLinks() != 0) {
                    int recursiveLevel = 1;
                    ListMultimap<String, ReTweet> retweetedUsers = user.getRetweetedUsers(true);
                    log.debug("Start topic inference for retweeted users");
                    Set<String> retweetedUserIds = retweetedUsers.keySet();
                    referenceId2TopicsDist = inferenceTopicDistributionsForLinks(ownTopicModel, retweetedUserIds, recursiveLevel);
                    if (algorithmParams.getCoefOutLinks() != 0) {
                        log.debug("Start retweeted users out-link regularization");
                        referenceOutRegWeights = retweetedWeightsCalculator.calculateOutLinkWeights(user);
                    }
                    if (algorithmParams.getCoefInLinks() != 0) {
                        //log.debug("Start retweeted users in-link regularization");
                        //referenceInRegWeights = retweetWeightsCalculator.calculateRetweetsHubWeights(user);
                        //todo: mongo db is currently not supported in-links for retweets
                        throw new UnsupportedOperationException("Retweets backreferences and in-link regularization not supported");
                    }
                }
            }
            //todo: mongo db is currently not supported in-links for retweets
            if (referenceType == ReferenceType.BACK_REFERENCES || referenceType == ReferenceType.ALL) {
                throw new UnsupportedOperationException("Retweets backreferences and in-link regularization not supported");
            }
        }

        Map<String, Integer> tweetsSizesMap = new HashMap<>();
        if (referenceId2TopicsDist != null) {
            referenceId2TopicsDist.keySet()
                    .forEach(id -> tweetsSizesMap.computeIfAbsent(id.userId, key -> id.tweetsSize));
        }

        if (backReferenceId2TopicsDist != null) {
            backReferenceId2TopicsDist.keySet()
                    .forEach(id ->
                            tweetsSizesMap.computeIfAbsent(id.userId, key -> id.tweetsSize));
        }

        Integer maxTweetsSize = null;
        Double maxTweetsSizeLog = null;
        if (!tweetsSizesMap.isEmpty()) {
            maxTweetsSize = Collections.max(tweetsSizesMap.entrySet(), (o1, o2) -> o1.getValue().compareTo(o2.getValue())).getValue();
            maxTweetsSizeLog = Math.log(maxTweetsSize);
        }
        log.debug("Max tweets size {}", maxTweetsSize);
        //calculate final topic distribution
        log.debug("Start calculate final topic distribution after regularization");
        List<Topic> ownTopics = ownTopicModel.getTopics();
        List<Double> ownTopicDistribution = ownTopicModel.getTopicsDistribution();
        List<Double> finalTopicWeights = new ArrayList<>(algorithmParams.getNumTopics());
        for (int i = 0; i < algorithmParams.getNumTopics(); i++) {
            double topicWeightOuterLinks = 0;
            double outerWeight = 0;
            double topicWeightInLinks = 0;
            double inWeight = 0;
            if (referenceId2TopicsDist != null) {
                Set<Key> referencesIds = referenceId2TopicsDist.keySet();
                if (referenceOutRegWeights != null) {
                    for (Key referenceId : referencesIds) {
                        double topicWeight = referenceId2TopicsDist.get(referenceId).get(i);
                        Integer tweetsSize = tweetsSizesMap.get(referenceId.userId);
                        double weightByTweetsSize = tweetsSize == 0 || tweetsSize == null ? 0 : Math.log(tweetsSize) / maxTweetsSizeLog;
                        double outUserWeight = referenceOutRegWeights.get(referenceId.userId) * weightByTweetsSize;
                        topicWeightOuterLinks += topicWeight * outUserWeight;
                        outerWeight += outUserWeight;
                    }
                }
                if (referenceInRegWeights != null) {
                    for (Key referenceId : referencesIds) {
                        double topicWeight = referenceId2TopicsDist.get(referenceId).get(i);
                        Integer tweetsSize = tweetsSizesMap.get(referenceId.userId);
                        double weightByTweetsSize = tweetsSize == 0 || tweetsSize == null ? 0 : Math.log(tweetsSize) / maxTweetsSizeLog;
                        double inUserWeight = referenceInRegWeights.get(referenceId.userId) * weightByTweetsSize;
                        topicWeightInLinks += topicWeight * inUserWeight;
                        inWeight += inUserWeight;
                    }
                }
            }

            if (backReferenceId2TopicsDist != null) {
                Set<Key> backReferencesIds = backReferenceId2TopicsDist.keySet();
                if (backReferenceInRegWeights != null) {
                    for (Key backReferenceId : backReferencesIds) {
                        double topicWeight = backReferenceId2TopicsDist.get(backReferenceId).get(i);
                        Integer tweetsSize = tweetsSizesMap.get(backReferenceId.userId);
                        double weightByTweetsSize = tweetsSize == 0 || tweetsSize == null ? 0 : Math.log(tweetsSize) / maxTweetsSizeLog;
                        double linkUserWeight = backReferenceInRegWeights.get(backReferenceId.userId) * weightByTweetsSize;
                        topicWeightInLinks += topicWeight * linkUserWeight;
                        inWeight += linkUserWeight;
                    }
                }
                if (backReferenceOutRegWeights != null) {
                    for (Key backReferenceId : backReferencesIds) {
                        double topicWeight = backReferenceId2TopicsDist.get(backReferenceId).get(i);
                        Integer tweetsSize = tweetsSizesMap.get(backReferenceId.userId);
                        double weightByTweetsSize = tweetsSize == 0 || tweetsSize == null ? 0 : Math.log(tweetsSize) / maxTweetsSizeLog;
                        double outUserWeight = backReferenceOutRegWeights.get(backReferenceId.userId) * weightByTweetsSize;
                        topicWeightOuterLinks += topicWeight * outUserWeight;
                        outerWeight += outUserWeight;
                    }
                }
            }

            topicWeightInLinks *= algorithmParams.getCoefInLinks();
            inWeight *= algorithmParams.getCoefInLinks();

            topicWeightOuterLinks *= algorithmParams.getCoefOutLinks();
            outerWeight *= algorithmParams.getCoefOutLinks();

            double selfTopicWeigh = ownTopicDistribution.get(i) * algorithmParams.getCoefOwn() /** user.getTweets(true).size()*/;

            double finalTopicWeight = (topicWeightOuterLinks + topicWeightInLinks + selfTopicWeigh)
                    / (outerWeight + inWeight + algorithmParams.getCoefOwn()/**user.getTweets(true).size()*/);

            finalTopicWeights.add(finalTopicWeight);
        }

        //put to sorted
        Map<Topic, Double> sortedTopicDistribution = new HashMap<>();
        for (int i = 0; i < algorithmParams.getNumTopics(); i++) {
            sortedTopicDistribution.put(ownTopics.get(i), finalTopicWeights.get(i));
        }

        LinkedHashMap<Topic, Double> finalTopicDistribution = new LinkedHashMap<>();
        sortedTopicDistribution.entrySet().parallelStream().sorted(Comparator.comparingDouble(e -> -e.getValue()))
                .forEachOrdered(entry -> finalTopicDistribution.put(entry.getKey(), entry.getValue()));

        if (log.isInfoEnabled()) {
            StringBuilder stringBuilder = new StringBuilder("Result \n");
            stringBuilder.append("Sorted own user topic distribution:")
                    .append(ownTopicModel.toStringSortedByDistribution())
                    .append("\n")
                    .append("\n")
                    .append("Sorted topic distribution after regularization: ")
                    .append(TopicModel.toStringSortedByDistribution(finalTopicDistribution));
            log.info(stringBuilder.toString());
        }

        return finalTopicDistribution;
    }

    public LinkedHashMap<Topic, Double> inferenceTopicsWithImplicitLinks(User user,
                                                                         RelationType relationType,
                                                                         TopicModel ownTopicModel) {
        //out-links
        ListMultimap<Key, Double> outImplicitId2TopicsDist = null;
        Map<String, Double> outImplicitRegWeights = null;
        //in-links
        ListMultimap<Key, Double> inImplicitId2TopicsDist = null;
        Map<String, Double> inImplicitRegWeights = null;

        if (relationType == RelationType.FOLLOWING) {
            if (algorithmParams.getCoefInLinks() != 0) {
                int recursiveLevel = 1;
                log.debug("Start in-link regularization");
                inImplicitRegWeights = implicitFollowingLinkWeightsCalculator.calculateInLinkWeights(user);
                log.debug("Start topic inference for in-link regularization");
                inImplicitId2TopicsDist = inferenceTopicDistributionsForLinks(ownTopicModel, inImplicitRegWeights.keySet(), recursiveLevel);
            }
            if (algorithmParams.getCoefOutLinks() != 0) {
                int recursiveLevel = 1;
                log.debug("Start out-link regularization");
                outImplicitRegWeights = implicitFollowingLinkWeightsCalculator.calculateOutLinkWeights(user);
                log.debug("Start topic inference out-link regularization");
                outImplicitId2TopicsDist = inferenceTopicDistributionsForLinks(ownTopicModel, outImplicitRegWeights.keySet(), recursiveLevel);
            }
        }
        if (relationType == RelationType.RETWEETS) {
            //if (algorithmParams.getCoefInLinks() != 0 || algorithmParams.getCoefOutLinks() != 0) {
            //    int recursiveLevel = 1;
            //    ListMultimap<String, ReTweet> retweetedUsers = user.getRetweetedUsers(true);
            //    log.debug("Start topic inference for retweeted users");
            //    Set<String> retweetedUserIds = retweetedUsers.keySet();
            //    outImplicitId2TopicsDist = inferenceTopicDistributionsForLinks(ownTopicModel, retweetedUserIds, recursiveLevel);
            //    if (algorithmParams.getCoefOutLinks() != 0) {
            //        log.debug("Start retweeted users out-link regularization");
            //        outImplicitRegWeights = retweetWeightsCalculator.calculateOutLinkWeights(user);
            //    }
            //    if (algorithmParams.getCoefInLinks() != 0) {
            //        //log.debug("Start retweeted users in-link regularization");
            //        //referenceInRegWeights = retweetWeightsCalculator.calculateRetweetsHubWeights(user);
            //        //todo: mongo db is currently not supported in-links for retweets
            throw new UnsupportedOperationException("Retweets backreferences and in-link regularization not supported");
            //    }
            //}
        }

        Map<String, Integer> tweetsSizesMap = new HashMap<>();
        if (outImplicitId2TopicsDist != null) {
            outImplicitId2TopicsDist.keySet()
                    .forEach(id -> tweetsSizesMap.computeIfAbsent(id.userId, key -> id.tweetsSize));
        }

        if (inImplicitId2TopicsDist != null) {
            inImplicitId2TopicsDist.keySet()
                    .forEach(id ->
                            tweetsSizesMap.computeIfAbsent(id.userId, key -> id.tweetsSize));
        }

        Integer maxTweetsSize = null;
        Double maxTweetsSizeLog = null;
        if (!tweetsSizesMap.isEmpty()) {
            maxTweetsSize = Collections.max(tweetsSizesMap.entrySet(), (o1, o2) -> o1.getValue().compareTo(o2.getValue())).getValue();
            maxTweetsSizeLog = Math.log(maxTweetsSize);
        }
        log.debug("Max tweets size {}", maxTweetsSize);

        //calculate final topic distribution
        log.debug("Start calculate final topic distribution after regularization");
        List<Topic> ownTopics = ownTopicModel.getTopics();
        List<Double> ownTopicDistribution = ownTopicModel.getTopicsDistribution();
        List<Double> finalTopicWeights = new ArrayList<>(algorithmParams.getNumTopics());
        for (int i = 0; i < algorithmParams.getNumTopics(); i++) {
            double topicWeightOuterLinks = 0;
            double outerWeight = 0;
            double topicWeightInLinks = 0;
            double inWeight = 0;
            if (outImplicitId2TopicsDist != null) {
                for (Key referenceId : outImplicitId2TopicsDist.keySet()) {
                    double topicWeight = outImplicitId2TopicsDist.get(referenceId).get(i);
                    Integer tweetsSize = tweetsSizesMap.get(referenceId.userId);
                    double weightByTweetsSize = tweetsSize == null || tweetsSize == 0 ? 0 : Math.log(tweetsSize) / maxTweetsSizeLog;
                    double outUserWeight = outImplicitRegWeights.get(referenceId.userId) * weightByTweetsSize;
                    topicWeightOuterLinks += topicWeight * outUserWeight;
                    outerWeight += outUserWeight;
                }
            }

            if (inImplicitId2TopicsDist != null) {
                for (Key backReferenceId : inImplicitId2TopicsDist.keySet()) {
                    double topicWeight = inImplicitId2TopicsDist.get(backReferenceId).get(i);
                    Integer tweetsSize = tweetsSizesMap.get(backReferenceId.userId);
                    double weightByTweetsSize = tweetsSize == null || tweetsSize == 0 ? 0 : Math.log(tweetsSize) / maxTweetsSizeLog;
                    double linkUserWeight = inImplicitRegWeights.get(backReferenceId.userId) * weightByTweetsSize;
                    topicWeightInLinks += topicWeight * linkUserWeight;
                    inWeight += linkUserWeight;
                }
            }

            topicWeightInLinks *= algorithmParams.getCoefInLinks();
            inWeight *= algorithmParams.getCoefInLinks();

            topicWeightOuterLinks *= algorithmParams.getCoefOutLinks();
            outerWeight *= algorithmParams.getCoefOutLinks();

            double selfTopicWeigh = ownTopicDistribution.get(i) * algorithmParams.getCoefOwn();

            double finalTopicWeight = (topicWeightOuterLinks + topicWeightInLinks + selfTopicWeigh)
                    / (outerWeight + inWeight + algorithmParams.getCoefOwn());

            finalTopicWeights.add(finalTopicWeight);
        }

        //put to sorted
        Map<Topic, Double> sortedTopicDistribution = new HashMap<>();
        for (int i = 0; i < algorithmParams.getNumTopics(); i++) {
            sortedTopicDistribution.put(ownTopics.get(i), finalTopicWeights.get(i));
        }

        LinkedHashMap<Topic, Double> finalTopicDistribution = new LinkedHashMap<>();
        sortedTopicDistribution.entrySet().parallelStream().sorted(Comparator.comparingDouble(e -> -e.getValue()))
                .forEachOrdered(entry -> finalTopicDistribution.put(entry.getKey(), entry.getValue()));

        if (log.isInfoEnabled()) {
            StringBuilder stringBuilder = new StringBuilder("Result \n");
            stringBuilder.append("Sorted own user topic distribution:")
                    .append(ownTopicModel.toStringSortedByDistribution())
                    .append("\n")
                    .append("\n")
                    .append("Sorted topic distribution after regularization: ")
                    .append(TopicModel.toStringSortedByDistribution(finalTopicDistribution));
            log.info(stringBuilder.toString());
        }

        return finalTopicDistribution;
    }

    public static class Key {
        private String userId;
        private Integer tweetsSize;

        public Key(String userId, Integer tweetsSize) {
            this.userId = userId;
            this.tweetsSize = tweetsSize;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("userId", userId)
                    .add("tweetsSize", tweetsSize)
                    .toString();
        }
    }

    private ListMultimap<Key, Double> inferenceTopicDistributionsForLinks(TopicModel topicModel,
                                                                          Set<String> userIds,
                                                                          int recursiveLevel) {
        log.debug("Start inferenceTopicDistributionsForLinks. Size={}", userIds.size());
        --recursiveLevel;
        ListMultimap<Key, Double> userId2TopicDistribution = Multimaps.synchronizedListMultimap(ArrayListMultimap.create());
        if (recursiveLevel == 0) {
            userIds.parallelStream().forEach(userId -> {
                Pair<List<Double>, Integer> topicDistributionsPair = linksTopicDistributionInferencer.inferenceDistributionOverUserTopics(userId, topicModel.getTopicInferencer());
                Key key = new Key(userId, topicDistributionsPair.getRight());
                if (CollectionUtils.isNotEmpty(topicDistributionsPair.getLeft())) {
                    userId2TopicDistribution.putAll(key, topicDistributionsPair.getLeft());
                }
                log.trace("Inferencing topic distribution for links progress : {} %", userId2TopicDistribution.keySet().size() * 100 / userIds.size());
            });
            log.debug("End inferenceTopicDistributionsForLinks. Result keySize={}", userId2TopicDistribution.keySet().size());
            return userId2TopicDistribution;
        }
        /*else {
            for (String userId : userIds) {
                for (LinkType linkType : linkTypes) {
                    if (linkType.equals(LinkType.FOLLOWERS)) {
                        Set<String> followers = user.getFollowers(true);
                        inferenceTopicDistributionsForLinks(topicInferencer, followers, recursiveLevel, linkTypes);
                        ...
                    }
                }
            }
        }*/
        throw new UnsupportedOperationException("Only recursive level 1 is currently supported");
    }
}
