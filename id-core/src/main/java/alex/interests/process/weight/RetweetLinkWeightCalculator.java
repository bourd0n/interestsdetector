package alex.interests.process.weight;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.data.model.ReTweet;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ListMultimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static alex.interests.process.weight.RetweetingWeightsCalculator.findRetweetingUsersWithWeightInLocalUserNet;

public class RetweetLinkWeightCalculator implements LinkWeightCalculator {

    private static final Logger log = LoggerFactory.getLogger(RetweetingWeightsCalculator.class);

    private final DataExtractor dataExtractor;
    private final AlgorithmParams algorithmParams;
    private final boolean useImplicit;

    public RetweetLinkWeightCalculator(DataExtractor dataExtractor, AlgorithmParams algorithmParams, boolean useImplicit) {
        this.algorithmParams = algorithmParams;
        this.dataExtractor = dataExtractor;
        this.useImplicit = useImplicit;
    }

    @Override
    public Map<String, Double> calculateOutLinkWeights(User sourceUser) {
        return null;
    }

    @Override
    public Map<String, Double> calculateInLinkWeights(User sourceUser) {
        String sourceUserId = sourceUser.getId();
        log.debug("Start calculate in-links weights for user {}", sourceUser);

        final Map<String, Integer> retweetingSourceUsers =
                findRetweetingUsersWithWeightInLocalUserNet(sourceUser, dataExtractor);

        Map<String, Integer> implicitLink2SumWeight = new ConcurrentHashMap<>();
        retweetingSourceUsers.entrySet().parallelStream().forEach(retweetingEntry -> {
            final ListMultimap<String, ReTweet> retweetedOfRetweeting = dataExtractor.loadRetweetedUsers(retweetingEntry.getKey());
            retweetedOfRetweeting.asMap().entrySet().forEach(
                    entry -> implicitLink2SumWeight.compute(entry.getKey(),
                            (key, value) -> key.equals(sourceUserId) ? null : MoreObjects.firstNonNull(value, 0) + retweetingEntry.getValue() * entry.getValue().size()));
        });

        log.debug("in: filtered implicit links size {}", implicitLink2SumWeight.size());


        Set<Map.Entry<String, Integer>> entriesFiltered;
        if (useImplicit) {
            entriesFiltered = implicitLink2SumWeight.entrySet()
                    .parallelStream()
                    .filter(entry -> entry.getValue() > 20)
                    .sorted((o1, o2) -> -o1.getValue().compareTo(o2.getValue()))
                    .limit(500)
                    .collect(Collectors.toSet());
        } else {
            Set<String> explicitRTLinks = new HashSet<>();
            explicitRTLinks.addAll(retweetingSourceUsers.keySet());
            explicitRTLinks.addAll(sourceUser.getRetweetedUsers(true).keySet());
            entriesFiltered = implicitLink2SumWeight.entrySet()
                    .parallelStream()
                    .filter(entry -> explicitRTLinks.contains(entry.getKey()))
                    .sorted((o1, o2) -> -o1.getValue().compareTo(o2.getValue()))
                    .limit(500)
                    .collect(Collectors.toSet());
        }

        Map<String, Double> implicitId2Weight = new HashMap<>();
        Double weightRSquareSourceUserSum = Math.sqrt(retweetingSourceUsers.values().parallelStream().mapToDouble(v -> v * v).sum());
        for (Map.Entry<String, Integer> entry : entriesFiltered) {
            final Map<String, Integer> allRetweetingUsersOfImplicitLink
                    = findRetweetingUsersWithWeightInLocalUserNet(entry.getKey(), dataExtractor);

            Double weightRSquareFollowerSum = Math.sqrt(allRetweetingUsersOfImplicitLink.values().parallelStream().mapToDouble(v -> v * v).sum());
            Double normalizationCoef = weightRSquareSourceUserSum * weightRSquareFollowerSum;
            Double sumOfWeightMult = (double) (retweetingSourceUsers.containsKey(entry.getKey()) ?
                    entry.getValue() + retweetingSourceUsers.get(entry.getKey())
                    : entry.getValue());
            implicitId2Weight.put(entry.getKey(), sumOfWeightMult / normalizationCoef);
        }
        log.debug("in: implicitId2Weight returned");

        return implicitId2Weight;
    }
}
