package alex.interests.process.weight;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.data.model.ReTweet;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static alex.interests.process.weight.RetweetingWeightsCalculator.findRetweetingUsersWithWeightInLocalUserNet;

public class RetweetedWeightsCalculator implements LinkWeightCalculator {
    private static final Logger log = LoggerFactory.getLogger(RetweetedWeightsCalculator.class);

    private final DataExtractor dataExtractor;
    private final AlgorithmParams algorithmParams;

    public RetweetedWeightsCalculator(DataExtractor dataExtractor, AlgorithmParams algorithmParams) {
        this.algorithmParams = algorithmParams;
        this.dataExtractor = dataExtractor;
    }

    public Map<String, Double> calculateOutLinkWeights(User sourceUser) {
        ListMultimap<String, ReTweet> sourceUserRetweetedUsers = sourceUser.getRetweetedUsers(true);
        final Set<String> sourceRTUsers = sourceUserRetweetedUsers.keySet();
        String sourceUserId = sourceUser.getId();
        log.debug("Start calculate out-links weights for user {}", sourceUser);
        //calculate Wr({u,v}, a) table
        Table<String, String, Double> edgeWeightsTable = HashBasedTable.create();
        //wr(u,u)=1
        edgeWeightsTable.put(sourceUserId, sourceUserId, 1.0);
        for (String sourceUserRTid : sourceRTUsers) {
            //weight is number of retweets
            edgeWeightsTable.put(sourceUserId, sourceUserRTid, (double) sourceUserRetweetedUsers.get(sourceUserRTid).size());
            //wr(v,v)=1
            edgeWeightsTable.put(sourceUserRTid, sourceUserRTid, 1.0);
            //final User rtUserById = dataExtractor.getUserByIdWithoutReferences(sourceUserRTid);
            //if (rtUserById == null)
            //    in mongo some users not exists
            // continue;
            ListMultimap<String, ReTweet> rtUsersOfSourceRTUser = dataExtractor.loadRetweetedUsers(sourceUserRTid);
            rtUsersOfSourceRTUser.keySet().stream()
                    .filter(sourceRTUsers::contains)
                    .forEach(rtUserIdOfSourceRTUser ->
                            edgeWeightsTable.put(sourceUserRTid, rtUserIdOfSourceRTUser, (double) rtUsersOfSourceRTUser.get(rtUserIdOfSourceRTUser).size()));
        }
        log.debug("out-links edgeWeightsTable constructed. Size {}", edgeWeightsTable.size());
        Map<String, Double> rtId2Weight = new HashMap<>();
        //calculate Wa(u,v)
        Double weightRSquareSourceUserSum = sumOfSquareWeighRForUser_authority(sourceUserId, edgeWeightsTable);
        Map<String, Double> authorityForSourceUserToWeight = edgeWeightsTable.row(sourceUserId);
        for (String rtUserId : sourceRTUsers) {
            Double weightRSquareRTSum = getSumOfSquareWeighRForRTUser_authority(rtUserId);
            if (weightRSquareRTSum == 0) {
                //in mongo some users not exists
                continue;
            }
            Double normalizationCoef = Math.sqrt(weightRSquareRTSum * weightRSquareSourceUserSum);
            Map<String, Double> authorityForFriendToWeight = edgeWeightsTable.row(rtUserId);
            Sets.SetView<String> keyIntersection = Sets.intersection(authorityForSourceUserToWeight.keySet(), authorityForFriendToWeight.keySet());
            Double sumOfWeightMult = keyIntersection.stream()
                    .mapToDouble(commonAuthId -> authorityForSourceUserToWeight.get(commonAuthId) * authorityForFriendToWeight.get(commonAuthId))
                    .sum();
            rtId2Weight.put(rtUserId, sumOfWeightMult / normalizationCoef);
        }
        log.debug("out-links rtId2Weight calculated. Size {}", rtId2Weight.size());
        return rtId2Weight;
    }

    //todo:
    public Map<String, Double> calculateInLinkWeights(User sourceUser) {
        return calculateInLinkWeightsWithRetweetingUsers(sourceUser);
    }

    /**
     * Finding inlinks by retweets is difficult - api could be run by self user. So we try to find it in ego-net
     */
    public Map<String, Double> calculateInLinkWeightsWithRetweetingUsers(User sourceUser) {
        ListMultimap<String, ReTweet> sourceUserRetweetedUsers = sourceUser.getRetweetedUsers(true);
        final Set<String> sourceRTUsers = sourceUserRetweetedUsers.keySet();
        String sourceUserId = sourceUser.getId();
        log.debug("Start calculate in-links weights for user {}", sourceUser);
        //calculate Wr({u,v}, a) table
        Table<String, String, Double> edgeWeightsTable = HashBasedTable.create();
        //wr(u,u)=1
        edgeWeightsTable.put(sourceUserId, sourceUserId, 1.0);
        final Map<String, Integer> retweetingSourceUsers =
                findRetweetingUsersWithWeightInLocalUserNet(sourceUser, dataExtractor);

        for (Map.Entry<String, Integer> entry : retweetingSourceUsers.entrySet()) {
            edgeWeightsTable.put(entry.getKey(), sourceUserId, Double.valueOf(entry.getValue()));
        }

        Map<String, Double> retweetedRTUsersSumOfSquareWeightR = new HashMap<>();
        for (String sourceUserRTid : sourceRTUsers) {
            //weight is number of retweets
            edgeWeightsTable.put(sourceUserId, sourceUserRTid, (double) sourceUserRetweetedUsers.get(sourceUserRTid).size());
            //wr(v,v)=1
            edgeWeightsTable.put(sourceUserRTid, sourceUserRTid, 1.0);
            final Map<String, Integer> retweetingRTUsers = findRetweetingUsersWithWeightInLocalUserNet(sourceUserRTid, dataExtractor);
            retweetingRTUsers.entrySet().parallelStream()
                    .filter(entry -> retweetingSourceUsers.containsKey(entry.getKey()))
                    .forEach(entry -> edgeWeightsTable.put(entry.getKey(), sourceUserRTid, Double.valueOf(entry.getValue())));

            Double weightRSquareRTSum = retweetingRTUsers.entrySet().parallelStream().mapToDouble(value -> {
                double weight = (double) value.getValue();
                return weight * weight;
            }).sum();

            retweetedRTUsersSumOfSquareWeightR.put(sourceUserRTid, weightRSquareRTSum);

        }
        log.debug("in-links edgeWeightsTable constructed. Size {}", edgeWeightsTable.size());
        Map<String, Double> rtId2Weight = new HashMap<>();
        //calculate Wa(u,v)
        Double weightRSquareSourceUserSum = retweetingSourceUsers.entrySet().parallelStream().mapToDouble(value -> {
            double weight = (double) value.getValue();
            return weight * weight;
        }).sum();

        Map<String, Double> authorityForSourceUserToWeight = edgeWeightsTable.column(sourceUserId);
        for (String rtUserId : sourceRTUsers) {
            Double weightRSquareRTSum = retweetedRTUsersSumOfSquareWeightR.get(rtUserId);
            if (weightRSquareRTSum == null || weightRSquareRTSum == 0) {
                //in mongo some users not exists
                continue;
            }
            Double normalizationCoef = Math.sqrt(weightRSquareRTSum * weightRSquareSourceUserSum);
            Map<String, Double> authorityForFriendToWeight = edgeWeightsTable.column(rtUserId);
            Sets.SetView<String> keyIntersection = Sets.intersection(authorityForSourceUserToWeight.keySet(), authorityForFriendToWeight.keySet());
            Double sumOfWeightMult = keyIntersection.stream()
                    .mapToDouble(commonAuthId -> authorityForSourceUserToWeight.get(commonAuthId) * authorityForFriendToWeight.get(commonAuthId))
                    .sum();
            rtId2Weight.put(rtUserId, sumOfWeightMult / normalizationCoef);
        }
        log.debug("in-links rtId2Weight calculated. Size {}", rtId2Weight.size());
        return rtId2Weight;
    }

    private Double sumOfSquareWeighRForUser_authority(String userId, Table<String, String, Double> edgeWeightsTable) {
        return edgeWeightsTable
                .row(userId)
                .values()
                .stream()
                .mapToDouble(weight -> weight * weight)
                .sum();
    }

    private Double sumOfSquareWeighRForUser_hubs(String userId, Table<String, String, Double> edgeWeightsTable) {
        return edgeWeightsTable
                .column(userId)
                .values()
                .stream()
                .mapToDouble(weight -> weight * weight)
                .sum();
    }

    private Double getSumOfSquareWeighRForRTUser_authority(String rtUserId) {
        //final User friendById = dataExtractor.getUserById(rtUserId);
        //if (friendById == null) {
        //    return 0.0;
        //}
        //final ListMultimap<String, ReTweet> rtOfRT = friendById.getRetweetedUsers(true);
        final ListMultimap<String, ReTweet> rtOfRT = dataExtractor.loadRetweetedUsers(rtUserId);
        return 1 /*requested rt*/ + rtOfRT.keySet()
                .stream()
                .mapToDouble(rtId -> {
                    final int retweetsSize = rtOfRT.get(rtId).size();
                    return retweetsSize * retweetsSize;
                })
                .sum();
    }

}
