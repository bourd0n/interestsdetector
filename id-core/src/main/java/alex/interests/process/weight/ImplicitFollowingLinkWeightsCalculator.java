package alex.interests.process.weight;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import com.google.common.base.MoreObjects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class ImplicitFollowingLinkWeightsCalculator implements LinkWeightCalculator {

    private static final Logger log = LoggerFactory.getLogger(ImplicitFollowingLinkWeightsCalculator.class);

    private final DataExtractor dataExtractor;
    private final AlgorithmParams algorithmParams;

    public ImplicitFollowingLinkWeightsCalculator(DataExtractor dataExtractor, AlgorithmParams algorithmParams) {
        this.algorithmParams = algorithmParams;
        this.dataExtractor = dataExtractor;
    }

    @Override
    public Map<String, Double> calculateOutLinkWeights(User sourceUser) {
        final String sourceUserId = sourceUser.getId();
        final Collection<String> friends = dataExtractor.loadUserFriends(sourceUserId);
        Map<String, Integer> implicitLink2SumWeight = new ConcurrentHashMap<>();
        log.debug("out: Start gather implicit links for user {}.", sourceUser);
        friends.parallelStream().forEach(friendId -> {
            final Collection<String> friendFollowers = dataExtractor.loadUserFollowers(friendId);
            friendFollowers.forEach(
                    s -> implicitLink2SumWeight.compute(s,
                            (key, value) -> key.equals(sourceUserId) ? null : MoreObjects.firstNonNull(value, 0) + 1));
        });

        final List<String> entriesFilteredByCount = implicitLink2SumWeight.entrySet()
                .parallelStream()
                .filter(entry -> entry.getValue() > 10)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        final List<String> entitiesFilteredByTweetExistence = dataExtractor.filterUsersWithTweets(entriesFilteredByCount);

        final Set<Map.Entry<String, Integer>> entriesFiltered = implicitLink2SumWeight.entrySet()
                .parallelStream()
                .filter(entry -> entitiesFilteredByTweetExistence.contains(entry.getKey()))
                .sorted((o1, o2) -> -o1.getValue().compareTo(o2.getValue()))
                .limit(500)
                .collect(Collectors.toSet());

        log.debug("out: filtered implicit links size {}", entriesFiltered.size());
        Map<String, Double> implicitId2Weight = new HashMap<>();
        Double weightRSquareSourceUserSum = Math.sqrt(friends.size() + 1);
        for (Map.Entry<String, Integer> entry : entriesFiltered) {
            Double weightRSquareFollowerSum = Math.sqrt(dataExtractor.loadUserFriends(entry.getKey()).size() + 1);
            Double normalizationCoef = weightRSquareSourceUserSum * weightRSquareFollowerSum;
            Double sumOfWeightMult = (double) (friends.contains(entry.getKey()) ? entry.getValue() + 1 : entry.getValue());
            implicitId2Weight.put(entry.getKey(), sumOfWeightMult / normalizationCoef);
        }
        log.debug("out: implicitId2Weight returned");
        return implicitId2Weight;
    }

    @Override
    public Map<String, Double> calculateInLinkWeights(User sourceUser) {
        final String sourceUserId = sourceUser.getId();
        final Collection<String> followers = dataExtractor.loadUserFollowers(sourceUserId);
        Map<String, Integer> implicitLink2SumWeight = new ConcurrentHashMap<>();
        log.debug("in: Start gather implicit links for user {}.", sourceUser);
        followers.parallelStream().forEach(followerId -> {
            final Collection<String> followerFriends = dataExtractor.loadUserFriends(followerId);
            followerFriends.forEach(
                    s -> implicitLink2SumWeight.compute(s,
                            (key, value) -> key.equals(sourceUserId) ? null : MoreObjects.firstNonNull(value, 0) + 1));
        });

        final List<String> entriesFilteredByCount = implicitLink2SumWeight.entrySet()
                .parallelStream()
                .filter(entry -> entry.getValue() > 10)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        final List<String> entitiesFilteredByTweetExistence = dataExtractor.filterUsersWithTweets(entriesFilteredByCount);

        final Set<Map.Entry<String, Integer>> entriesFiltered = implicitLink2SumWeight.entrySet()
                .parallelStream()
                .filter(entry -> entitiesFilteredByTweetExistence.contains(entry.getKey()))
                .sorted((o1, o2) -> -o1.getValue().compareTo(o2.getValue()))
                .limit(500)
                .collect(Collectors.toSet());

        log.debug("in: filtered implicit links size {}", entriesFiltered.size());
        Map<String, Double> implicitId2Weight = new HashMap<>();
        Double weightRSquareSourceUserSum = Math.sqrt(followers.size() + 1);
        for (Map.Entry<String, Integer> entry : entriesFiltered) {
            Double weightRSquareFollowerSum = Math.sqrt(dataExtractor.loadUserFollowers(entry.getKey()).size() + 1);
            Double normalizationCoef = weightRSquareSourceUserSum * weightRSquareFollowerSum;
            Double sumOfWeightMult = (double) (followers.contains(entry.getKey()) ? entry.getValue() + 1 : entry.getValue());
            implicitId2Weight.put(entry.getKey(), sumOfWeightMult / normalizationCoef);
        }
        log.debug("in: implicitId2Weight returned");
        return implicitId2Weight;
    }
}
