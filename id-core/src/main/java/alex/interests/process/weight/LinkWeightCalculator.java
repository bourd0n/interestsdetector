package alex.interests.process.weight;

import alex.interests.data.model.User;

import java.util.Map;

public interface LinkWeightCalculator {

    Map<String, Double> calculateOutLinkWeights(User sourceUser);

    Map<String, Double> calculateInLinkWeights(User sourceUser);
}
