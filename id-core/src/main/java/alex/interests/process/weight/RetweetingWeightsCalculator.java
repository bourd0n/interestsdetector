package alex.interests.process.weight;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.data.model.ReTweet;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import com.google.common.collect.ListMultimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jws.soap.SOAPBinding;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RetweetingWeightsCalculator implements LinkWeightCalculator {

    private static final Logger log = LoggerFactory.getLogger(RetweetingWeightsCalculator.class);

    private final DataExtractor dataExtractor;
    private final AlgorithmParams algorithmParams;

    public RetweetingWeightsCalculator(DataExtractor dataExtractor, AlgorithmParams algorithmParams) {
        this.algorithmParams = algorithmParams;
        this.dataExtractor = dataExtractor;
    }

    @Override
    public Map<String, Double> calculateOutLinkWeights(User sourceUser) {
        return null;
    }

    @Override
    public Map<String, Double> calculateInLinkWeights(User sourceUser) {
        return null;
    }

    public static Map<String, Integer> findRetweetingUsersWithWeightInLocalUserNet(Object sourceUser, DataExtractor dataExtractor) {
        final Set<String> egoNet = new HashSet<>();
        String sourceUserId;
        if (sourceUser instanceof String) {
            egoNet.addAll(dataExtractor.loadUserFollowers((String)sourceUser));
            egoNet.addAll(dataExtractor.loadUserFriends((String) sourceUser));
            sourceUserId = (String) sourceUser;
        } else if (sourceUser instanceof User){
            egoNet.addAll(((User) sourceUser).getFollowers(true));
            egoNet.addAll(((User) sourceUser).getFriends(true));
            sourceUserId = ((User) sourceUser).getId();
        } else {
            throw new RuntimeException("findRetweetingUsersWithWeightInLocalUserNet work for objects User and String");
        }
        Map<String, Integer> retweetingUsersWithWeight = new HashMap<>();
        for (String alterId : egoNet) {
            final ListMultimap<String, ReTweet> retweetedUsersForAlterUserId = dataExtractor.loadRetweetedUsers(alterId);
            if (retweetedUsersForAlterUserId.keySet().contains(sourceUserId)) {
                retweetingUsersWithWeight.put(alterId, retweetedUsersForAlterUserId.get(sourceUserId).size());
            }
        }
        log.debug("Search retweeting users for {} return {} users", sourceUser, retweetingUsersWithWeight.keySet().size());
        return retweetingUsersWithWeight;
    }
}
