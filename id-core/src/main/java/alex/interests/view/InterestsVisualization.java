package alex.interests.view;

import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.model.Topic;
import alex.interests.data.model.User;
import alex.interests.process.UserInterestsInferencer;
import alex.interests.process.mapping.WikiTopicMapper;
import alex.interests.process.mapping.entities.Category;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.params.InterestsMapperParams;
import alex.interests.process.params.ReferenceType;
import alex.interests.process.params.RelationType;
import alex.interests.process.topic.TopicModel;
import alex.interests.utils.ConfigProperties;
import alex.interests.utils.JsonSerializer;
import com.mxgraph.analysis.mxAnalysisGraph;
import com.mxgraph.analysis.mxGraphStructure;
import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.view.mxGraph;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class InterestsVisualization {

    private static final Logger log = LoggerFactory.getLogger(InterestsVisualization.class);

    public static mxGraph constructGraph(Map<Category, Double> topicsToInterests) {
        return constructGraph(topicsToInterests, null);
    }

    public static mxGraph constructGraph(Map<Category, Double> topicsToInterests, String imageFile) {
        mxGraph graph = new mxGraph();
        graph.setMultigraph(false);
        Object parent = graph.getDefaultParent();

        Map<String, Object> alreadyCreatedVertexes = new HashMap<>();
        graph.getModel().beginUpdate();
        try {
            for (Map.Entry<Category, Double> categoryEntry : topicsToInterests.entrySet()) {
                final Category category = categoryEntry.getKey();
                final String id = String.valueOf(category.getId());
                if (alreadyCreatedVertexes.containsKey(id)) {
                    continue;
                }
                final Object categoryVertex = graph.insertVertex(parent, id, category.getTitle(), 20, 20, 80, 30);
                alreadyCreatedVertexes.put(id, categoryVertex);
                processChildCategories(graph, categoryVertex, category.getChildCategories(), alreadyCreatedVertexes, parent);
            }

            mxHierarchicalLayout layout = new mxHierarchicalLayout(graph);
            layout.execute(graph.getDefaultParent());
        } finally {
            graph.getModel().endUpdate();
        }
        mxAnalysisGraph analysisGraph = new mxAnalysisGraph();
        analysisGraph.setGraph(graph);
        mxGraphStructure.makeSimple(analysisGraph);

        if (StringUtils.isNotEmpty(imageFile)) {
            BufferedImage image = mxCellRenderer.createBufferedImage(graph, null, 1, Color.WHITE, true, null);
            try {
                ImageIO.write(image, "PNG", new File(imageFile));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        return graph;
    }

    public static JFrame visualize(Map<Category, Double> topicsToInterests) {
        return visualize(topicsToInterests, null);
    }

    public static JFrame visualize(Map<Category, Double> topicsToInterests, String imageFile) {
        JFrame jFrame = new JFrame("Result");
        mxGraph graph = constructGraph(topicsToInterests, imageFile);
        mxGraphComponent graphComponent = new mxGraphComponent(graph);
        jFrame.getContentPane().add(graphComponent);
        return jFrame;
    }

    private static void processChildCategories(mxGraph graph, Object parentCategoryVertex, Map<Integer, Category> childCategories, Map<String, Object> alreadyCreatedVertexes, Object parent) {
        for (Map.Entry<Integer, Category> categoryEntry : childCategories.entrySet()) {
            final Category category = categoryEntry.getValue();
            final String id = String.valueOf(category.getId());
            final Object categoryVertexChild = alreadyCreatedVertexes.computeIfAbsent(id, s -> graph.insertVertex(parent, id, category.getTitle(), 20, 20, 80, 30));
            graph.insertEdge(parent, null, null, categoryVertexChild, parentCategoryVertex);
            processChildCategories(graph, categoryVertexChild, category.getChildCategories(), alreadyCreatedVertexes, parent);
        }
    }

    public static JFrame visualizeForUser(String userId) {
        String fileTopics = ConfigProperties.getInstance().getBasicPath() + "diplom\\serialization\\test_topic_distribution" + userId + ".json";
        String fileCategories = ConfigProperties.getInstance().getBasicPath() + "diplom\\serialization\\test_category_distribution" + userId + ".json";
        String fileImage = ConfigProperties.getInstance().getBasicPath() + "diplom\\serialization\\" + userId + ".png";

        Map<Topic, Double> calculatedTopics = JsonSerializer.readTopics(fileTopics);
        if (calculatedTopics == null) {
            final AlgorithmParams algorithmParamsInOutOwn = new AlgorithmParams.AlgorithmParamsBuilder()
                    .numIterations(2000)
                    .numThreads(1)
                    .numTopics(75)
                    .build();
            final MongoExtractor mongoExtractor = MongoExtractor.getInstance();
            final User user = mongoExtractor.getUserById(userId);
            UserInterestsInferencer interestsInferencer = new UserInterestsInferencer(mongoExtractor, algorithmParamsInOutOwn);
            calculatedTopics = interestsInferencer.inferenceTopics(user,
                    RelationType.FOLLOWING, ReferenceType.ALL);
            JsonSerializer.write(fileTopics, calculatedTopics);
        }
        log.debug(TopicModel.toStringSortedByDistribution(calculatedTopics));

        Map<Category, Double> topicsToInterests = JsonSerializer.readCategories(fileCategories);
        if (topicsToInterests == null) {
            WikiTopicMapper topicMapper = new WikiTopicMapper(InterestsMapperParams.DEFAULT_MAPPER_PARAMS);
            topicsToInterests = topicMapper.mapTopicsToInterests(calculatedTopics);
            log.debug("Topic to interests map : {}", topicsToInterests);
            log.debug("Categories titles sorted: {} ", topicsToInterests.keySet().stream().map(Category::getTitle).collect(Collectors.toList()));
            JsonSerializer.write(fileCategories, topicsToInterests);
        }

        //final OptionalDouble averageWeight = topicsToInterests.keySet().parallelStream().mapToDouble(d -> d).average();
        //final Double avg = averageWeight.getAsDouble() * 0.7;
        //topicsToInterests = topicsToInterests.headMap(avg);
        return InterestsVisualization.visualize(topicsToInterests, fileImage);
    }

    public static void main(String[] args) throws IOException {
        String userId = "24107824";
        JFrame frame = InterestsVisualization.visualizeForUser(userId);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 320);
        frame.setVisible(true);
    }
}
