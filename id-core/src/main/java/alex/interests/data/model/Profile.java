package alex.interests.data.model;

public class Profile {
    private String name;
    private String screenName;
    private String description;

    public Profile(String name, String screenName, String description) {
        this.description = description;
        this.name = name;
        this.screenName = screenName;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getScreenName() {
        return screenName;
    }
}