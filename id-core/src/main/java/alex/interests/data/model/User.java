package alex.interests.data.model;

import alex.interests.process.topic.TopicModel;
import com.google.common.collect.ListMultimap;

import java.util.List;
import java.util.Set;

public interface User {
    String getId();

    Profile getProfile(boolean load);

    Set<String> getFriends(boolean load);

    Set<String> getFollowers(boolean load);

    ListMultimap<String, ReTweet> getRetweetedUsers(boolean load);

    List<Tweet> getTweets(boolean load);

    boolean isTopicsCalculated();

    TopicModel getOwnTopicModel();

    void setOwnTopicModel(TopicModel ownTopicModel);
}
