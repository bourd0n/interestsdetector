package alex.interests.data.model;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.process.topic.TopicModel;
import com.google.common.base.MoreObjects;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import org.apache.commons.collections.CollectionUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import static alex.interests.data.model.UserField.*;

public class UserImpl implements User {

    private String id;
    private volatile Profile profile;
    private final Set<String> friends;
    private final Set<String> followers;
    private final ListMultimap<String, ReTweet> retweetedUsers;
    private final List<Tweet> tweets;
    private volatile TopicModel ownTopicModel;
    private volatile boolean topicsCalculated = false;
    private final Map<UserField, Boolean> loadedParameters = new ConcurrentHashMap<>();
    private final DataExtractor dataExtractor;
    private final ReentrantLock friendLoadingLock = new ReentrantLock();
    private final ReentrantLock followerLoadingLock = new ReentrantLock();
    private final ReentrantLock retweetedUsersLoadingLock = new ReentrantLock();

    protected UserImpl(UserBuilder userBuilder) {
        id = userBuilder.id;
        tweets = userBuilder.tweets;
        followers = userBuilder.followers;
        friends = userBuilder.friends;
        retweetedUsers = userBuilder.retweetedUsers;
        loadedParameters.putAll(userBuilder.loadedParameters);
        dataExtractor = userBuilder.dataExtractor;
    }

    public String getId() {
        return this.id;
    }

    public Profile getProfile(boolean load) {
        if (!loadedParameters.getOrDefault(PROFILE, false) && load) {
            dataExtractor.loadUserProfile(id);
            loadedParameters.putIfAbsent(PROFILE, true);
        }
        return profile;
    }

    public Set<String> getFriends(boolean load) {
        if (!loadedParameters.getOrDefault(FRIENDS, false) && load) {
            friendLoadingLock.lock();
            try {
                if (!loadedParameters.getOrDefault(FRIENDS, false)) {
                    final Collection<String> userFriends = dataExtractor.loadUserFriends(id);
                    friends.addAll(userFriends);
                    loadedParameters.putIfAbsent(FRIENDS, true);
                }
            } finally {
                friendLoadingLock.unlock();
            }
        }
        return Collections.unmodifiableSet(friends);
    }

    public Set<String> getFollowers(boolean load) {
        if (!loadedParameters.getOrDefault(FOLLOWERS, false) && load) {
            followerLoadingLock.lock();
            try {
                if (!loadedParameters.getOrDefault(FOLLOWERS, false)) {
                    final Collection<String> userFollowers = dataExtractor.loadUserFollowers(id);
                    followers.addAll(userFollowers);
                    loadedParameters.putIfAbsent(FOLLOWERS, true);
                }
            } finally {
                followerLoadingLock.unlock();
            }
        }
        return Collections.unmodifiableSet(followers);
    }

    public ListMultimap<String, ReTweet> getRetweetedUsers(boolean load) {
        if (!loadedParameters.getOrDefault(RETWEETED_USERS, false) && load) {
            dataExtractor.loadRetweetedUsers(id);
            loadedParameters.putIfAbsent(RETWEETED_USERS, true);
        }
        return Multimaps.unmodifiableListMultimap(retweetedUsers);
    }

    public List<Tweet> getTweets(boolean load) {
        if (!loadedParameters.getOrDefault(TWEETS, false) && load) {
            dataExtractor.loadUserTweets(id);
            loadedParameters.putIfAbsent(TWEETS, true);
        }
        return Collections.unmodifiableList(tweets);
    }

    public boolean isTopicsCalculated() {
        return topicsCalculated;
    }

    public boolean isParameterLoaded(UserField field) {
        return loadedParameters.get(field);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("friendsSize", friends.size())
                .add("followersSize", followers.size())
                .add("tweetsSize", tweets.size())
                .toString();
    }

    public TopicModel getOwnTopicModel() {
        return ownTopicModel;
    }

    public void setOwnTopicModel(TopicModel ownTopicModel) {
        if (!topicsCalculated) {
            this.ownTopicModel = ownTopicModel;
            this.topicsCalculated = true;
        }
    }

    public static class UserBuilder {
        private String id;
        private Profile profile;
        private Set<String> friends = new HashSet<>();
        private Set<String> followers = new HashSet<>();
        private ListMultimap<String, ReTweet> retweetedUsers = ArrayListMultimap.create();
        private List<Tweet> tweets = new ArrayList<>();
        private DataExtractor dataExtractor;
        private Map<UserField, Boolean> loadedParameters = new HashMap<>();

        public UserBuilder(String id) {
            this.id = id;
            loadedParameters.put(ID, true);
        }

        public UserBuilder profile(Profile profile) {
            this.profile = profile;
            loadedParameters.put(PROFILE, true);
            return this;
        }

        public UserBuilder friends(Collection<String> friends) {
            if (CollectionUtils.isNotEmpty(friends)) {
                this.friends.addAll(friends);
            }
            loadedParameters.put(FRIENDS, true);
            return this;
        }

        public UserBuilder followers(Collection<String> followers) {
            if (CollectionUtils.isNotEmpty(followers)) {
                this.followers.addAll(followers);
            }
            loadedParameters.put(FOLLOWERS, true);
            return this;
        }

        public UserBuilder tweets(Collection<Tweet> tweets) {
            if (CollectionUtils.isNotEmpty(tweets)) {
                this.tweets.addAll(tweets);
            }
            loadedParameters.put(TWEETS, true);
            return this;
        }

        public UserBuilder retweetedUsers(Multimap<String, ReTweet> reTweetMultimap) {
            retweetedUsers.putAll(reTweetMultimap);
            loadedParameters.put(RETWEETED_USERS, true);
            return this;
        }

        public UserBuilder dataExtractor(DataExtractor extractor) {
            this.dataExtractor = extractor;
            return this;
        }

        public User build() {
            return new UserImpl(this);
        }
    }
}