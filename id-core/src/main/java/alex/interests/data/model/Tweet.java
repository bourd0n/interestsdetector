package alex.interests.data.model;

import org.apache.commons.lang.ObjectUtils;

import java.util.Collection;
import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;

public class Tweet {

    protected final String id;
    protected final String text;
    protected final String userId;
    protected final Collection<String> hashtags;
    protected String createdAt;
    //not use yet
    protected Collection<String> mentionedUserIds;

    public Tweet(Long id, String text, Long userId, Collection<String> hashtags, String createdAt) {
        checkNotNull(id, "Argument id can't bu null");
        this.id = id.toString();
        this.text = text;
        this.userId = ObjectUtils.toString(userId, null);
        this.hashtags = hashtags;
        this.createdAt = createdAt;
    }

    public Tweet(Long id, String text, Long userId, Collection<String> hashtags) {
        checkNotNull(id, "Argument id can't bu null");
        this.id = id.toString();
        this.text = text;
        this.userId = ObjectUtils.toString(userId, null);
        this.hashtags = hashtags;
    }


    public Tweet(String id, String text, String userId, Collection<String> hashtags) {
        checkNotNull(id, "Argument id can't bu null");
        this.id = id;
        this.text = text;
        this.userId = userId;
        this.hashtags = hashtags;
    }

    public Tweet(String id, String text, String userId, Collection<String> hashtags, String createdAt) {
        checkNotNull(id, "Argument id can't bu null");
        this.id = id;
        this.text = text;
        this.userId = userId;
        this.hashtags = hashtags;
        this.createdAt = createdAt;
    }

    public String getText() {
        return text;
    }

    public Collection<String> getHashtags() {
        return hashtags;
    }

    public Collection<String> getMentionedUserIds() {
        return mentionedUserIds;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", userId='" + userId + '\'' +
                ", hashtags=" + hashtags +
                '}';
    }
}