package alex.interests.data.model;

import com.google.common.base.MoreObjects;

/**
 * Simple representation
 */
public class Interest implements Comparable<Interest> {

    private int id;
    private double weight;
    private String title;

    public Interest() {
    }

    public Interest(int id, double weight, String title) {
        this.id = id;
        this.weight = weight;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("title", title)
                .add("weight", weight)
                .toString();
    }

    @Override
    public int compareTo(Interest o) {
        return Double.compare(this.weight, o.weight);
    }
}