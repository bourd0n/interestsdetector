package alex.interests.data.model;

import com.google.common.base.*;
import com.google.common.base.Objects;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.*;

public class Topic implements Serializable {

    private String id;
    private Map<String, Double> termsDistribution = new HashMap<>();

    //only for serializing
    @Deprecated
    protected Topic() {
    }

    public Topic(String id, Map<String, Double> termsDistribution) {
        this.id = id;
        this.termsDistribution.putAll(termsDistribution);
    }

    public Topic(Map<String, Double> termsDistribution) {
        this.id = UUID.randomUUID().toString();
        this.termsDistribution.putAll(termsDistribution);
    }

    public String getId() {
        return id;
    }

    public Map<String, Double> getTermsDistribution() {
        return termsDistribution;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Topic topic = (Topic) o;
        return Objects.equal(id, topic.id) &&
                Objects.equal(termsDistribution, topic.termsDistribution);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, termsDistribution);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("Topic {");
        DecimalFormat df = new DecimalFormat("#.#");
        for (Map.Entry<String, Double> termDistributionEntry : termsDistribution.entrySet()) {
            stringBuilder.append(termDistributionEntry.getKey())
                    .append("(")
                    .append(df.format(termDistributionEntry.getValue()))
                    .append(")")
                    .append(" ");
        }
        stringBuilder.append("}");
        return stringBuilder.toString();
    }
}