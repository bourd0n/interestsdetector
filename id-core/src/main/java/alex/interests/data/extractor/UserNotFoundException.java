package alex.interests.data.extractor;

public class UserNotFoundException extends Exception {

    private final String userId;

    public UserNotFoundException(String userId) {
        this.userId = userId;
    }

    @Override
    public String getMessage() {
        return "User with id " + userId + " is not found.";
    }
}
