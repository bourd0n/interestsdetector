package alex.interests.data.extractor.twitter;


import alex.interests.data.extractor.LocalMongoExtractor;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ispras.crawler.ssgps.im.twitter.links.LinksTwitterBFSCrawler;
import ru.ispras.crawler.ssgps.im.twitter.links.ProfileTwitterBFSCrawler;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static alex.interests.data.extractor.MongoCollections.PROFILE;

public class TwitterUtil {
    private static final Logger log = LoggerFactory.getLogger(TwitterUtil.class);

    private static final int timelineLimit = 1000;
    private static final LinksTwitterBFSCrawler crawler = new LinksTwitterBFSCrawler(1, timelineLimit);
    private static final ProfileTwitterBFSCrawler profileCrawler = new ProfileTwitterBFSCrawler();
    private static final Twitter twitter = new TwitterFactory(new ConfigurationBuilder().setDebugEnabled(true)
            .setOAuthConsumerKey("j360GiXDDSphpAs1iTYV6PuZv")
            .setOAuthConsumerSecret("EmVcIIceqG1E8xBNOqf7KszjtZPr1uCZXNZ737xz6ZiSWWS8qL")
            .setOAuthAccessToken("1648547509-DWD44PYBzxFKiHkgNiEwCz1PvQ8HnlC9cTN7b9M")
            .setOAuthAccessTokenSecret("9Kx17bpP6JNB3WUK79LbKxTnmDHEVOm6FVMIw5feQhMqe")
            .build())
            .getInstance();

    public static void loadUserById(long id) {
        //maybe it will be already processed
        final DB db = LocalMongoExtractor.getInstance().db();
        final BasicDBObject idDbQuery = new BasicDBObject("id", id);
        final DBCollection processed_users = db.getCollection(PROFILE.toString());
        final DBObject processedUser = processed_users.findOne(idDbQuery);
        if (processedUser == null) {
            final String path = TwitterUtil.class.getResource("/twitterAllStepan.cfg").getPath();
            try {
                crawler.crawl(path, id, crawler.getNeighbourLimit(), timelineLimit);
                profileCrawler.crawl(path, id);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Load from twitter
     */
    public static long getUserIdByScreenName(String screenName) {
        final User user;
        try {
            user = twitter.showUser(screenName);
        } catch (TwitterException e) {
            throw new RuntimeException(e);
        }
        return user.getId();
    }

    public static Map<Long, String> getLoadedUsers() {
        final DB db = LocalMongoExtractor.getInstance().db();
        //todo : add extract only required fields
        final DBCollection processed_users = db.getCollection(PROFILE.toString());
        final List<DBObject> dbObjects = processed_users.find().toArray();
        return dbObjects.parallelStream().collect(Collectors.toMap(dbObjectForKey ->  new Long(String.valueOf(dbObjectForKey.get("id"))),
                dbObjectForValue -> (String) ((DBObject) ((DBObject) dbObjectForValue.get("value")).get("object")).get("screen_name")));
    }
}
