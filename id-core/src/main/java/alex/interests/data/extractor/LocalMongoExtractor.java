package alex.interests.data.extractor;

import alex.interests.data.model.Topic;
import alex.interests.process.mapping.WikiTopicMapper;
import alex.interests.process.mapping.entities.Category;
import alex.interests.process.params.InterestsMapperParams;
import alex.interests.utils.ConfigProperties;
import alex.interests.utils.JsonSerializer;
import com.google.common.collect.ImmutableMap;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.Map;

public class LocalMongoExtractor extends MongoExtractor implements DataExtractor {
    private static final Logger log = LoggerFactory.getLogger(LocalMongoExtractor.class);

    private static volatile LocalMongoExtractor INSTANCE;

    @Override
    protected void init() {
        ConfigProperties CONFIG = ConfigProperties.getInstance();
        String cacheEnabled = CONFIG.getProperty("cache.enabled");
        String host = CONFIG.getProperty("local.mongo.server");
        String port = CONFIG.getProperty("local.mongo.port");
        isCacheEnabled = Boolean.valueOf(cacheEnabled);
        tweetsCount = Integer.parseInt(CONFIG.getProperty("tweets.count"));
        tweetsLang = CONFIG.getProperty("tweets.lang");
        client = new MongoClient(host, Integer.valueOf(port));
        db = client.getDB(getDbName());
        checkUserExistence = true;
    }

    public static LocalMongoExtractor getInstance() {
        LocalMongoExtractor localInstance = INSTANCE;
        if (localInstance == null) {
            synchronized (MongoExtractor.class) {
                localInstance = INSTANCE;
                if (localInstance == null) {
                    INSTANCE = localInstance = new LocalMongoExtractor();
                    INSTANCE.init();
                }
            }
        }
        return localInstance;
    }

    @Override
    protected DBObject getTimelineFields() {
        return MongoQueries.TWEETS_FIELDS_LOCAL;
    }

    @Override
    protected String getUserIdKey() {
        return "id";
    }

    @Override
    protected String getDbName() {
        return "im-twitter";
    }

    @Override
    protected Object getValueFromDB(DBObject dbObject, String key) {
        return ((DBObject) dbObject.get("value")).get("object");
    }

    @Override
    protected void finalize() throws Throwable {
        client.close();
        super.finalize();
    }

    public LinkedHashMap<Category, Double> loadInterestsForUser(long id) {
        log.debug("Load userInterests for user {}.", id);
        final DBCollection interestsCollection = db.getCollection(MongoCollections.INTERESTS.toString());
        final BasicDBObject idDbQuery = new BasicDBObject("id", id);
        final DBObject interestsForUser = interestsCollection.findOne(idDbQuery);
        if (interestsForUser != null) {
            final Object categories = interestsForUser.get("categories");
            return JsonSerializer.deserializeCategories(categories);
        }
        return null;
    }

    public void saveCategoriesForUser(long id, LinkedHashMap<Category, Double> categories) {
        final DBCollection interestsCollection = db.getCollection(MongoCollections.INTERESTS.toString());
        final BasicDBObject objectForSave = new BasicDBObject("id", id);
        final String jsonStr = JsonSerializer.serializeCategories(categories);
        final Object parsedJson = JSON.parse(jsonStr);
        objectForSave.put("categories", parsedJson);
        interestsCollection.save(objectForSave);
    }

    public static void main(String[] args) {
        WikiTopicMapper wikiTopicMapper = new WikiTopicMapper(InterestsMapperParams.DEFAULT_MAPPER_PARAMS);
        Map<String, Double> doubleMap = ImmutableMap.<String, Double>builder().put("machinelearning", 26.0).build();
        Topic topic = new Topic(doubleMap);
        final LinkedHashMap<Category, Double> categoryDoubleLinkedHashMap = wikiTopicMapper.mapTopicsToInterests(ImmutableMap.<Topic, Double>builder().put(topic, 0.5).build());
        LocalMongoExtractor.getInstance().saveCategoriesForUser(123L, categoryDoubleLinkedHashMap);
    }
}
