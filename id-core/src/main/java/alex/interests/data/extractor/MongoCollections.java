package alex.interests.data.extractor;

public enum MongoCollections {
    TIMELINE, FRIENDS, FOLLOWERS, PROFILE, INTERESTS;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
