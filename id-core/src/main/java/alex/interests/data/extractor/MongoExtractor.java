package alex.interests.data.extractor;

import alex.interests.data.model.Profile;
import alex.interests.data.model.ReTweet;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.data.model.UserImpl.UserBuilder;
import alex.interests.utils.ConfigProperties;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.mongodb.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.concurrent.ThreadSafe;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static alex.interests.data.extractor.MongoCollections.*;

@ThreadSafe
public class MongoExtractor implements DataExtractor {
    private static final Logger log = LoggerFactory.getLogger(MongoExtractor.class);

    public static final String DB_NAME = "twitterIMDec";

    private static volatile MongoExtractor INSTANCE;

    protected DB db;
    protected MongoClient client;
    protected int tweetsCount;
    protected String tweetsLang;
    protected boolean isCacheEnabled;
    protected boolean checkUserExistence;

    private final LoadingCache<String, User> USER_CACHE = CacheBuilder.newBuilder()
            .maximumSize(10000)
            .build(new CacheLoader<String, User>() {
                @Override
                public User load(String key) throws Exception {
                    return getUserByIdInternal(key, true);
                }
            });

    private static final Pattern reTweetPattern = Pattern.compile("(RT|rt|Rt|rT)\\s(@\\S+):\\s(.+)");

    public static MongoExtractor getInstance() {
        MongoExtractor localInstance = INSTANCE;
        if (localInstance == null) {
            synchronized (MongoExtractor.class) {
                localInstance = INSTANCE;
                if (localInstance == null) {
                    INSTANCE = localInstance = new MongoExtractor();
                    INSTANCE.init();
                }
            }
        }
        return localInstance;
    }

    protected void init() {
        ConfigProperties CONFIG = ConfigProperties.getInstance();
        String host = CONFIG.getProperty("mongo.server");
        String port = CONFIG.getProperty("mongo.port");
        String cacheEnabled = CONFIG.getProperty("cache.enabled");
        isCacheEnabled = Boolean.valueOf(cacheEnabled);
        tweetsCount = Integer.parseInt(CONFIG.getProperty("tweets.count"));
        tweetsLang = CONFIG.getProperty("tweets.lang");
        client = new MongoClient(host, Integer.valueOf(port));
        db = client.getDB(getDbName());
        checkUserExistence = true;
    }

    public DB db() {
        return db;
    }

    protected String getDbName() {
        return DB_NAME;
    }

    protected String getUserIdKey() {
        return "userId";
    }

    protected Object getValueFromDB(DBObject dbObject, String key) {
        return dbObject.get(key);
    }

    protected DBObject getTimelineFields() {
        return MongoQueries.TWEETS_FIELDS;
    }

    @Deprecated
    public Collection<User> getUserWithTweets(int usersLimit) {
        if (isCacheEnabled && USER_CACHE.size() >= usersLimit) {
            return USER_CACHE.asMap().values().stream().limit(usersLimit).collect(Collectors.toSet());
        }
        ListMultimap<Long, Tweet> userId2Tweets = ArrayListMultimap.create();
        Map<Long, ListMultimap<String, ReTweet>> userId2ReTweets = new HashMap<>();
        extractTweets(usersLimit, userId2Tweets, userId2ReTweets);
        List<Long> userIds = new ArrayList<>(userId2Tweets.keySet());
        final Map<String, User> usersByIds = new HashMap<>(usersLimit);
        for (Long userId : userIds) {
            UserBuilder userBuilder = new UserBuilder(userId.toString())
                    .tweets(userId2Tweets.get(userId))
                    .retweetedUsers(userId2ReTweets.get(userId))
                    .dataExtractor(this);
            usersByIds.put(userId.toString(), userBuilder.build());
        }
        if (isCacheEnabled) {
            USER_CACHE.putAll(usersByIds);
        }
        return usersByIds.values();
    }

    @Deprecated
    public Collection<User> getUserWithTweetsAndReferences(int usersLimit) {
        if (isCacheEnabled && USER_CACHE.size() >= usersLimit) {
            return USER_CACHE.asMap().values().stream().limit(usersLimit).collect(Collectors.toSet());
        }
        final Map<String, User> usersByIds = new HashMap<>(usersLimit);
        ListMultimap<Long, Tweet> userId2Tweets = ArrayListMultimap.create();
        Map<Long, ListMultimap<String, ReTweet>> userId2ReTweets = new HashMap<>();
        extractTweets(usersLimit, userId2Tweets, userId2ReTweets);
        List<Long> userIds = new ArrayList<>(userId2Tweets.keySet());
        Map<Long, Collection<String>> followersByUserId = extractReferencesForUsers(userIds, FOLLOWERS.toString(), true).asMap();
        Map<Long, Collection<String>> friendsByUserId = extractReferencesForUsers(userIds, FRIENDS.toString(), true).asMap();
        for (Long userId : userIds) {
            UserBuilder userBuilder = new UserBuilder(userId.toString())
                    .tweets(userId2Tweets.get(userId))
                    .retweetedUsers(userId2ReTweets.get(userId))
                    .friends(friendsByUserId.get(userId))
                    .followers(followersByUserId.get(userId))
                    .dataExtractor(this);
            usersByIds.put(userId.toString(), userBuilder.build());
        }
        if (isCacheEnabled) {
            USER_CACHE.putAll(usersByIds);
        }
        log.trace("getUserWithTweetsAndReferences return {}", usersByIds);
        return usersByIds.values();
    }

    private ListMultimap<Long, String> extractReferencesForUsers(List<Long> userIds, String referenceType, boolean filterNotExisted) {
        final String userIdKey = getUserIdKey();
        DBObject queryReferences = BasicDBObjectBuilder.start()
                .push(userIdKey)
                .add("$in", userIds)
                .get();
        List<DBObject> referencesByUsers = db.getCollection(referenceType)
                .find(queryReferences)
                .toArray();
        ListMultimap<Long, String> userId2ReferencesIds = ArrayListMultimap.create();
        for (DBObject dbObject : referencesByUsers) {
            Long userId = (Long) dbObject.get(userIdKey);
            final BasicDBList referencesIds = (BasicDBList) getValueFromDB(dbObject, referenceType);
            final List<String> filteredReferencesIds = filterExistedReferences(referencesIds, filterNotExisted);
            userId2ReferencesIds.putAll(userId, filteredReferencesIds);
        }
        log.trace("extractReferencesForUsers {} return {} collection", userIds, userId2ReferencesIds.size());
        return userId2ReferencesIds;
    }

    private List<String> filterExistedReferences(BasicDBList referencesIds, boolean filterNotExisted) {
        if (!checkUserExistence || !filterNotExisted) {
            return referencesIds.parallelStream().map(String::valueOf).collect(Collectors.toList());
        }
        final String userIdKey = getUserIdKey();
        //in mongo db some users not existed - should filter it
        DBObject queryReferences = BasicDBObjectBuilder.start()
                .push(userIdKey)
                .add("$in", referencesIds)
                .get();
        DBObject fieldsToQuery = new BasicDBObject(userIdKey, 1);
        List<DBObject> existedReferences = db.getCollection(TIMELINE.toString()).find(queryReferences, fieldsToQuery).toArray();
        return existedReferences.parallelStream().map(dbO -> String.valueOf(dbO.get(userIdKey))).collect(Collectors.toList());
    }

    public List<String> filterUsersWithTweets(List<String> userIds) {
        final String userIdKey = getUserIdKey();
        //in mongo db some users not existed - should filter it
        final List<Long> userIdsLong = userIds.parallelStream().map(Long::valueOf).collect(Collectors.toList());
        DBObject queryReferences = BasicDBObjectBuilder.start()
                .push(userIdKey)
                .add("$in", userIdsLong)
                .get();
        DBObject fieldsToQuery = new BasicDBObject(userIdKey, 1);
        List<DBObject> existedReferences = db.getCollection(TIMELINE.toString()).find(queryReferences, fieldsToQuery).toArray();
        return existedReferences.parallelStream().map(dbO -> String.valueOf(dbO.get(userIdKey))).collect(Collectors.toList());
    }

    private void extractTweets(int usersLimit,
                               ListMultimap<Long, Tweet> userId2Tweets,
                               Map<Long, ListMultimap<String, ReTweet>> userId2ReTweets) {
        DBObject queryTweets = MongoQueries.getQueryForTweets(tweetsCount, tweetsLang);
        List<DBObject> tweetDbObjects = db.getCollection(TIMELINE.toString())
                .find(queryTweets, MongoQueries.TWEETS_FIELDS)
                .limit(usersLimit)
                .toArray();
        log.trace("Querying timelines finished. Return {} objects", tweetDbObjects.size());
        if (!tweetDbObjects.isEmpty()) {
            processTimelines(tweetDbObjects, userId2Tweets, userId2ReTweets);
        }
    }

    private void extractTweetsForUser(Long userId,
                                      List<Tweet> tweets,
                                      ListMultimap<String, ReTweet> reTweets) {
        DBObject queryTweets = new BasicDBObject(getUserIdKey(), userId);
        List<DBObject> tweetDbObjects = db.getCollection(TIMELINE.toString())
                .find(queryTweets, getTimelineFields())
                .toArray();
        ListMultimap<Long, Tweet> userId2Tweets = tweets == null ? null : ArrayListMultimap.create();
        Map<Long, ListMultimap<String, ReTweet>> userId2ReTweets = reTweets == null ? null : new HashMap<>();
        if (!tweetDbObjects.isEmpty()) {
            processTimelines(tweetDbObjects, userId2Tweets, userId2ReTweets);
            if (tweets != null && userId2Tweets.containsKey(userId)) {
                tweets.addAll(userId2Tweets.get(userId));
            }
        }
        if (userId2ReTweets != null && userId2ReTweets.containsKey(userId)) {
            reTweets.putAll(userId2ReTweets.get(userId));
        }
    }

    private void processTimelines(List<DBObject> tweetDbObjects,
                                  ListMultimap<Long, Tweet> userId2Tweets,
                                  Map<Long, ListMultimap<String, ReTweet>> userId2ReTweets) {
        boolean isNeedRetweets = userId2ReTweets != null;
        boolean isNeedTweets = userId2Tweets != null;
        log.trace("processTimelines started for {} objects", tweetDbObjects.size());
        for (DBObject dbObject : tweetDbObjects) {
            Long userId = new Long(String.valueOf(dbObject.get(getUserIdKey())));
            List<Tweet> tweets = isNeedTweets ? new ArrayList<>() : null;
            ListMultimap<String, ReTweet> reTweets = isNeedRetweets ? ArrayListMultimap.create() : null;
            BasicDBList timelines = (BasicDBList) getValueFromDB(dbObject, "timeline");
            for (Object timeline : timelines) {
                BasicDBObject timelineDB = (BasicDBObject) timeline;
                if (!StringUtils.equals(timelineDB.getString("lang"), tweetsLang)) {
                    continue;
                }
                Long tweetId = timelineDB.getLong("id");
                String text = timelineDB.getString("text");
                String date = timelineDB.getString("created_at");
                BasicDBObject entities = (BasicDBObject) timelineDB.get("entities");
                List<String> hashtagsStrList = Collections.emptyList();
                if (entities != null) {
                    BasicDBList hashtags = (BasicDBList) entities.get("hashtags");
                    hashtagsStrList = new ArrayList<>(hashtags.size());
                    for (Object hashtagObj : hashtags) {
                        hashtagsStrList.add(((BasicDBObject) hashtagObj).getString("text"));
                    }
                }
                BasicDBObject retweetedStatus = (BasicDBObject) timelineDB.get("retweeted_status");
                if (retweetedStatus != null && reTweets != null) {
                    Long retweetedTweetId = retweetedStatus.getLong("id");
                    Long retweetedUserId = ((BasicDBObject) retweetedStatus.get("user")).getLong("id");
                    if (!checkUserExistsInDB(retweetedUserId.toString()))
                        continue;
                    String retweetedText = retweetedStatus.getString("text");
                    ReTweet reTweet = new ReTweet(tweetId, retweetedText, userId, hashtagsStrList, retweetedUserId, retweetedTweetId);
                    reTweets.put(retweetedUserId.toString(), reTweet);
                    continue;
                }
                //note! in test db exists some tweets that starts with RT, but not considered as retweets
                Matcher matcher = reTweetPattern.matcher(text);
                if (matcher.matches() && reTweets != null) {
                    //just continue because user @UserName is not exists in DB
                    //String retweetedUserId = matcher.group(2); //@UserName
                    //String retweetedText = matcher.group(3);
                    //ReTweet reTweet = new ReTweet(tweetId.toString(), retweetedText, userId.toString(), hashtagsStrList, retweetedUserId, null);
                    //reTweets.put(retweetedUserId, reTweet);
                    continue;
                }
                if (isNeedTweets) {
                    Tweet tweet = new Tweet(tweetId, text, userId, hashtagsStrList, date);
                    tweets.add(tweet);
                }
            }
            if (isNeedRetweets) {
                userId2ReTweets.put(userId, reTweets);
            }
            if (isNeedTweets) {
                userId2Tweets.putAll(userId, tweets);
            }
        }
        log.trace("processTimelines finished for {} objects", tweetDbObjects.size());
    }

    private User getUserByIdInternal(String userId, boolean withReferences) throws UserNotFoundException {
        checkUserExistsInDBExc(userId);
        log.trace("getUserByIdInternal start load user by id {} and referencesLoaded={}", userId, withReferences);
        Long userIdLong = Long.valueOf(userId);
        List<Long> userIdsSingleton = Collections.singletonList(userIdLong);
        List<Tweet> tweets = new ArrayList<>();
        ListMultimap<String, ReTweet> reTweets = ArrayListMultimap.create();
        extractTweetsForUser(userIdLong, tweets, reTweets);
        UserBuilder userBuilder = new UserBuilder(userId)
                .tweets(tweets)
                .retweetedUsers(reTweets)
                .dataExtractor(MongoExtractor.getInstance());
        if (withReferences) {
            List<String> friends = extractReferencesForUsers(userIdsSingleton, FRIENDS.toString(), true).get(userIdLong);
            List<String> followers = extractReferencesForUsers(userIdsSingleton, FOLLOWERS.toString(), true).get(userIdLong);
            userBuilder.friends(friends).followers(followers);
        }
        User user = userBuilder.build();
        log.trace("getUserByIdInternal return {}", user);
        return user;
    }

    private void checkUserExistsInDBExc(String userId) throws UserNotFoundException {
        if (checkUserExistence && !checkUserExistsInDB(userId)) {
            throw new UserNotFoundException(userId);
        }
    }

    private boolean checkUserExistsInDB(String userId) {
        if (!checkUserExistence) {
            return true;
        }
        try {
            final String userIdKey = getUserIdKey();
            DBObject queryUser = new BasicDBObject(userIdKey, Long.valueOf(userId));
            DBObject fieldsToQuery = new BasicDBObject(userIdKey, 1);
            DBObject findUser = db.getCollection(TIMELINE.toString()).findOne(queryUser, fieldsToQuery);
            return findUser != null;
        } catch (NumberFormatException e) {
            log.trace("User with id {} cannot be find in mongo db", userId);
            return false;
        }
    }

    @Override
    public User getUserById(String id) {
        try {
            return isCacheEnabled ? USER_CACHE.get(id) : getUserByIdInternal(id, true);
        } catch (ExecutionException | UserNotFoundException e) {
            log.trace("Exception [{}] occurred during load user with id {}", e.getCause().getMessage(), id);
            return null;
        }
    }

    @Override
    public User getUserByIdWithoutReferences(String id) {
        User fromCache = isCacheEnabled ? USER_CACHE.getIfPresent(id) : null;
        if (fromCache != null) {
            return fromCache;
        } else {
            User loadedUser = null;
            try {
                loadedUser = getUserByIdInternal(id, false);
                if (isCacheEnabled) {
                    USER_CACHE.put(id, loadedUser);
                }
            } catch (UserNotFoundException e) {
                log.trace("Exception [{}] occurred during load user with id {}", e.getMessage(), id);
            }
            return loadedUser;
        }
    }

    /**
     * no filtering non existed
     * @param userId
     * @return
     */
    @Override
    public Collection<String> loadUserFriends(String userId) {
        final Long id = Long.valueOf(userId);
        Map<Long, Collection<String>> friendsByUserId = extractReferencesForUsers(Collections.singletonList(id), FRIENDS.toString(), false).asMap();
        final Collection<String> friends = friendsByUserId.get(id);
        return CollectionUtils.isNotEmpty(friends) ? friends : Collections.emptySet();
    }

    /**
     * no filtering non existed
     * @param userId
     * @return
     */
    @Override
    public Collection<String> loadUserFollowers(String userId) {
        final Long id = Long.valueOf(userId);
        Map<Long, Collection<String>> followersByUserId = extractReferencesForUsers(Collections.singletonList(id), FOLLOWERS.toString(), false).asMap();
        final Collection<String> followers = followersByUserId.get(id);
        return CollectionUtils.isNotEmpty(followers) ? followers : Collections.emptySet();
    }

    //@Override
    //public Map<String, Integer> loadUserFollowersCountBulk(Collection<String> userIds) {
    //
    //}

    @Override
    public List<Tweet> loadUserTweets(String id) {
        List<Tweet> tweetList = new ArrayList<>();
        extractTweetsForUser(Long.valueOf(id), tweetList, null);
        return tweetList;
    }

    @Override
    public ListMultimap<String, ReTweet> loadRetweetedUsers(String userId) {
        ListMultimap<String, ReTweet> reTweets = ArrayListMultimap.create();
        extractTweetsForUser(Long.valueOf(userId), null, reTweets);
        return reTweets;
    }

    @Override
    protected void finalize() throws Throwable {
        client.close();
        super.finalize();
    }

    @Override
    public Profile loadUserProfile(String userId) {
        final DBObject findedUser = db.getCollection(PROFILE.toString()).findOne(new BasicDBObject(getUserIdKey(), Long.valueOf(userId)));
        final DBObject userProfile = (DBObject) getValueFromDB(findedUser, PROFILE.toString());
        final String name = (String) userProfile.get("name");
        final String screenName = (String) userProfile.get("screen_name");
        final String description = (String) userProfile.get("description");
        return new Profile(name, screenName, description);
    }

    /**
     * Not implemented yet
     */

    @Override
    public Collection<User> getUsersByIds(Collection<String> ids) {
        throw new UnsupportedOperationException();
    }

}