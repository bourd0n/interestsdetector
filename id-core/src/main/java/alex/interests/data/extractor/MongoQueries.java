package alex.interests.data.extractor;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;

public class MongoQueries {

    public static final DBObject TWEETS_FIELDS = BasicDBObjectBuilder.start()
            .add("userId", 1)
            .add("timeline", 1)
            .add("timeline.text", 1)
            .add("timeline.id", 1)
            .add("timeline.retweeted_status.text", 1)
            .add("timeline.retweeted_status.user", 1)
            .add("timeline.retweeted_status.id", 1)
            .add("timeline.entities.hashtags.text", 1)
            .add("timeline.lang", 1)
            .add("timeline.created_at", 1)
            .get();

    public static final DBObject TWEETS_FIELDS_LOCAL = BasicDBObjectBuilder.start()
            .add("id", 1)
            .add("value.object", 1)
            .add("value.object.text", 1)
            .add("value.object.id", 1)
            .add("value.object.retweeted_status.text", 1)
            .add("value.object.retweeted_status.user", 1)
            .add("value.object.retweeted_status.id", 1)
            .add("value.object.entities.hashtags.text", 1)
            .add("value.object.lang", 1)
            .add("value.object.created_at", 1)
            .get();

    public static DBObject getQueryForTweets(int tweetCount, String tweetLang) {
        return BasicDBObjectBuilder.start()
                .add("timeline", new BasicDBObject("$exists", true))
                .add("$where", "function () {return this.timeline.filter(function (elem) {return (elem.lang == \"" + tweetLang + "\");}).length >= " + tweetCount + ";}")
                .get();
    }

}
