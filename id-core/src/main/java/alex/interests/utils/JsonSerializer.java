package alex.interests.utils;

import alex.interests.data.model.Interest;
import alex.interests.data.model.Topic;
import alex.interests.process.mapping.entities.Category;
import alex.interests.process.mapping.entities.CategoryWeight;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapLikeType;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class JsonSerializer {

    private static final ObjectMapper objectMapper = new ObjectMapper()
            .configure(SerializationFeature.INDENT_OUTPUT, true)
            .configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true)
            .setSerializationInclusion(JsonInclude.Include.NON_NULL);

    private static final MapLikeType mapTopicType = objectMapper
            .getTypeFactory()
            .constructMapLikeType(LinkedHashMap.class, Topic.class, Double.class);

    private static final CollectionType caategoryWeightType = objectMapper
            .getTypeFactory()
            .constructCollectionType(List.class, CategoryWeight.class);

    private static final CollectionType listInterestsType = objectMapper
            .getTypeFactory()
            .constructCollectionType(List.class, Interest.class);

    public static LinkedHashMap<Topic, Double> readTopics(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            try {
                LinkedHashMap<Topic, Double> calculatedTopicsRead = objectMapper.readValue(file, mapTopicType);
                return calculatedTopicsRead;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    public static void write(String fileName, Object value) {
        File file = new File(fileName);
        try {
            file.createNewFile();
            objectMapper.writeValue(file, value);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    @Deprecated
    //not worked
    public static LinkedHashMap<Category, Double> readCategories(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            try {
                LinkedHashMap<Category, Double> categories = objectMapper.readValue(file, caategoryWeightType);
                return categories;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return null;
    }

    public static LinkedHashMap<Category, Double> deserializeCategories(Object jsonCategories) {
        try {
            List<CategoryWeight> categoriesWeight = objectMapper.readValue(String.valueOf(jsonCategories), caategoryWeightType);
            LinkedHashMap<Category, Double> categories = new LinkedHashMap<>();
            categoriesWeight.stream().forEachOrdered(cw -> categories.put(cw.getCategory(), cw.getWeight()));
            return categories;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Interest> deserializeInterests(String jsonInterests) {
        try {
            return objectMapper.readValue(jsonInterests, listInterestsType);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String serialize(Object value) {
        try {
            return objectMapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String serializeCategories(Map<Category, Double> categories) {
        try {
            final List<CategoryWeight> categoryWeights = categories.entrySet().stream().map(entry -> new CategoryWeight(entry.getKey(), entry.getValue())).collect(Collectors.toList());
            return objectMapper.writeValueAsString(categoryWeights);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        Category category = new Category();
        category.setId(123);
        category.setTitle("123");
        category.getTerms().add("123");
        category.addOwnWeight(123);
        System.out.println(JsonSerializer.serialize(category));

        System.out.println();

        LinkedHashMap<Category, Double> categoryDoubleLinkedHashMap = new LinkedHashMap<>();
        categoryDoubleLinkedHashMap.put(category, 1123.0);
        final String s = JsonSerializer.serializeCategories(categoryDoubleLinkedHashMap);
        System.out.println(s);
        final LinkedHashMap<Category, Double> categoryDoubleLinkedHashMap1 = JsonSerializer.deserializeCategories(s);
    }
}
