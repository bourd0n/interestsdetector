package alex.interests.utils;

import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class WordMap {

    private final Map<String, String> wordMap;

    public WordMap(String pathToDict) {
        this(pathToDict, null);
    }

    public WordMap(String pathToDict, String delimeter) {
        wordMap = new HashMap<>();
        try {
            init(pathToDict, delimeter);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private void init(String pathToDict, String delimeter) throws FileNotFoundException {
        Scanner scanner = new Scanner(getClass().getResourceAsStream(pathToDict));

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (!line.isEmpty()) {
                if (StringUtils.isNotEmpty(delimeter)) {
                    String[] s = line.split(delimeter);
                    String key = s[0].toLowerCase().trim();
                    final String value = s[1].trim();
                    wordMap.put(key, value);
                } else {
                    String word = line.toLowerCase().trim();
                    wordMap.put(word, word);
                }
            }
        }
    }

    public String find(String key) {
        return wordMap.get(key);
    }

    public boolean containsKey(String key) {
        return wordMap.containsKey(key);
    }
}
