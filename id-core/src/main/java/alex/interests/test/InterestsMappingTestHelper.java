package alex.interests.test;

import alex.interests.data.extractor.LocalMongoExtractor;
import alex.interests.data.model.Interest;
import alex.interests.process.mapping.entities.Category;
import alex.interests.utils.JsonSerializer;
import com.google.common.collect.Lists;
import com.mongodb.*;
import com.mongodb.util.JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class InterestsMappingTestHelper {

    public static void saveSimpleInterests(String userId, Method method, List<Interest> interests) {
        final DB db = LocalMongoExtractor.getInstance().db();
        final DBCollection interestsTestCol = db.getCollection("interests_for_tests");
        final Long id = Long.valueOf(userId);
        final BasicDBObject queryById = new BasicDBObject("id", id);
        DBObject userTestInterestsObject = interestsTestCol.findOne(queryById);
        if (userTestInterestsObject == null) {
            userTestInterestsObject = queryById;
        }
        final String jsonStr = JsonSerializer.serialize(interests);
        final Object parsedJson = JSON.parse(jsonStr);
        userTestInterestsObject.put(method.name(), parsedJson);
        interestsTestCol.save(userTestInterestsObject);
    }

    public static List<Interest> loadSimpleInterests(String userId, Method method) {
        final DB db = LocalMongoExtractor.getInstance().db();
        final DBCollection interestsTestCol = db.getCollection("interests_for_tests");
        final Long id = Long.valueOf(userId);
        final BasicDBObject queryById = new BasicDBObject("id", id);
        DBObject userTestInterestsObject = interestsTestCol.findOne(queryById);
        if (userTestInterestsObject == null) {
            return null;
        }
        final Object methodInterests = userTestInterestsObject.get(method.name());
        return methodInterests != null ? JsonSerializer.deserializeInterests(methodInterests.toString()) : null;
    }

    public static List<Interest> loadAllSimpleInterests(String userId) {
        final DB db = LocalMongoExtractor.getInstance().db();
        final DBCollection interestsTestCol = db.getCollection("interests_for_tests");
        final Long id = Long.valueOf(userId);
        final BasicDBObject queryById = new BasicDBObject("id", id);
        DBObject userTestInterestsObject = interestsTestCol.findOne(queryById);
        if (userTestInterestsObject == null) {
            return null;
        }
        List<Interest> interests = new ArrayList<>();
        for (Method method : Method.values()) {
            final Object methodInterests = userTestInterestsObject.get(method.name());
            if (methodInterests != null) {
                interests.addAll(JsonSerializer.deserializeInterests(methodInterests.toString()));
            }
        }
        return interests;
    }

    public static List<Interest> categoriesToInterests(Map<Category, Double> categories) {
        return categories
                .entrySet()
                .parallelStream()
                .map(entry -> new Interest(entry.getKey().getId(), entry.getValue(), entry.getKey().getTitle()))
                .collect(Collectors.toList());
    }

    public static void saveRelevantInterests(String userId, List<Integer> interestsIds){
        final DB db = LocalMongoExtractor.getInstance().db();
        final DBCollection relevantInterestsCol = db.getCollection("relevant_interests");
        final Long id = Long.valueOf(userId);
        final BasicDBObject queryById = new BasicDBObject("id", id);
        DBObject userTestInterestsObject = relevantInterestsCol.findOne(queryById);
        if (userTestInterestsObject == null) {
            userTestInterestsObject = queryById;
            List<List<Integer>> relevantInterests = new ArrayList<>();
            relevantInterests.add(interestsIds);
            queryById.put("rel_interests", relevantInterests);
        } else {
            final BasicDBList rel_interests = (BasicDBList) userTestInterestsObject.get("rel_interests");
            rel_interests.add(interestsIds);
        }
        relevantInterestsCol.save(userTestInterestsObject);
    }

}
