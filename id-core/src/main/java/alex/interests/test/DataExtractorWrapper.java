package alex.interests.test;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.data.model.Profile;
import alex.interests.data.model.ReTweet;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import com.google.common.collect.ListMultimap;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class DataExtractorWrapper implements DataExtractor {
    private final DataExtractor wrappedDataExtractor;
    private final AlgorithmParams algorithmParams;

    public DataExtractorWrapper(DataExtractor wrappedDataExtractor, AlgorithmParams algorithmParams) {
        this.wrappedDataExtractor = wrappedDataExtractor;
        this.algorithmParams = algorithmParams;
    }

    //for tests
    @Override
    public Collection<User> getUserWithTweets(int count) {
        return wrappedDataExtractor.getUserWithTweets(count)
                .stream()
                .map(user -> new UserWrapper(user, algorithmParams))
                .collect(Collectors.toList());
    }

    @Override
    public Collection<User> getUserWithTweetsAndReferences(int count) {
        return wrappedDataExtractor.getUserWithTweetsAndReferences(count)
                .stream()
                .map(user -> new UserWrapper(user, algorithmParams))
                .collect(Collectors.toList());
    }

    @Override
    public User getUserById(String id) {
        return new UserWrapper(wrappedDataExtractor.getUserById(id), algorithmParams);
    }

    //other simple delegation
    @Override
    public ListMultimap<String, ReTweet> loadRetweetedUsers(String userId) {
        return wrappedDataExtractor.loadRetweetedUsers(userId);
    }

    @Override
    public List<Tweet> loadUserTweets(String userId) {
        return wrappedDataExtractor.loadUserTweets(userId);
    }

    @Override
    public Collection<String> loadUserFollowers(String userId) {
        return wrappedDataExtractor.loadUserFollowers(userId);
    }

    @Override
    public List<String> filterUsersWithTweets(List<String> userIds) {
        return wrappedDataExtractor.filterUsersWithTweets(userIds);
    }

    @Override
    public Collection<String> loadUserFriends(String userId) {
        return wrappedDataExtractor.loadUserFriends(userId);
    }

    @Override
    public Profile loadUserProfile(String userId) {
        return wrappedDataExtractor.loadUserProfile(userId);
    }

    @Override
    public User getUserByIdWithoutReferences(String id) {
        return wrappedDataExtractor.getUserByIdWithoutReferences(id);
    }

    @Override
    public Collection<User> getUsersByIds(Collection<String> ids) {
        return wrappedDataExtractor.getUsersByIds(ids);
    }
}
