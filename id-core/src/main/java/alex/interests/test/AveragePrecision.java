package alex.interests.test;

import alex.interests.data.extractor.LocalMongoExtractor;
import alex.interests.data.extractor.twitter.TwitterUtil;
import alex.interests.data.model.Interest;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Sets;
import com.google.common.math.DoubleMath;
import com.mongodb.*;

import java.util.*;
import java.util.stream.Collectors;

public class AveragePrecision {

    private final String userId;
    private final int level;

    public AveragePrecision(String userId, int level) {
        this.level = level;
        this.userId = userId;
    }

    public static Map<Method, Double> calculateMeanAvgPrecisionForAllUsers(int level) {
        ListMultimap<Method, Double> meanAvgPrecision = ArrayListMultimap.create();
        final Map<Long, String> loadedUsers = TwitterUtil.getLoadedUsers();
        for (Long userId : loadedUsers.keySet()) {
            final AveragePrecision averagePrecision = new AveragePrecision(userId.toString(), level);
            final Map<Method, Double> methodDoubleMap = averagePrecision.calculateMeanAvgPrecision();
            if (methodDoubleMap != null) {
                methodDoubleMap.forEach(meanAvgPrecision::put);
            }
        }

        final Map<Method, Double> result = new HashMap<>();
        for (Method method : Method.values()) {
            final List<Double> avgPrecisions = meanAvgPrecision.get(method);
            final double meanAvgPr = DoubleMath.mean(avgPrecisions);
            result.put(method, meanAvgPr);
        }
        return result;
    }

    public Map<Method, Double> calculateMeanAvgPrecision() {
        final DB db = LocalMongoExtractor.getInstance().db();
        final DBCollection relevantInterestsCol = db.getCollection("relevant_interests");
        final Long id = Long.valueOf(userId);
        final BasicDBObject queryById = new BasicDBObject("id", id);
        DBObject userWithRelevantInterests = relevantInterestsCol.findOne(queryById);
        final BasicDBList relInterests = (BasicDBList) userWithRelevantInterests.get("rel_interests");
        final ListMultimap<Method, Double> methods2ListAP = ArrayListMultimap.create();
        for (Object relInterest : relInterests) {
            BasicDBList singleRelInterests = (BasicDBList) relInterest;
            final Map<Method, Double> avgPrecisionForMethods = calculateAvgPrecision(singleRelInterests);
            if (avgPrecisionForMethods == null) {
                return null;
            }
            avgPrecisionForMethods.forEach(methods2ListAP::put);
        }

        final Map<Method, Double> result = new HashMap<>();
        for (Method method : Method.values()) {
            final List<Double> avgPrecisions = methods2ListAP.get(method);
            final double meanAvgPrecision = DoubleMath.mean(avgPrecisions);
            result.put(method, meanAvgPrecision);
        }
        return result;
    }

    private Map<Method, Double> calculateAvgPrecision(BasicDBList singleRelInterests) {
        Map<Method, Double> result = new HashMap<>();
        for (Method method : Method.values()) {
            final Double avgPrecision = calculateAvgPrecisionForMethod(method, singleRelInterests);
            if (avgPrecision == null) {
                return null;
            }
            result.put(method, avgPrecision);
        }
        return result;
    }

    private Double calculateAvgPrecisionForMethod(Method method, BasicDBList singleRelInterests) {
        final List<Interest> interests = InterestsMappingTestHelper.loadSimpleInterests(userId, method);
        if (interests == null) {
            return null;
        }
        final Set<Integer> relInterests = singleRelInterests
                .parallelStream()
                .map(o -> (Integer) o)
                .collect(Collectors.toSet());
        final List<Integer> retrievedInterestsIds = interests.parallelStream()
                .sorted(Comparator.<Interest>reverseOrder())
                .map(Interest::getId)
                .limit(level)
                .collect(Collectors.toList());
        double avgPrecision = 0.0;
        final Set<Integer> currentRetrieved = new HashSet<>();
        for (Integer retrievedInterestsId : retrievedInterestsIds) {
            currentRetrieved.add(retrievedInterestsId);
            //if current is relevant
            if (relInterests.contains(retrievedInterestsId)) {
                final int intersectionSize = Sets.intersection(currentRetrieved, relInterests).size();
                double precision = (double) intersectionSize / currentRetrieved.size();
                avgPrecision += precision / level;
            }
        }
        return avgPrecision;
    }
}
