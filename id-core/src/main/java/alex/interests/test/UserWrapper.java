package alex.interests.test;

import alex.interests.data.model.Profile;
import alex.interests.data.model.ReTweet;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.topic.TopicModel;
import com.google.common.collect.ListMultimap;

import java.util.List;
import java.util.Set;

public class UserWrapper implements User {
    private final User wrappedUser;
    private final AlgorithmParams algorithmParams;

    public UserWrapper(User wrappedUser, AlgorithmParams algorithmParams) {
        this.wrappedUser = wrappedUser;
        this.algorithmParams = algorithmParams;
    }

    //need to override for tests - not all tweets is needed
    @Override
    public List<Tweet> getTweets(boolean load) {
        final List<Tweet> wrappedUserTweets = wrappedUser.getTweets(load);
        return wrappedUserTweets.subList(0, (int) Math.round(wrappedUserTweets.size() * algorithmParams.getTrainTweetsRatio()));
    }

    public List<Tweet> getTweetsForTest(boolean load) {
        final List<Tweet> wrappedUserTweets = wrappedUser.getTweets(load);
        final int roundSize = (int) Math.round(wrappedUserTweets.size() * algorithmParams.getTrainTweetsRatio());
        return wrappedUserTweets.subList(roundSize + 1, wrappedUserTweets.size());
    }

    public User getWrappedUser() {
        return wrappedUser;
    }

    //all other is simple delegation
    @Override
    public Set<String> getFollowers(boolean load) {
        return wrappedUser.getFollowers(load);
    }

    @Override
    public Set<String> getFriends(boolean load) {
        return wrappedUser.getFriends(load);
    }

    @Override
    public String getId() {
        return wrappedUser.getId();
    }

    @Override
    public TopicModel getOwnTopicModel() {
        return wrappedUser.getOwnTopicModel();
    }

    @Override
    public Profile getProfile(boolean load) {
        return wrappedUser.getProfile(load);
    }

    @Override
    public ListMultimap<String, ReTweet> getRetweetedUsers(boolean load) {
        return wrappedUser.getRetweetedUsers(load);
    }

    @Override
    public boolean isTopicsCalculated() {
        return wrappedUser.isTopicsCalculated();
    }

    @Override
    public void setOwnTopicModel(TopicModel ownTopicModel) {
        wrappedUser.setOwnTopicModel(ownTopicModel);
    }

    @Override
    public String toString() {
        return wrappedUser.toString();
    }
}
