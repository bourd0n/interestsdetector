package alex.interests.test;

import alex.interests.data.model.Topic;
import alex.interests.data.model.Tweet;
import alex.interests.process.topic.lda.LDAHelper;
import cc.mallet.pipe.iterator.ArrayIterator;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import cc.mallet.types.Token;
import cc.mallet.types.TokenSequence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class PerplexityUtils {
    private static final Logger log = LoggerFactory.getLogger(PerplexityUtils.class);

    public static double calculatePerplexity(List<Tweet> tweets, Map<Topic, Double> topicsDistribution) {
        InstanceList instanceList = LDAHelper.thruTestPipe(new ArrayIterator(tweets));
        log.debug("Start calculate perplexity for {} test tweets", tweets.size());
        int allTweetsTokenSize = 0;
        double sumByInstances = 0.0;
        for (Instance instance : instanceList) {
            final TokenSequence tokenSequence = (TokenSequence) instance.getData();
            allTweetsTokenSize += tokenSequence.size();
            double sumOfInstanceByTokens = 0.0;
            for (Token token : tokenSequence) {
                double sumForTokenByTopics = 0.0;
                for (Map.Entry<Topic, Double> topicEntry : topicsDistribution.entrySet()) {
                    final Double topicWeight = topicEntry.getValue();
                    final Topic topic = topicEntry.getKey();
                    final Map<String, Double> termsDistribution = topic.getTermsDistribution();
                    for (Map.Entry<String, Double> termEntry : termsDistribution.entrySet()) {
                        if (termEntry.getKey().equals(token.getText())) {
                            sumForTokenByTopics += termEntry.getValue() * topicWeight;
                        }
                    }
                }
                //is ok?
                sumOfInstanceByTokens += Math.log(sumForTokenByTopics != 0 ? sumForTokenByTopics : 0.00001);
            }
            sumByInstances += sumOfInstanceByTokens;
        }
        double perplexityArg = -sumByInstances / allTweetsTokenSize;
        double perplexity = Math.exp(perplexityArg);
        log.info("Perplexity for {} test tweets = {} ", tweets.size(), perplexity);
        return perplexity;
    }
}
