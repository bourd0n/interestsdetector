package alex.interests.test.weight;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.weight.FollowerWeightsCalculator;
import alex.interests.process.weight.LinkWeightCalculator;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MockFollowerWeightsCalculator extends FollowerWeightsCalculator implements LinkWeightCalculator {
    private static final Logger log = LoggerFactory.getLogger(MockFriendWeightsCalculator.class);

    private final Map<String, Double> outLinkWeights;
    private final Map<String, Double> inLinkWeights;

    public MockFollowerWeightsCalculator(DataExtractor dataExtractor, AlgorithmParams algorithmParams, User user) {
        super(dataExtractor, algorithmParams);
        outLinkWeights = super.calculateOutLinkWeights(user);
        inLinkWeights = super.calculateInLinkWeights(user);
    }

    //current implementation assume that weight can be 1.0 or 0.0 (no link)
    public Map<String, Double> calculateOutLinkWeights(User sourceUser) {
        return outLinkWeights;
    }

    //current implementation assume that weight can be 1.0 or 0.0 (no link)
    public Map<String, Double> calculateInLinkWeights(User sourceUser) {
        return inLinkWeights;
    }
}
