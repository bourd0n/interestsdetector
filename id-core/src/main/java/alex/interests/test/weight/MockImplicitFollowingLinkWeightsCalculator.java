package alex.interests.test.weight;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.weight.ImplicitFollowingLinkWeightsCalculator;
import alex.interests.process.weight.LinkWeightCalculator;
import com.google.common.base.MoreObjects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class MockImplicitFollowingLinkWeightsCalculator extends ImplicitFollowingLinkWeightsCalculator implements LinkWeightCalculator {
    private static final Logger log = LoggerFactory.getLogger(MockFriendWeightsCalculator.class);

    private final Map<String, Double> outLinkWeights;
    private final Map<String, Double> inLinkWeights;

    public MockImplicitFollowingLinkWeightsCalculator(DataExtractor dataExtractor, AlgorithmParams algorithmParams, User user) {
        super(dataExtractor, algorithmParams);
        outLinkWeights = super.calculateOutLinkWeights(user);
        inLinkWeights = super.calculateInLinkWeights(user);
    }

    //current implementation assume that weight can be 1.0 or 0.0 (no link)
    public Map<String, Double> calculateOutLinkWeights(User sourceUser) {
        return outLinkWeights;
    }

    //current implementation assume that weight can be 1.0 or 0.0 (no link)
    public Map<String, Double> calculateInLinkWeights(User sourceUser) {
        return inLinkWeights;
    }
}
