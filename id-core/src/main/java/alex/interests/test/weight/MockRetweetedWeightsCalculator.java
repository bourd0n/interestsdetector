package alex.interests.test.weight;

import alex.interests.data.extractor.DataExtractor;
import alex.interests.data.model.ReTweet;
import alex.interests.data.model.User;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.weight.LinkWeightCalculator;
import alex.interests.process.weight.RetweetedWeightsCalculator;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static alex.interests.process.weight.RetweetingWeightsCalculator.findRetweetingUsersWithWeightInLocalUserNet;

public class MockRetweetedWeightsCalculator extends RetweetedWeightsCalculator implements LinkWeightCalculator {
    private static final Logger log = LoggerFactory.getLogger(MockFriendWeightsCalculator.class);

    private final Map<String, Double> outLinkWeights;
    private final Map<String, Double> inLinkWeights;

    public MockRetweetedWeightsCalculator(DataExtractor dataExtractor, AlgorithmParams algorithmParams, User user) {
        super(dataExtractor, algorithmParams);
        outLinkWeights = super.calculateOutLinkWeights(user);
        //not supported for retweeted
        inLinkWeights = null;
    }

    //current implementation assume that weight can be 1.0 or 0.0 (no link)
    public Map<String, Double> calculateOutLinkWeights(User sourceUser) {
        return outLinkWeights;
    }

    //current implementation assume that weight can be 1.0 or 0.0 (no link)
    public Map<String, Double> calculateInLinkWeights(User sourceUser) {
        throw new UnsupportedOperationException("calculateInLinkWeights not supported");
    }
}
