package alex.interests.test;

import alex.interests.data.extractor.LocalMongoExtractor;
import alex.interests.data.model.Interest;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.topic.lda.LDAHelper;
import cc.mallet.types.Instance;
import cc.mallet.types.Token;
import cc.mallet.types.TokenSequence;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Iterables;
import org.apache.commons.collections.CollectionUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.languagetool.JLanguageTool;
import org.languagetool.MultiThreadedJLanguageTool;
import org.languagetool.language.AmericanEnglish;
import org.languagetool.language.English;
import org.languagetool.rules.RuleMatch;
import org.languagetool.rules.spelling.SpellingCheckRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class BaselineMethod {

    private static final Logger log = LoggerFactory.getLogger(BaselineMethod.class);

    private static final CloseableHttpClient httpClient = HttpClients.createDefault();
    private static final English english = new AmericanEnglish();

    public List<Interest> getInterestsForUser(User user) {
        final LinkedHashMap<String, Double> topWords = getTopWords(user);
        final List<Interest> interests = mapToCategories(topWords);
        return interests;
    }

    public LinkedHashMap<String, Double> getTopWords(User user) {
        final List<Tweet> tweets = user.getTweets(true);
        final Instance instance = LDAHelper.allTweetsToOneInstanceTest(tweets);
        final TokenSequence data = (TokenSequence) instance.getData();
        final int allTermsSize = data.size();
        LinkedHashMap<String, Double> termsWithTF = new LinkedHashMap<>();
        final List<Map.Entry<String, Long>> termsByWeight = data.parallelStream()
                .collect(Collectors.groupingBy(Token::getText, Collectors.counting()))
                .entrySet()
                .parallelStream()
                .sorted(Comparator.comparingLong(e -> -e.getValue()))
                .limit(100)
                .collect(Collectors.toList());
        termsByWeight.parallelStream()
                .forEachOrdered(e -> termsWithTF.put(e.getKey(), ((double) e.getValue()) / allTermsSize));
        return termsWithTF;
    }

    public List<Interest> mapToCategories(Map<String, Double> terms) {
        Map<String, Double> interestsWeights = new HashMap<>();
        for (Map.Entry<String, Double> termEntry : terms.entrySet()) {
            String term = termEntry.getKey();
            final Double termWeight = termEntry.getValue();

            List<String> categories = getCategories(term);

            if (CollectionUtils.isEmpty(categories)) {
                JLanguageTool langTool = new MultiThreadedJLanguageTool(english);
                try {
                    List<RuleMatch> matches = langTool.check(term);
                    RuleMatch ruleMatch = matches.parallelStream()
                            .filter(rm -> rm.getRule() instanceof SpellingCheckRule).findFirst().orElse(null);
                    if (ruleMatch != null) {
                        final List<String> suggestedReplacements = ruleMatch.getSuggestedReplacements();
                        String spellCheckedReplacement = Iterables.getFirst(suggestedReplacements, null);
                        if (spellCheckedReplacement == null || suggestedReplacements.size() > 2) {
                            continue;
                        }
                        log.debug("Try to search term '{}' by spellchecked version '{}'", term, spellCheckedReplacement);
                        //spellCheckedReplacement = URLEncoder.encode(spellCheckedReplacement, "UTF-8");
                        term = spellCheckedReplacement;
                    }
                } catch (IOException e) {
                    log.error("Exception from LangTool", e);
                }
                categories = getCategories(term);
            }


            if (CollectionUtils.isNotEmpty(categories)) {
                if (categories.size() == 1) {
                    String oneCategory = Iterables.getOnlyElement(categories);
                    if (!"Disambiguation pages".equalsIgnoreCase(oneCategory)) {
                        interestsWeights.compute(oneCategory, (key, oldValue) -> MoreObjects.firstNonNull(oldValue, 0.0) + termWeight);
                    }
                } else {
                    List<String> matchedByNameCategories = findMatchedByNameCategory(term, categories);
                    matchedByNameCategories.forEach(oneCategory -> interestsWeights.compute(oneCategory,
                            (key, oldValue) -> MoreObjects.firstNonNull(oldValue, 0.0) + termWeight));
                }
            }
        }

        List<Interest> interests = new ArrayList<>();
        final int[] i = {0};
        interestsWeights.entrySet().parallelStream()
                .sorted(Comparator.comparingDouble(e -> -e.getValue()))
                .forEachOrdered(entry -> {
                    i[0]++;
                    interests.add(new Interest(i[0], entry.getValue(), entry.getKey()));
                });
        return interests;
    }

    private List<String> findMatchedByNameCategory(String term, List<String> categories) {
        return categories.parallelStream().filter(categoryName -> term.equalsIgnoreCase(categoryName)
                || (term + "s").equalsIgnoreCase(categoryName))
                .collect(Collectors.toList());
    }

    protected List<String> getCategories(String term) {
        try {
            //term = URLEncoder.encode(term, "UTF-8");
            Document doc = Jsoup.connect("http://en.wikipedia.org/wiki/" + term).get();
            Elements categories = doc.select("#catlinks #mw-normal-catlinks ul li a");
            return categories.parallelStream().map(Element::text).collect(Collectors.toList());
        } catch (IOException e) {
            log.error("Exception during wikipedia parse", e);
        }
        return null;
    }

    public static void main(String[] args) {
        LocalMongoExtractor localMongoExtractor = LocalMongoExtractor.getInstance();
        final User userById = localMongoExtractor.getUserById("24107824");
        final List<Interest> interestsForUser = new BaselineMethod().getInterestsForUser(userById);
        System.out.println(interestsForUser);
    }
}
