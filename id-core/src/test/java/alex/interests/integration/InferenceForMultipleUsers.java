package alex.interests.integration;

import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.model.Topic;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.UserInterestsInferencer;
import alex.interests.process.params.*;
import alex.interests.process.topic.TopicModel;
import alex.interests.process.topic.lda.LDAExtractor;
import alex.interests.process.topic.lda.LDAHelper;
import alex.interests.test.DataExtractorWrapper;
import alex.interests.test.PerplexityUtils;
import alex.interests.test.UserWrapper;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.Instance;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import org.apache.commons.lang.ArrayUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static alex.interests.process.params.AlgorithmParams.AlgorithmParamsBuilder;

public class InferenceForMultipleUsers {

    private static final Logger log = LoggerFactory.getLogger(InferenceForMultipleUsers.class);
    private static final Logger resultLog = LoggerFactory.getLogger("resultLogger");

    //constants
    String inout_own = "inout_own";
    String inout = "inout";
    String in_own = "in_own";
    String out_own = "out_own";
    String own = "own";
    String out_own_rt = "out_own_rt";
    String inout_implicit = "inout_implicit";

    //todo : fix
    @Test
    @Deprecated
    public void inferenceTopicWithRegularizationAndPerplexityTesting() throws Exception {

        final AlgorithmParams algorithmParams = new AlgorithmParamsBuilder()
                .numIterations(2000)
                .numThreads(4)
                .numTopics(75)
                .build();
        int numUsers = 3;
        int numUsersToExtract = 20;
        final MongoExtractor mongoExtractor = MongoExtractor.getInstance();
        final Collection<User> users = new DataExtractorWrapper(mongoExtractor, algorithmParams)
                .getUserWithTweetsAndReferences(numUsersToExtract);

        Set<String> alreadyProcessedUsers = new HashSet<>();
        final Properties properties = new Properties();
        try (InputStream propsResource = getClass().getResourceAsStream("/test.resources.properties")) {
            properties.load(propsResource);
        }
        try (final InputStream fis = new FileInputStream(properties.getProperty("processed_users.file.name"))) {
            BufferedReader in = new BufferedReader(new InputStreamReader(fis));
            String line;
            while ((line = in.readLine()) != null) {
                alreadyProcessedUsers.add(line);
            }
        }
        final Set<User> notProcessedUsers = users
                .stream()
                .filter(user -> !alreadyProcessedUsers.contains(user.getId()))
                .limit(numUsers)
                .collect(Collectors.toSet());
        //<user-id, link reg type, perplexity>
        Table<String, LinkRegularization, Double> perplexitiesTable = HashBasedTable.create();

        //<user-id, link reg type, perplexity>
        Table<String, LinkRegularization, Double> perplexitiesTableRetweets = HashBasedTable.create();

        if (notProcessedUsers.isEmpty()) {
            System.err.println("Empty user to process. If need - increase numUsersToExtract");
            return;
        }
        final Set<String> usersToProcessIds = notProcessedUsers.stream().map(User::getId).collect(Collectors.toSet());
        log.debug("Start test for users with ids {}", usersToProcessIds);
        for (User user : notProcessedUsers) {
            log.debug("Start test for {}", user);
            final TopicModel ownTopicForTrainTweetsModel;
            LDAExtractor ldaExtractor = new LDAExtractor(algorithmParams);
            ownTopicForTrainTweetsModel = ldaExtractor.extractUserOwnTopics(user);
            UserInterestsInferencer interestsInferencer = new UserInterestsInferencer(mongoExtractor, algorithmParams);
            UserWrapper userWrapper = (UserWrapper) user;
            final List<Tweet> tweetsForTest = userWrapper.getTweetsForTest(true);

            //perform topic inference for test tweets
            final TopicInferencer topicInferencer = ownTopicForTrainTweetsModel.getTopicInferencer();
            final Instance instanceTest = LDAHelper.allTweetsToOneInstance(tweetsForTest);
            final TopicInferenceParams topicInferenceParams = algorithmParams.getTopicInferenceParams();
            final double[] sampledDistribution = topicInferencer.getSampledDistribution(instanceTest,
                    topicInferenceParams.numIterations,
                    topicInferenceParams.thinning,
                    topicInferenceParams.burnIn);
            final List<Double> topicsDistrForTestTweets = Arrays.asList(ArrayUtils.toObject(sampledDistribution));
            final TopicModel ownTopicModel = new TopicModel(ownTopicForTrainTweetsModel.getTopics(),
                    topicsDistrForTestTweets,
                    ownTopicForTrainTweetsModel.getParallelTopicModel());
            //inout
            Map<Topic, Double> calculatedTopicsInOut = interestsInferencer.inferenceTopics(user,
                    RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
            final double perplexityWithInOutRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsInOut);
            log.debug("perplexity inout regularization = {}", perplexityWithInOutRegularization);
            perplexitiesTable.put(user.getId(), LinkRegularization.INOUT, perplexityWithInOutRegularization);

            //none
            Map<Topic, Double> calculatedTopicsNoReg = interestsInferencer.inferenceTopics(user,
                    RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
            final double perplexityWithNoRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsNoReg);
            log.debug("perplexity none regularization = {}", perplexityWithNoRegularization);
            perplexitiesTable.put(user.getId(), LinkRegularization.NONE, perplexityWithNoRegularization);

            //in
            Map<Topic, Double> calculatedTopicsInReg = interestsInferencer.inferenceTopics(user,
                    RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
            final double perplexityWithInRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsInReg);
            log.debug("perplexity in regularization = {}", perplexityWithInRegularization);
            perplexitiesTable.put(user.getId(), LinkRegularization.IN, perplexityWithInRegularization);

            //out
            Map<Topic, Double> calculatedTopicsOutReg = interestsInferencer.inferenceTopics(user,
                    RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
            final double perplexityWithOutRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsOutReg);
            log.debug("perplexity out regularization = {}", perplexityWithOutRegularization);
            perplexitiesTable.put(user.getId(), LinkRegularization.OUT, perplexityWithOutRegularization);

            //out retweets
            Map<Topic, Double> calculatedTopicsRetweetsOut = interestsInferencer.inferenceTopics(user,
                    RelationType.RETWEETS, ReferenceType.REFERENCES, ownTopicModel);
            final double perplexityWithRetweetOutRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsRetweetsOut);
            log.debug("perplexity out retweets regularization = {}", perplexityWithRetweetOutRegularization);
            perplexitiesTableRetweets.put(user.getId(), LinkRegularization.OUT, perplexityWithRetweetOutRegularization);

            log.info("Final results for {}", user);
            log.info("perplexity inout regularization = {}", perplexityWithInOutRegularization);
            log.info("perplexity none regularization = {}", perplexityWithNoRegularization);
            log.info("perplexity in regularization = {}", perplexityWithInRegularization);
            log.info("perplexity out regularization = {}", perplexityWithOutRegularization);
            log.info("perplexity retweet out regularization = {}", perplexityWithRetweetOutRegularization);
        }

        //final results

        final double sumINOUT = sumForRegularizationType(perplexitiesTable, LinkRegularization.INOUT);
        final double sumNONE = sumForRegularizationType(perplexitiesTable, LinkRegularization.NONE);
        final double sumIN = sumForRegularizationType(perplexitiesTable, LinkRegularization.IN);
        final double sumOUT = sumForRegularizationType(perplexitiesTable, LinkRegularization.OUT);
        final double sumRetweetsOut = sumForRegularizationType(perplexitiesTableRetweets, LinkRegularization.OUT);

        resultLog.info("==========Final results============");
        resultLog.info("Algorithm was run for users: {}", notProcessedUsers);
        resultLog.info("Users ids: {}", usersToProcessIds);
        resultLog.info("Perplexities table for {} users : {}", numUsers, perplexitiesTable);
        resultLog.info("Sum perplexities values: \n inout {} \n none  {} \n in    {} \n out   {} \n rtout {}",
                sumINOUT,
                sumNONE,
                sumIN,
                sumOUT,
                sumRetweetsOut);
        resultLog.info("Average perplexities values: \n inout {} \n none  {} \n in    {} \n out   {} \n rtout {}",
                sumINOUT / numUsers,
                sumNONE / numUsers,
                sumIN / numUsers,
                sumOUT / numUsers,
                sumRetweetsOut / numUsers
        );

        perplexitiesTable.rowMap().entrySet().forEach(entry -> {
            resultLog.info("Perplexity values for user {} \n {}", entry.getKey(), entry.getValue());
        });

        perplexitiesTableRetweets.rowMap().entrySet().forEach(entry -> {
            resultLog.info("Perplexity retweets values for user {} \n {}", entry.getKey(), entry.getValue());
        });

        try (FileWriter fileWriter = new FileWriter(properties.getProperty("processed_users.file.name"), true)) {
            for (User notProcessedUser : notProcessedUsers) {
                fileWriter.append(notProcessedUser.getId()).append("\n");
            }
        } catch (Exception ignored) {
            log.error("Exception occurred during writing to processed_users.lst", ignored);
        }
    }


    @Test
    public void inferenceTopicWithRegularizationAndPerplexityTesting_fixedUsers() throws Exception {

        final List<String> userIds = Lists.newArrayList(/*"134965175"*/ "84510198");
        int numUsers = userIds.size();
        int numTopics = 50;

        final AlgorithmParams algorithmParamsInOutOwn = new AlgorithmParamsBuilder()
                .numIterations(2000)
                .numThreads(1)
                .numTopics(numTopics)
                .build();

        final AlgorithmParams algorithmParamsIn = new AlgorithmParamsBuilder(algorithmParamsInOutOwn)
                .coefInLinks(0.1)
                .coefOutLinks(0)
                .coefOwn(0.9)
                .build();

        final AlgorithmParams algorithmParamsOut = new AlgorithmParamsBuilder(algorithmParamsInOutOwn)
                .coefInLinks(0)
                .coefOutLinks(0.1)
                .coefOwn(0.9)
                .build();

        final AlgorithmParams algorithmParamsOwn = new AlgorithmParamsBuilder(algorithmParamsInOutOwn)
                .coefInLinks(0)
                .coefOutLinks(0)
                .coefOwn(1)
                .build();

        final MongoExtractor mongoExtractor = MongoExtractor.getInstance();
        final DataExtractorWrapper extractorWrapper = new DataExtractorWrapper(mongoExtractor, algorithmParamsInOutOwn);
        final Collection<User> users = userIds.stream().map(extractorWrapper::getUserById).collect(Collectors.toList());

        //<user-id, link reg type, perplexity>
        Table<String, String, Double> perplexitiesTable = HashBasedTable.create();

        log.debug("Start test for users with ids {}", userIds);
        for (User user : users) {
            log.debug("Start test for {}", user);
            final TopicModel ownTopicForTrainTweetsModel;
            LDAExtractor ldaExtractor = new LDAExtractor(algorithmParamsInOutOwn);
            ownTopicForTrainTweetsModel = ldaExtractor.extractUserOwnTopics(user);
            //UserInterestsInferencer interestsInferencer = new UserInterestsInferencer(mongoExtractor, algorithmParamsInOutOwn);
            UserWrapper userWrapper = (UserWrapper) user;
            final List<Tweet> tweetsForTest = userWrapper.getTweetsForTest(true);

            //perform topic inference for test tweets
            final TopicInferencer topicInferencer = ownTopicForTrainTweetsModel.getTopicInferencer();
            final Instance instanceTest = LDAHelper.allTweetsToOneInstance(tweetsForTest);
            final TopicInferenceParams topicInferenceParams = algorithmParamsInOutOwn.getTopicInferenceParams();
            final double[] sampledDistribution = topicInferencer.getSampledDistribution(instanceTest,
                    topicInferenceParams.numIterations,
                    topicInferenceParams.thinning,
                    topicInferenceParams.burnIn);
            final List<Double> topicsDistrForTestTweets = Arrays.asList(ArrayUtils.toObject(sampledDistribution));
            final TopicModel ownTopicModel = new TopicModel(ownTopicForTrainTweetsModel.getTopics(),
                    topicsDistrForTestTweets,
                    ownTopicForTrainTweetsModel.getParallelTopicModel());

            //inout + own
            Map<Topic, Double> calculatedTopicsInOutOwn = new UserInterestsInferencer(mongoExtractor, algorithmParamsInOutOwn)
                    .inferenceTopics(user,
                            RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
            final double perplexityWithInOutOwnRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsInOutOwn);
            log.debug("perplexity inout own regularization = {}", perplexityWithInOutOwnRegularization);
            perplexitiesTable.put(user.getId(), inout_own, perplexityWithInOutOwnRegularization);

            //inout
            //final AlgorithmParams algorithmParamsInOut = new AlgorithmParamsBuilder(algorithmParamsInOutOwn)
            //        .coefInLinks(0.5)
            //        .coefOutLinks(0.5)
            //        .coefOwn(0)
            //        .build();
            //SortedMap<Double, Topic> calculatedTopicsInOut = new UserInterestsInferencer(mongoExtractor, algorithmParamsInOut)
            //        .inferenceTopics(user,
            //                RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
            //final double perplexityWithInOutRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsInOut);
            //log.debug("perplexity inout regularization = {}", perplexityWithInOutRegularization);
            //perplexitiesTable.put(user.getId(), inout, perplexityWithInOutRegularization);

            //none
            Map<Topic, Double> calculatedTopicsNoReg = new UserInterestsInferencer(mongoExtractor, algorithmParamsOwn)
                    .inferenceTopics(user,
                            RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
            final double perplexityWithNoRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsNoReg);
            log.debug("perplexity none regularization = {}", perplexityWithNoRegularization);
            perplexitiesTable.put(user.getId(), own, perplexityWithNoRegularization);

            //in
            Map<Topic, Double> calculatedTopicsInReg = new UserInterestsInferencer(mongoExtractor, algorithmParamsIn).inferenceTopics(user,
                    RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
            final double perplexityWithInRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsInReg);
            log.debug("perplexity in own regularization = {}", perplexityWithInRegularization);
            perplexitiesTable.put(user.getId(), in_own, perplexityWithInRegularization);

            //out
            Map<Topic, Double> calculatedTopicsOutReg = new UserInterestsInferencer(mongoExtractor, algorithmParamsOut)
                    .inferenceTopics(user,
                            RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
            final double perplexityWithOutRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsOutReg);
            log.debug("perplexity out own regularization = {}", perplexityWithOutRegularization);
            perplexitiesTable.put(user.getId(), out_own, perplexityWithOutRegularization);

            //out retweets
            Map<Topic, Double> calculatedTopicsRetweetsOut = new UserInterestsInferencer(mongoExtractor, algorithmParamsOut).inferenceTopics(user,
                    RelationType.RETWEETS, ReferenceType.REFERENCES, ownTopicModel);
            final double perplexityWithRetweetOutRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsRetweetsOut);
            log.debug("perplexity out retweets regularization = {}", perplexityWithRetweetOutRegularization);
            perplexitiesTable.put(user.getId(), out_own_rt, perplexityWithRetweetOutRegularization);

            //inout implicit
            Map<Topic, Double> calculatedTopicsInOutImplicitOwn = new UserInterestsInferencer(mongoExtractor, algorithmParamsInOutOwn)
                    .inferenceTopicsWithImplicitLinks(user,
                            RelationType.FOLLOWING, ownTopicModel);
            final double perplexityWithInOutOwnImplicitRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsInOutImplicitOwn);
            perplexitiesTable.put(user.getId(), inout_implicit, perplexityWithInOutOwnImplicitRegularization);
            log.debug("perplexity inout implicit own regularization = {}", perplexityWithInOutOwnImplicitRegularization);

            //in implicit
            Map<Topic, Double> calculatedTopicsInImplicitOwn = new UserInterestsInferencer(mongoExtractor, algorithmParamsIn)
                    .inferenceTopicsWithImplicitLinks(user,
                            RelationType.FOLLOWING, ownTopicModel);
            final double perplexityWithInOwnImplicitRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsInImplicitOwn);
            perplexitiesTable.put(user.getId(), "in_implicit", perplexityWithInOwnImplicitRegularization);
            log.debug("perplexity in implicit own regularization = {}", perplexityWithInOwnImplicitRegularization);

            //out implicit
            Map<Topic, Double> calculatedTopicsOutImplicitOwn = new UserInterestsInferencer(mongoExtractor, algorithmParamsOut)
                    .inferenceTopicsWithImplicitLinks(user,
                            RelationType.FOLLOWING, ownTopicModel);
            final double perplexityWithOutOwnImplicitRegularization = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopicsOutImplicitOwn);
            perplexitiesTable.put(user.getId(), "out_implicit", perplexityWithOutOwnImplicitRegularization);
            log.debug("perplexity out implicit own regularization = {}", perplexityWithOutOwnImplicitRegularization);

            log.info("Final results for {}", user);
            log.info("perplexity inout regularization = {}", perplexityWithInOutOwnRegularization);
            log.info("perplexity none regularization = {}", perplexityWithNoRegularization);
            log.info("perplexity in regularization = {}", perplexityWithInRegularization);
            log.info("perplexity out regularization = {}", perplexityWithOutRegularization);
            log.info("perplexity retweet out regularization = {}", perplexityWithRetweetOutRegularization);
            log.info("perplexity inout implicit own regularization = {}", perplexityWithInOutOwnImplicitRegularization);
            log.info("perplexity in implicit own regularization = {}", perplexityWithInOwnImplicitRegularization);
            log.info("perplexity out implicit own regularization = {}", perplexityWithOutOwnImplicitRegularization);
        }

        /*//final results

        final double sumINOUT_own = sumForRegularizationType(perplexitiesTable, inout_own);
        //final double sumINOUT = sumForRegularizationType(perplexitiesTable, inout);
        final double sumNONE = sumForRegularizationType(perplexitiesTable, own);
        final double sumIN_own = sumForRegularizationType(perplexitiesTable, in_own);
        final double sumOUT_own = sumForRegularizationType(perplexitiesTable, out_own);
        final double sumRetweetsOut_own = sumForRegularizationType(perplexitiesTable, out_own_rt);

        resultLog.info("==========Final results============");
        resultLog.info("Num Topics {}", numTopics);
        resultLog.info("Algorithm was run for users: {}", users);
        resultLog.info("Users ids: {}", userIds);
        resultLog.info("Perplexities table for {} users : {}", numUsers, perplexitiesTable);
        resultLog.info("Sum perplexities values: \n inout_own {} \n none  {} \n in_own {} \n out_own {} \n rtout_own {}",
                sumINOUT_own,
                *//*sumINOUT,*//*
                sumNONE,
                sumIN_own,
                sumOUT_own,
                sumRetweetsOut_own);
        resultLog.info("Average perplexities values: \n" +
                        " inout_own {} \n" +
                        " none  {} \n" +
                        " in_own {} \n" +
                        " out_own {} \n" +
                        " rtout_own {}",
                sumINOUT_own / numUsers,
               *//* sumINOUT / numUsers,*//*
                sumNONE / numUsers,
                sumIN_own / numUsers,
                sumOUT_own / numUsers,
                sumRetweetsOut_own / numUsers
        );

        perplexitiesTable.rowMap().entrySet().forEach(entry -> {
            resultLog.info("Perplexity values for user {} \n {}", entry.getKey(), entry.getValue());
        });*/
    }


    private double sumForRegularizationType(Table<String, LinkRegularization, Double> perplexitiesTable,
                                            LinkRegularization linkRegularization) {
        return perplexitiesTable.column(linkRegularization).values()
                .parallelStream()
                .mapToDouble(d -> d)
                .sum();
    }

    private double sumForRegularizationType(Table<String, String, Double> perplexitiesTable,
                                            String linkRegularization) {
        return perplexitiesTable.column(linkRegularization).values()
                .parallelStream()
                .mapToDouble(d -> d)
                .sum();
    }

    public void testOutput() {
        int numUsers = 2;
        Table<String, LinkRegularization, Double> perplexitiesTable = HashBasedTable.create();
        perplexitiesTable.put("1", LinkRegularization.IN, 1.1);
        perplexitiesTable.put("1", LinkRegularization.NONE, 2.3);
        perplexitiesTable.put("2", LinkRegularization.OUT, 123.0);
        final double sumINOUT = sumForRegularizationType(perplexitiesTable, LinkRegularization.INOUT);
        final double sumNONE = sumForRegularizationType(perplexitiesTable, LinkRegularization.NONE);
        final double sumIN = sumForRegularizationType(perplexitiesTable, LinkRegularization.IN);
        final double sumOUT = sumForRegularizationType(perplexitiesTable, LinkRegularization.OUT);

        log.debug("Debug alalala");
        log.info("==========Final results============");
        log.info("Perplexities table for {} users : {}", numUsers, perplexitiesTable);
        log.info("Sum perplexities values: \n inout {} \n none  {} \n in    {} \n out   {}",
                sumINOUT,
                sumNONE,
                sumIN,
                sumOUT);
        log.info("Average perplexities values: \n inout {} \n none  {} \n in    {} \n out   {}",
                sumINOUT / numUsers,
                sumNONE / numUsers,
                sumIN / numUsers,
                sumOUT / numUsers
        );

        perplexitiesTable.rowMap().entrySet().forEach(entry -> {
            log.info("Perplexity values for user {} \n {}", entry.getKey(), entry.getValue());
        });
    }

    public void testWriteToProcessedUsers() throws IOException {
        Set<String> alreadyProcessedUsers = new HashSet<>();
        final Properties properties = new Properties();
        try (InputStream propsResource = getClass().getResourceAsStream("/test.resources.properties")) {
            properties.load(propsResource);
        }
        try (final InputStream fis = new FileInputStream(properties.getProperty("processed_users.file.name"))) {
            BufferedReader in = new BufferedReader(new InputStreamReader(fis));
            String line;
            while ((line = in.readLine()) != null) {
                alreadyProcessedUsers.add(line);
            }
        }

        Set<String> users = Sets.newHashSet("1", "2", "3", "6", "4");

        final Set<String> notProcessedUsers = users
                .stream()
                .filter(user -> !alreadyProcessedUsers.contains(user))
                .limit(2)
                .collect(Collectors.toSet());

        try (FileWriter fileWriter = new FileWriter(properties.getProperty("processed_users.file.name"), true)) {
            for (String notProcessedUser : notProcessedUsers) {
                fileWriter.append(notProcessedUser).append("\n");
            }
        } catch (Exception ignored) {
            log.error("Exception occured during writing to processed_users.lst", ignored);
        }
    }

}
