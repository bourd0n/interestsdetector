package alex.interests.integration;

import org.junit.Test;
import org.languagetool.JLanguageTool;
import org.languagetool.MultiThreadedJLanguageTool;
import org.languagetool.language.AmericanEnglish;
import org.languagetool.rules.RuleMatch;

import java.io.IOException;
import java.util.List;

public class NormalizationTest {

    @Test
    public void spellCheckTest() throws IOException {
        JLanguageTool langTool = new MultiThreadedJLanguageTool(new AmericanEnglish());
        List<RuleMatch> matches = langTool.check("bigdata");


        for (RuleMatch match : matches) {
            System.out.println(match.getRule().getClass());
            System.out.println("Potential error at line " +
                    match.getLine() + ", column " +
                    match.getColumn() + ": " + match.getMessage());
            System.out.println("Suggested correction: " +
                    match.getSuggestedReplacements());
        }
    }
}
