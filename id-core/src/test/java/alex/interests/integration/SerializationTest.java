package alex.interests.integration;

import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.model.User;
import alex.interests.process.mapping.entities.Category;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.topic.TopicModel;
import alex.interests.process.topic.lda.LDAExtractor;
import alex.interests.test.DataExtractorWrapper;
import alex.interests.utils.JsonSerializer;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class SerializationTest {

    @Test
    public void testSerializationTopicModel() throws IOException {
        try (InputStream propsResource = getClass().getResourceAsStream("/test.resources.properties")) {
            Properties properties = new Properties();
            properties.load(propsResource);
            AlgorithmParams algorithmParams = new AlgorithmParams.AlgorithmParamsBuilder()
                    .numIterations(50)
                    .numThreads(1)
                    .build();
            final MongoExtractor mongoExtractor = MongoExtractor.getInstance();
            DataExtractorWrapper dataExtractorWrapper = new DataExtractorWrapper(mongoExtractor, algorithmParams);
            User user = dataExtractorWrapper.getUserById("620601589");
            LDAExtractor ldaExtractor = new LDAExtractor(algorithmParams);
            TopicModel ownTopicModel = ldaExtractor.extractUserOwnTopics(user);
            ownTopicModel.writeModel(properties.getProperty("model.file.name"), properties.getProperty("distribution.file.name"));
            final TopicModel topicModel = TopicModel.readModel(properties.getProperty("model.file.name"), properties.getProperty("distribution.file.name"));
        }
    }

    //@Test
    //public void testSerializationCategoriesModel() throws IOException {
    //    SortedMap<Double, Category> categories = new TreeMap<>(Comparator.reverseOrder());
    //    Category parent = new Category();
    //    parent.setId(1);
    //    Category child1 = new Category();
    //    child1.setId(2);
    //    Category child2 = new Category();
    //    child2.setId(3);
    //    child2.addOwnWeight(2);
    //    parent.addChildCategory(child1);
    //    parent.addChildCategory(child2);
    //    categories.put(1.0, parent);
    //    categories.put(0.5, child1);
    //    String fileCategories = "I:\\programming\\diplom\\diplom\\serialization\\test_category_distribution.json";
    //    JsonSerializer.write(fileCategories, categories);
    //
    //    final SortedMap<Double, Category> categoriesDeserialize = JsonSerializer.readCategories(fileCategories);
    //}
}
