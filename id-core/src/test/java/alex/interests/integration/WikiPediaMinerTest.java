package alex.interests.integration;


import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.model.Topic;
import alex.interests.data.model.User;
import alex.interests.process.UserInterestsInferencer;
import alex.interests.process.mapping.WikiTopicMapper;
import alex.interests.process.mapping.entities.Category;
import alex.interests.process.mapping.entities.SearchTermResult;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.params.InterestsMapperParams;
import alex.interests.process.params.ReferenceType;
import alex.interests.process.params.RelationType;
import alex.interests.process.topic.TopicModel;
import alex.interests.test.DataExtractorWrapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.MapLikeType;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.*;
import java.util.stream.Collectors;

public class WikiPediaMinerTest {
    private static final Logger log = LoggerFactory.getLogger(WikiPediaMinerTest.class);

    @Test
    public void testSimpleGet() throws IOException {
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            String term = "big data";
            HttpGet httpGet = new HttpGet("http://wikipedia-miner.cms.waikato.ac.nz/services/search?query="+ URLEncoder.encode(term, "UTF-8")+"&responseFormat=json");
            System.out.println(httpGet.getURI());
            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                System.out.println(response.getStatusLine());
                HttpEntity entity = response.getEntity();
                String jsonStr = EntityUtils.toString(entity);
                ObjectMapper objectMapper = new ObjectMapper();
                final SearchTermResult searchTermResult = objectMapper.readValue(jsonStr, SearchTermResult.class);
                System.out.println(searchTermResult);
            }
        }
    }

    @Test
    public void testWikiTopicMapper() {
        InterestsMapperParams params = new InterestsMapperParams(15, 0.4, 2);
        WikiTopicMapper topicMapper = new WikiTopicMapper(params);

        //app(53) apps(40) ios(36) mobile(35) techcrunch(24) apple(21) android(17) tuaw(16)
        Map<String, Double> termDistribution = new HashMap<>();
        //termDistribution.put(53.0, "app");
        //termDistribution.put(40.0, "apps");
        termDistribution.put("ios", 36.0);
        termDistribution.put("apple", 31.0);
        Topic topic1 = new Topic(termDistribution);

        Map<String, Double> termDistribution2 = new HashMap<>();
        termDistribution2.put("techcrunch", 24.0);
        termDistribution2.put("apple", 21.0);
        termDistribution2.put("android", 17.0);
        termDistribution2.put("tuaw", 16.0);
        Topic topic2 = new Topic(termDistribution2);

        Map<Topic, Double> topics = new HashMap<>();
        topics.put(topic1, 0.6);
        //topics.put(0.3, topic2);
        final Map<Category, Double> topicsToInterests = topicMapper.mapTopicsToInterests(topics);
        System.out.println(topicsToInterests);
    }

    @Test
    public void testRealWikiTopicMapper() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        objectMapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        final MapLikeType sortedMapType = objectMapper.getTypeFactory().constructMapLikeType(SortedMap.class, Double.class, Topic.class);

        final AlgorithmParams algorithmParamsInOutOwn = new AlgorithmParams.AlgorithmParamsBuilder()
                .numIterations(2000)
                .numThreads(1)
                .numTopics(75)
                .build();
        String userId = "24107824";
        File file = new File("D:\\MY\\diplom\\diplom\\serialization\\test_topic_distribution" + userId + ".json");
        Map<Topic, Double> calculatedTopics;
        if (!file.exists()) {
            final MongoExtractor mongoExtractor = MongoExtractor.getInstance();
            final User user = mongoExtractor.getUserById(userId);
            UserInterestsInferencer interestsInferencer = new UserInterestsInferencer(mongoExtractor, algorithmParamsInOutOwn);
            calculatedTopics = interestsInferencer.inferenceTopics(user,
                    RelationType.FOLLOWING, ReferenceType.ALL);
            file.createNewFile();
            objectMapper.writeValue(file, calculatedTopics);
        } else {
            Map<Topic, Double> calculatedTopicsRead = objectMapper.readValue(file, sortedMapType);
            calculatedTopics = new HashMap<>();
            calculatedTopics.putAll(calculatedTopicsRead);
        }
        log.debug(TopicModel.toStringSortedByDistribution(calculatedTopics));
        InterestsMapperParams params = InterestsMapperParams.DEFAULT_MAPPER_PARAMS;
        WikiTopicMapper topicMapper = new WikiTopicMapper(params);

        final Map<Category, Double> topicsToInterests = topicMapper.mapTopicsToInterests(calculatedTopics);
        log.debug("Topic to interests map : {}", topicsToInterests);
        log.debug("Categories titles sorted: {} ", topicsToInterests.keySet().stream().map(Category::getTitle).collect(Collectors.toList()));
    }
}
