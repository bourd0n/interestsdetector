package alex.interests.integration;

import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.topic.lda.LDAHelper;
import cc.mallet.pipe.iterator.ArrayIterator;
import cc.mallet.types.InstanceList;
import org.junit.Test;

import java.util.List;

public class PreprocessingTweetsTest {

    @Test
    public void testTweetsPreprocessing() throws Exception {
        final MongoExtractor mongoExtractor = MongoExtractor.getInstance();
        User user = mongoExtractor.getUserById("620601589");
        final List<Tweet> tweets = user.getTweets(true);
        final InstanceList instanceList = LDAHelper.thruTestPipe(new ArrayIterator(tweets));
    }
}
