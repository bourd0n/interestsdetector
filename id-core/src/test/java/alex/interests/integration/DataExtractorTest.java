package alex.interests.integration;

import alex.interests.data.extractor.LocalMongoExtractor;
import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.extractor.twitter.TwitterUtil;
import alex.interests.data.model.Interest;
import alex.interests.data.model.User;
import alex.interests.test.Method;
import org.junit.Test;

import java.util.*;

import static alex.interests.test.InterestsMappingTestHelper.*;

public class DataExtractorTest {

    @Test
    public void testLocalExtract() {
        final User userById = LocalMongoExtractor.getInstance().getUserById("98680213");
        System.out.println(userById);
    }

    @Test
    public void testMongoExtract() {
        final User userById = MongoExtractor.getInstance().getUserById("77433468");
        System.out.println(userById);
    }

    @Test
    public void testTwitterUtil() {
        long id = 1648547509L;
        TwitterUtil.loadUserById(id);
    }

    @Test
    public void testGetLoadedUsers() {
        final Map<Long, String> loadedUsers = TwitterUtil.getLoadedUsers();
        System.out.println(loadedUsers);
    }

    @Test
    public void testSaveInterestsForTesting() {
        String userId = "1648547509";
        Method method = Method.wiki_complex_20;
        List<Interest> list = new ArrayList<>();
        list.add(new Interest(10, 1.0, "Test1"));
        list.add(new Interest(20, 2.0, "Test2"));
        saveSimpleInterests(userId, method, list);
    }
}
