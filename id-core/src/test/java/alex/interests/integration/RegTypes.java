package alex.interests.integration;

import alex.interests.process.params.AlgorithmParams;

public class RegTypes {

    public static final int numTopics = 100;
    public static final AlgorithmParams algorithmParamsInOut = new AlgorithmParams.AlgorithmParamsBuilder()
            .numIterations(1300)
            .numThreads(20)
            .numTopics(numTopics)
            .build();
    public static final AlgorithmParams algorithmParamsIn = new AlgorithmParams.AlgorithmParamsBuilder(algorithmParamsInOut)
            .coefInLinks(0.1)
            .coefOutLinks(0)
            .coefOwn(0.9)
            .build();
    public static final AlgorithmParams algorithmParamsOut = new AlgorithmParams.AlgorithmParamsBuilder(algorithmParamsInOut)
            .coefInLinks(0)
            .coefOutLinks(0.1)
            .coefOwn(0.9)
            .build();
    public static final AlgorithmParams algorithmParamsOwn = new AlgorithmParams.AlgorithmParamsBuilder(algorithmParamsInOut)
            .coefInLinks(0)
            .coefOutLinks(0)
            .coefOwn(1)
            .build();

    public enum RegType {
        inout_explicit(algorithmParamsInOut),
        in_explicit(algorithmParamsIn),
        out_explicit(algorithmParamsOut),
        inout_implicit(algorithmParamsInOut),
        in_implicit(algorithmParamsIn),
        out_implicit(algorithmParamsOut),
        own(algorithmParamsOwn),
        rt_out_explicit(algorithmParamsOut);

        AlgorithmParams algorithmParams;

        RegType(AlgorithmParams algorithmParams) {
            this.algorithmParams = algorithmParams;
        }

        @Override
        public String toString() {
            return this.name();
        }
    }

}