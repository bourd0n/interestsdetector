package alex.interests.integration;

import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.model.Topic;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.UserInterestsInferencer;
import alex.interests.process.params.AlgorithmParams;
import alex.interests.process.params.ReferenceType;
import alex.interests.process.params.RelationType;
import alex.interests.process.topic.TopicModel;
import alex.interests.process.topic.lda.LDAExtractor;
import alex.interests.process.topic.lda.LDAHelper;
import alex.interests.process.weight.FollowerWeightsCalculator;
import alex.interests.test.DataExtractorWrapper;
import cc.mallet.types.Instance;
import com.google.common.collect.Iterables;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class TopicExtractorTest {

    private static final Logger log = LoggerFactory.getLogger(TopicExtractorTest.class);

    @Test
    public void testExtractOwnTopics_1000_iter(){
        User users = MongoExtractor.getInstance().getUserById("24107824");
        AlgorithmParams algorithmParams = new AlgorithmParams.AlgorithmParamsBuilder()
                .numIterations(1300)
                .numThreads(20)
                .build();
        LDAExtractor extractor = new LDAExtractor(algorithmParams);
        final TopicModel topicModel = extractor.extractUserOwnTopics(users);
    }

    @Test
    public void testExtract100Users(){
        log.debug("Start testExtract100Users");
        Collection<User> users = MongoExtractor.getInstance().getUserWithTweetsAndReferences(100);
        log.debug("End testExtract100Users size {}. Content: {}", users.size(), users.stream().map(User::getId).collect(Collectors.toSet()));
    }

    @Test
    public void inferenceTopicWithRegularization_fast(){
        User userWithTweetsAndReferences =  MongoExtractor.getInstance().getUserById("1380471");
        AlgorithmParams algorithmParams = new AlgorithmParams.AlgorithmParamsBuilder()
                .numIterations(100)
                .numThreads(1)
                .coefInLinks(0.05)
                .coefOutLinks(0.05)
                .coefOwn(0.9)
                .build();
        UserInterestsInferencer interestsInferencer = new UserInterestsInferencer(MongoExtractor.getInstance(), algorithmParams);
        Map<Topic, Double> doubleTopicSortedMap = interestsInferencer
                .inferenceTopics(userWithTweetsAndReferences,
                        RelationType.FOLLOWING, ReferenceType.ALL);
        final double sum = doubleTopicSortedMap.values().stream().mapToDouble(d -> d).sum();
        System.out.println(sum);
    }

    @Test
    public void inferenceTopicWithRegularization_long(){
        AlgorithmParams algorithmParams = new AlgorithmParams.AlgorithmParamsBuilder()
                .numIterations(2000)
                .numThreads(1)
                .build();
        User user = new DataExtractorWrapper(MongoExtractor.getInstance(), algorithmParams).getUserById("620601589");
        UserInterestsInferencer interestsInferencer = new UserInterestsInferencer(MongoExtractor.getInstance(), algorithmParams);
        Map<Topic, Double> doubleTopicSortedMap = interestsInferencer
                .inferenceTopics(user,
                        RelationType.FOLLOWING, ReferenceType.ALL);
    }

    @Test
    public void followerAuthoritiesWeightsCalculatorTest(){
        User user = Iterables.getOnlyElement(MongoExtractor.getInstance().getUserWithTweetsAndReferences(1));
        FollowerWeightsCalculator followerWeightsCalculator = new FollowerWeightsCalculator(MongoExtractor.getInstance(), AlgorithmParams.DEFAULT_ALGORITHM_PARAMS);
        followerWeightsCalculator.calculateOutLinkWeights(user);
    }

    @Test
    public void followerHubsWeightsCalculatorTest(){
        User user = Iterables.getOnlyElement(MongoExtractor.getInstance().getUserWithTweetsAndReferences(1));
        FollowerWeightsCalculator followerWeightsCalculator = new FollowerWeightsCalculator(MongoExtractor.getInstance(), AlgorithmParams.DEFAULT_ALGORITHM_PARAMS);
        followerWeightsCalculator.calculateInLinkWeights(user);
    }

    @Test
    public void testExtractingReferences() {
        log.debug("Start");
        User user = MongoExtractor.getInstance().getUserByIdWithoutReferences("42934309");
        final Set<String> friends = user.getFriends(true);
        log.debug("End. Friend size : {}", friends.size());
    }

    @Test
    public void testLDAHelper() {
        User targetUser = MongoExtractor.getInstance().getUserByIdWithoutReferences("433651921");
        List<Tweet> tweets = targetUser.getTweets(true);
        if (CollectionUtils.isEmpty(tweets)) {
            Assert.fail();
        } else {
            try {
                Instance instanceOneLine = LDAHelper.allTweetsToOneInstance(tweets);
            } catch (Exception e) {
                log.error("Exception occurred for 433651921 user");
                log.error("Exception", e);
                throw e;
            }
        }
    }

}
