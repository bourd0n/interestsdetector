package alex.interests.integration;

import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.model.Topic;
import alex.interests.data.model.Tweet;
import alex.interests.data.model.User;
import alex.interests.process.UserInterestsInferencer;
import alex.interests.process.params.ReferenceType;
import alex.interests.process.params.RelationType;
import alex.interests.process.topic.TopicModel;
import alex.interests.process.topic.lda.LDAExtractor;
import alex.interests.test.DataExtractorWrapper;
import alex.interests.test.PerplexityUtils;
import alex.interests.test.UserWrapper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.CollectionLikeType;
import com.google.common.collect.Iterables;
import org.apache.commons.collections.CollectionUtils;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static alex.interests.integration.RegTypes.*;
import static alex.interests.integration.RegTypes.RegType.*;

public class ScheduleTestForUsers {
    private static final Logger log = LoggerFactory.getLogger(ScheduleTestForUsers.class);
    private static final Logger resultLog = LoggerFactory.getLogger("resultLogger");

    private static final MongoExtractor mongoExtractor = MongoExtractor.getInstance();


    private Map<RegType, Double> perplexities;
    private String userId;
    private User user;
    private TopicModel ownTopicModel;
    private List<Tweet> tweetsForTest;

    @Before
    public void setUp() throws IOException {
        perplexities = new HashMap<>();
        userId = "17425484";//getNextId();
        if (userId == null) {
            return;
        }
        final DataExtractorWrapper extractor = new DataExtractorWrapper(mongoExtractor, algorithmParamsInOut);
        user = extractor.getUserById(userId);
        log.debug("Start test for {}", user);
        final TopicModel ownTopicForTrainTweetsModel = new LDAExtractor(algorithmParamsInOut, extractor).extractUserOwnTopics(user);
        tweetsForTest = ((UserWrapper) user).getTweetsForTest(true);

        ownTopicModel = ownTopicForTrainTweetsModel;
        //perform topic inference for test tweets
        //final TopicInferencer topicInferencer = ownTopicForTrainTweetsModel.getTopicInferencer();
        //final Instance instanceTest = LDAHelper.allTweetsToOneInstance(tweetsForTest);
        //final TopicInferenceParams topicInferenceParams = algorithmParamsInOut.getTopicInferenceParams();
        //final double[] sampledDistribution = topicInferencer.getSampledDistribution(instanceTest,
        //        topicInferenceParams.numIterations,
        //        topicInferenceParams.thinning,
        //        topicInferenceParams.burnIn);
        //final List<Double> topicsDistrForTestTweets = Arrays.asList(ArrayUtils.toObject(sampledDistribution));
        //ownTopicModel = new TopicModel(ownTopicForTrainTweetsModel.getTopics(),
        //        topicsDistrForTestTweets,
        //        ownTopicForTrainTweetsModel.getParallelTopicModel());
    }

    @Test
    public void inferenceTopicWithRegularizationAndPerplexity() {
        if (userId == null) {
            return;
        }
        try {
            final long l1 = System.currentTimeMillis();
            calculatePerplexityFollowing(inout_explicit);
            final long l2 = System.currentTimeMillis();
            calculatePerplexityFollowing(own);
            final long l3 = System.currentTimeMillis();
            calculatePerplexityFollowing(in_explicit);
            final long l4 = System.currentTimeMillis();
            calculatePerplexityFollowing(out_explicit);
            final long l5 = System.currentTimeMillis();
            calculatePerplexityRetweeting(rt_out_explicit);
            final long l6 = System.currentTimeMillis();
            calculatePerplexityFollowingImplicit(inout_implicit);
            final long l7 = System.currentTimeMillis();
            calculatePerplexityFollowingImplicit(out_implicit);
            final long l8 = System.currentTimeMillis();
            calculatePerplexityFollowingImplicit(in_implicit);
            final long l9 = System.currentTimeMillis();

            resultLog.info("{} time {} ms", inout_explicit, l2 - l1);
            resultLog.info("{} time {} ms", own, l3 - l2);
            resultLog.info("{} time {} ms", in_explicit, l4 - l3);
            resultLog.info("{} time {} ms", out_explicit, l5 - l4);
            resultLog.info("{} time {} ms", rt_out_explicit, l6 - l5);
            resultLog.info("{} time {} ms", inout_implicit, l7 - l6);
            resultLog.info("{} time {} ms", out_implicit, l8 - l7);
            resultLog.info("{} time {} ms", in_implicit, l9 - l8);
            resultLog.info("Final results for {}", user);
            perplexities.entrySet().forEach(entry ->
                    resultLog.info("perplexity {} regularization = {}", entry.getKey(), entry.getValue()));

            TestHelper.writeToExcelWorkBook(user, numTopics, perplexities);
        } catch (Throwable throwable) {
            log.error("Exception for user " + userId, throwable);
            resultLog.error("Exception for user {}. Exception {}", userId, throwable.getMessage());
        }
    }

    protected double calculatePerplexityFollowing(RegType regType) {
        Map<Topic, Double> calculatedTopics = new UserInterestsInferencer(mongoExtractor, regType.algorithmParams)
                .inferenceTopics(user,
                        RelationType.FOLLOWING, ReferenceType.ALL, ownTopicModel);
        double perplexity = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopics);
        log.debug("perplexity {} regularization = {}. Sum distr {}", regType, perplexity, getSum(calculatedTopics));
        perplexities.put(regType, perplexity);
        return perplexity;
    }

    private double getSum(Map<Topic, Double> calculatedTopics) {
        return calculatedTopics.values().parallelStream().mapToDouble(value -> value).sum();
    }

    protected double calculatePerplexityFollowingImplicit(RegType regType) {
        Map<Topic, Double> calculatedTopics = new UserInterestsInferencer(mongoExtractor, regType.algorithmParams)
                .inferenceTopicsWithImplicitLinks(user,
                        RelationType.FOLLOWING, ownTopicModel);
        double perplexity = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopics);
        log.debug("perplexity {} regularization = {}. Sum distr {}", regType, perplexity, getSum(calculatedTopics));
        perplexities.put(regType, perplexity);
        return perplexity;
    }

    protected double calculatePerplexityRetweeting(RegType regType) {
        Map<Topic, Double> calculatedTopics = new UserInterestsInferencer(mongoExtractor, regType.algorithmParams)
                .inferenceTopics(user,
                        RelationType.RETWEETS, ReferenceType.REFERENCES, ownTopicModel);
        double perplexity = PerplexityUtils.calculatePerplexity(tweetsForTest, calculatedTopics);
        log.debug("perplexity {} regularization = {}. Sum distr {}", regType, perplexity, getSum(calculatedTopics));
        perplexities.put(regType, perplexity);
        return perplexity;
    }

    private String getNextId() throws IOException {
        final Properties properties = new Properties();
        try (InputStream propsResource = getClass().getResourceAsStream("/test.resources.properties")) {
            properties.load(propsResource);
        }
        final String userToProcessFile = properties.getProperty("users.to.process.file");

        ObjectMapper objectMapper = new ObjectMapper()
                .configure(SerializationFeature.INDENT_OUTPUT, true)
                .configure(SerializationFeature.WRITE_NULL_MAP_VALUES, true)
                .setSerializationInclusion(JsonInclude.Include.NON_NULL);
        final CollectionLikeType type = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, String.class);

        final File file = new File(userToProcessFile);
        List<String> usersToProcess = objectMapper.readValue(file, type);

        if (CollectionUtils.isEmpty(usersToProcess)) {
            return null;
        }
        final String lastId = Iterables.getLast(usersToProcess);
        usersToProcess.remove(lastId);
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, usersToProcess);

        return lastId;
    }

}
