package alex.interests.integration;

import alex.interests.data.extractor.MongoExtractor;
import alex.interests.data.model.User;
import alex.interests.integration.RegTypes.RegType;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class ExcelWriteTest {
    private static final Logger log = LoggerFactory.getLogger(ExcelWriteTest.class);

    @Test
    public void testWriteToExcel() {
        String userId = "84510198";
        User user = MongoExtractor.getInstance().getUserById(userId);
        Map<RegType, Double> doubleMap = new HashMap<>();
        for (RegType regType : RegType.values()) {
            doubleMap.put(regType, 123423.2543);
        }
        TestHelper.writeToExcelWorkBook(user, 100, doubleMap);
    }


}
