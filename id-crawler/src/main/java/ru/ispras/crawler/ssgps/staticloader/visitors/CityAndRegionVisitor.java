package ru.ispras.crawler.ssgps.staticloader.visitors;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.IdTitle;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteCitiesRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteCountriesRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteRegionsRequest;
import ru.ispras.crawler.core.visitor.AbstractNonFailingVisitor;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 05.06.14
 * Time: 15:32
 */
public class CityAndRegionVisitor extends AbstractNonFailingVisitor<VkontakteCountriesRequest, BasicResponse<List<IdTitle>>> {
    @Override
    public Collection<IRequest> visit(VkontakteCountriesRequest request, BasicResponse<List<IdTitle>> response) {
        List<IdTitle> countries = response.getObject();
        ArrayList<IRequest> requests = new ArrayList<>();
        for (IdTitle country: countries) {
            requests.add(new VkontakteCitiesRequest(country.getId()));
            requests.add(new VkontakteRegionsRequest(country.getId()));
        }
        return requests;
    }

    @Override
    public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
        Set<Class<? extends IRequest>> set = new HashSet<>();
        set.add(VkontakteCitiesRequest.class);
        set.add(VkontakteRegionsRequest.class);
        return set;
    }
}
