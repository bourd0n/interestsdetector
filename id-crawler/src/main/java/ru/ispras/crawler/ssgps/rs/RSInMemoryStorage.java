package ru.ispras.crawler.ssgps.rs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIFriendsRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIMutualFriendsRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIPhotoRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIProfileRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIVideoRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIWallRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.Photo;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.Post;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.User;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.Video;
import ru.ispras.crawler.core.saver.ISaver;

public class RSInMemoryStorage {
	public static class ProfileSaver implements ISaver<FacebookAPIProfileRequest, BasicResponse<User>> {
		private final Map<Long, User> profileMap = new HashMap<>();

		@Override
		public synchronized void save(FacebookAPIProfileRequest request, BasicResponse<User> response) {
			profileMap.put(request.getId(), response.getObject());
		}

		public synchronized Map<Long, User> getProfileMap() {
			return new HashMap<>(profileMap);
		}
	}

	public static class FriendsSaver implements ISaver<FacebookAPIFriendsRequest, BasicResponse<List<Long>>> {
		private final Map<Long, List<Long>> friendsMap = new HashMap<>();

		@Override
		public synchronized void save(FacebookAPIFriendsRequest request, BasicResponse<List<Long>> response) {
			friendsMap.put(request.getId(), response.getObject());
		}

		public synchronized Map<Long, List<Long>> getFriendsMap() {
			return new HashMap<>(friendsMap);
		}
	}

	public static class MutualFriendsSaver implements ISaver<FacebookAPIMutualFriendsRequest, BasicResponse<List<Long>>> {
		private final Map<Long, List<Long>> mutualFriendsMap = new HashMap<>();

		@Override
		public synchronized void save(FacebookAPIMutualFriendsRequest request, BasicResponse<List<Long>> response) {
			mutualFriendsMap.put(request.getId(), response.getObject());
		}

		public synchronized Map<Long, List<Long>> getMutualFriendsMap() {
			return new HashMap<>(mutualFriendsMap);
		}
	}

	public static class WallSaver implements ISaver<FacebookAPIWallRequest, BasicResponse<List<Post>>> {
		private final Map<Long, List<Post>> wallMap = new HashMap<>();

		@Override
		public synchronized void save(FacebookAPIWallRequest request, BasicResponse<List<Post>> response) {
			wallMap.put(request.getId(), response.getObject());
		}

		public synchronized Map<Long, List<Post>> getWallMap() {
			return new HashMap<>(wallMap);
		}
	}

	public static class PhotoSaver implements ISaver<FacebookAPIPhotoRequest, BasicResponse<Photo>> {
		private final Map<Long, Photo> objectMap = new HashMap<>();

		@Override
		public synchronized void save(FacebookAPIPhotoRequest request, BasicResponse<Photo> response) {
			objectMap.put(request.getId(), response.getObject());
		}

		public synchronized Map<Long, Photo> getObjectMap() {
			return new HashMap<>(objectMap);
		}
	}
	
	public static class VideoSaver implements ISaver<FacebookAPIVideoRequest, BasicResponse<Video>> {
		private final Map<Long, Video> objectMap = new HashMap<>();

		@Override
		public synchronized void save(FacebookAPIVideoRequest request, BasicResponse<Video> response) {
			objectMap.put(request.getId(), response.getObject());
		}

		public synchronized Map<Long, Video> getObjectMap() {
			return new HashMap<>(objectMap);
		}
	}

	private final ProfileSaver profileSaver = new ProfileSaver();
	private final FriendsSaver friendsSaver = new FriendsSaver();
	private final MutualFriendsSaver mutualFriendsSaver = new MutualFriendsSaver();
	private final WallSaver wallSaver = new WallSaver();
	private final PhotoSaver photoSaver = new PhotoSaver();
	private final VideoSaver videoSaver = new VideoSaver();

	public ProfileSaver getProfileSaver() {
		return profileSaver;
	}

	public FriendsSaver getFriendsSaver() {
		return friendsSaver;
	}

	public MutualFriendsSaver getMutualFriendsSaver() {
		return mutualFriendsSaver;
	}

	public WallSaver getWallSaver() {
		return wallSaver;
	}

	public PhotoSaver getPhotoSaver() {
		return photoSaver;
	}

	public VideoSaver getVideoSaver() {
		return videoSaver;
	}

	

}
