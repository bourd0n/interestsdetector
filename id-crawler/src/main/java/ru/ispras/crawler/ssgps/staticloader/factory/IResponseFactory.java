package ru.ispras.crawler.ssgps.staticloader.factory;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;


/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 20.06.14
 * Time: 15:50
 */
public interface IResponseFactory<ReqIn extends IRequest, ReqOut extends IRequest, Resp, RespOut> {
    abstract public ReqOut getRequest(ReqIn reqIn, Resp response);

    abstract public BasicResponse<RespOut> getResponse(ReqIn reqIn, Resp response);
}


