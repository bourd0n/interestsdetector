package ru.ispras.crawler.ssgps.rsgg.twitter;

import java.io.IOException;
import java.util.List;

import ru.ispras.crawler.core.Crawler;
import ru.ispras.crawler.core.connection.DelayedConnection;
import ru.ispras.crawler.core.connection.GroupingConnection;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserFollowersConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserFriendsConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserProfileConnection;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionFactory;
import ru.ispras.crawler.core.connection.utils.BatchConfigurableConnectionFactoryUtils;
import ru.ispras.crawler.core.connection.utils.ConnectionConfigurationReader;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFollowersRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFriendsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserProfileRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.User;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserFollowersRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserFriendsRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserProfileRequestIdConverter;
import ru.ispras.crawler.core.saver.GroupingSaver;
import ru.ispras.crawler.core.saver.mongodb.MongoDBSaverLoader;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.ChunkingMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.JacksonMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.responsechunker.BasicResponseChunker;
import ru.ispras.crawler.core.visitor.graph.SamplingVisitor;
import ru.ispras.crawler.core.visitor.graph.sampler.ForestFireSampler;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DB;
import com.mongodb.Mongo;

public class RSGGTwitterCrawler {
	public static void main(String[] args) throws Exception {
		if (args.length < 3) {
			System.out.println("Usage: <oauth config file> <seed user id> <user limit>");
			return;
		}
		new RSGGTwitterCrawler().crawl(args[0], Long.parseLong(args[1]), Integer.parseInt(args[2]));
	}

	public void crawl(String oauthConfigFile, long seedId, int userLimit) throws IOException {
		Crawler crawler = new Crawler(createConfig(oauthConfigFile, userLimit));
		crawler.crawl(new RSGGTwitterRequest(seedId));
		crawler.awaitTerminationAndShutdown();
	}
	
	private Crawler.Configuration createConfig(String configFile, int userLimit)
			throws IOException {
		IConnection<HttpRequest, HttpResponse> twitterHttpConnection = new DelayedConnection<>(
				BatchConfigurableConnectionFactoryUtils.create(new TwitterHttpConnectionFactory(),
						ConnectionConfigurationReader.getConfigurations(TwitterHttpConnectionConfiguration.class,
								configFile)));

		GroupingConnection.Configuration groupingConnectionConfig = new GroupingConnection.Configuration();
		groupingConnectionConfig.put(TwitterUserProfileRequest.class,
				new TwitterUserProfileConnection(twitterHttpConnection));
		groupingConnectionConfig.put(TwitterUserFollowersRequest.class,
				new TwitterUserFollowersConnection(twitterHttpConnection));
		groupingConnectionConfig.put(TwitterUserFriendsRequest.class,
				new TwitterUserFriendsConnection(twitterHttpConnection));

		DB db = new Mongo().getDB("rsgg-twitter");

		GroupingSaver.Configuration groupingSaverConfig = new GroupingSaver.Configuration();
		groupingSaverConfig.put(TwitterUserProfileRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("profile"),
						new TwitterUserProfileRequestIdConverter(),
						new JacksonMongoDBResponseConverter<>(new TypeReference<BasicResponse<User>>() {})));
		groupingSaverConfig.put(TwitterUserFollowersRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("followers"),
						new TwitterUserFollowersRequestIdConverter(),
						new ChunkingMongoDBResponseConverter<>(
								new BasicResponseChunker<Long>(100000),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<List<Long>>>() {}))));
		groupingSaverConfig.put(TwitterUserFriendsRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("friends"),
						new TwitterUserFriendsRequestIdConverter(),
						new ChunkingMongoDBResponseConverter<>(
								new BasicResponseChunker<Long>(100000),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<List<Long>>>() {}))));

		Crawler.Configuration config = new Crawler.Configuration();
		config.put(RSGGTwitterRequest.class,
				new GroupingConnection(groupingConnectionConfig),
				new SamplingVisitor<RSGGTwitterRequest, GroupingResponse, Long>(
						new ForestFireSampler<Long>(),
						new RSGGTwitterRequestIdConverter(),
						new RSGGTwitterResponseNeighbourExtractor(),
						RSGGTwitterRequest.class,
						userLimit),
				new GroupingSaver(groupingSaverConfig));
		return config;
	}
}
