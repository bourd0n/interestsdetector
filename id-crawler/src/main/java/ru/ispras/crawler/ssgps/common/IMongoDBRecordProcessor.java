package ru.ispras.crawler.ssgps.common;

import com.mongodb.DBObject;

public interface IMongoDBRecordProcessor {
	
	/**
	 * Processes record {@code record}
	 * @param record
	 */
	public void processRecord(DBObject record);
}
