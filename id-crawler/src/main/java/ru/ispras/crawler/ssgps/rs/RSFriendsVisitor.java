package ru.ispras.crawler.ssgps.rs;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIFriendsRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIMutualFriendsRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIProfileRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIWallRequest;
import ru.ispras.crawler.core.visitor.AbstractNonFailingVisitor;

public class RSFriendsVisitor extends AbstractNonFailingVisitor<FacebookAPIFriendsRequest, BasicResponse<List<Long>>> {
	private static final String FRIENDS_REQUEST_PARAMETERS = "limit=5000";
	private static final String WALL_REQUEST_PARAMETERS =	
			"fields=created_time,from,message,object_id,story,to,type,with_tags,comments.limit(5000),likes.limit(5000)";
	private static final int PAGES = 1;
	
	
	private final int wallLimit;
	
	public RSFriendsVisitor(int wallLimit){
		this.wallLimit = wallLimit;
	}
		
	@Override
	public Collection<? extends IRequest> visit(FacebookAPIFriendsRequest request, BasicResponse<List<Long>> response) {
		List<Long> array = response.getObject();
		long userId = request.getId();
		
		Set<FacebookAPIRequest> newRequests = new HashSet<FacebookAPIRequest>();
		
		newRequests.add(new FacebookAPIProfileRequest(userId));
		newRequests.add(new FacebookAPIWallRequest(userId, getWallRequestParameters(wallLimit), PAGES));
		
		for (int i=0; i <array.size(); i++){
			long id = array.get(i);
			newRequests.add(new FacebookAPIProfileRequest(id));
			newRequests.add(new FacebookAPIMutualFriendsRequest(id, FRIENDS_REQUEST_PARAMETERS, PAGES));
			newRequests.add(new FacebookAPIWallRequest(id, getWallRequestParameters(wallLimit), PAGES));
		}
		return newRequests;
	}

	@Override
	public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
		Set<Class<? extends IRequest>> result = new HashSet<Class<? extends IRequest>>();
		result.add(FacebookAPIProfileRequest.class);
		result.add(FacebookAPIMutualFriendsRequest.class);
		result.add(FacebookAPIWallRequest.class);
		return result;
	}
	
	private String getWallRequestParameters(int limit){
		return WALL_REQUEST_PARAMETERS+"&limit="+Integer.toString(limit);
	}
}
