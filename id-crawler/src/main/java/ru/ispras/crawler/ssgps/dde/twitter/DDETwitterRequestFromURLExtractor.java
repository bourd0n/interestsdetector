package ru.ispras.crawler.ssgps.dde.twitter;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import ru.ispras.crawler.core.connection.exception.ConnectionCreationException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.http.HttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.vkontakte.VkontakteResolveScreenNameConnection;
import ru.ispras.crawler.core.connection.vkontakte.http.VkontakteHttpConnectionFactory;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.StatusEntities;
import ru.ispras.crawler.core.datamodel.twitter.entities.URLEntity;
import ru.ispras.crawler.core.datamodel.twitter.entities.User;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteResolveScreenNameRequest;

// XXX Extracted as is, should be refined in future.
public class DDETwitterRequestFromURLExtractor {
    private static final Logger logger = Logger.getLogger(DDETwitterRequestFromURLExtractor.class);
	private final static Set<String> facebookHosts = new HashSet<>(Arrays.asList("facebook.com", "www.facebook.com"));
    private final static Set<String> vkontakteHosts = new HashSet<>(Arrays.asList("vk.com", "www.vk.com",
            "vkontakte.ru", "www.vkontakte.ru"));

	private final static Pattern facebookIdPattern = Pattern.compile("(id=)(\\d+)");
    private final static int facebookIdPatternGroup = 2;

    private final static Pattern vkontakteIdPattern = Pattern.compile("(id)(\\d+)");
    private final static int vkontakteIdPatternGroup = 2;

    private VkontakteResolveScreenNameConnection resolveScreenNameConnection;

    public DDETwitterRequestFromURLExtractor(){
        try {
            resolveScreenNameConnection =
                    new VkontakteResolveScreenNameConnection(new VkontakteHttpConnectionFactory().create(new HttpConnectionConfiguration()));
        } catch (ConnectionCreationException e) {
            logger.error("Error while creating VkontakteResolveScreenNameConnection",e);
            resolveScreenNameConnection = null;
        }
    }


    /**
     * Checks if profile contains link to supported SN
     * @param profile
     * @return
     */
    public static boolean checkProfileForURL(User profile){
        String urlString = extractURLString(profile);
        try {
            URL url = new URL(urlString);
            return url != null && (facebookHosts.contains(url.getHost().toLowerCase()) || vkontakteHosts.contains(url.getHost().toLowerCase()));
        } catch (MalformedURLException e) {
            return false;
        }
    }

    /**
     * @param profile
     * @return request if user's url contains link to another SN, null otherwise
     */
    public IRequest extractRequestFromURL(User profile){
        String urlString = extractURLString(profile);
        URL url = null;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            return null;
        }

        long twitterUserId = profile.getId();

        //facebook
        if (facebookHosts.contains(url.getHost().toLowerCase())) {
            String screenName = extractUserScreenName(url);
            long id = extractUserId(url, facebookIdPattern, facebookIdPatternGroup);

            if (id != 0) {
                return new DDETwitterFacebookUserProfileRequest(id, twitterUserId);
            } else if (screenName != null) {
                return new DDETwitterFacebookUserProfileRequest(screenName, twitterUserId);
            }else {
                return null;
            }
        }

        //vk
        if (vkontakteHosts.contains(url.getHost().toLowerCase())) {
            String screenName = extractUserScreenName(url);
            long id = extractUserId(url, vkontakteIdPattern, vkontakteIdPatternGroup);

            if (id != 0) {
                return new DDETwitterVkontakteUserRequest(id, twitterUserId);
            } else if (screenName != null) {
                if (resolveScreenNameConnection != null){
                    try {
                        id = resolveScreenNameConnection.
                                getResponse(new VkontakteResolveScreenNameRequest(screenName)).getObject().getObjectId();
                        return new DDETwitterVkontakteUserRequest(id, twitterUserId);
                    } catch (UnavailableConnectionException e) {
                        logger.error("Error while fetching vkontakte user id: "+screenName, e);
                    }
                }
            }else {
                return null;
            }
        }
        return null;
    }


	private static String extractURLString(User profile) {
		if (profile.getEntities() == null) {
			return null;
		}
		StatusEntities urlJSON = profile.getEntities().getUrl();
		if (urlJSON == null || urlJSON.getUrlEntities() == null) {
			return null;
		}
		List<URLEntity> urlArray = urlJSON.getUrlEntities();
		if (urlArray.size() == 0) {
			return null;
		}
		return urlArray.get(0).getExpandedURL();
	}

	private long extractUserId(URL url, Pattern pattern, int group) {
		String ref = url.getRef();
		String file = url.getFile();
		if (ref != null) {
			// http://www.facebook.com/home.php#!/profile.php?id=1796091109
			return parseLongId(ref, pattern, group);
		} else {
			// http://www.facebook.com/profile.php?id=179609110
			return parseLongId(file, pattern, group);
		}
	}

	private static String extractUserScreenName(URL url) {
		String ref = url.getRef();
		String file = url.getFile();
		if (ref == null) {
			// http://www.facebook.com/userName
			return file.substring(1);
		}
		return null;
	}

	private static long parseLongId(String str, Pattern pattern, int group) {
		Matcher matcher = pattern.matcher(str);
		if (matcher.find()) {
            try{
			    return Long.parseLong(matcher.group(group));
            }catch (NumberFormatException e){
                //do nothing
            }
		}
		return 0;
	}
}
