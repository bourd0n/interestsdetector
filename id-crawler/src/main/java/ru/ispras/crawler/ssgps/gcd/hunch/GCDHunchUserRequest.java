package ru.ispras.crawler.ssgps.gcd.hunch;

import ru.ispras.crawler.core.datamodel.hunch.HunchUserFollowersRequest;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserFollowingRequest;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserGroupingRequest;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserItemRatingsRequest;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserProfileRequest;

public class GCDHunchUserRequest extends HunchUserGroupingRequest {

	public GCDHunchUserRequest(String screenName) {
		super(screenName, new HunchUserProfileRequest(screenName),
				new HunchUserFollowersRequest(screenName),
				new HunchUserFollowingRequest(screenName),
				new HunchUserItemRatingsRequest(screenName));
	}
}
