package ru.ispras.crawler.ssgps.vkontakte.staticloader.converter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DB;
import com.mongodb.Mongo;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteIdRequest;
import ru.ispras.crawler.core.requestidconverter.vkontakte.staticdata.VkontakteIdRequestConverter;
import ru.ispras.crawler.ssgps.vkontakte.ConverterSaver;
import ru.ispras.crawler.core.saver.mongodb.MongoDBSaverLoader;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.JacksonMongoDBResponseConverter;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 24.06.14
 * Time: 14:58
 */
public class StaticDataConverterSaver<ReqIn extends IRequest, Resp, RespOut> extends ConverterSaver<ReqIn, VkontakteIdRequest, Resp, RespOut> {
    private static DB getDB() throws IOException {
        return new Mongo().getDB("StaticData");
    }

    protected static <RespOut> MongoDBSaverLoader<VkontakteIdRequest, BasicResponse<RespOut>, Long> getMongoSaver(String name, Class<RespOut> clazz) throws IOException {
        DB db = getDB();
        return new MongoDBSaverLoader<>(db.getCollection(name),
                new VkontakteIdRequestConverter(),
                new JacksonMongoDBResponseConverter<>(
                        new TypeReference<BasicResponse<RespOut>>() {
                        }));
    }

    public StaticDataConverterSaver(String name, Class<RespOut> clazz, IResponseConverter<ReqIn, VkontakteIdRequest, Resp, RespOut> factory) throws IOException {
        super(getMongoSaver(name, clazz), factory);
    }
}
