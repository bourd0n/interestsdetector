package ru.ispras.crawler.ssgps.dde.facebook;

import java.util.List;
import java.util.Map;

import ru.ispras.crawler.core.Crawler;
import ru.ispras.crawler.core.connection.DelayedConnection;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserProfileConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserTimelineConnection;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionFactory;
import ru.ispras.crawler.core.connection.utils.BatchConfigurableConnectionFactoryUtils;
import ru.ispras.crawler.core.connection.utils.ConnectionConfigurationReader;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserTimelineRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.Status;
import ru.ispras.crawler.core.datamodel.twitter.entities.User;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserProfileRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserTimelineRequestIdConverter;
import ru.ispras.crawler.core.saver.CompositeSaver;
import ru.ispras.crawler.core.saver.mongodb.MongoDBSaverLoader;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.ChunkingMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.JacksonMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.responsechunker.BasicResponseChunker;
import ru.ispras.crawler.core.visitor.NoOpVisitor;
import ru.ispras.crawler.ssgps.common.twitter.TwitterUserProfileTimelineVisitor;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DB;
import com.mongodb.Mongo;

public class DDEFacebookCrawler {
	private final Crawler crawler;

	public DDEFacebookCrawler(String oauthConfigFile, String facebookDBName, int timelineLimit) throws Exception {
		crawler = init(oauthConfigFile, facebookDBName, timelineLimit);
	}

	private Crawler init(String authConfigFile, String facebookDBName, int timelineLimit) throws Exception {
		IConnection<HttpRequest, HttpResponse> twitterHttpConnection = new DelayedConnection<>(
				BatchConfigurableConnectionFactoryUtils.create(new TwitterHttpConnectionFactory(),
						ConnectionConfigurationReader.getConfigurations(TwitterHttpConnectionConfiguration.class,
								authConfigFile)));

		DB facebookDB = new Mongo().getDB(facebookDBName);
		DB twitterDB = new Mongo().getDB("dde-facebook-twitter");

		Crawler.Configuration config = new Crawler.Configuration();
		config.put(DDEFacebookTwitterUserProfileRequest.class,
				new TwitterUserProfileConnection(twitterHttpConnection),
				new TwitterUserProfileTimelineVisitor(timelineLimit),
				new CompositeSaver<>(
						new MongoDBSaverLoader<>(twitterDB.getCollection("profile"),
								new TwitterUserProfileRequestIdConverter(),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<User>>() {})),
						new MongoDBFacebookToTwitterUserLinkSaver(
								facebookDB.getCollection("twitterLink"),
								twitterDB.getCollection("facebookLink"))));
		config.put(TwitterUserTimelineRequest.class,
				new TwitterUserTimelineConnection(twitterHttpConnection),
				new NoOpVisitor<>(),
				new MongoDBSaverLoader<>(twitterDB.getCollection("timeline"),
						new TwitterUserTimelineRequestIdConverter(),
						new ChunkingMongoDBResponseConverter<>(
								new BasicResponseChunker<Status>(),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<List<Status>>>() {}))));
		return new Crawler(config);
	}

	public static void main(String[] args) throws Exception {
		if (args.length < 3) {
			System.out.println("Usage: <oauth config file> <facebook database name> <timeline limit>");
			return;
		}
		
		final int timelineLimit = Integer.parseInt(args[2]);
		
		DDEFacebookCrawler ddeDatasetGenerator = new DDEFacebookCrawler(args[0], args[1], timelineLimit);
		
		FacebookMongoDBReader reader = new FacebookMongoDBReader(args[1]);
		DDEFacebookRecordProcessor processor = new DDEFacebookRecordProcessor();
		reader.findAll("profile", processor);
		Map<Long, String> screenNames = processor.getTwitterLinks();
		for (long facebookId : screenNames.keySet()) {
			ddeDatasetGenerator.crawler.crawl(new DDEFacebookTwitterUserProfileRequest(screenNames.get(facebookId),
					facebookId));
		}
		ddeDatasetGenerator.crawler.awaitTermination();
		ddeDatasetGenerator.crawler.shutdown();
	}
}
