package ru.ispras.crawler.ssgps.gcd.facebook;

import java.util.List;

import org.apache.log4j.Logger;

import ru.ispras.crawler.core.connection.DelayedConnection;
import ru.ispras.crawler.core.connection.GroupingConnection;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.facebook.FacebookProfileConnection;
import ru.ispras.crawler.core.connection.facebook.FacebookUserFavoritesConnection;
import ru.ispras.crawler.core.connection.facebook.FacebookUserFriendsConnection;
import ru.ispras.crawler.core.connection.facebook.http.FacebookHttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.facebook.http.FacebookHttpConnectionFactory;
import ru.ispras.crawler.core.connection.utils.BatchConfigurableConnectionFactoryUtils;
import ru.ispras.crawler.core.connection.utils.ConnectionConfigurationReader;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.datamodel.facebook.FacebookResponse;
import ru.ispras.crawler.core.datamodel.facebook.FacebookUserFavoritesRequest;
import ru.ispras.crawler.core.datamodel.facebook.FacebookUserFriendsRequest;
import ru.ispras.crawler.core.datamodel.facebook.FacebookUserProfileRequest;
import ru.ispras.crawler.core.datamodel.facebook.entities.Favorites;
import ru.ispras.crawler.core.datamodel.facebook.entities.Profile;
import ru.ispras.crawler.core.datamodel.grouping.GroupingRequest;
import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.requestidconverter.facebook.FacebookUserFavoritesRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.facebook.FacebookUserFriendsRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.facebook.FacebookUserProfileRequestIdConverter;
import ru.ispras.crawler.core.saver.GroupingSaver;
import ru.ispras.crawler.core.saver.mongodb.MongoDBSaverLoader;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.ChunkingMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.JacksonMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.responsechunker.BasicResponseChunker;
import ru.ispras.crawler.ssgps.common.ConnectionBasedMetropolisHastingsCrawler;
import ru.ispras.crawler.ssgps.common.SeedListLoader;
import ru.ispras.crawler.ssgps.gcd.facebook.FacebookMetropolisHastingsCrawler.FacebookMetropolisHastingsUserInfoRequest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DB;
import com.mongodb.Mongo;

public class FacebookMetropolisHastingsCrawler
		extends
		ConnectionBasedMetropolisHastingsCrawler<FacebookMetropolisHastingsUserInfoRequest, GroupingResponse, FacebookUserFriendsRequest, BasicResponse<List<Long>>> {

	private static final Logger logger = Logger.getLogger(FacebookMetropolisHastingsCrawler.class); 
	
	public static void main(String[] args) throws Exception {
		if (args.length < 3) {
			System.out.println("Usage: <auth config file> <seeds file> <number of transitions>");
			return;
		}
		
		IConnection<HttpRequest, HttpResponse> facebookHttpConnection = new DelayedConnection<>(
				BatchConfigurableConnectionFactoryUtils.create(new FacebookHttpConnectionFactory(),
						ConnectionConfigurationReader.getConfigurations(FacebookHttpConnectionConfiguration.class,
								args[0])));
		final int numberOfTransitions = Integer.parseInt(args[2]);
		final List<Long> seeds = SeedListLoader.loadSeeds(args[1]);
		final FacebookMetropolisHastingsCrawler crawler = new FacebookMetropolisHastingsCrawler(
				numberOfTransitions, facebookHttpConnection, new Mongo().getDB("gcd-facebook"));
		for (Long id : seeds) {
			try {
				crawler.crawl(id);
			}
			catch (Exception e) {
				logger.error("Error downloading user: " + id, e);
			}
		}
	}
	
	// *************************************************************************
	
	public FacebookMetropolisHastingsCrawler(
			int numberOfTransitions,
			IConnection<HttpRequest, HttpResponse> httpConnection,
			DB db) {
		super(numberOfTransitions,
			new GroupingConnection(getUserInfoConnectionConfiguration(httpConnection)),
			new GroupingSaver(getUserInfoSaverConfiguration(db)),
			new FacebookUserFriendsConnection(httpConnection),
			new MongoDBSaverLoader<>(
					db.getCollection("friends"),
					new FacebookUserFriendsRequestIdConverter(),
					new ChunkingMongoDBResponseConverter<>(
							new BasicResponseChunker<Long>(100000),
							new JacksonMongoDBResponseConverter<>(
									new TypeReference<BasicResponse<List<Long>>>() {}))));
	}

	private static GroupingConnection.Configuration getUserInfoConnectionConfiguration(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		GroupingConnection.Configuration result = new GroupingConnection.Configuration();
		result.put(FacebookUserProfileRequest.class, new FacebookProfileConnection(httpConnection));
		result.put(FacebookUserFavoritesRequest.class, new FacebookUserFavoritesConnection(httpConnection));
		return result;
	}

	private static GroupingSaver.Configuration getUserInfoSaverConfiguration(DB db) {
		GroupingSaver.Configuration result = new GroupingSaver.Configuration();
		result.put(FacebookUserProfileRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("profile"),
						new FacebookUserProfileRequestIdConverter(),
						new JacksonMongoDBResponseConverter<>(
								new TypeReference<FacebookResponse<Profile>>() {})));
		result.put(FacebookUserFavoritesRequest.class,
				new MongoDBSaverLoader<>(db.getCollection("favorites"),
						new FacebookUserFavoritesRequestIdConverter(),
						new JacksonMongoDBResponseConverter<>(
								new TypeReference<BasicResponse<Favorites>>() {})));
		return result;
	}

	@Override
	protected List<Long> convertFriendsToLongList(BasicResponse<List<Long>> response) {
		return response.getObject();
	}

	@Override
	protected FacebookUserFriendsRequest createFriendsRequest(long user) {
		return new FacebookUserFriendsRequest(user);
	}

	@Override
	protected FacebookMetropolisHastingsUserInfoRequest createUserInfoRequest(long user) {
		return new FacebookMetropolisHastingsUserInfoRequest(user);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void extractNumberOfFriends(IRequest request, IResponse response) {
		if (request instanceof FacebookMetropolisHastingsUserInfoRequest) {
			final long user = ((FacebookMetropolisHastingsUserInfoRequest)request).getId();
			final FacebookResponse<Profile> facebookResponse = (FacebookResponse<Profile>)
					((GroupingResponse)response).getResponse(FacebookUserProfileRequest.class);
			final int numberOfFriends = facebookResponse.getObject().getFriendsCount();
			saveNumberOfFriends(user, numberOfFriends);
		}
	}
	
	// *******************************************************************************
	
	public static class FacebookMetropolisHastingsUserInfoRequest extends GroupingRequest {
		private long user;
		
		public FacebookMetropolisHastingsUserInfoRequest(long user) {
			super(new FacebookUserProfileRequest(user),	new FacebookUserFavoritesRequest(user));
			this.user = user;
		}
		
		public long getId() {
			return user;
		}
	}
}
