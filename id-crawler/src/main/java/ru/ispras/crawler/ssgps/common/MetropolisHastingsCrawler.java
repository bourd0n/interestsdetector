package ru.ispras.crawler.ssgps.common;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.commons.collections15.map.ReferenceMap;
import org.apache.log4j.Logger;

public abstract class MetropolisHastingsCrawler {

	private static final Logger logger = Logger.getLogger(MetropolisHastingsCrawler.class);
	private static final Random random = new Random();
	
	private final int maxNumberOfTransitions;
	private final Set<Long> failed;
	private final Map<Long, LinkedList<Long>> userToAncestors;
	private final ReferenceMap<Long, List<Long>> userToFriends;
	
	public MetropolisHastingsCrawler(int maxNumberOfTransitions) {
		this.maxNumberOfTransitions = maxNumberOfTransitions;
		this.failed = new HashSet<>();
		this.userToAncestors = new HashMap<>();
		this.userToFriends = new ReferenceMap<>(ReferenceMap.SOFT, ReferenceMap.SOFT);
	}
	
	public void crawl(long seed) throws DataDownloadException {
		long user = seed;
		downloadUserInfo(user);
		int numberOfTransitions = 0;
		while (numberOfTransitions < maxNumberOfTransitions) {
			final List<Long> friends;
			try {
				friends = getUserFriends(user);
				removeFailed(friends);
			}
			catch (DataDownloadException e) {
				user = processUserFriendsDownloadFailed(user);
				continue;
			}
			
			final long friend = getRandomFriend(friends);
			try {
				downloadUserInfo(friend);
			}
			catch (DataDownloadException e) {
				logger.info("Failed user profile/likes download: " + user);
				failed.add(friend);
				continue;
			}
			
			user = select(user, friend);
			numberOfTransitions++;
		}
	}

	// ***************************************************************************
	
	/**
	 * Download user information. 
	 */
	protected abstract void downloadUserInfo(long user)
			throws DataDownloadException;
	
	/**
	 * Download user friends and return them as list of user friend ids.
	 */
	protected abstract List<Long> downloadUserFriends(long user)
			throws DataDownloadException;

	/**
	 * Return number of friends for the given user. 
	 */
	protected abstract Integer getNumberOfFriends(long user);
	
	// ***************************************************************************
	
	private List<Long> getUserFriends(long user)
			throws DataDownloadException {
		List<Long> result = userToFriends.get(user);
		if (result == null) {
			result = downloadUserFriends(user);
			userToFriends.put(user, result);
		}
		return result;
	}
	
	private void removeFailed(List<Long> friends) throws DataDownloadException {
		friends.removeAll(failed);
		if (friends.isEmpty()) {
			throw new DataDownloadException("No friends available!");
		}
	}
	
	private long processUserFriendsDownloadFailed(Long user) throws DataDownloadException {
		logger.info("Failed user friends download: " + user);
		failed.add(user);
		final LinkedList<Long> ancestors = userToAncestors.get(user);
		if (ancestors == null || ancestors.isEmpty() ) {
			throw new DataDownloadException("No users for backtrace available!");
		}
		final Long ancestor = ancestors.pop();
		if (ancestors.isEmpty()) {
			userToAncestors.remove(user);
		}
		logger.info("Backtracking to " + ancestor);
		return ancestor;
	}
	
	private long getRandomFriend(List<Long> friends) {
		final int size = friends.size();
		final int index = random.nextInt(size);
		final long friend = friends.get(index); 
		logger.info("Selected friend: " + friend);
		return friend;
	}

	private long select(long user, long friend) {
		final Integer oldNodeFriendsNumber = getNumberOfFriends(user);
		final Integer newNodeFriendsNumber = getNumberOfFriends(friend);
		final double randDouble = random.nextDouble();
		if (randDouble * newNodeFriendsNumber <= oldNodeFriendsNumber) {
			updateUserAncestors(friend, user);
			logger.info("Transition from " + user + " to " + friend);
			return friend;
		} else {
			logger.info("Staying at " + user);
			return user;
		}
	}
	
	private void updateUserAncestors(Long friend, Long user) {
		LinkedList<Long> ancestors = userToAncestors.get(friend);
		if (ancestors == null) {
			ancestors = new LinkedList<>();
			userToAncestors.put(friend, ancestors);
		}
		ancestors.push(user);
	}
	
	public static class DataDownloadException extends Exception {

		private static final long serialVersionUID = -1384526724225423248L;

		public DataDownloadException(String message, Throwable cause) {
			super(message, cause);
		}

		public DataDownloadException(String message) {
			super(message);
		}

		public DataDownloadException(Throwable cause) {
			super(cause);
		}
	}
}
