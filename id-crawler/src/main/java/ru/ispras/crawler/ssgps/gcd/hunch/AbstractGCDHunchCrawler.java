package ru.ispras.crawler.ssgps.gcd.hunch;

import java.io.IOException;
import java.util.List;

import ru.ispras.crawler.core.Crawler;
import ru.ispras.crawler.core.connection.DelayedConnection;
import ru.ispras.crawler.core.connection.GroupingConnection;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.hunch.HunchItemConnection;
import ru.ispras.crawler.core.connection.hunch.HunchUserFollowersConnection;
import ru.ispras.crawler.core.connection.hunch.HunchUserFollowingConnection;
import ru.ispras.crawler.core.connection.hunch.HunchUserItemsConnection;
import ru.ispras.crawler.core.connection.hunch.HunchUserProfileConnection;
import ru.ispras.crawler.core.connection.hunch.http.HunchHttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.hunch.http.HunchHttpConnectionFactory;
import ru.ispras.crawler.core.connection.utils.BatchConfigurableConnectionFactoryUtils;
import ru.ispras.crawler.core.connection.utils.ConnectionConfigurationReader;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.hunch.HunchItemRequest;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserFollowersRequest;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserFollowingRequest;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserItemRatingsRequest;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserProfileRequest;
import ru.ispras.crawler.core.datamodel.hunch.entities.Item;
import ru.ispras.crawler.core.datamodel.hunch.entities.ItemRating;
import ru.ispras.crawler.core.datamodel.hunch.entities.User;
import ru.ispras.crawler.core.requestidconverter.hunch.HunchItemRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.hunch.HunchUserFollowersRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.hunch.HunchUserFollowingRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.hunch.HunchUserItemRatingsRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.hunch.HunchUserProfileRequestIdConverter;
import ru.ispras.crawler.core.saver.GroupingSaver;
import ru.ispras.crawler.core.saver.mongodb.MongoDBSaverLoader;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.ChunkingMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.JacksonMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.responsechunker.BasicResponseChunker;
import ru.ispras.crawler.core.visitor.IVisitor;
import ru.ispras.crawler.core.visitor.NoOpVisitor;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DB;
import com.mongodb.Mongo;

public abstract class AbstractGCDHunchCrawler {
	public void crawl(String oauthConfigFile, String seedName) throws IOException {
		Crawler crawler = new Crawler(createCrawlerConfiguration(oauthConfigFile));
		crawler.crawl(new GCDHunchUserRequest(seedName));
		crawler.awaitTerminationAndShutdown();
	}

	private Crawler.Configuration createCrawlerConfiguration(String oauthConfigFile) throws IOException {
		IConnection<HttpRequest, HttpResponse> hunchHttpConnection = new DelayedConnection<>(
				BatchConfigurableConnectionFactoryUtils.create(new HunchHttpConnectionFactory(),
						ConnectionConfigurationReader.getConfigurations(HunchHttpConnectionConfiguration.class,
								oauthConfigFile)));

		GroupingConnection.Configuration groupingConnectionConfiguration = new GroupingConnection.Configuration();
		groupingConnectionConfiguration.put(HunchUserProfileRequest.class,
				new HunchUserProfileConnection(hunchHttpConnection));
		groupingConnectionConfiguration.put(HunchUserFollowersRequest.class,
				new HunchUserFollowersConnection(hunchHttpConnection));
		groupingConnectionConfiguration.put(HunchUserFollowingRequest.class,
				new HunchUserFollowingConnection(hunchHttpConnection));
		groupingConnectionConfiguration.put(HunchUserItemRatingsRequest.class,
				new HunchUserItemsConnection(hunchHttpConnection));

		DB db = new Mongo().getDB("gcd-hunch");

		GroupingSaver.Configuration groupingSaverConfiguration = new GroupingSaver.Configuration();
		groupingSaverConfiguration.put(HunchUserProfileRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("profile"),
						new HunchUserProfileRequestIdConverter(),
						new JacksonMongoDBResponseConverter<>(
								new TypeReference<BasicResponse<User>>() {})));
		groupingSaverConfiguration.put(HunchUserFollowersRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("followers"),
						new HunchUserFollowersRequestIdConverter(),
						new ChunkingMongoDBResponseConverter<>(
								new BasicResponseChunker<String>(),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<List<String>>>() {}))));
		groupingSaverConfiguration.put(HunchUserFollowingRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("following"),
						new HunchUserFollowingRequestIdConverter(),
						new ChunkingMongoDBResponseConverter<>(
								new BasicResponseChunker<String>(),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<List<String>>>() {}))));
		groupingSaverConfiguration.put(HunchUserItemRatingsRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("items"),
						new HunchUserItemRatingsRequestIdConverter(),
						new ChunkingMongoDBResponseConverter<>(
								new BasicResponseChunker<ItemRating>(),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<List<ItemRating>>>() {}))));

		Crawler.Configuration config = new Crawler.Configuration();
		config.put(GCDHunchUserRequest.class,
				new GroupingConnection(groupingConnectionConfiguration),
				new GCDHunchUserVisitor(getNeighbourVisitor()),
				new GroupingSaver(groupingSaverConfiguration));
		config.put(HunchItemRequest.class,
				new HunchItemConnection(hunchHttpConnection),
				new NoOpVisitor<>(),
				new MongoDBSaverLoader<>(db.getCollection("item"),
						new HunchItemRequestIdConverter(),
						new JacksonMongoDBResponseConverter<>(
								new TypeReference<BasicResponse<Item>>() {})));
		return config;
	}

	protected abstract IVisitor<GCDHunchUserRequest, GroupingResponse> getNeighbourVisitor();
}
