package ru.ispras.crawler.ssgps.rs;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.AbstractFacebookAPIObjectRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIPhotoRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIVideoRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIWallRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.Post;
import ru.ispras.crawler.core.visitor.AbstractNonFailingVisitor;

public class RSWallVisitor extends AbstractNonFailingVisitor<FacebookAPIWallRequest, BasicResponse<List<Post>>> {

	private final Set<Long> visitedObjects = new HashSet<>();

	@Override
	public Collection<? extends IRequest> visit(FacebookAPIWallRequest request, BasicResponse<List<Post>> response) {
		return getObjectRequests(response.getObject());
	}

	private Collection<? extends IRequest> getObjectRequests(List<Post> posts) {
		Set<AbstractFacebookAPIObjectRequest> res = new HashSet<AbstractFacebookAPIObjectRequest>();

		for (int i = 0; i < posts.size(); i++) {
			Post post = posts.get(i);
			String objectId = post.getObjectId();
			String type = post.getType();
			if (objectId != null && type != null) {
				long id = Long.parseLong(objectId);
				synchronized (visitedObjects) {
					if (!visitedObjects.contains(id)){
						if ("photo".equals(type)){
							res.add(new FacebookAPIPhotoRequest(id));
						}
						if ("video".equals(type)){
							res.add(new FacebookAPIVideoRequest(id));
						}
						visitedObjects.add(id);
					}					
				}
			}
		}

		return res;
	}

	@Override
	public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
		Set<Class<? extends IRequest>> result = new HashSet< Class<? extends IRequest>>();
		result.add(FacebookAPIPhotoRequest.class);
		result.add(FacebookAPIVideoRequest.class);
		return result;
	}

}
