package ru.ispras.crawler.ssgps.gcd.hunch;

import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class GCDHunchUserRequestIdConverter implements IRequestIdConverter<GCDHunchUserRequest, String> {
	@Override
	public String getId(GCDHunchUserRequest request) {
		return request.getScreenName();
	}

	@Override
	public GCDHunchUserRequest getRequest(String screenName) {
		return new GCDHunchUserRequest(screenName);
	}
}
