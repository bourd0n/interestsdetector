package ru.ispras.crawler.ssgps.spam.twitter;

import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFriendsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserGroupingRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserTimelineRequest;

public class TwitterUserTimelineFriendsRequest extends TwitterUserGroupingRequest {
	public TwitterUserTimelineFriendsRequest(long id){
		super(id, new TwitterUserTimelineRequest(id), new TwitterUserFriendsRequest(id));
	}
}
