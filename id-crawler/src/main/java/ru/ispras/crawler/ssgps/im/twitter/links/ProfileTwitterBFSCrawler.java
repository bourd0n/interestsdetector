package ru.ispras.crawler.ssgps.im.twitter.links;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DB;
import com.mongodb.Mongo;
import ru.ispras.crawler.core.Crawler;
import ru.ispras.crawler.core.connection.DelayedConnection;
import ru.ispras.crawler.core.connection.GroupingConnection;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserProfileConnection;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionFactory;
import ru.ispras.crawler.core.connection.utils.BatchConfigurableConnectionFactoryUtils;
import ru.ispras.crawler.core.connection.utils.ConnectionConfigurationReader;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserProfileRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.User;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserProfileRequestIdConverter;
import ru.ispras.crawler.core.saver.GroupingSaver;
import ru.ispras.crawler.core.saver.mongodb.MongoDBSaverLoader;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.JacksonMongoDBResponseConverter;
import ru.ispras.crawler.ssgps.im.twitter.IMTwitterBFSCrawler;
import ru.ispras.crawler.ssgps.im.twitter.IMTwitterRequest;

import java.io.IOException;

public class ProfileTwitterBFSCrawler extends IMTwitterBFSCrawler {

    public ProfileTwitterBFSCrawler() {
        super(0, 1);
    }

    public void crawl(String oauthConfigFile, long seedId) throws IOException {
        Crawler crawler = new Crawler(createCrawlerConfig(oauthConfigFile, 1));
        crawler.crawl(new IMTwitterRequest(seedId, 1));
        crawler.awaitTermination(15, 2);
        crawler.shutdown();
    }

    @Override
    public void crawl(String oauthConfigFile, long seedId, Integer neighboursLimit, Integer timelineLimit) throws IOException {
        crawl(oauthConfigFile, seedId);
    }

    @Override
    protected Crawler.Configuration createCrawlerConfig(String configFile, Integer timelineLimit) throws IOException {
        IConnection<HttpRequest, HttpResponse> twitterHttpConnection = new DelayedConnection<>(
                BatchConfigurableConnectionFactoryUtils.create(new TwitterHttpConnectionFactory(),
                        ConnectionConfigurationReader.getConfigurations(TwitterHttpConnectionConfiguration.class,
                                configFile)));

        GroupingConnection.Configuration groupingConnectionConfig = new GroupingConnection.Configuration();
        groupingConnectionConfig.put(TwitterUserProfileRequest.class,
                new TwitterUserProfileConnection(twitterHttpConnection));

        DB db = new Mongo().getDB("im-twitter");

        GroupingSaver.Configuration groupingSaverConfig = new GroupingSaver.Configuration();
        groupingSaverConfig.put(TwitterUserProfileRequest.class,
                new MongoDBSaverLoader<>(
                        db.getCollection("profile"),
                        new TwitterUserProfileRequestIdConverter(),
                        new JacksonMongoDBResponseConverter<>(
                                new TypeReference<BasicResponse<User>>() {})));

        Crawler.Configuration config = new Crawler.Configuration();
        config.put(IMTwitterRequest.class,
                new GroupingConnection(groupingConnectionConfig),
                getNeighbourVisitor(),
                new GroupingSaver(groupingSaverConfig));
        return config;
    }
}
