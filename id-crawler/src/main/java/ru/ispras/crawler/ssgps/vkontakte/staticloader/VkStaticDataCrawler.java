package ru.ispras.crawler.ssgps.vkontakte.staticloader;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import ru.ispras.crawler.core.Crawler;
import ru.ispras.crawler.core.connection.DelayedConnection;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.http.HttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.utils.BatchConfigurableConnectionFactoryUtils;
import ru.ispras.crawler.core.connection.vkontakte.http.VkontakteHttpConnectionFactory;
import ru.ispras.crawler.core.connection.vkontakte.staticdata.VkontakteChairsConnection;
import ru.ispras.crawler.core.connection.vkontakte.staticdata.VkontakteCitiesConnection;
import ru.ispras.crawler.core.connection.vkontakte.staticdata.VkontakteCountriesConnection;
import ru.ispras.crawler.core.connection.vkontakte.staticdata.VkontakteFacultiesConnection;
import ru.ispras.crawler.core.connection.vkontakte.staticdata.VkontakteRegionsConnection;
import ru.ispras.crawler.core.connection.vkontakte.staticdata.VkontakteSchoolsConnection;
import ru.ispras.crawler.core.connection.vkontakte.staticdata.VkontakteUniversitiesConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.*;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.*;
import ru.ispras.crawler.core.visitor.NoOpVisitor;
import ru.ispras.crawler.ssgps.vkontakte.staticloader.converter.*;
import ru.ispras.crawler.ssgps.vkontakte.staticloader.visitors.ChairsVisitor;
import ru.ispras.crawler.ssgps.vkontakte.staticloader.visitors.CityAndRegionVisitor;
import ru.ispras.crawler.ssgps.vkontakte.staticloader.visitors.FacultiesVisitor;
import ru.ispras.crawler.ssgps.vkontakte.staticloader.visitors.SchoolAndUniversityVisitor;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 04.06.14
 * Time: 18:21
 */
public class VkStaticDataCrawler {

	private void downloadData() throws IOException {
        HttpConnectionConfiguration config = new HttpConnectionConfiguration();

        Crawler crawler = new Crawler(createCrawlerConfig(config), 1);

        crawler.crawl(Collections.singleton(new VkontakteCountriesRequest()));
        crawler.awaitTermination(3, 3);
        crawler.shutdown();
        System.out.println("Finished");
    }

    private Crawler.Configuration createCrawlerConfig(HttpConnectionConfiguration config) throws IOException {

        IConnection<HttpRequest, HttpResponse> vkontakteHttpConnection = new DelayedConnection<>(
                BatchConfigurableConnectionFactoryUtils.create(new VkontakteHttpConnectionFactory(),
                        Arrays.asList(config)));

        Crawler.Configuration result = new Crawler.Configuration();

        result.put(VkontakteCountriesRequest.class,
                new VkontakteCountriesConnection(vkontakteHttpConnection),
                new CityAndRegionVisitor(),
                new StaticDataConverterSaver<>("country", IdTitle.class, new CountryConverter()));


        result.put(VkontakteRegionsRequest.class,
                new VkontakteRegionsConnection(vkontakteHttpConnection),
                new NoOpVisitor<>(),
                new StaticDataConverterSaver<>("region", Region.class, new RegionConverter()));

        result.put(VkontakteCitiesRequest.class,
                new VkontakteCitiesConnection(vkontakteHttpConnection),
                new SchoolAndUniversityVisitor(),
                new StaticDataConverterSaver<>("city", City.class, new CityConverter()));

        result.put(VkontakteSchoolsRequest.class,
                new VkontakteSchoolsConnection(vkontakteHttpConnection),
                new NoOpVisitor<>(),
                new StaticDataConverterSaver<>("school", School.class, new SchoolConverter()));

        result.put(VkontakteUniversitiesRequest.class,
                new VkontakteUniversitiesConnection(vkontakteHttpConnection),
                new FacultiesVisitor(),
                new StaticDataConverterSaver<>("university", University.class, new UniversityConverter()));

        result.put(VkontakteFacultiesRequest.class,
                new VkontakteFacultiesConnection(vkontakteHttpConnection),
                new ChairsVisitor(),
                new StaticDataConverterSaver<>("faculty", Faculty.class, new FacultyConverter()));

        result.put(VkontakteChairsRequest.class,
                new VkontakteChairsConnection(vkontakteHttpConnection),
                new NoOpVisitor<>(),
                new StaticDataConverterSaver<>("chair", Chair.class, new ChairConverter()));

        return result;
    }

    public static void main(String[] args) throws IOException {
        new VkStaticDataCrawler().downloadData();
    }

}
