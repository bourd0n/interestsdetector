package ru.ispras.crawler.ssgps.vkontakte.staticloader.converter;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.IdTitle;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.University;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteIdRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteUniversitiesRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 24.06.14
 * Time: 17:25
 */
public class UniversityConverter implements IResponseConverter<VkontakteUniversitiesRequest, VkontakteIdRequest, IdTitle, University> {
    @Override
    public VkontakteIdRequest getRequest(VkontakteUniversitiesRequest vkontakteUniversitiesRequest, IdTitle response) {
        return new VkontakteIdRequest(response.getId());
    }

    @Override
    public BasicResponse<University> getResponse(VkontakteUniversitiesRequest request, IdTitle response) {
        return new BasicResponse<>(new University(response.getId(), response.getTitle(), request.getCityId()));
    }
}
