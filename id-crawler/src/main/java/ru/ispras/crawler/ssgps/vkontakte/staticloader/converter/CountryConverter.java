package ru.ispras.crawler.ssgps.vkontakte.staticloader.converter;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.IdTitle;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteCountriesRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteIdRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 24.06.14
 * Time: 17:24
 */
public class CountryConverter implements IResponseConverter<VkontakteCountriesRequest, VkontakteIdRequest, IdTitle, IdTitle> {
    @Override
    public VkontakteIdRequest getRequest(VkontakteCountriesRequest vkontakteCountriesRequest, IdTitle response) {
        return new VkontakteIdRequest(response.getId());
    }

    @Override
    public BasicResponse<IdTitle> getResponse(VkontakteCountriesRequest vkontakteCountriesRequest, IdTitle response) {
        return new BasicResponse<>(response);
    }
}
