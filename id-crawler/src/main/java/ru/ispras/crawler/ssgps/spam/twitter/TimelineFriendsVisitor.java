package ru.ispras.crawler.ssgps.spam.twitter;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFriendsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserTimelineRequest;
import ru.ispras.crawler.core.visitor.AbstractNonFailingVisitor;

public class TimelineFriendsVisitor extends
		AbstractNonFailingVisitor<TwitterUserTimelineFriendsRequest, GroupingResponse> {
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<? extends IRequest> visit(
			TwitterUserTimelineFriendsRequest request, GroupingResponse response) {
		BasicResponse<List<Long>> friendsIds = (BasicResponse<List<Long>>) response
				.getResponse(TwitterUserFriendsRequest.class);
		Collection<TwitterUserTimelineRequest> result = new LinkedList<TwitterUserTimelineRequest>();
		for (Long id : friendsIds.getObject()) {
			result.add(new TwitterUserTimelineRequest(id));
		}
		return result;
	}

	@Override
	public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
		return Collections.singleton(TwitterUserTimelineRequest.class);
	}
}
