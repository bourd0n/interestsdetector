package ru.ispras.crawler.ssgps.staticloader.factory;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.IdTitle;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.School;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteIdRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteSchoolsRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 24.06.14
 * Time: 17:25
 */
public class SchoolFactory implements IResponseFactory<VkontakteSchoolsRequest, VkontakteIdRequest, IdTitle, School> {
    @Override
    public VkontakteIdRequest getRequest(VkontakteSchoolsRequest vkontakteCountriesRequest, IdTitle response) {
        return new VkontakteIdRequest(response.getId());
    }

    @Override
    public BasicResponse<School> getResponse(VkontakteSchoolsRequest request, IdTitle response) {
        return new BasicResponse<>(new School(response.getId(), response.getTitle(), request.getCityId()));
    }
}
