package ru.ispras.crawler.ssgps.gcd.hunch;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserFollowersRequest;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserFollowingRequest;
import ru.ispras.crawler.core.visitor.graph.IResponseNeighbourExtractor;

public class GCDHunchResponseNeighbourExtractor implements IResponseNeighbourExtractor<GroupingResponse, String> {
	@SuppressWarnings("unchecked")
	@Override
	public Collection<String> getNeighbours(GroupingResponse response) {
		BasicResponse<List<String>> followers = (BasicResponse<List<String>>) response
				.getResponse(HunchUserFollowersRequest.class);
		BasicResponse<List<String>> following = (BasicResponse<List<String>>) response
				.getResponse(HunchUserFollowingRequest.class);
		Collection<String> result = new LinkedList<String>();
		result.addAll(followers.getObject());
		result.addAll(following.getObject());
		return result;
	}
}
