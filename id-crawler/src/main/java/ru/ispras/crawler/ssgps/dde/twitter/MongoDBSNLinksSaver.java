package ru.ispras.crawler.ssgps.dde.twitter;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.saver.ISaver;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * Saves link between two social networks. Link is a two ids in two target networks (named first and second)
 * @param <Req>
 * @param <Resp>
 */
public abstract class MongoDBSNLinksSaver<Req extends IRequest, Resp extends IResponse> implements
		ISaver<Req, Resp> {

    private final String firstSNFieldName;
	private final String secondSNFieldName;
    private final DBCollection collection;

    /**
     *
     * @param collection that stores links for two target SNs
     * @param firstSNFieldName user id in the first SN will be stored in this field
     * @param secondSNFieldName user id in the second SN will be stored in this field
     */
    public MongoDBSNLinksSaver(DBCollection collection, String firstSNFieldName, String secondSNFieldName){
        this.firstSNFieldName = firstSNFieldName;
        this.secondSNFieldName = secondSNFieldName;
        this.collection = collection;
    }

	@Override
	public void save(Req request, Resp response) {
		DBObject object = new BasicDBObject(firstSNFieldName, getFirstSNUserId(request, response));
		object.put(secondSNFieldName, getSecondSNUserId(request, response));
		collection.save(object);
	}

    /**
     * Extracts user id in the second social network
     * @param request
     * @param response
     * @return
     */
    protected abstract Object getSecondSNUserId(Req request, Resp response);

    /**
     * Extracts user id in the first social network
     * @param request
     * @param response
     * @return
     */
    protected abstract Object getFirstSNUserId(Req request, Resp response);
}
