package ru.ispras.crawler.ssgps.staticloader.factory;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.City;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteCitiesRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteIdRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 24.06.14
 * Time: 16:15
 */
public class CityFactory implements IResponseFactory<VkontakteCitiesRequest, VkontakteIdRequest, City, City> {
    @Override
    public VkontakteIdRequest getRequest(VkontakteCitiesRequest vkontakteCitiesRequest, City response) {
        return new VkontakteIdRequest(response.getId());
    }

    @Override
    public BasicResponse<City> getResponse(VkontakteCitiesRequest vkontakteCitiesRequest, City city) {
        return new BasicResponse<>(city.setCountry(vkontakteCitiesRequest.getCountryId()));
    }
}
