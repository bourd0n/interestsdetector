package ru.ispras.crawler.ssgps.vkontakte.staticloader.converter;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.Chair;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.IdTitle;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteChairsRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteIdRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 24.06.14
 * Time: 17:23
 */
public class ChairConverter implements IResponseConverter<VkontakteChairsRequest, VkontakteIdRequest, IdTitle, Chair> {
    @Override
    public VkontakteIdRequest getRequest(VkontakteChairsRequest vkontakteChairsRequest, IdTitle response) {
        return new VkontakteIdRequest(response.getId());
    }

    @Override
    public BasicResponse<Chair> getResponse(VkontakteChairsRequest request, IdTitle response) {
        return new BasicResponse<>(new Chair(response.getId(), response.getTitle(), request.getFacultyId()));
    }
}
