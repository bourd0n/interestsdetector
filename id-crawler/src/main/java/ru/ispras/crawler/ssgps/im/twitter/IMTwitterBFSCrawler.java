package ru.ispras.crawler.ssgps.im.twitter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;
import ru.ispras.crawler.core.visitor.IVisitor;
import ru.ispras.crawler.core.visitor.graph.SamplingVisitor;
import ru.ispras.crawler.core.visitor.graph.sampler.BreadthFirstSampler;
import ru.ispras.crawler.ssgps.CrawlerSSGPSConfiguration;
import ru.ispras.modis.utils.config.validation.ConfigurationProperty;

public class IMTwitterBFSCrawler extends AbstractIMTwitterCrawler {
	
	@ConfigurationProperty
	private static String limitsProperty = "limit";
	
	public static void main(String[] args) throws IOException {
		if (args.length < 3) {
			System.out.println("Usage: <oauth config file> <seed user id> <depth> {timeline limit}");
			return;
		}

		int timelineLimit = Integer.MAX_VALUE;
		if (args.length >= 4) {
			timelineLimit = Integer.parseInt(args[3]);
		}

		IMTwitterBFSCrawler crawler = new IMTwitterBFSCrawler(Integer.parseInt(args[2]), timelineLimit);
		crawler.crawl(args[0], Long.parseLong(args[1]), crawler.getNeighbourLimit(), timelineLimit);
	}

	protected final int depth;
	protected final Map<Integer, Integer> depth2limit;
	protected final Integer timelineLimit;
	protected final Integer neighbourLimit;

	public Integer getNeighbourLimit() {
		return neighbourLimit;
	}

	public IMTwitterBFSCrawler(int depth, Integer timelineLimit) {
		this.depth = depth;
		this.timelineLimit = timelineLimit;
		this.depth2limit = readDepthMap();
		this.neighbourLimit = calculateNeighbourLimit();
	}

	private Map<Integer, Integer> readDepthMap() {
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		
		for (int i = 0; i<depth; i++){
			String scope = String.valueOf(i);
			Integer limit = CrawlerSSGPSConfiguration.getConfiguration().getIntProperty(IMTwitterBFSCrawler.class, limitsProperty, scope);
			
			map.put(i, limit);
		}
		
		return map;
	}

	@Override
	protected IVisitor<IMTwitterRequest, GroupingResponse> getNeighbourVisitor() {
		return new SamplingVisitor<IMTwitterRequest, GroupingResponse, Long>(
				new BreadthFirstSampler<Long>(depth, depth2limit),
				new IMTwitterRequestIdConverter(neighbourLimit, timelineLimit),
				new IMTwitterResponseNeighbourExtractor(),
				IMTwitterRequest.class);
	}

	private Integer calculateNeighbourLimit() {
		Integer limit = 0;
		for (int d=0; d<depth; d++){
			limit = Math.max(limit, depth2limit.get(d));
		}
		return limit == Integer.MAX_VALUE ? null : limit;
	}
}
