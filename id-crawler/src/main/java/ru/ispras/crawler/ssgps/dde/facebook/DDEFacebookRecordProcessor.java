package ru.ispras.crawler.ssgps.dde.facebook;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.ispras.crawler.core.datamodel.facebook.entities.Profile;
import ru.ispras.crawler.ssgps.common.IObjectProcessor;

public class DDEFacebookRecordProcessor implements IObjectProcessor<Profile> {
	
	private final Map<Long, String> twitterLinks;
	private final Pattern twitterLink = Pattern.compile("^(http|https)://twitter.com/([^\\s]+)");
	
	public Map<Long, String> getTwitterLinks() {
		return twitterLinks;
	}

	public DDEFacebookRecordProcessor() {
		twitterLinks = new HashMap<Long, String>();
	}

	private String getTwitterScreenName(String link) {
		Matcher m = twitterLink.matcher(link);
		if(m.find()) {
			return m.group(2);
		}
		return null;
	}
	
	@Override
	public void processObject(Profile profile) {
		
		String twitter = profile.getTwitter();
		String website = profile.getWebsite();
		
		if (twitter != null) {
			twitterLinks.put(profile.getId(), twitter);
			return;
		}
		
		if (website != null) {
			String screenName = getTwitterScreenName(website);
			if (screenName != null) {
				twitterLinks.put(profile.getId(), screenName);
			}
		}
	}

}
