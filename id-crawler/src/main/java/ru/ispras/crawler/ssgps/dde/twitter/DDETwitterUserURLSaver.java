package ru.ispras.crawler.ssgps.dde.twitter;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserProfileRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.User;
import ru.ispras.crawler.core.saver.ISaver;

public class DDETwitterUserURLSaver
		implements ISaver<TwitterUserProfileRequest, BasicResponse<User>> {
	private final ISaver<? super TwitterUserProfileRequest, ? super BasicResponse<User>> wrapped;

	public DDETwitterUserURLSaver(ISaver<? super TwitterUserProfileRequest, ? super BasicResponse<User>> wrapped) {
		this.wrapped = wrapped;
	}

	@Override
	public void save(TwitterUserProfileRequest request, BasicResponse<User> response) {
		User profile = response.getObject();
		if (DDETwitterRequestFromURLExtractor.checkProfileForURL(profile)){
            wrapped.save(request, response);
        }
	}
}
