package ru.ispras.crawler.ssgps.common;


public interface IObjectProcessor<T> {
	
	public void processObject(T object);
	
}
