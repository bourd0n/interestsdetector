package ru.ispras.crawler.ssgps.vkontakte;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.saver.ISaver;
import ru.ispras.crawler.ssgps.vkontakte.staticloader.converter.IResponseConverter;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 20.06.14
 * Time: 15:32
 */
public class ConverterSaver<ReqIn extends IRequest, ReqOut extends IRequest, Resp, RespOut> implements ISaver<ReqIn, BasicResponse<List<Resp>>> {
    private final ISaver<ReqOut, BasicResponse<RespOut>> saver;
    private final IResponseConverter<ReqIn, ReqOut, Resp, RespOut> factory;

    public ConverterSaver(ISaver<ReqOut, BasicResponse<RespOut>> saver, IResponseConverter<ReqIn, ReqOut, Resp, RespOut> factory) {
        this.saver = saver;
        this.factory = factory;
    }

    @Override
    public void save(ReqIn request, BasicResponse<List<Resp>> response) {
        for (Resp resp: response.getObject()){
            saver.save(factory.getRequest(request, resp), factory.getResponse(request, resp));
        }
    }
}
