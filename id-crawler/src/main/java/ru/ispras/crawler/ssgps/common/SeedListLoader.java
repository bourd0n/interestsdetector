package ru.ispras.crawler.ssgps.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SeedListLoader {

	/**
	 * Reads list of numeric IDs from file
	 * @param fileName - path to input file 
	 * @return
	 * @throws FileNotFoundException
	 */
	public static List<Long> loadSeeds(String fileName) throws FileNotFoundException {
		List<Long> result = new ArrayList<Long>();

		Scanner scn = new Scanner(new FileInputStream(fileName));
		while (scn.hasNext()) {
			result.add(Long.parseLong(scn.nextLine()));
		}
		scn.close();
		return result;
	}

}
