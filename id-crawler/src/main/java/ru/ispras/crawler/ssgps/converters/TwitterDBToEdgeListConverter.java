package ru.ispras.crawler.ssgps.converters;

import java.io.FileWriter;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;


public class TwitterDBToEdgeListConverter {

	private static final Logger logger = Logger.getLogger(TwitterDBToEdgeListConverter.class);
	
	enum Direction{
		TO_SEED, FROM_SEED;
	}
	
	final Mongo mongo;
	final DB db;
	Set<Long> users = new HashSet<Long>();
	
	public TwitterDBToEdgeListConverter() throws UnknownHostException{
		mongo = new Mongo();
		db = mongo.getDB("twitter");
	}
	
	public void convert(String outputBurned, String outputAll) throws IOException{
		FileWriter fwBurned = new FileWriter(outputBurned);
		FileWriter fwAll = new FileWriter(outputAll);
		
		performCollection(fwBurned, fwAll, "followers",Direction.TO_SEED);
		performCollection(fwBurned, fwAll, "friends",Direction.FROM_SEED);
				
		fwAll.close();
		fwBurned.close();
	}
	
	private void performCollection(FileWriter fwBurned, FileWriter fwAll, String collectionName, Direction direction) throws IOException{
		DBCursor cursor = db.getCollection(collectionName).find().addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		int count = 0;
		
		while (cursor.hasNext()){
			
			if (++count % 1000 == 0){
				logger.info(count+" user's "+collectionName+" lists has been performed");
				fwBurned.flush();
				fwAll.flush();
			}
			
			DBObject object = cursor.next();
			Long seedId = (Long)object.get("userId");
			
			@SuppressWarnings("unchecked")
			List<Long> list = (List<Long>)object.get(collectionName);
			
			writeEdges(seedId, list, fwBurned, fwAll, direction);
		}
	}
	
	private void writeEdges(Long seedId, List<Long> list, FileWriter fwBurned, FileWriter fwAll, Direction direction) throws IOException{
		for (int i=0;i<list.size();i++){
			long x = list.get(i);
			String out = (direction==Direction.TO_SEED) ? (x+" "+seedId+"\n") : (seedId+" "+x+"\n"); 
			if (users.contains(x)){
				fwBurned.write(out);
			}
			fwAll.write(out);
		}
	}
	
	public void loadSet(){
		DBCursor cursor = db.getCollection("followers").find().addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		
		int count=0;
		
		while (cursor.hasNext()){
			
			if (++count % 10000 == 0){
				 logger.info(count+" user's ids loaded");
			}
			
			Long id = (Long)cursor.next().get("userId");
			users.add(id);
		}
	}
	

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		if (args.length<2){
			System.out.println("Usage: <only burned users file name> <all users file name>");
		}
		TwitterDBToEdgeListConverter experiment = new TwitterDBToEdgeListConverter();
		experiment.loadSet();
		logger.info("Users set has been loaded.");
		experiment.convert(args[0],args[1]);
		logger.info("Finished.");

	}

}