package ru.ispras.crawler.ssgps.dde.facebook;

import java.net.UnknownHostException;
import java.util.Map.Entry;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.facebook.FacebookUserProfileRequest;
import ru.ispras.crawler.core.datamodel.facebook.entities.Profile;
import ru.ispras.crawler.core.requestidconverter.facebook.FacebookUserProfileRequestIdConverter;
import ru.ispras.crawler.core.saver.ILoader;
import ru.ispras.crawler.core.saver.mongodb.MongoDBSaverLoader;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.JacksonMongoDBResponseConverter;
import ru.ispras.crawler.ssgps.common.IObjectProcessor;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DB;
import com.mongodb.Mongo;

public class FacebookMongoDBReader {

	final Mongo mongo;
	final DB db;

	public FacebookMongoDBReader(String dbName) throws UnknownHostException {
		mongo = new Mongo();
		db = mongo.getDB(dbName);
	}

	/**
	 * Find all entries in {@code collectionName} and execute processor.processRecord()
	 *  for every found record.  
	 * @param collectionName
	 * @param processor
	 */
	public void findAll(String collectionName, IObjectProcessor<Profile> processor) {
		
		ILoader<FacebookUserProfileRequest, BasicResponse<Profile>> loader = new MongoDBSaverLoader<>(
				db.getCollection(collectionName), 
				new FacebookUserProfileRequestIdConverter(), 
				new JacksonMongoDBResponseConverter<BasicResponse<Profile>>(new TypeReference<BasicResponse<Profile>>() {}));
		
		for (Entry<FacebookUserProfileRequest, BasicResponse<Profile>> entry: loader.load()){
			processor.processObject(entry.getValue().getObject());
		}
	}
}
