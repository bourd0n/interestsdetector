package ru.ispras.crawler.ssgps.gcd.hunch;

import java.io.IOException;

import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;
import ru.ispras.crawler.core.visitor.IVisitor;
import ru.ispras.crawler.core.visitor.graph.SamplingVisitor;
import ru.ispras.crawler.core.visitor.graph.sampler.BreadthFirstSampler;

public class GCDHunchBFSCrawler extends AbstractGCDHunchCrawler {
	public static void main(String[] args) throws IOException {
		if (args.length < 3) {
			System.out.println("Usage: <auth config file> <seed user screenName> <depth>");
			return;
		}
		new GCDHunchBFSCrawler(Integer.parseInt(args[2])).crawl(args[0], args[1]);
	}

	private final int depth;

	public GCDHunchBFSCrawler(int depth) {
		this.depth = depth;
	}

	@Override
	protected IVisitor<GCDHunchUserRequest, GroupingResponse> getNeighbourVisitor() {
		return new SamplingVisitor<GCDHunchUserRequest, GroupingResponse, String>(
				new BreadthFirstSampler<String>(depth),
				new GCDHunchUserRequestIdConverter(),
				new GCDHunchResponseNeighbourExtractor(),
				GCDHunchUserRequest.class);
	}
}
