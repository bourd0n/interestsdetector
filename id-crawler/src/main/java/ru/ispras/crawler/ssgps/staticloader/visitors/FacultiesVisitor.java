package ru.ispras.crawler.ssgps.staticloader.visitors;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.IdTitle;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteFacultiesRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteUniversitiesRequest;
import ru.ispras.crawler.core.visitor.AbstractNonFailingVisitor;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 05.06.14
 * Time: 17:54
 */
public class FacultiesVisitor  extends AbstractNonFailingVisitor<VkontakteUniversitiesRequest, BasicResponse<List<IdTitle>>> {
    @Override
    public Collection<? extends IRequest> visit(VkontakteUniversitiesRequest request, BasicResponse<List<IdTitle>> response) {
        List<IdTitle> universities = response.getObject();
        ArrayList<IRequest> requests = new ArrayList<>();
        for (IdTitle university: universities) {
            requests.add(new VkontakteFacultiesRequest(university.getId()));
        }
        return requests;
    }

    @Override
    public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
        return Collections.singleton(VkontakteFacultiesRequest.class);
    }
}
