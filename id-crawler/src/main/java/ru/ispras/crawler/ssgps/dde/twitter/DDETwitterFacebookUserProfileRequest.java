package ru.ispras.crawler.ssgps.dde.twitter;

import ru.ispras.crawler.core.datamodel.facebook.FacebookUserProfileRequest;

/**
 * 
 * @author andrey g
 *
 */
public class DDETwitterFacebookUserProfileRequest extends FacebookUserProfileRequest {

	private final long twitterId;
	
	public DDETwitterFacebookUserProfileRequest(long id, long twitterId) {
		super(id);
		this.twitterId =twitterId; 
	}

	public DDETwitterFacebookUserProfileRequest(String screenName, long twitterId) {
		super(screenName);
		this.twitterId =twitterId; 
	}

	public long getTwitterId(){
		return twitterId;
	}

}
