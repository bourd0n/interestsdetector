package ru.ispras.crawler.ssgps.im.twitter;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFollowersRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFriendsRequest;
import ru.ispras.crawler.core.visitor.graph.IResponseNeighbourExtractor;

public class IMTwitterResponseNeighbourExtractor implements IResponseNeighbourExtractor<GroupingResponse, Long> {
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Long> getNeighbours(GroupingResponse response) {
		BasicResponse<List<Long>> followers = (BasicResponse<List<Long>>) response
				.getResponse(TwitterUserFollowersRequest.class);
		BasicResponse<List<Long>> following = (BasicResponse<List<Long>>) response
				.getResponse(TwitterUserFriendsRequest.class);
		Collection<Long> result = new LinkedList<Long>();
		if (followers != null) {
			result.addAll(followers.getObject());
		}
		if (following != null) {
			result.addAll(following.getObject());
		}
		return result;
	}
}
