package ru.ispras.crawler.ssgps.common;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.saver.ISaver;

public abstract class ConnectionBasedMetropolisHastingsCrawler<UsReq extends IRequest, UsResp extends IResponse, FrReq extends IRequest, FrResp extends IResponse>
		extends MetropolisHastingsCrawler {
	private Set<Long> visited;
	private IConnection<? super FrReq, FrResp> friendsDownloader;
	private IConnection<? super UsReq, UsResp> userInfoDownloader;

	private ISaver<? super UsReq, ? super UsResp> userInfoSaver;
	private ISaver<? super FrReq, ? super FrResp> friendsSaver;
	
	private Map<Long, Integer> userToNumberOfFriends;

	public ConnectionBasedMetropolisHastingsCrawler(
			int numberOfTransitions,
			IConnection<? super UsReq, UsResp> userInfoDownloader,
			ISaver<? super UsReq, ? super UsResp> userInfoSaver,
			IConnection<? super FrReq, FrResp> friendsDownloader,
			ISaver<? super FrReq, ? super FrResp> friendsSaver) {
		super(numberOfTransitions);
		
		this.userToNumberOfFriends = new HashMap<>();
		
		this.userInfoDownloader = userInfoDownloader;
		this.userInfoSaver = userInfoSaver;

		this.friendsDownloader = friendsDownloader;
		this.friendsSaver = friendsSaver;
		
		this.visited = new HashSet<>();
	}

	@Override
	protected List<Long> downloadUserFriends(long user) throws DataDownloadException {
		final FrReq request = createFriendsRequest(user);
		FrResp response;
		try {
			response = friendsDownloader.getResponse(request);
		} catch (UnavailableConnectionException | RuntimeException e) {
			throw new DataDownloadException(e);
		}
		extractNumberOfFriends(request, response);
		friendsSaver.save(request, response);
		return convertFriendsToLongList(response);
	}
	
	@Override
	protected void downloadUserInfo(long user) throws DataDownloadException {
		if (!visited.contains(user)) {
			final UsReq request = createUserInfoRequest(user);
			UsResp response;
			try {
				response = userInfoDownloader.getResponse(request);
			} catch (UnavailableConnectionException | RuntimeException e) {
				throw new DataDownloadException(e);
			}
			extractNumberOfFriends(request, response);
			userInfoSaver.save(request, response);
			visited.add(user);
		}
	}

	@Override
	protected Integer getNumberOfFriends(long user) {
		return userToNumberOfFriends.get(user);
	}
	
	protected void saveNumberOfFriends(long user, int numberOfFriends) {
		userToNumberOfFriends.put(user, numberOfFriends);
	}
	
	/**
	 * Extract and save number of friends from request. 
	 */
	protected abstract void extractNumberOfFriends(IRequest request, IResponse response);
	
	/**
	 * Extract user ids from friends response. 
	 */
	protected abstract List<Long> convertFriendsToLongList(FrResp response);

	/**
	 * Create friends downloading request for the given user. 
	 */
	protected abstract FrReq createFriendsRequest(long user);

	/**
	 * Create request for downloading user information. 
	 */
	protected abstract UsReq createUserInfoRequest(long user);
}
