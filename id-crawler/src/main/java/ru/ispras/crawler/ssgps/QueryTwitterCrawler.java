package ru.ispras.crawler.ssgps;

import java.util.List;

import ru.ispras.crawler.core.connection.DelayedConnection;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterSearchTweetsConnection;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionFactory;
import ru.ispras.crawler.core.connection.utils.BatchConfigurableConnectionFactoryUtils;
import ru.ispras.crawler.core.connection.utils.ConnectionConfigurationReader;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterSearchTweetsRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.Status;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterSearchTweetsRequestIdConverter;
import ru.ispras.crawler.core.saver.ISaver;
import ru.ispras.crawler.core.saver.mongodb.MongoDBSaverLoader;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.ChunkingMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.JacksonMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.responsechunker.BasicResponseChunker;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DB;
import com.mongodb.Mongo;

public class QueryTwitterCrawler {
	public static void main(String[] args) throws Exception {
		if (args.length < 2) {
			System.out.println("Usage: <oauth config file> <query> {limit}");
			return;
		}

		Integer limit = null;
		if (args.length >= 3) {
			limit = Integer.parseInt(args[2]);
		}

		new QueryTwitterCrawler().crawl(args[0], args[1], limit);
	}

	public void crawl(String oauthConfig, String query, Integer limit) throws Exception {
		IConnection<HttpRequest, HttpResponse> twitterHttpConnection = new DelayedConnection<>(
				BatchConfigurableConnectionFactoryUtils.create(new TwitterHttpConnectionFactory(),
						ConnectionConfigurationReader.getConfigurations(TwitterHttpConnectionConfiguration.class,
								oauthConfig)));
		TwitterSearchTweetsConnection queryManager = new TwitterSearchTweetsConnection(twitterHttpConnection);

		DB db = new Mongo().getDB("query-twitter");
		ISaver<TwitterSearchTweetsRequest, BasicResponse<List<Status>>> saver = new MongoDBSaverLoader<>(
				db.getCollection("tweets"),
				new TwitterSearchTweetsRequestIdConverter(),
				new ChunkingMongoDBResponseConverter<>(
						new BasicResponseChunker<Status>(),
						new JacksonMongoDBResponseConverter<>(new TypeReference<BasicResponse<List<Status>>>() {})));

		TwitterSearchTweetsRequest request = new TwitterSearchTweetsRequest(query, limit);
		BasicResponse<List<Status>> response = queryManager.getResponse(request);
		saver.save(request, response);
	}
}
