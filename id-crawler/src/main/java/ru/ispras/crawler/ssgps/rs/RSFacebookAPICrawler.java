package ru.ispras.crawler.ssgps.rs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import ru.ispras.crawler.core.Crawler;
import ru.ispras.crawler.core.connection.DelayedConnection;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.facebook.api.FacebookAPIFriendsConnection;
import ru.ispras.crawler.core.connection.facebook.api.FacebookAPIMutualFriendsConnection;
import ru.ispras.crawler.core.connection.facebook.api.FacebookAPIPhotoConnection;
import ru.ispras.crawler.core.connection.facebook.api.FacebookAPIProfileConnection;
import ru.ispras.crawler.core.connection.facebook.api.FacebookAPIVideoConnection;
import ru.ispras.crawler.core.connection.facebook.api.FacebookAPIWallConnection;
import ru.ispras.crawler.core.connection.facebook.api.http.FacebookAPIHttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.facebook.api.http.FacebookAPIHttpConnectionFactory;
import ru.ispras.crawler.core.connection.utils.BatchConfigurableConnectionFactoryUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIFriendsRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIMutualFriendsRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIPhotoRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIProfileRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIVideoRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIWallRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.Photo;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.Post;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.User;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.Video;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.requestidconverter.facebook.api.FacebookAPIFriendsRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.facebook.api.FacebookAPIMutualFriendsRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.facebook.api.FacebookAPIPhotoRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.facebook.api.FacebookAPIProfileRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.facebook.api.FacebookAPIVideoRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.facebook.api.FacebookAPIWallRequestIdConverter;
import ru.ispras.crawler.core.saver.CompositeSaver;
import ru.ispras.crawler.core.saver.ISaver;
import ru.ispras.crawler.core.saver.mongodb.MongoDBSaverLoader;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.ChunkingMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.JacksonMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.responsechunker.BasicResponseChunker;
import ru.ispras.crawler.core.visitor.NoOpVisitor;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DB;
import com.mongodb.Mongo;

public class RSFacebookAPICrawler {
	private int wallLimit;

	public RSFacebookAPICrawler() {
		this(100);
	}

	public RSFacebookAPICrawler(int wallLimit) {
		this.wallLimit = wallLimit;
	}

	public RSInMemoryStorage downloadDataOnline(long seed, String accessToken) {
		RSInMemoryStorage inMemoryStorage = new RSInMemoryStorage();
		downloadData(inMemoryStorage, null, seed, accessToken);
		return inMemoryStorage;
	}

	public void downloadDataMongoDB(long seed, String accessToken) throws IOException {
		downloadData(null, getDB(seed), seed, accessToken);
	}

	public RSInMemoryStorage downloadDataOnlineAndMongoDB(long seed, String accessToken) throws IOException {
		RSInMemoryStorage inMemoryStorage = new RSInMemoryStorage();
		downloadData(inMemoryStorage, getDB(seed), seed, accessToken);
		return inMemoryStorage;
	}

	private DB getDB(long seedId) throws IOException {
		return new Mongo().getDB("RS"+Long.toString(seedId));
	}

	private void downloadData(RSInMemoryStorage inMemoryStorage, DB db, long seed, String accessToken) {
		FacebookAPIHttpConnectionConfiguration config = new FacebookAPIHttpConnectionConfiguration(accessToken);
		Crawler crawler = new Crawler(createCrawlerConfig(config, inMemoryStorage, db, wallLimit), 1);
		crawler.crawl(Collections.singleton(new FacebookAPIFriendsRequest(seed, "limit=5000", 1)));
		crawler.awaitTermination(3, 3);
		crawler.shutdown();
		System.out.println("Finished");
	}

	private Crawler.Configuration createCrawlerConfig(FacebookAPIHttpConnectionConfiguration config,
			RSInMemoryStorage inMemoryStorage, DB db, int wallSize) {
		IConnection<HttpRequest, HttpResponse> facebookAPIHttpConnection = new DelayedConnection<>(
				BatchConfigurableConnectionFactoryUtils.create(new FacebookAPIHttpConnectionFactory(),
						Arrays.asList(config)));

		Crawler.Configuration result = new Crawler.Configuration();
		result.put(FacebookAPIProfileRequest.class,
				new FacebookAPIProfileConnection(facebookAPIHttpConnection),
				new NoOpVisitor<>(),
				getProfileSaver(inMemoryStorage, db));
		result.put(FacebookAPIFriendsRequest.class,
				new FacebookAPIFriendsConnection(facebookAPIHttpConnection),
				new RSFriendsVisitor(wallSize),
				getFriendsSaver(inMemoryStorage, db));
		result.put(FacebookAPIMutualFriendsRequest.class,
				new FacebookAPIMutualFriendsConnection(
				facebookAPIHttpConnection),
				new NoOpVisitor<>(),
				getMutualFriendsSaver(inMemoryStorage, db));
		result.put(FacebookAPIWallRequest.class,
				new FacebookAPIWallConnection(facebookAPIHttpConnection),
				new RSWallVisitor(),
				getWallSaver(inMemoryStorage, db));
		result.put(FacebookAPIPhotoRequest.class,
				new FacebookAPIPhotoConnection(facebookAPIHttpConnection),
				new NoOpVisitor<>(),
				getPhotoSaver(inMemoryStorage, db));
		result.put(FacebookAPIVideoRequest.class,
				new FacebookAPIVideoConnection(facebookAPIHttpConnection),
				new NoOpVisitor<>(),
				getVideoSaver(inMemoryStorage, db));
		return result;
	}

	private ISaver<FacebookAPIProfileRequest, BasicResponse<User>> getProfileSaver(RSInMemoryStorage inMemoryStorage,
			DB db) {
		List<ISaver<FacebookAPIProfileRequest, BasicResponse<User>>> savers = new ArrayList<>();
		if (inMemoryStorage != null) {
			savers.add(inMemoryStorage.getProfileSaver());
		}
		if (db != null) {
			savers.add(new MongoDBSaverLoader<>(
					db.getCollection("profile"),
					new FacebookAPIProfileRequestIdConverter(),
					new JacksonMongoDBResponseConverter<>(
							new TypeReference<BasicResponse<User>>() {})));
		}
		return new CompositeSaver<>(savers);
	}

	private ISaver<FacebookAPIFriendsRequest, BasicResponse<List<Long>>> getFriendsSaver(RSInMemoryStorage inMemoryStorage,
			DB db) {
		List<ISaver<FacebookAPIFriendsRequest, BasicResponse<List<Long>>>> savers = new ArrayList<>();
		if (inMemoryStorage != null) {
			savers.add(inMemoryStorage.getFriendsSaver());
		}
		if (db != null) {
			savers.add(new MongoDBSaverLoader<>(
					db.getCollection("friends"),
					new FacebookAPIFriendsRequestIdConverter(),
					new ChunkingMongoDBResponseConverter<>(
							new BasicResponseChunker<Long>(100000),
							new JacksonMongoDBResponseConverter<>(
									new TypeReference<BasicResponse<List<Long>>>() {}))));
		}
		return new CompositeSaver<>(savers);
	}

	private ISaver<FacebookAPIMutualFriendsRequest, BasicResponse<List<Long>>> getMutualFriendsSaver(
			RSInMemoryStorage inMemoryStorage, DB db) {
		List<ISaver<FacebookAPIMutualFriendsRequest, BasicResponse<List<Long>>>> savers = new ArrayList<>();
		if (inMemoryStorage != null) {
			savers.add(inMemoryStorage.getMutualFriendsSaver());
		}
		if (db != null) {
			savers.add(new MongoDBSaverLoader<>(
					db.getCollection("friends"),
					new FacebookAPIMutualFriendsRequestIdConverter(),
					new ChunkingMongoDBResponseConverter<>(
							new BasicResponseChunker<Long>(100000),
							new JacksonMongoDBResponseConverter<>(
									new TypeReference<BasicResponse<List<Long>>>() {}))));
		}
		return new CompositeSaver<>(savers);
	}

	private ISaver<FacebookAPIWallRequest, BasicResponse<List<Post>>> getWallSaver(RSInMemoryStorage inMemoryStorage, DB db) {
		List<ISaver<FacebookAPIWallRequest, BasicResponse<List<Post>>>> savers = new ArrayList<>();
		if (inMemoryStorage != null) {
			savers.add(inMemoryStorage.getWallSaver());
		}
		if (db != null) {
			savers.add(new MongoDBSaverLoader<>(db.getCollection("wall"),
					new FacebookAPIWallRequestIdConverter(),
					new ChunkingMongoDBResponseConverter<>(
							new BasicResponseChunker<Post>(),
							new JacksonMongoDBResponseConverter<>(
									new TypeReference<BasicResponse<List<Post>>>() {}))));
		}
		return new CompositeSaver<>(savers);
	}

	private ISaver<FacebookAPIPhotoRequest, BasicResponse<Photo>> getPhotoSaver(RSInMemoryStorage inMemoryStorage, DB db) {
		List<ISaver<FacebookAPIPhotoRequest, BasicResponse<Photo>>> savers = new ArrayList<>();
		if (inMemoryStorage != null) {
			savers.add(inMemoryStorage.getPhotoSaver());
		}
		if (db != null) {
			savers.add(new MongoDBSaverLoader<>(
					db.getCollection("photo"),
					new FacebookAPIPhotoRequestIdConverter(),
					new JacksonMongoDBResponseConverter<>(
							new TypeReference<BasicResponse<Photo>>() {})));
		}
		return new CompositeSaver<>(savers);
	}

	private ISaver<FacebookAPIVideoRequest, BasicResponse<Video>> getVideoSaver(RSInMemoryStorage inMemoryStorage, DB db) {
		List<ISaver<FacebookAPIVideoRequest, BasicResponse<Video>>> savers = new ArrayList<>();
		if (inMemoryStorage != null) {
			savers.add(inMemoryStorage.getVideoSaver());
		}
		if (db != null) {
			savers.add(new MongoDBSaverLoader<>(
					db.getCollection("video"),
					new FacebookAPIVideoRequestIdConverter(),
					new JacksonMongoDBResponseConverter<>(
							new TypeReference<BasicResponse<Video>>() {})));
		}
		return new CompositeSaver<>(savers);
	}

	public static void main(String[] args) throws IOException {
		if (args.length < 2) {
			System.out.println("parameters: seed_id, token");
			return;
		}
		long seedId = -1;
		try {
			seedId = Long.parseLong(args[0]);
		} catch (NumberFormatException e) {
			System.out.println("seed_id should be a number");
			return;
		}

		String token = args[1];

		RSFacebookAPICrawler crawler = new RSFacebookAPICrawler();
		crawler.downloadDataMongoDB(seedId, token);
	}
}
