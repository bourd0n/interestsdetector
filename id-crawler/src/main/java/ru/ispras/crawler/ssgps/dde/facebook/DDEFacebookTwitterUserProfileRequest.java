package ru.ispras.crawler.ssgps.dde.facebook;

import ru.ispras.crawler.core.datamodel.twitter.TwitterUserProfileRequest;


public class DDEFacebookTwitterUserProfileRequest extends TwitterUserProfileRequest {

	private final long facebookId;
	
	public DDEFacebookTwitterUserProfileRequest(long twitterId, long facebookId) {
		super(twitterId);
		this.facebookId = facebookId; 
	}

	public DDEFacebookTwitterUserProfileRequest(String twitterScreenName, long facebookId) {
		super(twitterScreenName);
		this.facebookId = facebookId;
	}

	public long getFacebookId() {
		return facebookId;
	}
}
