package ru.ispras.crawler.ssgps.im.twitter.links;

import ru.ispras.crawler.ssgps.im.twitter.IMTwitterRequest;
import ru.ispras.crawler.ssgps.im.twitter.IMTwitterRequestIdConverter;

public class LinkTwitterRequestIdConverter extends IMTwitterRequestIdConverter {

    public LinkTwitterRequestIdConverter() {
    }

    public LinkTwitterRequestIdConverter(int neighboursLimit, int timelineLimit) {
        super(neighboursLimit, timelineLimit);
    }

    @Override
    public IMTwitterRequest getRequest(Long id) {
        return new IMTwitterRequest(id, neighboursLimit, timelineLimit, true);
    }
}
