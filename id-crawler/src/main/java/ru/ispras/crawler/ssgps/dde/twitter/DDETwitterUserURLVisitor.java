package ru.ispras.crawler.ssgps.dde.twitter;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserProfileRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserTimelineRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.User;
import ru.ispras.crawler.core.visitor.AbstractNonFailingVisitor;

/**
 * 
 * @author andrey g
 * 
 */
public class DDETwitterUserURLVisitor extends
		AbstractNonFailingVisitor<TwitterUserProfileRequest, BasicResponse<User>> {

    private final DDETwitterRequestFromURLExtractor extractor = new DDETwitterRequestFromURLExtractor();
    private final Integer timelineLimit;

    public DDETwitterUserURLVisitor(Integer timelineLimit){
        this.timelineLimit = timelineLimit;
    }

	@Override
	public Collection<? extends IRequest> visit(TwitterUserProfileRequest request, BasicResponse<User> response) {
		List<IRequest> result = new ArrayList<>();
		User profile = response.getObject();
		IRequest requestFromURL = extractor.extractRequestFromURL(profile);
		if (requestFromURL != null) {
            result.add(requestFromURL);
            if (timelineLimit == null){
			    result.add(new TwitterUserTimelineRequest(request.getId()));
            } else {
                result.add(new TwitterUserTimelineRequest(request.getId(), timelineLimit));
            }
		}

		return result;
	}

	@Override
	public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
		Set<Class<? extends IRequest>> result = new HashSet<>();
		result.add(TwitterUserTimelineRequest.class);
		result.add(DDETwitterFacebookUserProfileRequest.class);
        result.add(DDETwitterVkontakteUserRequest.class);
		return result;
	}
}
