package ru.ispras.crawler.ssgps.rsgg.twitter;

import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFollowersRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFriendsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserGroupingRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserProfileRequest;

public class RSGGTwitterRequest extends TwitterUserGroupingRequest {
	public RSGGTwitterRequest(long id) {
		super(id, new TwitterUserProfileRequest(id), new TwitterUserFollowersRequest(id),
				new TwitterUserFriendsRequest(id));
	}
}
