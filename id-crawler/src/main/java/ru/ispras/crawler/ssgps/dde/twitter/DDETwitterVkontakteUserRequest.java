package ru.ispras.crawler.ssgps.dde.twitter;

import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteBatchUserRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteUserRequest;

/**
 * Created with IntelliJ IDEA.
 * User: andrey
 * Date: 9/2/14
 * Time: 3:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class DDETwitterVkontakteUserRequest extends VkontakteBatchUserRequest {

    private final long twitterId;
    private final long id;

    public DDETwitterVkontakteUserRequest(long id, long twitterId) {
        super(new VkontakteUserRequest(id));
        this.twitterId = twitterId;
        this.id = id;
    }

    public long getTwitterId(){
        return twitterId;
    }

    public long getId(){
        return id;
    }
}
