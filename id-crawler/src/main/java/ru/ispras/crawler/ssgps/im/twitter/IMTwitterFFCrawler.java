package ru.ispras.crawler.ssgps.im.twitter;

import java.io.IOException;

import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;
import ru.ispras.crawler.core.visitor.IVisitor;
import ru.ispras.crawler.core.visitor.graph.SamplingVisitor;
import ru.ispras.crawler.core.visitor.graph.sampler.ForestFireSampler;

public class IMTwitterFFCrawler extends AbstractIMTwitterCrawler {
	public static void main(String[] args) throws IOException {
		if (args.length < 3) {
			System.out.println("Usage: <oauth config file> <seed user id> <user limit> {timeline limit}");
			return;
		}

		int timelineLimit = Integer.MAX_VALUE;
		if (args.length >= 4) {
			timelineLimit = Integer.parseInt(args[3]);
		}

		new IMTwitterFFCrawler(Integer.parseInt(args[2]), timelineLimit).crawl(args[0], Long.parseLong(args[1]), Integer.MAX_VALUE, timelineLimit);
	}

	private final int userLimit;
	private final Integer timelineLimit;

	public IMTwitterFFCrawler(int userLimit, Integer timelineLimit) {
		this.userLimit = userLimit;
		this.timelineLimit = timelineLimit;
	}

	@Override
	protected IVisitor<IMTwitterRequest, GroupingResponse> getNeighbourVisitor() {
		return new SamplingVisitor<IMTwitterRequest, GroupingResponse, Long>(
				new ForestFireSampler<Long>(),
				new IMTwitterRequestIdConverter(Integer.MAX_VALUE, timelineLimit),
				new IMTwitterResponseNeighbourExtractor(),
				IMTwitterRequest.class,
				userLimit);
	}


}
