package ru.ispras.crawler.ssgps;

import java.net.URI;
import java.util.List;

import ru.ispras.modis.utils.config.ConfigurationFilesCollector;
import ru.ispras.modis.utils.config.MultiSourceConfiguration;

/**
 * 
 * @author andrey g
 *
 */
public class CrawlerSSGPSConfiguration extends MultiSourceConfiguration {
	private static final String CONFIG_FILE = "crawler.ssgps.config.xml";

	private static CrawlerSSGPSConfiguration configuration;

	public static CrawlerSSGPSConfiguration getConfiguration() {
		if (configuration == null) {
			configuration = new CrawlerSSGPSConfiguration(
					ConfigurationFilesCollector
							.getCmdClasspathConfigFiles(CONFIG_FILE));
		}

		return configuration;
	}

	protected CrawlerSSGPSConfiguration(List<URI> initXmlFiles) {
		super(initXmlFiles);
	}
}
