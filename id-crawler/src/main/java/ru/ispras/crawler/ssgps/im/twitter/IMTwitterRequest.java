package ru.ispras.crawler.ssgps.im.twitter;

import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFavoritedTweetsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFollowersRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFriendsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserGroupingRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserMembershipsListsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserProfileRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserSubscribedListsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserTimelineRequest;

public class IMTwitterRequest extends TwitterUserGroupingRequest {
    public IMTwitterRequest(long id) {
        this(id, null, null);
    }

    public IMTwitterRequest(long id, Integer neighboursLimit, Integer timelineLimit) {
        super(id, new TwitterUserProfileRequest(id), new TwitterUserFollowersRequest(id, neighboursLimit),
                new TwitterUserFriendsRequest(id, neighboursLimit), new TwitterUserFavoritedTweetsRequest(id),
                new TwitterUserSubscribedListsRequest(id), new TwitterUserMembershipsListsRequest(id),
                new TwitterUserTimelineRequest(id, timelineLimit));
    }

    //todo: simple WA
    public IMTwitterRequest(long id, Integer neighboursLimit, Integer timelineLimit, boolean links) {
        super(id, new TwitterUserFollowersRequest(id, neighboursLimit),
                new TwitterUserFriendsRequest(id, neighboursLimit),
                new TwitterUserTimelineRequest(id, timelineLimit));
    }

    public IMTwitterRequest(long id, int profileOnly) {
        super(id, new TwitterUserProfileRequest(id));
    }
}
