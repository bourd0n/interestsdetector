package ru.ispras.crawler.ssgps.gcd.hunch;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;
import ru.ispras.crawler.core.datamodel.hunch.HunchItemRequest;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserItemRatingsRequest;
import ru.ispras.crawler.core.datamodel.hunch.entities.ItemRating;
import ru.ispras.crawler.core.visitor.AbstractNonFailingVisitor;
import ru.ispras.crawler.core.visitor.IVisitor;

public class GCDHunchUserVisitor extends AbstractNonFailingVisitor<GCDHunchUserRequest, GroupingResponse> {
	private final IVisitor<GCDHunchUserRequest, GroupingResponse> visitor;
	private final Set<Long> visitedItems = new HashSet<Long>();

	public GCDHunchUserVisitor(IVisitor<GCDHunchUserRequest, GroupingResponse> visitor) {
		this.visitor = visitor;
	}

	@Override
	public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
		Set<Class<? extends IRequest>> producedRequests = new HashSet<>();
		producedRequests.add(GCDHunchUserRequest.class);
		producedRequests.add(HunchItemRequest.class);
		return producedRequests;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<? extends IRequest> visit(GCDHunchUserRequest request, GroupingResponse response) {
		List<IRequest> result = new ArrayList<IRequest>();
		result.addAll(visitor.visit(request, response));
		
		BasicResponse<List<ItemRating>> ratedItems = (BasicResponse<List<ItemRating>>) response
				.getResponse(HunchUserItemRatingsRequest.class);
		
		synchronized (visitedItems){
			for (ItemRating rating: ratedItems.getObject()){
				long id = rating.getId();
				if (!visitedItems.contains(id)){
					result.add(new HunchItemRequest(id));
					visitedItems.add(id);
				}
			}
		}
	
		return result;
	}
}
