package ru.ispras.crawler.ssgps.common;

import java.net.UnknownHostException;

import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;

public class MongoDBReader {

	final Mongo mongo;
	final DB db;

	public MongoDBReader(String dbName) throws UnknownHostException {
		mongo = new Mongo();
		db = mongo.getDB(dbName);
	}

	/**
	 * Find all entries in {@code collectionName} and execute processor.processRecord()
	 *  for every found record.  
	 * @param collectionName
	 * @param processor
	 */
	public void findAll(String collectionName, IMongoDBRecordProcessor processor) {
		DBCursor cursor =  db.getCollection(collectionName).find().addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		while (cursor.hasNext()) {
			processor.processRecord(cursor.next());
		}
	}
}
