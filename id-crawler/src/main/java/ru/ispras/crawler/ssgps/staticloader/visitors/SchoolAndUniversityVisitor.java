package ru.ispras.crawler.ssgps.staticloader.visitors;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.City;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteCitiesRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteSchoolsRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteUniversitiesRequest;
import ru.ispras.crawler.core.visitor.AbstractNonFailingVisitor;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 05.06.14
 * Time: 16:16
 */
public class SchoolAndUniversityVisitor extends AbstractNonFailingVisitor<VkontakteCitiesRequest, BasicResponse<List<City>>> {
    @Override
    public Collection<IRequest> visit(VkontakteCitiesRequest request, BasicResponse<List<City>> response) {
        List<City> cities = response.getObject();
        ArrayList<IRequest> result = new ArrayList<>();
        for (City city: cities) {
            result.add(new VkontakteSchoolsRequest(city.getId()));
            result.add(new VkontakteUniversitiesRequest(city.getId()));
        }
        return result;
    }

    @Override
    public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
        Set<Class<? extends IRequest>> set = new HashSet<>();
        set.add(VkontakteSchoolsRequest.class);
        set.add(VkontakteUniversitiesRequest.class);
        return set;
    }
}
