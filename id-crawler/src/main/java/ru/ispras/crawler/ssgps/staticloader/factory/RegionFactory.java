package ru.ispras.crawler.ssgps.staticloader.factory;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.IdTitle;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.Region;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteIdRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteRegionsRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 24.06.14
 * Time: 17:25
 */
public class RegionFactory implements IResponseFactory<VkontakteRegionsRequest, VkontakteIdRequest, IdTitle, Region> {
    @Override
    public VkontakteIdRequest getRequest(VkontakteRegionsRequest vkontakteRegionsRequest, IdTitle response) {
        return new VkontakteIdRequest(response.getId());
    }

    @Override
    public BasicResponse<Region> getResponse(VkontakteRegionsRequest vkontakteRegionsRequest, IdTitle response) {
        return new BasicResponse<>(new Region(response.getId(), response.getTitle(), vkontakteRegionsRequest.getCountryId()));
    }
}
