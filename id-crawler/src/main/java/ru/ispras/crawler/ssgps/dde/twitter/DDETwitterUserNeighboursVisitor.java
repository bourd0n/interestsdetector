package ru.ispras.crawler.ssgps.dde.twitter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFollowersRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFriendsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserNeighboursRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserProfileRequest;
import ru.ispras.crawler.core.visitor.IVisitor;
import ru.ispras.crawler.core.visitor.graph.sampler.ForestFireSampler;

/**
 * 
 * @author andrey g
 * 
 */
public class DDETwitterUserNeighboursVisitor implements IVisitor<TwitterUserNeighboursRequest, GroupingResponse> {
	private Set<Long> candidates;
	private int candidatesCount;
	private int maxCandidatesCount;
	private final ForestFireSampler<Long> sampler;

	private Queue<Long> localQueue;

	public DDETwitterUserNeighboursVisitor(int maxCandidatesCount) {
		candidates = new HashSet<Long>();
		candidatesCount = 0;
		this.maxCandidatesCount = maxCandidatesCount;
		sampler = new ForestFireSampler<Long>();
		localQueue = new LinkedList<Long>();
	}

	public void resetCandidatesCountAndQueue() {
		candidatesCount = 0;
		localQueue = new LinkedList<Long>();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<? extends IRequest> visit(TwitterUserNeighboursRequest request, GroupingResponse response) {
		List<IRequest> result = new ArrayList<IRequest>();
		List<Long> userNeighbours = new ArrayList<Long>();

		List<Long> friends = ((BasicResponse<List<Long>>) response.getResponse(TwitterUserFriendsRequest.class)).getObject();
		userNeighbours.addAll(friends);

		List<Long> followers = ((BasicResponse<List<Long>>) response.getResponse(TwitterUserFollowersRequest.class)).getObject();
		userNeighbours.addAll(followers);

		for (Long id : userNeighbours) {
			// download profile for all neighbours
			synchronized (candidates) {
				if (!candidates.contains(id) && candidatesCount < maxCandidatesCount) {
					candidates.add(id);
					result.add(new TwitterUserProfileRequest(id));
					candidatesCount++;
				}
			}
		}

		// burn user
		if (candidatesCount < maxCandidatesCount) {
			for (Long id : sampler.getNextNodes(request.getId(), userNeighbours)) {
				synchronized (localQueue) {
					localQueue.add(id);
				}
			}
		}

		addFromLocalQueue(result);

		return result;
	}

	@Override
	public Collection<? extends IRequest> visit(TwitterUserNeighboursRequest request, Throwable problem) {
		List<IRequest> result = new ArrayList<IRequest>();
		addFromLocalQueue(result);
		return result;
	}

	private void addFromLocalQueue(List<IRequest> result) {
		if (candidatesCount < maxCandidatesCount) {
			synchronized (localQueue) {
				if (localQueue.size() > 0) {
					result.add(new TwitterUserNeighboursRequest(localQueue.poll()));
				}
			}
		}
	}

	@Override
	public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
		Set<Class<? extends IRequest>> result = new HashSet<Class<? extends IRequest>>();
		result.add(TwitterUserProfileRequest.class);
		result.add(TwitterUserNeighboursRequest.class);
		return result;
	}
}
