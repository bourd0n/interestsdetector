package ru.ispras.crawler.ssgps.im.twitter;

import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class IMTwitterRequestIdConverter implements IRequestIdConverter<IMTwitterRequest, Long> {
	protected final int neighboursLimit;
	protected final int timelineLimit;

	public IMTwitterRequestIdConverter() {
		this(Integer.MAX_VALUE, Integer.MAX_VALUE);
	}

	public IMTwitterRequestIdConverter(int neighboursLimit, int timelineLimit) {
		this.neighboursLimit = neighboursLimit;
		this.timelineLimit = timelineLimit;
	}

	@Override
	public Long getId(IMTwitterRequest request) {
		return request.getId();
	}

	@Override
	public IMTwitterRequest getRequest(Long id) {
		return new IMTwitterRequest(id, neighboursLimit, timelineLimit);
	}
}
