package ru.ispras.crawler.ssgps.dde.facebook;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.facebook.FacebookUserProfileRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserProfileRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.User;
import ru.ispras.crawler.core.requestidconverter.facebook.FacebookUserProfileRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserProfileRequestIdConverter;
import ru.ispras.crawler.core.saver.ISaver;
import ru.ispras.crawler.core.saver.mongodb.MongoDBSaverLoader;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.JacksonMongoDBResponseConverter;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DBCollection;

public class MongoDBFacebookToTwitterUserLinkSaver implements
		ISaver<DDEFacebookTwitterUserProfileRequest, BasicResponse<User>> {
	
	private final ISaver<TwitterUserProfileRequest, BasicResponse<Long>> twitterSaver;
	private final ISaver<FacebookUserProfileRequest, BasicResponse<Long>> facebookSaver;

	public MongoDBFacebookToTwitterUserLinkSaver(DBCollection facebookCollection, DBCollection twitterCollection) {
		twitterSaver = new MongoDBSaverLoader<>(twitterCollection, new TwitterUserProfileRequestIdConverter(), 
				new JacksonMongoDBResponseConverter<BasicResponse<Long>>(new TypeReference<BasicResponse<Long>>(){}));
		facebookSaver = new MongoDBSaverLoader<>(facebookCollection, new FacebookUserProfileRequestIdConverter(), 
				new JacksonMongoDBResponseConverter<BasicResponse<Long>>(new TypeReference<BasicResponse<Long>>(){}));
	}

	@Override
	public void save(DDEFacebookTwitterUserProfileRequest request, BasicResponse<User> response) {
		long facebookId = request.getFacebookId();
		long twitterId = response.getObject().getId();
		twitterSaver.save(new TwitterUserProfileRequest(twitterId), new BasicResponse<Long>(facebookId));
		facebookSaver.save(new FacebookUserProfileRequest(facebookId), new BasicResponse<Long>(twitterId));
	}
}
