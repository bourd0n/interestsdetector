package ru.ispras.crawler.ssgps.im.twitter.links;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DB;
import com.mongodb.Mongo;
import ru.ispras.crawler.core.Crawler;
import ru.ispras.crawler.core.connection.DelayedConnection;
import ru.ispras.crawler.core.connection.GroupingConnection;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserFollowersConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserFriendsConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserTimelineConnection;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionFactory;
import ru.ispras.crawler.core.connection.utils.BatchConfigurableConnectionFactoryUtils;
import ru.ispras.crawler.core.connection.utils.ConnectionConfigurationReader;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFollowersRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFriendsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserTimelineRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.Status;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserFollowersRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserFriendsRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserTimelineRequestIdConverter;
import ru.ispras.crawler.core.saver.GroupingSaver;
import ru.ispras.crawler.core.saver.mongodb.MongoDBSaverLoader;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.ChunkingMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.JacksonMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.responsechunker.BasicResponseChunker;
import ru.ispras.crawler.core.visitor.IVisitor;
import ru.ispras.crawler.core.visitor.graph.SamplingVisitor;
import ru.ispras.crawler.core.visitor.graph.sampler.BreadthFirstSampler;
import ru.ispras.crawler.ssgps.im.twitter.IMTwitterBFSCrawler;
import ru.ispras.crawler.ssgps.im.twitter.IMTwitterRequest;
import ru.ispras.crawler.ssgps.im.twitter.IMTwitterRequestIdConverter;
import ru.ispras.crawler.ssgps.im.twitter.IMTwitterResponseNeighbourExtractor;

import java.io.IOException;
import java.util.List;

public class LinksTwitterBFSCrawler extends IMTwitterBFSCrawler {
    public LinksTwitterBFSCrawler(int depth, Integer timelineLimit) {
        super(depth, timelineLimit);
    }

    public void crawl(String oauthConfigFile, long seedId, Integer neighboursLimit, Integer timelineLimit) throws IOException {
        Crawler crawler = new Crawler(createCrawlerConfig(oauthConfigFile, timelineLimit));
        crawler.crawl(new IMTwitterRequest(seedId, neighboursLimit, timelineLimit, true));
        crawler.awaitTermination(30, 2);
        crawler.shutdown();
    }

    @Override
    protected Crawler.Configuration createCrawlerConfig(String configFile, Integer timelineLimit) throws IOException {
        IConnection<HttpRequest, HttpResponse> twitterHttpConnection = new DelayedConnection<>(
                BatchConfigurableConnectionFactoryUtils.create(new TwitterHttpConnectionFactory(),
                        ConnectionConfigurationReader.getConfigurations(TwitterHttpConnectionConfiguration.class,
                                configFile)));

        GroupingConnection.Configuration groupingConnectionConfig = new GroupingConnection.Configuration();
        groupingConnectionConfig.put(TwitterUserFollowersRequest.class,
                new TwitterUserFollowersConnection(twitterHttpConnection));
        groupingConnectionConfig.put(TwitterUserFriendsRequest.class,
                new TwitterUserFriendsConnection(twitterHttpConnection));
        groupingConnectionConfig.put(TwitterUserTimelineRequest.class,
                new TwitterUserTimelineConnection(twitterHttpConnection));

        DB db = new Mongo().getDB("im-twitter");

        GroupingSaver.Configuration groupingSaverConfig = new GroupingSaver.Configuration();
        groupingSaverConfig.put(TwitterUserFollowersRequest.class,
                new MongoDBSaverLoader<>(
                        db.getCollection("followers"),
                        new TwitterUserFollowersRequestIdConverter(),
                        new ChunkingMongoDBResponseConverter<>(
                                new BasicResponseChunker<Long>(100000),
                                new JacksonMongoDBResponseConverter<>(
                                        new TypeReference<BasicResponse<List<Long>>>() {
                                        }))));
        groupingSaverConfig.put(TwitterUserFriendsRequest.class,
                new MongoDBSaverLoader<>(
                        db.getCollection("friends"),
                        new TwitterUserFriendsRequestIdConverter(),
                        new ChunkingMongoDBResponseConverter<>(
                                new BasicResponseChunker<Long>(100000),
                                new JacksonMongoDBResponseConverter<>(
                                        new TypeReference<BasicResponse<List<Long>>>() {
                                        }))));
        groupingSaverConfig.put(TwitterUserTimelineRequest.class,
                new MongoDBSaverLoader<>(
                        db.getCollection("timeline"),
                        new TwitterUserTimelineRequestIdConverter(),
                        new ChunkingMongoDBResponseConverter<>(
                                new BasicResponseChunker<Status>(),
                                new JacksonMongoDBResponseConverter<>(
                                        new TypeReference<BasicResponse<List<Status>>>() {
                                        }))));

        Crawler.Configuration config = new Crawler.Configuration();
        config.put(IMTwitterRequest.class,
                new GroupingConnection(groupingConnectionConfig),
                getNeighbourVisitor(),
                new GroupingSaver(groupingSaverConfig));
        return config;
    }

    @Override
    protected IVisitor<IMTwitterRequest, GroupingResponse> getNeighbourVisitor() {
        return new SamplingVisitor<IMTwitterRequest, GroupingResponse, Long>(
                new BreadthFirstSampler<Long>(depth, depth2limit),
                new LinkTwitterRequestIdConverter(neighbourLimit, timelineLimit),
                new IMTwitterResponseNeighbourExtractor(),
                IMTwitterRequest.class);
    }
}
