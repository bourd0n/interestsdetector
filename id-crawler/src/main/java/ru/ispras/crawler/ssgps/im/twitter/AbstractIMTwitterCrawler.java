package ru.ispras.crawler.ssgps.im.twitter;

import java.io.IOException;
import java.util.List;

import ru.ispras.crawler.core.Crawler;
import ru.ispras.crawler.core.connection.DelayedConnection;
import ru.ispras.crawler.core.connection.GroupingConnection;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserFavoritedTweetsConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserFollowersConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserFriendsConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserMembershipsListsConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserProfileConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserSubscribedListsConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserTimelineConnection;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionFactory;
import ru.ispras.crawler.core.connection.utils.BatchConfigurableConnectionFactoryUtils;
import ru.ispras.crawler.core.connection.utils.ConnectionConfigurationReader;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFavoritedTweetsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFollowersRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFriendsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserMembershipsListsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserProfileRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserSubscribedListsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserTimelineRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.Status;
import ru.ispras.crawler.core.datamodel.twitter.entities.User;
import ru.ispras.crawler.core.datamodel.twitter.entities.UserList;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserFavoritedTweetsRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserFollowersRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserFriendsRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserMembershipsListsRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserProfileRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserSubscribedListsRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserTimelineRequestIdConverter;
import ru.ispras.crawler.core.saver.GroupingSaver;
import ru.ispras.crawler.core.saver.mongodb.MongoDBSaverLoader;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.ChunkingMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.JacksonMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.responsechunker.BasicResponseChunker;
import ru.ispras.crawler.core.visitor.IVisitor;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DB;
import com.mongodb.Mongo;

public abstract class AbstractIMTwitterCrawler {
	public void crawl(String oauthConfigFile, long seedId, Integer neighboursLimit, Integer timelineLimit) throws IOException {
		Crawler crawler = new Crawler(createCrawlerConfig(oauthConfigFile, timelineLimit));
		crawler.crawl(new IMTwitterRequest(seedId, neighboursLimit, timelineLimit));
		crawler.awaitTerminationAndShutdown();
	}

	protected Crawler.Configuration createCrawlerConfig(String configFile, Integer timelineLimit) throws IOException {
		IConnection<HttpRequest, HttpResponse> twitterHttpConnection = new DelayedConnection<>(
				BatchConfigurableConnectionFactoryUtils.create(new TwitterHttpConnectionFactory(),
						ConnectionConfigurationReader.getConfigurations(TwitterHttpConnectionConfiguration.class,
								configFile)));

		GroupingConnection.Configuration groupingConnectionConfig = new GroupingConnection.Configuration();
		groupingConnectionConfig.put(TwitterUserProfileRequest.class,
				new TwitterUserProfileConnection(twitterHttpConnection));
		groupingConnectionConfig.put(TwitterUserFollowersRequest.class,
				new TwitterUserFollowersConnection(twitterHttpConnection));
		groupingConnectionConfig.put(TwitterUserFriendsRequest.class,
				new TwitterUserFriendsConnection(twitterHttpConnection));
		groupingConnectionConfig.put(TwitterUserFavoritedTweetsRequest.class,
				new TwitterUserFavoritedTweetsConnection(twitterHttpConnection));
		groupingConnectionConfig.put(TwitterUserSubscribedListsRequest.class,
				new TwitterUserSubscribedListsConnection(twitterHttpConnection));
		groupingConnectionConfig.put(TwitterUserMembershipsListsRequest.class,
				new TwitterUserMembershipsListsConnection(twitterHttpConnection));
		groupingConnectionConfig.put(TwitterUserTimelineRequest.class,
				new TwitterUserTimelineConnection(twitterHttpConnection));

		DB db = new Mongo().getDB("im-twitter");

		GroupingSaver.Configuration groupingSaverConfig = new GroupingSaver.Configuration();
		groupingSaverConfig.put(TwitterUserProfileRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("profile"),
						new TwitterUserProfileRequestIdConverter(),
						new JacksonMongoDBResponseConverter<>(
								new TypeReference<BasicResponse<User>>() {})));
		groupingSaverConfig.put(TwitterUserFollowersRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("followers"),
						new TwitterUserFollowersRequestIdConverter(),
						new ChunkingMongoDBResponseConverter<>(
								new BasicResponseChunker<Long>(100000),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<List<Long>>>() {}))));
		groupingSaverConfig.put(TwitterUserFriendsRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("friends"),
						new TwitterUserFriendsRequestIdConverter(),
						new ChunkingMongoDBResponseConverter<>(
								new BasicResponseChunker<Long>(100000),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<List<Long>>>() {}))));
		groupingSaverConfig.put(TwitterUserFavoritedTweetsRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("favoritedTweets"),
						new TwitterUserFavoritedTweetsRequestIdConverter(),
						new ChunkingMongoDBResponseConverter<>(
								new BasicResponseChunker<Status>(),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<List<Status>>>() {}))));
		groupingSaverConfig.put(TwitterUserSubscribedListsRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("subscribedLists"),
						new TwitterUserSubscribedListsRequestIdConverter(),
						new ChunkingMongoDBResponseConverter<>(
								new BasicResponseChunker<UserList>(),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<List<UserList>>>() {}))));
		groupingSaverConfig.put(TwitterUserMembershipsListsRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("membershipLists"),
						new TwitterUserMembershipsListsRequestIdConverter(),
						new ChunkingMongoDBResponseConverter<>(
								new BasicResponseChunker<UserList>(),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<List<UserList>>>() {}))));
		groupingSaverConfig.put(TwitterUserTimelineRequest.class,
				new MongoDBSaverLoader<>(
						db.getCollection("timeline"),
						new TwitterUserTimelineRequestIdConverter(),
						new ChunkingMongoDBResponseConverter<>(
								new BasicResponseChunker<Status>(),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<List<Status>>>() {}))));

		Crawler.Configuration config = new Crawler.Configuration();
		config.put(IMTwitterRequest.class,
				new GroupingConnection(groupingConnectionConfig),
				getNeighbourVisitor(),
				new GroupingSaver(groupingSaverConfig));
		return config;
	}

	protected abstract IVisitor<IMTwitterRequest, GroupingResponse> getNeighbourVisitor();
}
