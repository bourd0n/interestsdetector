package ru.ispras.crawler.ssgps.rsgg.twitter;

import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class RSGGTwitterRequestIdConverter implements IRequestIdConverter<RSGGTwitterRequest, Long> {
	@Override
	public Long getId(RSGGTwitterRequest request) {
		return request.getId();
	}

	@Override
	public RSGGTwitterRequest getRequest(Long id) {
		return new RSGGTwitterRequest(id);
	}
}
