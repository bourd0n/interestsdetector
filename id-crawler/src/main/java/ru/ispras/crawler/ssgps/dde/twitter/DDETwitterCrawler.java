package ru.ispras.crawler.ssgps.dde.twitter;

import java.util.Collections;
import java.util.List;

import ru.ispras.crawler.core.Crawler;
import ru.ispras.crawler.core.connection.DelayedConnection;
import ru.ispras.crawler.core.connection.GroupingConnection;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.facebook.FacebookProfileConnection;
import ru.ispras.crawler.core.connection.facebook.http.FacebookHttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.facebook.http.FacebookHttpConnectionFactory;
import ru.ispras.crawler.core.connection.http.HttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.twitter.TwitterUserFollowersConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserFriendsConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserProfileConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserTimelineConnection;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionFactory;
import ru.ispras.crawler.core.connection.utils.BatchConfigurableConnectionFactoryUtils;
import ru.ispras.crawler.core.connection.utils.ConnectionConfigurationReader;
import ru.ispras.crawler.core.connection.vkontakte.VkontakteUsersConnection;
import ru.ispras.crawler.core.connection.vkontakte.http.VkontakteHttpConnectionFactory;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.batch.BasicBatchResponse;
import ru.ispras.crawler.core.datamodel.facebook.FacebookResponse;
import ru.ispras.crawler.core.datamodel.facebook.entities.Profile;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.*;
import ru.ispras.crawler.core.datamodel.twitter.entities.Status;
import ru.ispras.crawler.core.datamodel.twitter.entities.User;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteUserRequest;
import ru.ispras.crawler.core.jackson.VkontakteObjectMapperFactory;
import ru.ispras.crawler.core.requestidconverter.facebook.FacebookUserProfileRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserProfileRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserTimelineRequestIdConverter;
import ru.ispras.crawler.core.requestidconverter.vkontakte.VkontakteSingleUserRequestIdConverter;
import ru.ispras.crawler.core.saver.BatchSaver;
import ru.ispras.crawler.core.saver.CompositeSaver;
import ru.ispras.crawler.core.saver.ISaver;
import ru.ispras.crawler.core.saver.NoOpSaver;
import ru.ispras.crawler.core.saver.mongodb.MongoDBSaverLoader;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.ChunkingMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.JacksonMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.responsechunker.BasicResponseChunker;
import ru.ispras.crawler.core.visitor.NoOpVisitor;
import ru.ispras.crawler.ssgps.common.SeedListLoader;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DB;
import com.mongodb.Mongo;

/**
 * 
 * @author andrey g
 *
 */
public class DDETwitterCrawler {
	private final Crawler crawler;
	private final DDETwitterUserNeighboursVisitor visitor;

	DDETwitterCrawler(String oauthConfigFile, int maxSeedsCandidates,
			Integer timelineLimit) throws Exception {
		visitor = new DDETwitterUserNeighboursVisitor(maxSeedsCandidates);
		crawler = init(oauthConfigFile, timelineLimit);
	}

	private Crawler init(String authConfigFile, Integer timelineLimit)
			throws Exception {
		IConnection<HttpRequest, HttpResponse> twitterHttpConnection = new DelayedConnection<>(
				BatchConfigurableConnectionFactoryUtils.create(new TwitterHttpConnectionFactory(),
						ConnectionConfigurationReader.getConfigurations(TwitterHttpConnectionConfiguration.class,
								authConfigFile)));
		IConnection<HttpRequest, HttpResponse> facebookHttpConnection = new DelayedConnection<>(
				BatchConfigurableConnectionFactoryUtils.create(new FacebookHttpConnectionFactory(),
						ConnectionConfigurationReader.getConfigurations(FacebookHttpConnectionConfiguration.class,
								authConfigFile)));

        IConnection<HttpRequest, HttpResponse> vkontakteHttpConnection =
                    new VkontakteHttpConnectionFactory().create(new HttpConnectionConfiguration());

		GroupingConnection.Configuration groupingConnectionConfig = new GroupingConnection.Configuration();
		groupingConnectionConfig.put(TwitterUserFollowersRequest.class,
				new TwitterUserFollowersConnection(twitterHttpConnection));
		groupingConnectionConfig.put(TwitterUserFriendsRequest.class,
				new TwitterUserFriendsConnection(twitterHttpConnection));

		final DB db = new Mongo().getDB("dde-twitter");
		
		Crawler.Configuration config = new Crawler.Configuration();
		config.put(TwitterUserNeighboursRequest.class,
				new GroupingConnection(groupingConnectionConfig),
				visitor,
				new NoOpSaver<>());
		config.put(TwitterUserProfileRequest.class,
				new TwitterUserProfileConnection(twitterHttpConnection),
				new DDETwitterUserURLVisitor(timelineLimit),
				new DDETwitterUserURLSaver(
						new MongoDBSaverLoader<>(
								db.getCollection("twitterProfile"),
								new TwitterUserProfileRequestIdConverter(),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<User>>() {}))));
		config.put(TwitterUserTimelineRequest.class,
				new TwitterUserTimelineConnection(twitterHttpConnection),
				new NoOpVisitor<>(),
				new MongoDBSaverLoader<>(
						db.getCollection("timeline"),
						new TwitterUserTimelineRequestIdConverter(),
						new ChunkingMongoDBResponseConverter<>(
								new BasicResponseChunker<Status>(),
								new JacksonMongoDBResponseConverter<>(
										new TypeReference<BasicResponse<List<Status>>>() {}))));
		config.put(DDETwitterFacebookUserProfileRequest.class,
				new FacebookProfileConnection(facebookHttpConnection),
				new NoOpVisitor<>(),
				new ISaver<DDETwitterFacebookUserProfileRequest, FacebookResponse<Profile>>(){
					private final ISaver<DDETwitterFacebookUserProfileRequest, FacebookResponse<Profile>> saver = new CompositeSaver<>(
							new MongoDBSaverLoader<>(
									db.getCollection("facebookProfile"),
									new FacebookUserProfileRequestIdConverter(),
									new JacksonMongoDBResponseConverter<>(
											new TypeReference<FacebookResponse<Profile>>() {})),
                            new MongoDBSNLinksSaver<DDETwitterFacebookUserProfileRequest, FacebookResponse<Profile>>
                                    (db.getCollection("twitterFacebookLink"), "twitter-id", "facebook-id") {
                                @Override
                                protected Object getSecondSNUserId(DDETwitterFacebookUserProfileRequest request, FacebookResponse<Profile> response) {
                                    return response.getObject().getId();
                                }

                                @Override
                                protected Object getFirstSNUserId(DDETwitterFacebookUserProfileRequest request, FacebookResponse<Profile> response) {
                                    return request.getTwitterId();
                                }
                            });

					@Override
					public void save(
							DDETwitterFacebookUserProfileRequest request,
							FacebookResponse<Profile> response) {
							long id = response.getObject().getId();
							saver.save(new DDETwitterFacebookUserProfileRequest(id, request.getTwitterId()), response);
					}
				});

        config.put(DDETwitterVkontakteUserRequest.class,
                new VkontakteUsersConnection(vkontakteHttpConnection),
                new NoOpVisitor<>(),
                new CompositeSaver<DDETwitterVkontakteUserRequest, BasicBatchResponse<VkontakteUserRequest, BasicResponse<ru.ispras.crawler.core.datamodel.vkontakte.entities.users.User>>>
                        (
                        new BatchSaver(
                                new MongoDBSaverLoader<>(
                                        db.getCollection("vkontakteProfile"),
                                        new VkontakteSingleUserRequestIdConverter(),
                                        new JacksonMongoDBResponseConverter<>(
                                                new TypeReference<BasicResponse<ru.ispras.crawler.core.datamodel.vkontakte.entities.users.User>>() {},
                                                new VkontakteObjectMapperFactory().create()))
                        ), new MongoDBSNLinksSaver<DDETwitterVkontakteUserRequest, BasicBatchResponse<VkontakteUserRequest, BasicResponse<ru.ispras.crawler.core.datamodel.vkontakte.entities.users.User>>>
                                (db.getCollection("twitterVkontakteLink"), "twitter-id", "vkontakte-id"){

                            @Override
                            protected Object getSecondSNUserId(DDETwitterVkontakteUserRequest request, BasicBatchResponse<VkontakteUserRequest, BasicResponse<ru.ispras.crawler.core.datamodel.vkontakte.entities.users.User>> response) {
                              return request.getId();
                            }

                            @Override
                            protected Object getFirstSNUserId(DDETwitterVkontakteUserRequest request, BasicBatchResponse<VkontakteUserRequest, BasicResponse<ru.ispras.crawler.core.datamodel.vkontakte.entities.users.User>> response) {
                                return request.getTwitterId();
                            }
                        }
                        )

                );
		return new Crawler(config);
	}

	public static void main(String[] args) throws Exception {
		if (args.length < 3) {
			System.out
					.println("Usage: <oauth config file> <users limit per seed> <seeds file> {timeline limit}");
			return;
		}
		Integer timelineLimit = null;
		if (args.length >= 4) {
			timelineLimit = Integer.parseInt(args[3]);
		}

		List<Long> seeds = SeedListLoader.loadSeeds(args[2]);
		DDETwitterCrawler dde = new DDETwitterCrawler(args[0],
				Integer.parseInt(args[1]), timelineLimit);
		for (Long id : seeds) {
			dde.visitor.resetCandidatesCountAndQueue();
			dde.crawler.crawl(Collections.singleton(new TwitterUserNeighboursRequest(id)));
			dde.crawler.awaitTermination();
		}
		dde.crawler.shutdown();
	}
}
