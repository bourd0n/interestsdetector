package ru.ispras.crawler.ssgps.spam.twitter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import ru.ispras.crawler.core.Crawler;
import ru.ispras.crawler.core.connection.DelayedConnection;
import ru.ispras.crawler.core.connection.GroupingConnection;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserFriendsConnection;
import ru.ispras.crawler.core.connection.twitter.TwitterUserTimelineConnection;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.twitter.http.TwitterHttpConnectionFactory;
import ru.ispras.crawler.core.connection.utils.BatchConfigurableConnectionFactoryUtils;
import ru.ispras.crawler.core.connection.utils.ConnectionConfigurationReader;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFriendsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserTimelineRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.Status;
import ru.ispras.crawler.core.requestidconverter.twitter.TwitterUserTimelineRequestIdConverter;
import ru.ispras.crawler.core.saver.GroupingSaver;
import ru.ispras.crawler.core.saver.ISaver;
import ru.ispras.crawler.core.saver.NoOpSaver;
import ru.ispras.crawler.core.saver.mongodb.MongoDBSaverLoader;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.ChunkingMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.JacksonMongoDBResponseConverter;
import ru.ispras.crawler.core.saver.responsechunker.BasicResponseChunker;
import ru.ispras.crawler.core.visitor.NoOpVisitor;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DB;
import com.mongodb.Mongo;

public class SpamCrawler {
	
	public void crawl(String oauthConfigFile, Collection<Long> seedIds, boolean crawlFriends) throws IOException {
		Crawler crawler = new Crawler(createCrawlerConfig(oauthConfigFile));
		for (Long id: seedIds){
			if (crawlFriends){
				crawler.crawl(new TwitterUserTimelineFriendsRequest(id));
			}else{
				crawler.crawl(new TwitterUserTimelineRequest(id));
			}
		}
		crawler.awaitTerminationAndShutdown();
	}
	
	
	private Crawler.Configuration createCrawlerConfig(String configFile) throws IOException {
		IConnection<HttpRequest, HttpResponse> twitterHttpConnection = new DelayedConnection<>(
				BatchConfigurableConnectionFactoryUtils.create(new TwitterHttpConnectionFactory(),
						ConnectionConfigurationReader.getConfigurations(TwitterHttpConnectionConfiguration.class,
								configFile)));

		DB db = new Mongo().getDB("spam-twitter");
		ISaver<TwitterUserTimelineRequest, BasicResponse<List<Status>>> saver = new MongoDBSaverLoader<>(
				db.getCollection("timeline"),
				new TwitterUserTimelineRequestIdConverter(),
				new ChunkingMongoDBResponseConverter<>(
						new BasicResponseChunker<Status>(),
						new JacksonMongoDBResponseConverter<>(new TypeReference<BasicResponse<List<Status>>>() {})));

		GroupingConnection.Configuration groupingConnectionConfig = new GroupingConnection.Configuration();
		groupingConnectionConfig.put(TwitterUserFriendsRequest.class,
				new TwitterUserFriendsConnection(twitterHttpConnection));
		groupingConnectionConfig.put(TwitterUserTimelineRequest.class,
				new TwitterUserTimelineConnection(twitterHttpConnection));

		GroupingSaver.Configuration groupingSaverConfig = new GroupingSaver.Configuration();
		groupingSaverConfig.put(TwitterUserFriendsRequest.class, new NoOpSaver<>());
		groupingSaverConfig.put(TwitterUserTimelineRequest.class, saver);

		Crawler.Configuration config = new Crawler.Configuration();

		config.put(TwitterUserTimelineFriendsRequest.class,
				new GroupingConnection(groupingConnectionConfig),
				new TimelineFriendsVisitor(),
				new GroupingSaver(groupingSaverConfig));

		config.put(TwitterUserTimelineRequest.class,
				new TwitterUserTimelineConnection(twitterHttpConnection),
				new NoOpVisitor<>(),
				saver);
		return config;
	}

	private static void printUsage(){
		System.out.println("Usage: <oauth config file> <seeds_file> <mode>");
		System.out.println("mode: 0 -- crawl only seeds, 1 -- crawl seeds and their friends");
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		if (args.length < 3){
			printUsage();
			return ;
		}
		String configFile = args[0];
		String inputFile = args[1];
		String mode = args[2];
		
		SpamCrawler crawler = new SpamCrawler();
		
		if ("0".equals(mode)){
			crawler.crawl(configFile, readList(inputFile), false);
		}else if ("1".equals(mode)){
			crawler.crawl(configFile, readList(inputFile), true);
		}else{
			printUsage();
		}
	}


	private static Collection<Long> readList(String inputFile) throws FileNotFoundException {
		Collection<Long> result = new ArrayList<Long>();
		try (Scanner scn = new Scanner(new FileInputStream(inputFile))) {
			while (scn.hasNext()){
				String line = scn.nextLine();
				try{
					result.add(Long.valueOf(line));
				}catch (NumberFormatException e){
					System.err.println("Cannot parse "+line);
				}
			}
		}
		return result;
	}

}
