package ru.ispras.crawler.ssgps.staticloader.visitors;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.IdTitle;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteChairsRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteFacultiesRequest;
import ru.ispras.crawler.core.visitor.AbstractNonFailingVisitor;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 05.06.14
 * Time: 17:58
 */
public class ChairsVisitor extends AbstractNonFailingVisitor<VkontakteFacultiesRequest, BasicResponse<List<IdTitle>>> {
    @Override
    public Collection<VkontakteChairsRequest> visit(VkontakteFacultiesRequest request, BasicResponse<List<IdTitle>> response) {
        List<IdTitle> faculties = response.getObject();
        ArrayList<VkontakteChairsRequest> requests = new ArrayList<>();
        for (IdTitle faculty: faculties) {
            requests.add(new VkontakteChairsRequest(faculty.getId()));
        }
        return requests;
    }

    @Override
    public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
        return Collections.singleton(VkontakteChairsRequest.class);
    }
}
