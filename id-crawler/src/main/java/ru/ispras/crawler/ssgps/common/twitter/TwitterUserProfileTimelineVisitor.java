package ru.ispras.crawler.ssgps.common.twitter;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserProfileRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserTimelineRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.User;
import ru.ispras.crawler.core.visitor.AbstractNonFailingVisitor;

public class TwitterUserProfileTimelineVisitor extends
		AbstractNonFailingVisitor<TwitterUserProfileRequest, BasicResponse<User>> {
	private final Integer timelineLimit;

	public TwitterUserProfileTimelineVisitor() {
		this(null);
	}

	public TwitterUserProfileTimelineVisitor(Integer timelineLimit) {
		this.timelineLimit = timelineLimit;
	}

	@Override
	public Collection<? extends IRequest> visit(TwitterUserProfileRequest request, BasicResponse<User> response) {
		long twitterId = response.getObject().getId();
		return Collections.singleton(new TwitterUserTimelineRequest(twitterId, timelineLimit));
	}

	@Override
	public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
		return Collections.singleton(TwitterUserTimelineRequest.class);
	}
}
