package ru.ispras.crawler.ssgps.vkontakte.staticloader.converter;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.Faculty;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.IdTitle;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteFacultiesRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteIdRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 24.06.14
 * Time: 17:24
 */
public class FacultyConverter implements IResponseConverter<VkontakteFacultiesRequest, VkontakteIdRequest, IdTitle, Faculty> {
    @Override
    public VkontakteIdRequest getRequest(VkontakteFacultiesRequest vkontakteFacultiesRequest, IdTitle response) {
        return new VkontakteIdRequest(response.getId());
    }

    @Override
    public BasicResponse<Faculty> getResponse(VkontakteFacultiesRequest vkontakteFacultiesRequest, IdTitle response) {
        return new BasicResponse<Faculty>(new Faculty(response.getId(), response.getTitle(), vkontakteFacultiesRequest.getUniversityId()));
    }
}
