package ru.ispras.crawler.core.connection.twitter;

import java.util.ArrayList;
import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserMembershipsListsRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.MembershipsLists;
import ru.ispras.crawler.core.datamodel.twitter.entities.UserList;

/**
 * {@link IConnection} for downloading memberships in lists of given user.
 *
 * See https://dev.twitter.com/rest/reference/get/lists/memberships
 */
public class TwitterUserMembershipsListsConnection extends
		AbstractTwitterConnection<TwitterUserMembershipsListsRequest, BasicResponse<List<UserList>>> {
	private static final String METHOD_BASE = "lists/memberships.json?%s&%s";

	public TwitterUserMembershipsListsConnection(IConnection<HttpRequest, HttpResponse> twitterHttpConnection) {
		super(twitterHttpConnection);
	}

	@Override
	public BasicResponse<List<UserList>> getResponse(TwitterUserMembershipsListsRequest request)
			throws UnavailableConnectionException {
		Long cursor = null;
		List<UserList> lists = new ArrayList<UserList>();
		while (true) {
			String method = String.format(METHOD_BASE,
					getIdParameter(request),
					getCursorParameter(cursor));
			
			MembershipsLists response = getHttpResponse(method, MembershipsLists.class);
			lists.addAll(response.getLists());
			
			long nextCursor = response.getNextCursor();
			if (nextCursor == 0) {
				break;
			}
			cursor = nextCursor;
		}
		return new BasicResponse<List<UserList>>(lists);
	}
}
