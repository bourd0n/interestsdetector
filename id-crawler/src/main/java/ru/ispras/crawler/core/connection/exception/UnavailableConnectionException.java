package ru.ispras.crawler.core.connection.exception;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Thrown when {@link IRequest} cannot be processed due to {@link IConnection}
 * issue (e.g., because of rate limits).
 * <p>
 * A timestamp (in milliseconds since Epoch) is specified to let the client know
 * when {@link IConnection} will be available for processing of the given
 * {@link IRequest}.
 * 
 * @author Alexander
 * 
 */
public class UnavailableConnectionException extends Exception {
	private static final long serialVersionUID = -5427883964305040704L;

	private final long timeout;

	public UnavailableConnectionException(long timeout) {
		this.timeout = timeout;
	}

	public UnavailableConnectionException(String message, long timeout) {
		super(message);
		this.timeout = timeout;
	}

	public UnavailableConnectionException(Throwable cause, long timeout) {
		super(cause);
		this.timeout = timeout;
	}

	public UnavailableConnectionException(String message, Throwable cause, long timeout) {
		super(message, cause);
		this.timeout = timeout;
	}

	public long getTimeout() {
		return timeout;
	}
}
