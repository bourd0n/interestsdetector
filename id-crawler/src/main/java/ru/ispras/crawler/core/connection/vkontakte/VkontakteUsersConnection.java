package ru.ispras.crawler.core.connection.vkontakte;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteBatchResponseUtils;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.batch.BasicBatchRequest;
import ru.ispras.crawler.core.datamodel.batch.BasicBatchResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteUserRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.users.User;

/**
 * Connection for https://vk.com/dev/users.get method
 * 
 * @author andrey
 *
 */
public class VkontakteUsersConnection 
		implements IConnection<BasicBatchRequest<VkontakteUserRequest>, BasicBatchResponse<VkontakteUserRequest, BasicResponse<User>>>{

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	private static final VkontakteBatchResponseUtils<VkontakteUserRequest, Long, User> utils = 
			new VkontakteBatchResponseUtils<VkontakteUserRequest, Long, User>(){
				@Override
				protected Long getId(VkontakteUserRequest req) {
					return req.getId();
				}
				@Override
				protected Long getId(User obj) {
					return obj.getId();
				}
	};
	
	public VkontakteUsersConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}

	private String getMethod(List<Long> ids) {
		return "users.get?user_ids="+StringUtils.join(ids,',')+
				"&fields=bdate,books,city,connections,contacts,counters,country,domain," +
                "education,followers_count,has_mobile,home_town,last_seen,lists,online,online_mobile,personal," +
                "photo_100,photo_200,photo_200_orig,photo_400_orig,photo_50,photo_max," +
                "photo_max_orig,relation,relatives,schools,screen_name,sex,site,status," +
                "timezone,tv,universities";
	}

	@Override
	public BasicBatchResponse<VkontakteUserRequest, BasicResponse<User>> getResponse(BasicBatchRequest<VkontakteUserRequest> request)
			throws UnavailableConnectionException {
		String url = getMethod(utils.getIds(request.getRequests()));
		List<User> response = VkontakteConnectionUtils.getSingleListResponse(url, httpConnection, User.class);
		return utils.getBasicBatchResponse(request.getRequests(), response);
	}
}
