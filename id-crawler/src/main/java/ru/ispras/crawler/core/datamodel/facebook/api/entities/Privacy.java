/*
 * Copyright 2012 Ryuji Yamashita
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.facebook.api.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ryuji Yamashita - roundrop at gmail.com, andrey g
 */
public class Privacy {

	public static enum PrivacyType {
		EVERYONE, ALL_FRIENDS, NETWORKS_FRIENDS, FRIENDS_OF_FRIENDS, SOME_FRIENDS, NO_FRIENDS, SELF, CUSTOM, EMPTY;

		public static PrivacyType getInstance(String privacyTypeString) {
			if (privacyTypeString == null) {
				return null;
			}
			if (privacyTypeString.equals("")) {
				return PrivacyType.EMPTY;
			}
			if (privacyTypeString.toUpperCase().equals("FRIENDS")) {
				return PrivacyType.ALL_FRIENDS;
			}
			for (PrivacyType privacyType : PrivacyType.values()) {
				if (privacyType.toString().equals(
						privacyTypeString.toUpperCase())) {
					return privacyType;
				}
			}
			return null;
		}
	}
	
	private final PrivacyType value;
	private final PrivacyType friends;
	private final List<String> networks;
	private final List<String> allow;
	private final List<String> deny;
	private final List<String> description;

	@JsonCreator
	public Privacy(
			@JsonProperty("value") String value,
			@JsonProperty("friends") String friends,
			@JsonProperty("networks") List<String> networks,
			@JsonProperty("allow") List<String> allow,
			@JsonProperty("deny") List<String> deny,
			@JsonProperty("description") List<String> description) {
		this.value = PrivacyType.getInstance(value);
		this.friends = PrivacyType.getInstance(friends);
		this.networks = networks;
		this.allow = allow;
		this.deny = deny;
		this.description = description;
	}

	@JsonProperty("value")
	public String getValueString() {
		return value.name();
	}
	
	@JsonIgnore
	public PrivacyType getValue() {
		return value;
	}

	@JsonProperty("friends")
	public String getFriendsString() {
		return friends.name();
	}
	
	@JsonIgnore
	public PrivacyType getFriends() {
		return friends;
	}

	@JsonProperty("networks")
	public List<String> getNetworks() {
		return networks;
	}

	@JsonProperty("allow")
	public List<String> getAllow() {
		return allow;
	}

	@JsonProperty("deny")
	public List<String> getDeny() {
		return deny;
	}

	@JsonProperty("description")
	public List<String> getDescription() {
		return description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((allow == null) ? 0 : allow.hashCode());
		result = prime * result + ((deny == null) ? 0 : deny.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((friends == null) ? 0 : friends.hashCode());
		result = prime * result
				+ ((networks == null) ? 0 : networks.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Privacy other = (Privacy) obj;
		if (allow == null) {
			if (other.allow != null)
				return false;
		} else if (!allow.equals(other.allow))
			return false;
		if (deny == null) {
			if (other.deny != null)
				return false;
		} else if (!deny.equals(other.deny))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (friends != other.friends)
			return false;
		if (networks == null) {
			if (other.networks != null)
				return false;
		} else if (!networks.equals(other.networks))
			return false;
		if (value != other.value)
			return false;
		return true;
	}
}
