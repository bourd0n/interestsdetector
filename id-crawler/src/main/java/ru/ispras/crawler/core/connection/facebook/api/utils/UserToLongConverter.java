package ru.ispras.crawler.core.connection.facebook.api.utils;

import java.util.ArrayList;
import java.util.List;

import ru.ispras.crawler.core.datamodel.facebook.api.entities.User;

public class UserToLongConverter {
	public static Long getLong(User user){
		return Long.parseLong(user.getId());
	}
	
	public static List<Long> getLongList(List<User> users){
		List<Long> result = new ArrayList<Long>();
		for (User user: users){
			result.add(getLong(user));
		}
		return result;
	}
}
