package ru.ispras.crawler.core.requestidconverter.facebook.api;

import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIVideoRequest;

public class FacebookAPIVideoRequestIdConverter extends AbstractFacebookAPIRequestIdConverter<FacebookAPIVideoRequest> {
	@Override
	public FacebookAPIVideoRequest getRequest(Long id) {
		return new FacebookAPIVideoRequest(id);
	}
}
