package ru.ispras.crawler.core.datamodel.grouping;

import java.util.Collections;
import java.util.Map;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * {@link IResponse} which provides {@link IResponse} per {@link IRequest} type.
 * 
 * @author Ivan Andrianov
 * 
 */
public class GroupingResponse implements IResponse {
	private final Map<Class<? extends IRequest>, IResponse> responses;

	public GroupingResponse(Map<Class<? extends IRequest>, IResponse> responses) {
		this.responses = Collections.unmodifiableMap(responses);
	}

	public Map<Class<? extends IRequest>, IResponse> getResponses() {
		return responses;
	}
	
	public IResponse getResponse(Class<? extends IRequest> cls) {
		return responses.get(cls);
	}
}
