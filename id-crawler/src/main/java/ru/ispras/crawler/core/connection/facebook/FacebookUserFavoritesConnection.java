package ru.ispras.crawler.core.connection.facebook;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.facebook.FacebookUserFavoritesRequest;
import ru.ispras.crawler.core.datamodel.facebook.entities.Favorites;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * {@link IConnection} for downloading favorite items list for given Facebook user
 * Example: https://m.facebook.com/profile.php&id=100001698473676&v=likes
 *
 * @author andrey g
 *
 */
public class FacebookUserFavoritesConnection extends
		AbstractFacebookConnection<FacebookUserFavoritesRequest, BasicResponse<Favorites>> {

	private static final String BASE_METHOD = "profile.php?id=%d&v=likes";
	private static final String MORE_ITEMS_SELECTOR = "#m_more_item > a";
	private static final String FAVORITES_SELECTOR = ".timeline, .interests";
	private static final String SECTION_TITLE_SELECTOR = "h4.al, h4.aps";
	private static final String LIKE_SELECTOR = "div.acw";
	
	/* Hard-coded limit is used to prevent broken Facebook page lead
	 * us to infinite loop */
	private static final int REQUEST_PAGE_LIMIT = 10;

	private static Pattern linkIdPattern = Pattern.compile("id=([0-9]+)&");
	private static Pattern linkScreenNamePattern = Pattern.compile("/([\\w\\.]+)\\?");
	
	public FacebookUserFavoritesConnection(IConnection<HttpRequest, HttpResponse> facebookHttpConnection) {
		super(facebookHttpConnection);
	}

	@Override
	public BasicResponse<Favorites> getResponse(FacebookUserFavoritesRequest request) throws UnavailableConnectionException {
		long userId = request.getId();		
		Document doc = downloadAndParsePage(String.format(BASE_METHOD, userId));
		List<Favorites.Section> sections = getSectionsAsList(doc);

		return new BasicResponse<Favorites>(new Favorites(sections));
	}
	
	private Document downloadAndParsePage(final String url) throws UnavailableConnectionException {
		String page = getHttpResponse(url);
		return Jsoup.parse(page);
	}
	
	private Element downloadFavoritesSectionElement(final String url) throws UnavailableConnectionException {
		Document doc = downloadAndParsePage(url);
		Element favoritesElement = getFavoritesElement(doc);
		return favoritesElement.children().get(1);
	}
	
	private List<Favorites.Section> getSectionsAsList(Document doc) throws UnavailableConnectionException {
		List<Favorites.Section> sections = new ArrayList<Favorites.Section>();
		Element favoritesElement = getFavoritesElement(doc);
		if (favoritesElement != null) {
			Elements sectionElements = favoritesElement.children();
			
			for (Element sectionElement : sectionElements) {
				Element titleElement = sectionElement.select(SECTION_TITLE_SELECTOR).first();
				if (titleElement != null) {
					List<String> likes = parseFavoritesSectionElement(sectionElement);
					String title = titleElement.text();
					sections.add(new Favorites.Section(title, likes));
				}
			}
		}
		return sections;
	}
	
	private Element getFavoritesElement(Document doc) {
		return doc.select(FAVORITES_SELECTOR).first();
	}
		
	private List<String> parseFavoritesSectionElement(Element sectionElement) throws UnavailableConnectionException {
		List<String> likes = new ArrayList<String>();
		
		int pageCount = 0;
		Element nextSectionElement = sectionElement;
		while(true) {
			likes.addAll(parseFavoritesList(nextSectionElement));
			String moreItemsLink = getMoreItemsLink(nextSectionElement);
			++pageCount;
			if (moreItemsLink == null || pageCount > REQUEST_PAGE_LIMIT) {
				break;
			}
			nextSectionElement = downloadFavoritesSectionElement(moreItemsLink);
		}
		
		return likes;
	}
		
	private List<String> parseFavoritesList(Element sectionElement) {
		List<String> likes = new ArrayList<String>();
		Elements likeElements = sectionElement.select(LIKE_SELECTOR);
		for (Element likeElement : likeElements) {
			// exclude "see more" and "Find Pages You May Like" elements from list
			if (!"m_more_item".equals(likeElement.attr("id"))) {
				likes.add(getLikeHref(likeElement));
			}
		}
		return likes;
	}
	
	private String getMoreItemsLink(Element sectionElement) {
		Element linkElement = sectionElement.select(MORE_ITEMS_SELECTOR).first();
		if (linkElement != null && "See More".equals(linkElement.text())) {
			return linkElement.attr("href");
		}
		return null;
	}
	
	private String getLikeHref(Element likeElement) {
		Element linkElement = likeElement.select("a").first();
		return parseFacebookHref(linkElement.attr("href"));
	}
	
	private String parseFacebookHref(String href) {
		Matcher addLinkMatcher;
		// like can have either screenName either profileId
		if (href.contains("profile.php")) {
			addLinkMatcher = linkIdPattern.matcher(href);
		} else {
			addLinkMatcher = linkScreenNamePattern.matcher(href);
		}
		
		addLinkMatcher.find();
		return addLinkMatcher.group(1);
	}
}
