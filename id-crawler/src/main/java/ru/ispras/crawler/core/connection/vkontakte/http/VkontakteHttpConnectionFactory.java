package ru.ispras.crawler.core.connection.vkontakte.http;

import ru.ispras.crawler.core.connection.IConfigurableConnectionFactory;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.ConnectionCreationException;
import ru.ispras.crawler.core.connection.http.HttpConnection;
import ru.ispras.crawler.core.connection.http.HttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpClientFactory;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpRequestConverter;
import ru.ispras.crawler.core.connection.http.ext.TimeoutHttpExceptionHandler;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * Vkontakte API {@link IConfigurableConnectionFactory} implementation
 * for connections that do not require access token authorization.
 *
 * @author andrey g
 *
 */
public class VkontakteHttpConnectionFactory implements
IConfigurableConnectionFactory<HttpRequest, HttpResponse, HttpConnectionConfiguration> {

	@Override
	public IConnection<HttpRequest, HttpResponse> create(
			HttpConnectionConfiguration configuration)
			throws ConnectionCreationException {
		return new HttpConnection<>(new DefaultHttpRequestConverter(), new DefaultHttpClientFactory(),
				configuration, new VkontakteHttpResponseConverter(), new TimeoutHttpExceptionHandler());
	}

}
