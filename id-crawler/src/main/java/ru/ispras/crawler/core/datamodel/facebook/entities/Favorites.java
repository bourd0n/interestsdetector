package ru.ispras.crawler.core.datamodel.facebook.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Favorites {
	private final List<Section> sections;

	@JsonCreator
	public Favorites(@JsonProperty("sections") List<Section> sections) {
		super();
		this.sections = sections;
	}

	public static class Section{
		private final String title;
		private final List<String> favorites;
		
		@JsonCreator
		public Section(
				@JsonProperty("title") String title,
				@JsonProperty("favorites") List<String> favorites) {
			this.title = title;
			this.favorites = favorites;
		}

		@JsonProperty("title")
		public String getTitle() {
			return title;
		}

		@JsonProperty("favorites")
		public List<String> getFavorites() {
			return favorites;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((favorites == null) ? 0 : favorites.hashCode());
			result = prime * result + ((title == null) ? 0 : title.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Section other = (Section) obj;
			if (favorites == null) {
				if (other.favorites != null)
					return false;
			} else if (!favorites.equals(other.favorites))
				return false;
			if (title == null) {
				if (other.title != null)
					return false;
			} else if (!title.equals(other.title))
				return false;
			return true;
		}
	}

	@JsonProperty("sections")
	public List<Section> getSections() {
		return sections;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((sections == null) ? 0 : sections.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Favorites other = (Favorites) obj;
		if (sections == null) {
			if (other.sections != null)
				return false;
		} else if (!sections.equals(other.sections))
			return false;
		return true;
	}
}
