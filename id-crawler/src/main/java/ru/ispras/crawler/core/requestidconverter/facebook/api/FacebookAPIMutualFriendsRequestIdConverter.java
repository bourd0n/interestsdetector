package ru.ispras.crawler.core.requestidconverter.facebook.api;

import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIMutualFriendsRequest;

public class FacebookAPIMutualFriendsRequestIdConverter extends
		AbstractFacebookAPIRequestIdConverter<FacebookAPIMutualFriendsRequest> {
	@Override
	public FacebookAPIMutualFriendsRequest getRequest(Long id) {
		return new FacebookAPIMutualFriendsRequest(id);
	}
}
