package ru.ispras.crawler.core.datamodel.zvents;

import java.net.URL;
import java.util.Calendar;
import java.util.Collection;

/**
 * Response, representing information from Zvents event page.
 * 
 * @author sysoev
 */
public class EventZventsResponse implements IZventsResponse {
	
	private final String id;
	private final String locationId;
	private final URL url;
	private final String name;
	private final String description;
	private final Calendar calendar;
	private final Collection<Review> reviews;
	
	public EventZventsResponse(
			String id, String locationId,
			URL url, String name, String description,
			Calendar calendar, Collection<Review> reviews) {
		super();
		this.id = id;
		this.locationId = locationId;
		this.url = url;
		this.name = name;
		this.description = description;
		this.calendar = calendar;
		this.reviews = reviews;
	}

	// ********************************************************************

	public String getId() {
		return id;
	}

	public String getLocationId() {
		return locationId;
	}

	public URL getUrl() {
		return url;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public Calendar getCalendar() {
		return calendar;
	}

	public Collection<Review> getReviews() {
		return reviews;
	}

	public static class Review {
		private final String title;
		private final String text;
		private final String author;

		public Review(String title, String text, String author) {
			super();
			this.title = title;
			this.text = text;
			this.author = author;
		}

		public String getTitle() {
			return title;
		}

		public String getText() {
			return text;
		}

		public String getAuthor() {
			return author;
		}
	}
}
