package ru.ispras.crawler.core.connection.twitter;

import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserSubscribedListsRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.UserList;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * {@link IConnection} for downloading lists that given user is subscribed to
 *
 * See https://dev.twitter.com/rest/reference/get/lists/list
 */
public class TwitterUserSubscribedListsConnection extends
		AbstractTwitterConnection<TwitterUserSubscribedListsRequest, BasicResponse<List<UserList>>> {
	private static final String METHOD_BASE = "lists/list.json?%s";

	public TwitterUserSubscribedListsConnection(IConnection<HttpRequest, HttpResponse> twitterHttpConnection) {
		super(twitterHttpConnection);
	}

	@Override
	public BasicResponse<List<UserList>> getResponse(TwitterUserSubscribedListsRequest request)
			throws UnavailableConnectionException {
		String method = String.format(METHOD_BASE,
				getIdParameter(request));
		
		List<UserList> response = getHttpResponse(method, new TypeReference<List<UserList>>() {});

		return new BasicResponse<List<UserList>>(response);
	}
}
