package ru.ispras.crawler.core.datamodel.vkontakte;

/**
 * Request for https://vk.com/dev/groups.get method
 * 
 * @author andrey
 *
 */
public class VkontakteUserGroupsRequest extends AbstractVkontakteLimitedRequest {
	private final long userId;

	public VkontakteUserGroupsRequest(long userId) {
		super();
		this.userId = userId;
	}
	
	public VkontakteUserGroupsRequest(long userId, int limit) {
		super(limit);
		this.userId = userId;
	}

	public long getUserId() {
		return userId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (userId ^ (userId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VkontakteUserGroupsRequest other = (VkontakteUserGroupsRequest) obj;
		if (userId != other.userId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VkontakteUserGroupsRequest [userId=" + userId + "]";
	}
}
