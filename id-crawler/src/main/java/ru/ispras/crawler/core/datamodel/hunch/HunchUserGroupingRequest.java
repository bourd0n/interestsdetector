package ru.ispras.crawler.core.datamodel.hunch;

import java.util.List;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.grouping.GroupingRequest;

/**
 *  {@link GroupingRequest} that contains requests related to one one Hunch user
 */
public abstract class HunchUserGroupingRequest extends GroupingRequest {
	private final String screenName;

	public HunchUserGroupingRequest(String screenName, IRequest... requests) {
		super(requests);
		this.screenName = screenName;
	}

	public HunchUserGroupingRequest(String screenName, List<? extends IRequest> requests) {
		super(requests);
		this.screenName = screenName;
	}

	public String getScreenName() {
		return screenName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((screenName == null) ? 0 : screenName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HunchUserGroupingRequest other = (HunchUserGroupingRequest) obj;
		if (screenName == null) {
			if (other.screenName != null)
				return false;
		} else if (!screenName.equals(other.screenName))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + ":[screenName=" + screenName + "]";
	}
}
