package ru.ispras.crawler.core.connection.vkontakte.staticdata;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteRegionsRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 28.05.14
 * Time: 15:17
 */

/**
 * see description in https://vk.com/dev/database.getRegions
 */
public class VkontakteRegionsConnection extends AbstractVkontakteIdTitleConnection<VkontakteRegionsRequest> {
    public VkontakteRegionsConnection(IConnection<HttpRequest, HttpResponse> httpConnection) {
        super(httpConnection);
    }

    @Override
    protected String getMethod(VkontakteRegionsRequest request) {
        return "database.getRegions?country_id=" + request.getCountryId();
    }
}
