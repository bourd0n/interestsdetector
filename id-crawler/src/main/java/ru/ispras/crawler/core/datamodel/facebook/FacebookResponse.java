package ru.ispras.crawler.core.datamodel.facebook;

import org.jsoup.helper.Validate;

import ru.ispras.crawler.core.datamodel.BasicResponse;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * {@link BasicResponse} that additionally stores raw data (HTTP page)
 * @param <T>
 */
public class FacebookResponse<T> extends BasicResponse<T>{
	private final String rawData;
	
	@JsonCreator
	public FacebookResponse(@JsonProperty("object") T object, @JsonProperty("raw-data") String rawData) {
		super(object);
		Validate.notNull(rawData);
		this.rawData=rawData;
	}
	
	@JsonProperty("raw-data") 
	public String getRawData() {
		return rawData;
	}
}
