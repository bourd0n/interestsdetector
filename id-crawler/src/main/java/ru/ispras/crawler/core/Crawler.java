package ru.ispras.crawler.core;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.saver.ISaver;
import ru.ispras.crawler.core.visitor.IVisitor;

/**
 * Crawler, which
 * <ul>
 * <li> accepts a number of seed requests,
 * <li> computes responses for them,
 * <li> generates requests, based on the computed responses,
 * <li> fulfills these newly generated requests,
 * <li> and so on...
 * </ul>
 * <p> 
 * Request of each type is processed by the corresponding {@link RequestProcessor}.
 * 
 * @author sysoev
 */
public class Crawler {
	/**
	 * Configuration of {@link Crawler} instance.
	 * <p>
	 * Provides a type-safe way to setup needed {@link IConnection},
	 * {@link ISaver} and {@link ISaver} instances.
	 * 
	 * @author Ivan Andrianov
	 */
	public static class Configuration {
		private final Map<Class<? extends IRequest>, IConnection<?, ?>> connections = new HashMap<>();
		private final Map<Class<? extends IRequest>, IVisitor<?, ?>> visitors = new HashMap<>();
		private final Map<Class<? extends IRequest>, ISaver<?, ?>> savers = new HashMap<>();

		public <Req extends IRequest, Resp extends IResponse> void put(Class<Req> type,
				IConnection<? super Req, Resp> connection,
				IVisitor<? super Req, ? super Resp> visitor,
				ISaver<? super Req, ? super Resp> saver) {
			connections.put(type, connection);
			visitors.put(type, visitor);
			savers.put(type, saver);
		}
	}

	private Map<Class<? extends IRequest>, RequestProcessor<IRequest, IResponse>> requestProcessors;

	public Crawler(Configuration configuration) {
		this(configuration, 20);
	}

	public Crawler(Configuration configuration, int numThreads) {
		validateConfig(configuration);
		requestProcessors = createRequestProcessors(configuration, numThreads);
	}

	private void validateConfig(Configuration configuration) {
		final Set<Class<? extends IRequest>> requestTypes = configuration.connections.keySet();
		for (Class<? extends IRequest> requestType : requestTypes) {
			for (Class<? extends IRequest> producedRequestType : configuration.visitors.get(requestType)
					.getProducedRequestTypes()) {
				if (!requestTypes.contains(producedRequestType)) {
					throw new IllegalArgumentException(String.format(
							"Visitor for %s requests produces %s requests which are not configured.",
							requestType.getName(), producedRequestType.getName()));
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private Map<Class<? extends IRequest>, RequestProcessor<IRequest, IResponse>> createRequestProcessors(
			Configuration config, int numThreads) {
		final Map<Class<? extends IRequest>, RequestProcessor<IRequest, IResponse>> processors = new HashMap<>();
		for (Class<? extends IRequest> requestType : config.connections.keySet()) {
			IConnection<IRequest, IResponse> connection = (IConnection<IRequest, IResponse>) config.connections
					.get(requestType);
			IVisitor<IRequest, IResponse> visitor = (IVisitor<IRequest, IResponse>) config.visitors.get(requestType);
			ISaver<IRequest, IResponse> saver = (ISaver<IRequest, IResponse>) config.savers.get(requestType);
			processors.put(requestType, new RequestProcessor<IRequest, IResponse>(this, connection, visitor, saver,
					numThreads));
		}
		return processors;
	}

	// --------------------------------------------------------------------------------------

	/**
	 * Crawl provided requests.
	 */
	public void crawl(IRequest... seedRequests) {
		crawl(Arrays.asList(seedRequests));
	}

	/**
	 * Crawl provided requests.
	 */
	public void crawl(Collection<? extends IRequest> seedRequests) {
		addRequests(seedRequests);
	}

	/**
	 * Schedule provided requests for being processed.
	 */
	protected void addRequests(Collection<? extends IRequest> requests) {
		for (IRequest request : requests) {
			final RequestProcessor<IRequest, IResponse> processor = requestProcessors.get(request.getClass());
			processor.processRequest(request);
		}
	}

	// --------------------------------------------------------------------------------------

	/**
	 * Wait for all crawling requests to be finished. Then shutdown crawler - 
	 * close visitors and free resources.
	 */
	public void awaitTerminationAndShutdown() {
		awaitTermination();
		shutdown();
	}

	public void awaitTermination() {
		awaitTermination(60, 3);
	}

	/**
	 * Wait for crawling termination.
	 * <p>
	 * This method will cyclically sleep for <code>secondsToWait</code> seconds and check, whether
	 * all {@link RequestProcessor}s are empty. When there are <code>numRoundsToDetectReadyState</code>
	 * subsequent confirmations, that all {@link RequestProcessor}s are empty, crawler terminates them
	 * and shuts down.
	 * 
	 * @param secondsToWait
	 * @param numRoundsToDetectReadyState
	 */
	public void awaitTermination(
			final int secondsToWait, final int numRoundsToDetectReadyState) {
		int numConfirmed = 0;
		while (true) {
			sleep(secondsToWait);
			if (areAllRequestProcessorsEmpty()) {
				numConfirmed++;
				if (numConfirmed >= numRoundsToDetectReadyState) {
					break;
				}
			}
			else {
				numConfirmed = 0;
			}
		}
	}

	private void sleep(final int secondsToWait) throws RuntimeException {
		try {
			Thread.sleep(secondsToWait * 1000);
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	private boolean areAllRequestProcessorsEmpty() {
		for (RequestProcessor<IRequest, IResponse> processor : requestProcessors.values()) {
			if (!processor.isEmpty()) {
				return false;
			}
		}
		return true;
	}
	
	// --------------------------------------------------------------------------------------
	
	/**
	 * Shutdown all subordinate {@link RequestProcessor}s.
	 */
	public void shutdown() {
		for (RequestProcessor<IRequest, IResponse> processor : requestProcessors.values()) {
			processor.shutdown();
		}
	}
}
