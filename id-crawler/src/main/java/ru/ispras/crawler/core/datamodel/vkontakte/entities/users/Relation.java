package ru.ispras.crawler.core.datamodel.vkontakte.entities.users;

/**
 * Created by padre on 14.07.14
 */
public enum Relation {
    NONMARRIED, FRIEND, ENGAGEMENT, MARRIED, DIFFICULT, ACTIVESEARCH, INLOVE, UNKNOWN;

    public static Relation intToRelation(Integer number) {
        if (number == null) return UNKNOWN;
        switch (number) {
            case 1: return NONMARRIED;
            case 2: return FRIEND;
            case 3: return ENGAGEMENT;
            case 4: return MARRIED;
            case 5: return DIFFICULT;
            case 6: return ACTIVESEARCH;
            case 7: return INLOVE;
            default: return UNKNOWN;
        }
    }

    public static int relationToInt(Relation status) {
        switch (status) {
            case NONMARRIED: return 1;
            case FRIEND: return 2;
            case ENGAGEMENT: return 3;
            case MARRIED: return 4;
            case DIFFICULT: return 5;
            case ACTIVESEARCH: return 6;
            case INLOVE: return 7;
            default: return 0;
        }
    }
}
