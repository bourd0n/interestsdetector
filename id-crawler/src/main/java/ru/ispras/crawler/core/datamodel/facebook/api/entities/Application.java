/*
 * Copyright 2012 Ryuji Yamashita
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.facebook.api.entities;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ryuji Yamashita - roundrop at gmail.com, andrey g
 */
public class Application {
	private final String id;
	private final String name;
	private final String description;
	private final String category;
	private final String company;
	private final String iconUrl;
	private final String subcategory;
	private final String link;
	private final String logoUrl;
	private final String dailyActiveUsers;
	private final String weeklyActiveUsers;
	private final String monthlyActiveUsers;
	private final Map<String, Boolean> migrations;
	private final String namespace;
	private final Map<String, String> restrictions;
	private final List<String> appDomains;
	private final String authDialogDataHelpUrl;
	private final String authDialogDescription;
	private final String authDialogHeadline;
	private final String authDialogPermsExplanation;
	private final List<String> authReferralUserPerms;
	private final List<String> authReferralFriendPerms;
	private final String authReferralDefaultActivityPrivacy;
	private final Boolean authReferralEnabled;
	private final List<String> authReferralExtendedPerms;
	private final String authReferralResponseType;
	private final Boolean canvasFluidHeight;
	private final Boolean canvasFluidWidth;
	private final String canvasUrl;
	private final String contactEmail;
	private final Long createdTime;
	private final Long creatorUid;
	private final String deauthCallbackUrl;
	private final String iphoneAppStoreId;
	private final String hostingUrl;
	private final String mobileWebUrl;
	private final String pageTabDefaultName;
	private final String pageTabUrl;
	private final String privacyPolicyUrl;
	private final String secureCanvasUrl;
	private final String securePageTabUrl;
	private final String serverIpWhitelist;
	private final Boolean socialDiscovery;
	private final String termsOfServiceUrl;
	private final String userSupportEmail;
	private final String userSupportUrl;
	private final String websiteUrl;
	private String canvasName;

	@JsonCreator
	public Application(
			@JsonProperty("id") String id,
			@JsonProperty("name") String name,
			@JsonProperty("description") String description,
			@JsonProperty("category") String category,
			@JsonProperty("company") String company,
			@JsonProperty("icon_url") String iconUrl,
			@JsonProperty("subcategory") String subcategory,
			@JsonProperty("link") String link,
			@JsonProperty("logo_url") String logoUrl,
			@JsonProperty("daily_active_users") String dailyActiveUsers,
			@JsonProperty("weekly_active_users") String weeklyActiveUsers,
			@JsonProperty("monthly_active_users") String monthlyActiveUsers,
			@JsonProperty("migrations") Map<String, Boolean> migrations,
			@JsonProperty("namespace") String namespace,
			@JsonProperty("restrictions") Map<String, String> restrictions,
			@JsonProperty("app_domains") List<String> appDomains,
			@JsonProperty("auth_dialog_data_help_url") String authDialogDataHelpUrl,
			@JsonProperty("auth_dialog_description") String authDialogDescription,
			@JsonProperty("auth_dialog_headline") String authDialogHeadline,
			@JsonProperty("authd_ialog_perms_explanation") String authDialogPermsExplanation,
			@JsonProperty("auth_referral_user_perms") List<String> authReferralUserPerms,
			@JsonProperty("auth_referral_friend_perms") List<String> authReferralFriendPerms,
			@JsonProperty("auth_referral_default_activity_privacy") String authReferralDefaultActivityPrivacy,
			@JsonProperty("auth_referral_enabled") Boolean authReferralEnabled,
			@JsonProperty("auth_referral_extended_perms") List<String> authReferralExtendedPerms,
			@JsonProperty("auth_referral_response_type") String authReferralResponseType,
			@JsonProperty("canvas_fluid_height") Boolean canvasFluidHeight,
			@JsonProperty("canvas_fluid_width") Boolean canvasFluidWidth,
			@JsonProperty("canvas_url") String canvasUrl,
			@JsonProperty("contact_email") String contactEmail,
			@JsonProperty("created_time") Long createdTime,
			@JsonProperty("creator_uid") Long creatorUid,
			@JsonProperty("deauth_callback_url") String deauthCallbackUrl,
			@JsonProperty("iphone_app_store_id") String iphoneAppStoreId,
			@JsonProperty("hosting_url") String hostingUrl,
			@JsonProperty("mobile_web_url") String mobileWebUrl,
			@JsonProperty("page_tab_default_name") String pageTabDefaultName,
			@JsonProperty("page_tab_url") String pageTabUrl,
			@JsonProperty("privacy_policy_url") String privacyPolicyUrl,
			@JsonProperty("secure_canvas_url") String secureCanvasUrl,
			@JsonProperty("secure_page_tab_url") String securePageTabUrl,
			@JsonProperty("server_ip_whitelist") String serverIpWhitelist,
			@JsonProperty("social_discovery") Boolean socialDiscovery,
			@JsonProperty("terms_of_service_url") String termsOfServiceUrl,
			@JsonProperty("user_support_email") String userSupportEmail,
			@JsonProperty("user_support_url") String userSupportUrl,
			@JsonProperty("website_url") String websiteUrl,
			@JsonProperty("canvas_name") String canvasName) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.category = category;
		this.company = company;
		this.iconUrl = iconUrl;
		this.subcategory = subcategory;
		this.link = link;
		this.logoUrl = logoUrl;
		this.dailyActiveUsers = dailyActiveUsers;
		this.weeklyActiveUsers = weeklyActiveUsers;
		this.monthlyActiveUsers = monthlyActiveUsers;
		this.migrations = migrations;
		this.namespace = namespace;
		this.restrictions = restrictions;
		this.appDomains = appDomains;
		this.authDialogDataHelpUrl = authDialogDataHelpUrl;
		this.authDialogDescription = authDialogDescription;
		this.authDialogHeadline = authDialogHeadline;
		this.authDialogPermsExplanation = authDialogPermsExplanation;
		this.authReferralUserPerms = authReferralUserPerms;
		this.authReferralFriendPerms = authReferralFriendPerms;
		this.authReferralDefaultActivityPrivacy = authReferralDefaultActivityPrivacy;
		this.authReferralEnabled = authReferralEnabled;
		this.authReferralExtendedPerms = authReferralExtendedPerms;
		this.authReferralResponseType = authReferralResponseType;
		this.canvasFluidHeight = canvasFluidHeight;
		this.canvasFluidWidth = canvasFluidWidth;
		this.canvasUrl = canvasUrl;
		this.contactEmail = contactEmail;
		this.createdTime = createdTime;
		this.creatorUid = creatorUid;
		this.deauthCallbackUrl = deauthCallbackUrl;
		this.iphoneAppStoreId = iphoneAppStoreId;
		this.hostingUrl = hostingUrl;
		this.mobileWebUrl = mobileWebUrl;
		this.pageTabDefaultName = pageTabDefaultName;
		this.pageTabUrl = pageTabUrl;
		this.privacyPolicyUrl = privacyPolicyUrl;
		this.secureCanvasUrl = secureCanvasUrl;
		this.securePageTabUrl = securePageTabUrl;
		this.serverIpWhitelist = serverIpWhitelist;
		this.socialDiscovery = socialDiscovery;
		this.termsOfServiceUrl = termsOfServiceUrl;
		this.userSupportEmail = userSupportEmail;
		this.userSupportUrl = userSupportUrl;
		this.websiteUrl = websiteUrl;
		this.canvasName = canvasName;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("category")
	public String getCategory() {
		return category;
	}

	@JsonProperty("company")
	public String getCompany() {
		return company;
	}

	@JsonProperty("icon_url")
	public String getIconUrl() {
		return iconUrl;
	}

	@JsonProperty("subcategory")
	public String getSubcategory() {
		return subcategory;
	}

	@JsonProperty("link")
	public String getLink() {
		return link;
	}

	@JsonProperty("logo_url")
	public String getLogoUrl() {
		return logoUrl;
	}

	@JsonProperty("daily_active_users")
	public String getDailyActiveUsers() {
		return dailyActiveUsers;
	}

	@JsonProperty("weekly_active_users")
	public String getWeeklyActiveUsers() {
		return weeklyActiveUsers;
	}

	@JsonProperty("monthly_active_users")
	public String getMonthlyActiveUsers() {
		return monthlyActiveUsers;
	}

	@JsonProperty("migrations")
	public Map<String, Boolean> getMigrations() {
		return migrations;
	}
	
	@JsonProperty("namespace")
	public String getNamespace() {
		return namespace;
	}

	@JsonProperty("restrictions")
	public Map<String, String> getRestrictions() {
		return restrictions;
	}

	@JsonProperty("app_domains")
	public List<String> getAppDomains() {
		return appDomains;
	}

	@JsonProperty("auth_dialog_data_help_url")
	public String getAuthDialogDataHelpUrl() {
		return authDialogDataHelpUrl;
	}

	@JsonProperty("auth_dialog_description")
	public String getAuthDialogDescription() {
		return authDialogDescription;
	}
	
	@JsonProperty("auth_dialog_headline")
	public String getAuthDialogHeadline() {
		return authDialogHeadline;
	}
	
	@JsonProperty("authd_ialog_perms_explanation")
	public String getAuthDialogPermsExplanation() {
		return authDialogPermsExplanation;
	}
	
	@JsonProperty("auth_referral_user_perms")
	public List<String> getAuthReferralUserPerms() {
		return authReferralUserPerms;
	}
	
	@JsonProperty("auth_referral_friend_perms")
	public List<String> getAuthReferralFriendPerms() {
		return authReferralFriendPerms;
	}
	
	@JsonProperty("auth_referral_default_activity_privacy")
	public String getAuthReferralDefaultActivityPrivacy() {
		return authReferralDefaultActivityPrivacy;
	}

	@JsonProperty("auth_referral_enabled")
	public Boolean authReferralEnabled() {
		return authReferralEnabled;
	}

	@JsonProperty("auth_referral_extended_perms")
	public List<String> getAuthReferralExtendedPerms() {
		return authReferralExtendedPerms;
	}
	
	@JsonProperty("auth_referral_response_type")
	public String getAuthReferralResponseType() {
		return authReferralResponseType;
	}

	@JsonProperty("canvas_fluid_height")
	public Boolean canvasFluidHeight() {
		return canvasFluidHeight;
	}
	
	@JsonProperty("canvas_fluid_width")
	public Boolean canvasFluidWidth() {
		return canvasFluidWidth;
	}
	
	@JsonProperty("canvas_url")
	public String getCanvasUrl() {
		return canvasUrl;
	}
	
	@JsonProperty("contact_email")
	public String getContactEmail() {
		return contactEmail;
	}
	
	@JsonProperty("created_time")
	public Long getCreatedTime() {
		return createdTime;
	}
	
	@JsonProperty("creator_uid")
	public Long getCreatorUid() {
		return creatorUid;
	}
	
	@JsonProperty("deauth_callback_url")
	public String getDeauthCallbackUrl() {
		return deauthCallbackUrl;
	}
	
	@JsonProperty("iphone_app_store_id")
	public String getIphoneAppStoreId() {
		return iphoneAppStoreId;
	}
	
	@JsonProperty("hosting_url")
	public String getHostingUrl() {
		return hostingUrl;
	}
	
	@JsonProperty("mobile_web_url")
	public String getMobileWebUrl() {
		return mobileWebUrl;
	}
	
	@JsonProperty("page_tab_default_name")
	public String getPageTabDefaultName() {
		return pageTabDefaultName;
	}
	
	@JsonProperty("page_tab_url")
	public String getPageTabUrl() {
		return pageTabUrl;
	}
	
	@JsonProperty("privacy_policy_url")
	public String getPrivacyPolicyUrl() {
		return privacyPolicyUrl;
	}
	
	@JsonProperty("secure_canvas_url")
	public String getSecureCanvasUrl() {
		return secureCanvasUrl;
	}
	
	@JsonProperty("secure_page_tab_url")
	public String getSecurePageTabUrl() {
		return securePageTabUrl;
	}
	
	@JsonProperty("server_ip_whitelist")
	public String getServerIpWhitelist() {
		return serverIpWhitelist;
	}
	
	@JsonProperty("social_discovery")
	public Boolean socialDiscovery() {
		return socialDiscovery;
	}
	
	@JsonProperty("terms_of_service_url")
	public String getTermsOfServiceUrl() {
		return termsOfServiceUrl;
	}
	
	@JsonProperty("user_support_email")
	public String getUserSupportEmail() {
		return userSupportEmail;
	}
	
	@JsonProperty("user_support_url")
	public String getUserSupportUrl() {
		return userSupportUrl;
	}
	
	@JsonProperty("website_url")
	public String getWebsiteUrl() {
		return websiteUrl;
	}
	
	@JsonProperty("canvas_name")
	public String getCanvasName() {
		return canvasName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Application other = (Application) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
