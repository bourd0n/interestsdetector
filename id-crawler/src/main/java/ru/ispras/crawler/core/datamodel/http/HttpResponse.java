package ru.ispras.crawler.core.datamodel.http;

import java.nio.charset.Charset;

import ru.ispras.crawler.core.datamodel.IResponse;

public class HttpResponse implements IResponse {
	private final byte[] bytes;
	private final Charset charset;

	public HttpResponse(byte[] bytes, Charset charset) {
		this.bytes = bytes;
		this.charset = charset;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public String getString() {
		return new String(bytes, charset);
	}
}
