package ru.ispras.crawler.core.connection.vkontakte.staticdata;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 28.05.14
 * Time: 16:15
 */

import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.City;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteCitiesRequest;

/**
 * see description in https://vk.com/dev/database.getCities
 */
public class VkontakteCitiesConnection implements IConnection<VkontakteCitiesRequest, BasicResponse<List<City>>> {
    private final IConnection<HttpRequest, HttpResponse> httpConnection;

    public VkontakteCitiesConnection(IConnection<HttpRequest, HttpResponse> httpConnection) {
        this.httpConnection = httpConnection;
    }

    private String getMethod(VkontakteCitiesRequest request) {
        return "database.getCities?need_all=1&country_id=" + request.getCountryId();
    }

    @Override
    public BasicResponse<List<City>> getResponse(VkontakteCitiesRequest request) throws UnavailableConnectionException {
        String url = getMethod(request);
        List<City> response = VkontakteConnectionUtils.getPagedResponse(url, 1000, Integer.MAX_VALUE, httpConnection, City.class);
        return new BasicResponse<>(response);
    }
}
