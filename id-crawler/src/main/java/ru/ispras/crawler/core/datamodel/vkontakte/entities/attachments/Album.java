package ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 16.05.14
 * Time: 17:48
 */
public class Album  extends Attachment {
	public static class Thumb {
	    private final Long pid;
	    private final Long aid;
	    private final Long ownerId;
	    private final String src;
	    private final String srcBig;
	    private final String srcSmall;
	    private final String srcXbig;
	    private final String srcXxbig;
	    private final String srcXxxbig;
	    private final int width;
	    private final int height;
	    private final String text;
	    private final Long created;
	    private final String accessKey;

	    @JsonCreator
	    public Thumb(
	            @JsonProperty("pid") Long pid,
	            @JsonProperty("aid") Long aid,
	            @JsonProperty("owner_id") Long ownerId,
	            @JsonProperty("src") String src,
	            @JsonProperty("src_big") String srcBig,
	            @JsonProperty("src_small") String srcSmall,
	            @JsonProperty("src_xbig") String srcXbig,
	            @JsonProperty("src_xxbig") String srcXxbig,
	            @JsonProperty("src_xxxbig") String srcXxxbig,
	            @JsonProperty("width") int width,
	            @JsonProperty("height") int height,
	            @JsonProperty("text") String text,
	            @JsonProperty("created") Long created,
	            @JsonProperty("access_key") String accessKey) {
	        this.pid = pid;
	        this.aid = aid;
	        this.ownerId = ownerId;
	        this.src = src;
	        this.srcBig = srcBig;
	        this.srcSmall = srcSmall;
	        this.srcXbig = srcXbig;
	        this.srcXxbig = srcXxbig;
	        this.srcXxxbig = srcXxxbig;
	        this.width = width;
	        this.height = height;
	        this.text = text;
	        this.created = created;
	        this.accessKey = accessKey;
	    }
	    @JsonProperty("pid")
	    public Long getPid() {
	        return pid;
	    }
	    @JsonProperty("aid")
	    public Long getAid() {
	        return aid;
	    }
	    @JsonProperty("owner_id")
	    public Long getOwnerId() {
	        return ownerId;
	    }
	    @JsonProperty("src")
	    public String getSrc() {
	        return src;
	    }
	    @JsonProperty("src_big")
	    public String getSrcBig() {
	        return srcBig;
	    }
	    @JsonProperty("src_small")
	    public String getSrcSmall() {
	        return srcSmall;
	    }
	    @JsonProperty("src_xbig")
	    public String getSrcXbig() {
	        return srcXbig;
	    }
	    @JsonProperty("src_xxbig")
	    public String getSrcXxbig() {
	        return srcXxbig;
	    }
	    @JsonProperty("src_xxxbig")
	    public String getSrcXxxbig() {
	        return srcXxxbig;
	    }
	    @JsonProperty("width")
	    public int getWidth() {
	        return width;
	    }
	    @JsonProperty("height")
	    public int getHeight() {
	        return height;
	    }
	    @JsonProperty("text")
	    public String getText() {
	        return text;
	    }
	    @JsonProperty("created")
	    public Long getCreated() {
	        return created;
	    }
	    @JsonProperty("access_key")
	    public String getAccessKey() {
	        return accessKey;
	    }
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((aid == null) ? 0 : aid.hashCode());
			result = prime * result
					+ ((ownerId == null) ? 0 : ownerId.hashCode());
			result = prime * result + ((pid == null) ? 0 : pid.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Thumb other = (Thumb) obj;
			if (aid == null) {
				if (other.aid != null)
					return false;
			} else if (!aid.equals(other.aid))
				return false;
			if (ownerId == null) {
				if (other.ownerId != null)
					return false;
			} else if (!ownerId.equals(other.ownerId))
				return false;
			if (pid == null) {
				if (other.pid != null)
					return false;
			} else if (!pid.equals(other.pid))
				return false;
			return true;
		}	    
	}

    private final Long aid;
    private final Thumb thumb;
    private final Long owner_id;
    private final String title;
    private final String description;
    private final Long created;
    private final Long updated;
    private final int size;

    @JsonCreator
    public Album(
            @JsonProperty("aid") Long aid,
            @JsonProperty("thumb") Thumb thumb,
            @JsonProperty("owner_id") Long owner_id,
            @JsonProperty("title") String title,
            @JsonProperty("description") String description,
            @JsonProperty("created") Long created,
            @JsonProperty("updated") Long updated,
            @JsonProperty("size") int size) {
        this.aid = aid;
        this.thumb = thumb;
        this.owner_id = owner_id;
        this.title = title;
        this.description = description;
        this.created = created;
        this.updated = updated;
        this.size = size;
    }

    @JsonProperty("aid")
    public Long getAid() {
        return aid;
    }

    @JsonProperty("thumb")
    public Thumb getThumb() {
        return thumb;
    }

    @JsonProperty("owner_id")
    public Long getOwner_id() {
        return owner_id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("created")
    public Long getCreated() {
        return created;
    }

    @JsonProperty("updated")
    public Long getUpdated() {
        return updated;
    }

    @JsonProperty("size")
    public int getSize() {
        return size;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((aid == null) ? 0 : aid.hashCode());
		result = prime * result
				+ ((owner_id == null) ? 0 : owner_id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Album other = (Album) obj;
		if (aid == null) {
			if (other.aid != null)
				return false;
		} else if (!aid.equals(other.aid))
			return false;
		if (owner_id == null) {
			if (other.owner_id != null)
				return false;
		} else if (!owner_id.equals(other.owner_id))
			return false;
		return true;
	}
}
