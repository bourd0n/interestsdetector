package ru.ispras.crawler.core.datamodel.vkontakte.entities.stats;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 19.05.14
 * Time: 19:06
 */
public class VisitorsByPCities {
    private final int visitors;
    private final int value;
    private final String name;

    @JsonCreator
    public VisitorsByPCities(
            @JsonProperty("visitors") int visitors,
            @JsonProperty("value") int value,
            @JsonProperty("name") String name) {
        this.visitors = visitors;
        this.value = value;
        this.name = name;
    }

    @JsonProperty("visitors")
	public int getVisitors() {
		return visitors;
	}

    @JsonProperty("value")
	public int getValue() {
		return value;
	}

    @JsonProperty("name")
	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + value;
		result = prime * result + visitors;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VisitorsByPCities other = (VisitorsByPCities) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (value != other.value)
			return false;
		if (visitors != other.visitors)
			return false;
		return true;
	}
 
}
