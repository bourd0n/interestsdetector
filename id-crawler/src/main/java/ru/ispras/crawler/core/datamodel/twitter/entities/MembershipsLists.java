package ru.ispras.crawler.core.datamodel.twitter.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class represents Twitter API memberships lists page 
 * 
 * @author andrey g
 *
 */
public class MembershipsLists {
	private final long previousCursor;
	private final long nextCursor;
	private final List<UserList> lists;
	
	@JsonCreator
	public MembershipsLists(
			@JsonProperty("previous_cursor") long previousCursor,
			@JsonProperty("next_cursor") long nextCursor,
			@JsonProperty("lists") List<UserList> lists) {
		this.previousCursor = previousCursor;
		this.nextCursor = nextCursor;
		this.lists = lists;
	}

	@JsonProperty("previous_cursor")
	public long getPreviousCursor() {
		return previousCursor;
	}

	@JsonProperty("next_cursor")
	public long getNextCursor() {
		return nextCursor;
	}

	@JsonProperty("lists")
	public List<UserList> getLists() {
		return lists;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lists == null) ? 0 : lists.hashCode());
		result = prime * result + (int) (nextCursor ^ (nextCursor >>> 32));
		result = prime * result
				+ (int) (previousCursor ^ (previousCursor >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MembershipsLists other = (MembershipsLists) obj;
		if (lists == null) {
			if (other.lists != null)
				return false;
		} else if (!lists.equals(other.lists))
			return false;
		if (nextCursor != other.nextCursor)
			return false;
		if (previousCursor != other.previousCursor)
			return false;
		return true;
	}
}
