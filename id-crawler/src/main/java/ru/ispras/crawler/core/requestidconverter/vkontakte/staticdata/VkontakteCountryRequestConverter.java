package ru.ispras.crawler.core.requestidconverter.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteCountriesRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 03.06.14
 * Time: 16:48
 */
public class VkontakteCountryRequestConverter implements IRequestIdConverter<VkontakteCountriesRequest, Long> {
    @Override
    public VkontakteCountriesRequest getRequest(Long aLong) {
        return new VkontakteCountriesRequest();
    }

    @Override
    public Long getId(VkontakteCountriesRequest request) {
        return 1L;
    }
}
