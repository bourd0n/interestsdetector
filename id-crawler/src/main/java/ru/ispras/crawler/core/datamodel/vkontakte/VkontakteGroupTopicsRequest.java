package ru.ispras.crawler.core.datamodel.vkontakte;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;

/**
 * Request for https://vk.com/dev/board.getTopics method
 * 
 * @author andrey
 *
 */
public class VkontakteGroupTopicsRequest extends AbstractVkontakteLimitedRequest {
	
	public static enum Order{
		ASC_CREATION("-2"), DESC_CREATION("2"),
		ASC_UPDATE("-1"), DESC_UPDATE("1");
		
		private final String value;
		
		private Order(String value){
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}
	
	private final Long groupId;
	private final List<Long> topicsIds;
	private final Order order;
	
	public VkontakteGroupTopicsRequest(Long groupId, List<Long> topicsIds, Order order, int limit) {
		super(limit);
		Validate.notNull(topicsIds);
		this.groupId = groupId;
		this.topicsIds = new ArrayList<Long>(topicsIds);
		this.order = order;
	}
	
	public VkontakteGroupTopicsRequest(Long groupId, List<Long> topicsIds, Order order) {
		super();
		Validate.notNull(topicsIds);
		this.groupId = groupId;
		this.topicsIds = new ArrayList<Long>(topicsIds);
		this.order = order;
	}
	
	public VkontakteGroupTopicsRequest(Long groupId, int limit) {
		super(limit);
		this.groupId = groupId;
		this.topicsIds = new ArrayList<Long>();
		this.order = null;
	}
	
	public VkontakteGroupTopicsRequest(Long groupId) {
		super();
		this.groupId = groupId;
		this.topicsIds = new ArrayList<Long>();
		this.order = null;
	}
	
	public VkontakteGroupTopicsRequest(Long groupId, Order order, int limit) {
		super(limit);
		this.groupId = groupId;
		this.topicsIds = new ArrayList<Long>();
		this.order = order;
	}
	
	public VkontakteGroupTopicsRequest(Long groupId, Order order) {
		super();
		this.groupId = groupId;
		this.topicsIds = new ArrayList<Long>();
		this.order = order;
	}

	public Long getGroupId() {
		return groupId;
	}

	public List<Long> getTopicsIds() {
		return topicsIds;
	}

	public Order getOrder() {
		return order;
	}

	@Override
	public String toString() {
		return "VkontakteGroupTopicsRequest [groupId=" + groupId
				+ ", topicsIds=" + topicsIds + ", order=" + order + "]";
	}
}
