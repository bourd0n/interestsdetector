package ru.ispras.crawler.core.connection.vkontakte;

import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteTopicCommentsRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.SortOrder;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.walls.Comment;
import ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils;

/**
 * Connection for https://vk.com/dev/board.getComments method.
 * Requires access token
 * 
 * @author andrey
 *
 */
public class VkontakteTopicCommentsConnection implements IConnection<VkontakteTopicCommentsRequest, BasicResponse<List<Comment>>> {

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	public VkontakteTopicCommentsConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}

	private String getMethod(VkontakteTopicCommentsRequest request) {
		return "board.getComments?group_id="+request.getGroupId()+
				"&topic_id="+request.getTopicId()+
				"&need_likes="+VkontakteUtils.booleanToFlag(request.getNeedLikes())+
				"&sort="+sortToString(request.getSort());		
	}
	
	private String sortToString(SortOrder sort) {
		return (sort == SortOrder.ASC) ? "asc" : "desc";
	}

	@Override
	public BasicResponse<List<Comment>> getResponse(
			VkontakteTopicCommentsRequest request)
			throws UnavailableConnectionException {
		String url = getMethod(request);
		List<Comment> response = VkontakteConnectionUtils.getPagedResponse(url, 100, request.getLimit(), httpConnection, Comment.class);
		return new BasicResponse<List<Comment>>(response);
	}

}
