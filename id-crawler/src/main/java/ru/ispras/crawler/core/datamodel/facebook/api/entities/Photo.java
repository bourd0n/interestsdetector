/*
 * Copyright 2012 Ryuji Yamashita
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.facebook.api.entities;

import java.util.Date;
import java.util.List;

import ru.ispras.crawler.core.datamodel.facebook.api.utils.FacebookAPIDateConverter;
import ru.ispras.crawler.core.datamodel.utils.DateConverter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ryuji Yamashita - roundrop at gmail.com, andrey g
 */
public class Photo {

	private final String id;
	private final Category from;
	private final ResponseList<Tag> tags;
	private final String name;
	private final String icon;
	private final String picture;
	private final String source;
	private final Integer height;
	private final Integer width;
	private final List<Photo.Image> images;
	private final String link;
	private final Place place;
	private final Date createdTime;
	private final Date updatedTime;
	private final Integer position;
	private final ResponseList<Comment> comments;
	private final ResponseList<Category> likes;
	private final Category album;
	
	private final static DateConverter dateConverter = new FacebookAPIDateConverter();

	
	@JsonCreator
	public Photo(
			@JsonProperty("id") String id,
			@JsonProperty("from") Category from,
			@JsonProperty("tags") ResponseList<Tag> tags,
			@JsonProperty("name") String name,
			@JsonProperty("icon") String icon,
			@JsonProperty("picture") String picture,
			@JsonProperty("source") String source,
			@JsonProperty("heigh") Integer height,
			@JsonProperty("width") Integer width,
			@JsonProperty("images") List<Image> images,
			@JsonProperty("link") String link,
			@JsonProperty("place") Place place,
			@JsonProperty("created_time") String createdTime,
			@JsonProperty("updated_time") String updatedTime,
			@JsonProperty("position") Integer position,
			@JsonProperty("comments") ResponseList<Comment> comments,
			@JsonProperty("likes") ResponseList<Category> likes,
			@JsonProperty("album") Category album) {
		this.id = id;
		this.from = from;
		this.tags = tags;
		this.name = name;
		this.icon = icon;
		this.picture = picture;
		this.source = source;
		this.height = height;
		this.width = width;
		this.images = images;
		this.link = link;
		this.place = place;
		this.createdTime = dateConverter.getDate(createdTime);
		this.updatedTime = dateConverter.getDate(updatedTime);
		this.position = position;
		this.comments = comments;
		this.likes = likes;
		this.album = album;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("from")
	public Category getFrom() {
		return from;
	}

	@JsonProperty("tags")
	public ResponseList<Tag> getTags() {
		return tags;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("icon")
	public String getIcon() {
		return icon;
	}

	@JsonProperty("picture")
	public String getPicture() {
		return picture;
	}

	@JsonProperty("source")
	public String getSource() {
		return source;
	}

	@JsonProperty("heigh")
	public Integer getHeight() {
		return height;
	}

	@JsonProperty("width")
	public Integer getWidth() {
		return width;
	}

	@JsonProperty("images")
	public List<Photo.Image> getImages() {
		return images;
	}

	@JsonProperty("link")
	public String getLink() {
		return link;
	}

	@JsonProperty("place")
	public Place getPlace() {
		return place;
	}

	@JsonProperty("created_time")
	public String getCreatedTimeString() {
		return dateConverter.getString(createdTime);
	}
	
	@JsonIgnore
	public Date getCreatedTime() {
		return createdTime;
	}

	@JsonProperty("updated_time")
	public String getUpdatedTimeString() {
		return dateConverter.getString(updatedTime);
	}
	
	@JsonIgnore
	public Date getUpdatedTime() {
		return updatedTime;
	}

	@JsonProperty("position")
	public Integer getPosition() {
		return position;
	}

	@JsonProperty("comments")
	public ResponseList<Comment> getComments() {
		return comments;
	}

	@JsonProperty("likes")
	public ResponseList<Category> getLikes() {
		return likes;
	}

	@JsonProperty("album")
	public Category getAlbum() {
		return album;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Photo other = (Photo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public static class Image {
		private final Integer height;
		private final Integer width;
		private final String source;

		@JsonCreator
		public Image(
				@JsonProperty("height") Integer height,
				@JsonProperty("width") Integer width,
				@JsonProperty("source") String source) {
			this.height = height;
			this.width = width;
			this.source = source;
		}

		@JsonProperty("height")
		public Integer getHeight() {
			return height;
		}

		@JsonProperty("width")
		public Integer getWidth() {
			return width;
		}

		@JsonProperty("source")
		public String getSource() {
			return source;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((height == null) ? 0 : height.hashCode());
			result = prime * result
					+ ((source == null) ? 0 : source.hashCode());
			result = prime * result + ((width == null) ? 0 : width.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Image other = (Image) obj;
			if (height == null) {
				if (other.height != null)
					return false;
			} else if (!height.equals(other.height))
				return false;
			if (source == null) {
				if (other.source != null)
					return false;
			} else if (!source.equals(other.source))
				return false;
			if (width == null) {
				if (other.width != null)
					return false;
			} else if (!width.equals(other.width))
				return false;
			return true;
		}
	}
}
