package ru.ispras.crawler.core.saver.responsechunker;

import java.util.ArrayList;
import java.util.List;

import ru.ispras.crawler.core.datamodel.BasicResponse;

/**
 * {@link IResponseChunker} for list-based {@link BasicResponse}s.
 * <p>
 * This class splits elements of list contained in the given
 * {@link BasicResponse} according to the given chunk size.
 * <p>
 * Default chunk size is 4000.
 * 
 * @author Ivan Andrianov
 */
public class BasicResponseChunker<T> implements IResponseChunker<BasicResponse<List<T>>> {
	private final int chunkSize;

	public BasicResponseChunker() {
		this(4000);
	}

	public BasicResponseChunker(int chunkSize) {
		this.chunkSize = chunkSize;
	}

	@Override
	public List<BasicResponse<List<T>>> split(BasicResponse<List<T>> response) {
		List<T> objects = response.getObject();
		List<BasicResponse<List<T>>> result = new ArrayList<>();
		List<T> chunk = new ArrayList<>();
		for (T object : objects) {
			chunk.add(object);
			if (chunk.size() == chunkSize) {
				result.add(new BasicResponse<>(chunk));
				chunk = new ArrayList<>();
			}
		}
		// As result is required to be non-empty by interface contract we add an
		// empty chunk if input is empty.
		if (!chunk.isEmpty() || objects.isEmpty()) {
			result.add(new BasicResponse<>(chunk));
		}
		return result;
	}

	@Override
	public BasicResponse<List<T>> merge(List<BasicResponse<List<T>>> responseChunks) {
		List<T> objects = new ArrayList<>();
		for (BasicResponse<List<T>> responseChunk : responseChunks) {
			objects.addAll(responseChunk.getObject());
		}
		return new BasicResponse<>(objects);
	}
}
