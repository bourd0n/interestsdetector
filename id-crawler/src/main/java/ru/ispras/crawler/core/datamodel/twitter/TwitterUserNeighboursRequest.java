package ru.ispras.crawler.core.datamodel.twitter;

import ru.ispras.crawler.core.datamodel.grouping.GroupingRequest;

/**
 * {@link GroupingRequest} for for downloading Twitter user followers and friends.
 *
 * See:
 * https://dev.twitter.com/rest/reference/get/followers/ids
 * https://dev.twitter.com/rest/reference/get/friends/ids
 */
public class TwitterUserNeighboursRequest extends TwitterUserGroupingRequest {
	public TwitterUserNeighboursRequest(long id) {
		super(id, new TwitterUserFollowersRequest(id), new TwitterUserFriendsRequest(id));
	}
}
