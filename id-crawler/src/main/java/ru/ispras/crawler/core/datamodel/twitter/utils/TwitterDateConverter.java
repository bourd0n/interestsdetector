package ru.ispras.crawler.core.datamodel.twitter.utils;

import java.text.SimpleDateFormat;
import java.util.Locale;

import ru.ispras.crawler.core.datamodel.utils.DateConverter;

/**
 * {@link DateConverter} subclass specifying {@link SimpleDateFormat} used by
 * Twitter API.
 * 
 * @author Ivan Andrianov
 * 
 */
public class TwitterDateConverter extends DateConverter {
	public TwitterDateConverter() {
		super(new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH));
	}
}
