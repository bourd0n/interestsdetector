/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.twitter.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A data interface representing one single user mention entity.
 *
 * @author Yusuke Yamamoto - yusuke at mac.com, andrey g
 * @since Twitter4J 2.1.9
 */
public class UserMentionEntity{
    private final String name;
    private final String screenName;
    private final long id;
    private final List<Integer> indices;
  
    @JsonCreator
    public UserMentionEntity(
    		@JsonProperty("name") String name,
    		@JsonProperty("screen_name") String screenName,
    		@JsonProperty("id") long id,
    		@JsonProperty("indices") List<Integer> indices) {
		this.name = name;
		this.screenName = screenName;
		this.id = id;
		this.indices = indices;
	}

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("screen_name")
    public String getScreenName() {
        return screenName;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("indices")
	public List<Integer> getIndices() {
		return indices;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserMentionEntity other = (UserMentionEntity) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
