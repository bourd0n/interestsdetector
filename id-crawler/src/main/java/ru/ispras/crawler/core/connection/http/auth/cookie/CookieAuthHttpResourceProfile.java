package ru.ispras.crawler.core.connection.http.auth.cookie;

/**
 * Class which represents a profile of an HTTP resource in terms of cookie
 * authentication:
 * <ul>
 * <li>
 * login page URL
 * <li>
 * login form field name
 * <li>
 * password form field name
 * <li>
 * cookie name
 * </ul>
 * 
 * @author Ivan Andrianov
 */
public class CookieAuthHttpResourceProfile {
	private final String loginPage;
	private final String loginFieldName;
	private final String passwordFieldName;
	private final String userCookieName;

	public CookieAuthHttpResourceProfile(String loginPage, String loginFieldName, String passwordFieldName,
			String userCookieName) {
		this.loginPage = loginPage;
		this.loginFieldName = loginFieldName;
		this.passwordFieldName = passwordFieldName;
		this.userCookieName = userCookieName;
	}

	public String getLoginPage() {
		return loginPage;
	}

	public String getLoginFieldName() {
		return loginFieldName;
	}

	public String getPasswordFieldName() {
		return passwordFieldName;
	}

	public String getUserCookieName() {
		return userCookieName;
	}
}
