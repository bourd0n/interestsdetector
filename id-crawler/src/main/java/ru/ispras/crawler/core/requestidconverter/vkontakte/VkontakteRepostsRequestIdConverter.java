package ru.ispras.crawler.core.requestidconverter.vkontakte;

import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteRepostsRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class VkontakteRepostsRequestIdConverter implements
		IRequestIdConverter<VkontakteRepostsRequest, String> {

	@Override
	public String getId(VkontakteRepostsRequest request) {
		return request.getOwnerId()+"_"+request.getPostId();
	}

	@Override
	public VkontakteRepostsRequest getRequest(String id) {
		String[] elems = id.split("_");
		return new VkontakteRepostsRequest(Long.parseLong(elems[0]), Long.parseLong(elems[1]));
	}

}
