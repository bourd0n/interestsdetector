package ru.ispras.crawler.core.requestidconverter.facebook.api;

import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIPhotoRequest;

public class FacebookAPIPhotoRequestIdConverter extends AbstractFacebookAPIRequestIdConverter<FacebookAPIPhotoRequest> {
	@Override
	public FacebookAPIPhotoRequest getRequest(Long id) {
		return new FacebookAPIPhotoRequest(id);
	}
}
