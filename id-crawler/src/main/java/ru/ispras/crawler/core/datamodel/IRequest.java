package ru.ispras.crawler.core.datamodel;

/**
 * Abstraction for entity classes which stores information about some concrete
 * request.
 * <p>
 * Implementations are expected to be immutable.
 * 
 * @author Alexander
 */
public interface IRequest {
	
}
