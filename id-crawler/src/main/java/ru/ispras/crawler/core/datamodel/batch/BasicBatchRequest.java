package ru.ispras.crawler.core.datamodel.batch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * An implementation of {@link IBatchRequest} that just stores list of requests
 * @param <Req>
 */
public class BasicBatchRequest<Req extends IRequest> implements IBatchRequest<Req> {
	private final List<Req> requests;

	public BasicBatchRequest(List<Req> requests) {
		this.requests = new ArrayList<>(requests);
	}

	@Override
	public List<Req> getRequests() {
		return Collections.unmodifiableList(requests);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((requests == null) ? 0 : requests.hashCode());
		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicBatchRequest other = (BasicBatchRequest) obj;
		if (requests == null) {
			if (other.requests != null)
				return false;
		} else if (!requests.equals(other.requests))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return getClass().getCanonicalName()+" [requests=" + requests + "]";
	}
}
