package ru.ispras.crawler.core.datamodel.twitter;

import org.apache.commons.lang.Validate;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Abstract {@link IRequest} for downloading some data about Twitter user.
 * 
 * @author Ivan Andrianov
 * 
 */
public abstract class AbstractTwitterUserRequest implements IRequest {
	private final long id;

	public AbstractTwitterUserRequest(long id) {
		Validate.isTrue(id > 0, "id should be > 0");
		this.id = id;
	}

	public long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		AbstractTwitterUserRequest other = (AbstractTwitterUserRequest) obj;
		return id == other.id;
	}
	
	@Override
	public String toString() {
		return getClass().getCanonicalName() + "["  + id + "]";
	}
}
