package ru.ispras.crawler.core.requestidconverter;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Converter of {@link IRequest}s to their identifiers and vice versa.
 * <p>
 * Implementations are expected to be thread-safe.
 * 
 * @author Ivan Andrianov
 */
public interface IRequestIdConverter<Req extends IRequest, ID> {
	ID getId(Req request);

	Req getRequest(ID id);
}
