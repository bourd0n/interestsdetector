package ru.ispras.crawler.core.datamodel.facebook;

import ru.ispras.crawler.core.datamodel.IRequest;
/**
 * {@link IRequest} for downloading favorite items list for given Facebook user
 * Example: https://m.facebook.com/profile.php&id=100001698473676&v=likes
 *
 * @author andrey g
 *
 */
public class FacebookUserFavoritesRequest extends AbstractFacebookUserRequest {

	public FacebookUserFavoritesRequest(long id) {
		super(id);
	}
}
