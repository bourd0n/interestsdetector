package ru.ispras.crawler.core.datamodel.twitter;

import org.apache.commons.lang.Validate;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} for downloading Tweets corresponding to the given query.
 *
 * See https://dev.twitter.com/rest/reference/get/search/tweets
 *
 * @author Ivan Andrianov
 * 
 */
public class TwitterSearchTweetsRequest implements IRequest {
	public static class LocationQuery {
		private final double latitude;
		private final double longitude;
		private final int kilometerRadius;

		public LocationQuery(double latitude, double longitude, int kilometerRadius) {
			Validate.isTrue(latitude >= -90 && latitude <= 90, "Latitude should be in [-90; 90].");
			Validate.isTrue(longitude >= -180 && longitude <= 180,
					"Longitude should be in [-180; 180].");
			Validate.isTrue(kilometerRadius > 0, "Radius should be > 0.");
			this.latitude = latitude;
			this.longitude = longitude;
			this.kilometerRadius = kilometerRadius;
		}

		public double getLatitude() {
			return latitude;
		}

		public double getLongitude() {
			return longitude;
		}

		public int getKilometerRadius() {
			return kilometerRadius;
		}

		@Override
		public String toString() {
			return "[latitude=" + latitude + ", longitude="
					+ longitude + ", kilometerRadius=" + kilometerRadius + "]";
		}
	}

	private final String textQuery;
	private final LocationQuery locationQuery;
	private final String language;
	private final int limit;

	public TwitterSearchTweetsRequest(String textQuery) {
		this(textQuery, null, null, Integer.MAX_VALUE);
	}

	public TwitterSearchTweetsRequest(String textQuery, int limit) {
		this(textQuery, null, null, limit);
	}

	public TwitterSearchTweetsRequest(String textQuery, LocationQuery locationQuery) {
		this(textQuery, locationQuery, null, Integer.MAX_VALUE);
	}

	public TwitterSearchTweetsRequest(String textQuery, LocationQuery locationQuery, int limit) {
		this(textQuery, locationQuery, null, limit);
	}

	public TwitterSearchTweetsRequest(String textQuery, String language) {
		this(textQuery, null, language, Integer.MAX_VALUE);
	}

	public TwitterSearchTweetsRequest(String textQuery, String language, int limit) {
		this(textQuery, null, language, limit);
	}

	public TwitterSearchTweetsRequest(String textQuery, LocationQuery locationQuery, String language) {
		this(textQuery, locationQuery, language, Integer.MAX_VALUE);
	}

	public TwitterSearchTweetsRequest(String textQuery, LocationQuery locationQuery,
			String language, int limit) {
		Validate.notEmpty(textQuery, "Text query can not be empty.");
		Validate.isTrue(textQuery.length() <= 1000, "Text query is too long.");
		this.textQuery = textQuery;
		this.locationQuery = locationQuery;
		this.language = language;
		this.limit = limit;
	}

	public String getTextQuery() {
		return textQuery;
	}

	public LocationQuery getLocationQuery() {
		return locationQuery;
	}

	public String getLanguage() {
		return language;
	}

	public int getLimit() {
		return limit;
	}

	@Override
	public String toString() {
		return getClass().getName() + ":[textQuery=" + textQuery
				+ ", locationQuery=" + locationQuery + ", language=" + language
				+ ", limit=" + limit + "]";
	}
}
