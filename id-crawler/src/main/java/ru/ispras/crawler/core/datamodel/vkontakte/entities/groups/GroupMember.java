package ru.ispras.crawler.core.datamodel.vkontakte.entities.groups;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import static ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils.booleanToFlag;
import static ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils.flagToBoolean;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 19.05.14
 * Time: 18:44
 */
public class GroupMember {
    private final boolean member;
    private final boolean request;
    private final boolean invitation;

    @JsonCreator
    public GroupMember(
            @JsonProperty("member") int member,
            @JsonProperty("request") int request,
            @JsonProperty("invitation") int invitation) {
        this.member = flagToBoolean(member);
        this.request = flagToBoolean(request);
        this.invitation = flagToBoolean(invitation);
    }

    @JsonProperty("member")
    public int getMember() {
        return booleanToFlag(member);
    }

    @JsonProperty("request")
    public int getRequest() {
        return booleanToFlag(request);
    }

    @JsonProperty("invitation")
    public int getInvitation() {
        return booleanToFlag(invitation);
    }

    @JsonIgnore
    public boolean isMember() {
        return member;
    }

    @JsonIgnore
    public boolean isRequest() {
        return request;
    }

    @JsonIgnore
    public boolean isInvitation() {
        return invitation;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (invitation ? 1231 : 1237);
		result = prime * result + (member ? 1231 : 1237);
		result = prime * result + (request ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroupMember other = (GroupMember) obj;
		if (invitation != other.invitation)
			return false;
		if (member != other.member)
			return false;
		if (request != other.request)
			return false;
		return true;
	} 
}
