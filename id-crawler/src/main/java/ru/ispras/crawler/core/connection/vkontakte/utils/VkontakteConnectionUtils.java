package ru.ispras.crawler.core.connection.vkontakte.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.TargetUnreachableException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.ResponseList;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.VkontakteResponse;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.ispras.crawler.core.jackson.VkontakteObjectMapperFactory;

/**
 * Vkontakte-related utils for downloading data
 */
public class VkontakteConnectionUtils {
	
	private static final ObjectMapper objectMapper = new VkontakteObjectMapperFactory().create();
	
	private static final Logger logger = Logger.getLogger(VkontakteConnectionUtils.class);
	
	private static final String BASE_URL = "https://api.vk.com/method/";
	
	private static final String API_VERSION = "5.21";

    /**
     * Get response that is presented as single object
     * @param url
     * @param vkontakteHttpConnection
     * @param clazz
     * @param <T>
     * @return
     * @throws UnavailableConnectionException
     */
	public static <T> T  getSingleResponse(String url, IConnection<HttpRequest, HttpResponse> vkontakteHttpConnection, Class<T> clazz) throws UnavailableConnectionException {
		String httpResponse = getHttpResponse(url,  vkontakteHttpConnection);
		VkontakteResponse<T> result = null;
		try {
			JavaType type = objectMapper.getTypeFactory().constructParametricType(VkontakteResponse.class, clazz);
			result = objectMapper.readValue(httpResponse, type);
		} catch (JsonParseException e) {
			throw new TargetUnreachableException(e);
		} catch (JsonMappingException e) {
			throw new IllegalStateException(e);
		} catch (IOException e) {
			throw new TargetUnreachableException(e);
		}
		return result.getResponse();
	}

    /**
     * Get response that is presented as list of objects
     * @param url
     * @param vkontakteHttpConnection
     * @param clazz
     * @param <T>
     * @return
     * @throws UnavailableConnectionException
     */
	public static <T> List<T>  getSingleListResponse(String url, IConnection<HttpRequest, HttpResponse> vkontakteHttpConnection, Class<T> clazz) throws UnavailableConnectionException {
		String httpResponse = getHttpResponse(url,  vkontakteHttpConnection);
		VkontakteResponse<List<T>> result = null;
		try {
			JavaType listType = objectMapper.getTypeFactory().constructParametricType(List.class, clazz);
			JavaType type = objectMapper.getTypeFactory().constructParametricType(VkontakteResponse.class, listType);
			result = objectMapper.readValue(httpResponse, type);
		} catch (JsonParseException e) {
			throw new TargetUnreachableException(e);
		} catch (JsonMappingException e) {
			throw new IllegalStateException(e);
		} catch (IOException e) {
			throw new TargetUnreachableException(e);
		}
		return result.getResponse();
	}

    /**
     * Get response that is presented as {@link ResponseList}
     * @param url
     * @param vkontakteHttpConnection
     * @param clazz
     * @param <T>
     * @return
     * @throws UnavailableConnectionException
     */
	public static <T> ResponseList<T>  getSingleResponseList(String url, IConnection<HttpRequest, HttpResponse> vkontakteHttpConnection, Class<T> clazz) throws UnavailableConnectionException {
		String httpResponse = getHttpResponse(url,  vkontakteHttpConnection);
		VkontakteResponse<ResponseList<T>> result = null;
		try {
			JavaType responseListType = objectMapper.getTypeFactory().constructParametricType(ResponseList.class, clazz);
			JavaType type = objectMapper.getTypeFactory().constructParametricType(VkontakteResponse.class, responseListType);
			result = objectMapper.readValue(httpResponse, type);
		} catch (JsonParseException e) {
			throw new TargetUnreachableException(e);
		} catch (JsonMappingException e) {
			throw new IllegalStateException(e);
		} catch (IOException e) {
			throw new TargetUnreachableException(e);
		}
		return result.getResponse();
	}

    /**
     * Get response that is presented as list of objects.
     * Response is collected from several paged responses.
     * @param baseUrl
     * @param pageSize
     * @param limit
     * @param vkontakteHttpConnection
     * @param clazz
     * @param <T>
     * @return
     * @throws UnavailableConnectionException
     */
	public static <T> List<T> getPagedResponse(String baseUrl, int pageSize, int limit, 
			IConnection<HttpRequest, HttpResponse> vkontakteHttpConnection, Class<T> clazz)
			throws UnavailableConnectionException {
		List<T> result = new ArrayList<T>();
		int offset = 0;
		int numOfNewObjects = -1;
		while (limit > 0 && numOfNewObjects != 0) {
			limit = limit-pageSize;
			String url = addPageInfo(baseUrl, offset, pageSize);
			ResponseList<T> response = getSingleResponseList(url, vkontakteHttpConnection, clazz);
			offset = offset + pageSize;
			if (response.getItems() != null){
				numOfNewObjects = response.getItems().size();
				result.addAll(response.getItems());
			}else{
				numOfNewObjects = 0;
			}
		}
		return result;
	}
	
	private static String getHttpResponse(String url, IConnection<HttpRequest, HttpResponse> vkontakteHttpConnection) 
			throws UnavailableConnectionException {
		try {
			logger.info("Vkontakte API request: " + url);
			HttpRequest httpRequest = new HttpRequest(new URL(addBaseUrlAndVersion(url)));
			HttpResponse httpResponse = vkontakteHttpConnection.getResponse(httpRequest);
			return httpResponse.getString();
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private static String addPageInfo(String url, int offset, int pageSize) {
		String params = "offset="+offset+"&count="+pageSize;
		if (url.indexOf('?') < 0) {
			return url+"?" + params;
		} else {
			return url+"&" + params;
		}
	}
	
	private static String addBaseUrlAndVersion(String url) {
		if (url.indexOf('?') < 0) {
			return BASE_URL + url + "?v=" + API_VERSION;
		} else {
			return BASE_URL + url + "&v=" + API_VERSION;
		}
	}

	public static String encodeURL(String str){
		String res="";
		try {
			res = URLEncoder.encode(str, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// never reach this
			throw new RuntimeException(e);
		}
		return res;
	}
}
