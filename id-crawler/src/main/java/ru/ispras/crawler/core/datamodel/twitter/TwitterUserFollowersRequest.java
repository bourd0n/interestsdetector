package ru.ispras.crawler.core.datamodel.twitter;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} for downloading Twitter user followers.
 *
 * See https://dev.twitter.com/rest/reference/get/followers/ids
 *
 * @author Ivan Andrianov
 * 
 */
public class TwitterUserFollowersRequest extends AbstractTwitterUserNeigboursRequest {
	public TwitterUserFollowersRequest(long id) {
		super(id);
	}
	
	public TwitterUserFollowersRequest(long id, int limit) {
		super(id, limit);
	}
}
