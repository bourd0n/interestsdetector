package ru.ispras.crawler.core.datamodel.facebook.api;

/**
 * {@link FacebookAPIRequest} (https://graph.facebook.com/...) that requires downloading several pages that contain result
 */
public class FacebookAPIPaggedRequest extends FacebookAPIRequest {

	private int pagesLimit;
	
	public FacebookAPIPaggedRequest(long id, String path, String parameters, int pagesLimit) {
		super(id, path, parameters);
		this.pagesLimit = pagesLimit;
	}
	
	public FacebookAPIPaggedRequest(long id, String path, String parameters) {
		this(id, path, parameters, Integer.MAX_VALUE);
	}

	public int getPagesLimit() {
		return pagesLimit;
	}
}
