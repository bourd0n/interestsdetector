package ru.ispras.crawler.core.connection.http.ext;

import org.apache.http.client.HttpClient;

import ru.ispras.crawler.core.connection.exception.ConnectionCreationException;
import ru.ispras.crawler.core.connection.http.HttpConnectionConfiguration;

/**
 * Creates {@link HttpClient} instance by using given
 * {@link HttpConnectionConfiguration}.
 * 
 * @author Ivan Andrianov
 */
public interface IHttpClientFactory<CC extends HttpConnectionConfiguration> {
	HttpClient create(CC configuration) throws ConnectionCreationException;
}
