package ru.ispras.crawler.core.datamodel.vkontakte;

import org.apache.commons.lang.Validate;

/**
 * Request for https://vk.com/dev/likes.getList method
 * 
 * @author andrey
 *
 */
public class VkontakteLikesRequest extends AbstractVkontakteLimitedRequest {

	private final Type type;
	private final long ownerId;
	private final long itemId;
	
	public static enum Type{
		POST("post"), COMMENT("comment"), PHOTO("photo"), AUDIO("audio"), VIDEO("video"), NOTE("note"), 
		PHOTO_COMMENT("photo_comment"), VIDEO_COMMENT("video_comment"), TOPC_COMMENT("topic_comment"),
		SITEPAGE("sitepage");
		
		private final String value;
		
		private Type(String value){
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public VkontakteLikesRequest(Type type, long ownerId, long itemId) {
		super();
		Validate.notNull(type);
		this.type = type;
		this.ownerId = ownerId;
		this.itemId = itemId;
	}
	
	public VkontakteLikesRequest(Type type, long ownerId, long itemId, int limit) {
		super(limit);
		Validate.notNull(type);
		this.type = type;
		this.ownerId = ownerId;
		this.itemId = itemId;
	}

	public Type getType() {
		return type;
	}

	public long getOwnerId() {
		return ownerId;
	}

	public long getItemId() {
		return itemId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (itemId ^ (itemId >>> 32));
		result = prime * result + (int) (ownerId ^ (ownerId >>> 32));
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VkontakteLikesRequest other = (VkontakteLikesRequest) obj;
		if (itemId != other.itemId)
			return false;
		if (ownerId != other.ownerId)
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VkontakteLikesRequest [type=" + type + ", ownerId=" + ownerId
				+ ", itemId=" + itemId + "]";
	}
}
