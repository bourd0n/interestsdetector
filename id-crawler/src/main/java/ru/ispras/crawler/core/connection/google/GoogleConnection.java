package ru.ispras.crawler.core.connection.google;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.google.GoogleRequest;
import ru.ispras.crawler.core.datamodel.google.GoogleResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * {@link IConnection} for fetching number of results returning for given request
 * Example: http://www.google.com/search?q=Crawler
 *
 */
public class GoogleConnection implements IConnection<GoogleRequest, GoogleResponse> {
	private static final String GOOGLE_SEARCH_QUERY_BASE = "http://www.google.com/search?q=%s";
	private final IConnection<? super HttpRequest, ? extends HttpResponse> httpConnection;

	public GoogleConnection(IConnection<? super HttpRequest, ? extends HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}

	@Override
	public GoogleResponse getResponse(GoogleRequest request) throws UnavailableConnectionException {
		HttpRequest httpRequest;
		try {
			httpRequest = new HttpRequest(new URL(String.format(
					GOOGLE_SEARCH_QUERY_BASE,
					URLEncoder.encode(request.getQuery(), "UTF-8"))));
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException(e);
		} catch (UnsupportedEncodingException e) {
			// This situation is impossible
			throw new RuntimeException(e);
		}
		HttpResponse httpResponse = httpConnection.getResponse(httpRequest);
		return new GoogleResponse(getNumberFromResponse(httpResponse));
	}

	private long getNumberFromResponse(HttpResponse response) {
		final Document doc = Jsoup.parse(response.getString(),
				"http://www.google.com/");
		String stringWithNumber = doc.getElementById("resultStats").ownText();
		Pattern pattern = Pattern.compile("[0-9,]+");
		Matcher matcher = pattern.matcher(stringWithNumber);
		if (matcher.find()) {
			return Long.parseLong(matcher.group().replace(",", ""));
		} else {
			return 0;
		}
	}
}
