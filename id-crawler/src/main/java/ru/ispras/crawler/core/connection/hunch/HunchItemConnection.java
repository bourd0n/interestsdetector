package ru.ispras.crawler.core.connection.hunch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.hunch.HunchItemRequest;
import ru.ispras.crawler.core.datamodel.hunch.entities.Item;

/**
 * {@link IConnection} for downloading Hunch item
 * Example: https://hunch.com/item/hn_12345 (item info), https://hunch.com/feed/ws/itemratings/?result_id=hn_12345 (item ratings)
 */
public class HunchItemConnection implements IConnection<HunchItemRequest, BasicResponse<Item>> {

	private final IConnection<HttpRequest, HttpResponse> hunchHttpConnection;
	
	// get info page
	private static final String ITEM_INFO_PAGE = "item/hn_%d/";
	private static final String ITEM_RATINGS = "feed/ws/itemratings/?result_id=hn_%d";
	
	/*
	 * These selectors are used to parse Item info page
	 */
	private static final String ITEM_TITLE_SELECTOR = "h1.item-name";
	private static final String ITEM_DOMAINS_SELECTOR = "div.lists > a";
	private static final String ITEM_TAGS_SELECTOR = "ul#item-tags > li > a.tag-button";
	private static final String ITEM_PICTURE_SELECTOR = "div.image > a > img";
	private static final String ITEM_INFO_SELECTOR = "div.info > div.widget > p";
	private static final String ITEN_INFO_AVERAGE_RATING = "body > div > div.header > div.left";
	
	private final Pattern extractAverageRating = Pattern.compile("\\(average: ([\\d\\.]+)/[\\d\\.]\\)");
	
	
	public HunchItemConnection(IConnection<HttpRequest, HttpResponse> hunchHttpConnection) {
		this.hunchHttpConnection = hunchHttpConnection;
	}

	@Override
	public BasicResponse<Item> getResponse(HunchItemRequest request) throws UnavailableConnectionException {
		final long itemId = request.getId();
		return new BasicResponse<Item>(getFullItemInfo(itemId));
	}
	
	private Item getFullItemInfo(long id) throws UnavailableConnectionException {
		final String method = String.format(ITEM_INFO_PAGE, id);
		final Document page = HunchUtils.getDocument(method, hunchHttpConnection);
		return parseItemInfoPage(page, id);
	}

	private Item parseItemInfoPage(Document page, long id) throws UnavailableConnectionException {
		String title = getTitle(page);
		List<String> domains = getDomains(page);
		List<String> tags = getTags(page);
		Map<String, String> info = getItemInfo(page);
		String pictureUrl = getPictureLink(page);
		//XXX additional request???
		float averageRating = getAverageRating(id);
		return new Item(id, title, tags, domains, pictureUrl, averageRating, info);
	}
	
	private String getTitle(Document page) {
		Element titleElement = page.select(ITEM_TITLE_SELECTOR).first();
		if (titleElement != null) {
			return titleElement.text();
		}
		return "";
	}
	
	private List<String> getDomains(Document page) {
		return getTextsOfElements(page, ITEM_DOMAINS_SELECTOR);
	}
	
	private List<String> getTags(Document page) {
		return getTextsOfElements(page, ITEM_TAGS_SELECTOR);
	}
		
	private String getPictureLink(Document page) {
		Element pictureElement = page.select(ITEM_PICTURE_SELECTOR).first();
		if (pictureElement != null) {
			return pictureElement.attr("src");
		}
		return null;
	}

	private Map<String, String> getItemInfo(Document page) {
		Map<String, String> info = new HashMap<String, String>();
		Elements elements = page.select(ITEM_INFO_SELECTOR);
		for (Element element : elements) {
			Element keyElement = element.select("strong").first();
			if (keyElement != null) {
				String key = keyElement.text().trim();
				key = key.substring(0, key.length() - 1); // remove trailing ":"
				String value = joinTextNodes(element);
				info.put(key, value);
			}
		}
		return info;
	}
	
	private List<String> getTextsOfElements(Document page, String selector) {
		List<String> texts = new ArrayList<String>();
		Elements elements = page.select(selector);
		for (Element element : elements) {
			texts.add(element.text());
		}
		return texts;
	}

	private float getAverageRating(long id) throws UnavailableConnectionException {
		// Getting Average Rating for item 
		// requires additional request to hunch.com
		final String method = String.format(ITEM_RATINGS, id);
		Document page = HunchUtils.getDocument(method, hunchHttpConnection);

		Element ratingElement = page.select(ITEN_INFO_AVERAGE_RATING).first();
		if (ratingElement != null) {
			Matcher matcher = extractAverageRating.matcher(ratingElement.text());
			if (matcher.find()) {
				return Float.parseFloat(matcher.group(1));
			}
		}
		return 0;
	}

	private String joinTextNodes(Element element) {
		String text = "";
		List<TextNode> textNodes = element.textNodes();
		for (TextNode textNode : textNodes) {
			text += textNode.text();
		}
		return text.trim();
	}
}
