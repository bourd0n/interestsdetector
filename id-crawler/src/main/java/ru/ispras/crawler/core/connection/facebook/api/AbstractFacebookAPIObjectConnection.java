package ru.ispras.crawler.core.connection.facebook.api;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.facebook.api.AbstractFacebookAPIObjectRequest;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
/**
 * Abstract {@link IConnection} which contains common FacebookAPI-related (https://graph.facebook.com/...) utils for downloading single objects (profiles, photos, vidoes).
 *
 * @author andrey g
 *
 */
public abstract class AbstractFacebookAPIObjectConnection<T> extends
		AbstractFacebookAPIConnection<AbstractFacebookAPIObjectRequest, BasicResponse<T>> {
	
	private final Class<T> clazz;
	
	public AbstractFacebookAPIObjectConnection(IConnection<HttpRequest, HttpResponse> facebookAPIHttpConnection, Class<T> clazz) {
		super(facebookAPIHttpConnection);
		this.clazz = clazz;
	}

	@Override
	public BasicResponse<T> getResponse(AbstractFacebookAPIObjectRequest request) throws UnavailableConnectionException {
		return new BasicResponse<T>(getSingleResponse(request, clazz));
	}
}
