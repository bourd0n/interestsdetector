package ru.ispras.crawler.core.datamodel.facebook.api;

/**
 * {@link FacebookAPIPaggedRequest} for downloading friends list of given Facebook user via Facebook API.
 * Example: https://graph.facebook.com/100001698473676/friends
 *
 * @author andrey g
 *
 */
public class FacebookAPIFriendsRequest extends FacebookAPIPaggedRequest{

	public FacebookAPIFriendsRequest(long id) {
		this(id, "");
	}
	
	public FacebookAPIFriendsRequest(long id, String parameters) {
		super(id, "friends", parameters);
	}
	
	public FacebookAPIFriendsRequest(long id, String parameters, int limit) {
		super(id, "friends", parameters, limit);
	}
}
