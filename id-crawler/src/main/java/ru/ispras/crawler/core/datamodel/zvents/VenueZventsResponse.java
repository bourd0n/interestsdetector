package ru.ispras.crawler.core.datamodel.zvents;

import java.net.URL;

import ru.ispras.modis.geo.Coordinate;

/**
 * Response, representing information from Zvents venue page.
 * 
 * @author sysoev
 */
public class VenueZventsResponse implements IZventsResponse {
	
	private final String id;
	private final URL url;
	private final String name;
	private final String address;
	private final Coordinate coordinate;
	
	public VenueZventsResponse(String id, URL url, String name, String address, Coordinate coordinate) {
		super();
		this.id = id;
		this.url = url;
		this.name = name;
		this.address = address;
		this.coordinate = coordinate;
	}

	public String getId() {
		return id;
	}

	public URL getUrl() {
		return url;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}
	
	public boolean hasCoordinate() {
		return coordinate != null;
	}
}
