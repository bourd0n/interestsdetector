package ru.ispras.crawler.core.datamodel.facebook.api;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} for downloading profile for given Facebook user via Facebook API.
 * Example: https://graph.facebook.com/100001698473676
 *
 * @author andrey g
 *
 */
public class FacebookAPIProfileRequest extends AbstractFacebookAPIObjectRequest {

	public FacebookAPIProfileRequest(long id) {
		this(id, "");
	}
	
	public FacebookAPIProfileRequest(long id, String parameters) {
		super(id, parameters);
	}

}
