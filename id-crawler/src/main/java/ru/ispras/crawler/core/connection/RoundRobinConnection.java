package ru.ispras.crawler.core.connection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;

import ru.ispras.crawler.core.connection.exception.PermanentlyUnavailableConnectionException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * Round robin {@link IConnection} implementation.
 * <p>
 * Switches given {@link IConnection}s randomly while
 * {@link IConnection#getResponse(IRequest)} throws
 * {@link UnavailableConnectionException}. If all {@link IConnection}s failed
 * then rethrows {@link UnavailableConnectionException}.
 * <p>
 * Access to {@link IConnection}s is controlled by associated {@link Lock}s to
 * eliminate blocking in {@link IConnection#getResponse(IRequest)}.
 * 
 * @author Ivan Andrianov
 */
public class RoundRobinConnection<Req extends IRequest, Resp extends IResponse> implements IConnection<Req, Resp> {
	private static final Logger logger = Logger.getLogger(RoundRobinConnection.class);

	private final List<IConnection<Req, Resp>> connections;
	private final List<Lock> locks;
	private final Set<Integer> deadIndices = new HashSet<>();

	public RoundRobinConnection(List<IConnection<Req, Resp>> connections) {
		Validate.notEmpty(connections, "No connections provided.");
		this.connections = connections;
		this.locks = new ArrayList<>();
		for (int i = 0; i < connections.size(); i++) {
			locks.add(new ReentrantLock());
		}
	}

	@Override
	public Resp getResponse(Req request) throws UnavailableConnectionException {
		boolean allDead = true;
		long timeout = Long.MAX_VALUE;
		Set<Integer> triedIndices = new HashSet<>();
		synchronized (deadIndices) {
			triedIndices.addAll(deadIndices);
		}
		int index = ThreadLocalRandom.current().nextInt(connections.size());
		while (triedIndices.size() < connections.size()) {
			index = (index + 1) % connections.size();
			if (triedIndices.contains(index)) {
				continue;
			}
			IConnection<Req, Resp> connection = connections.get(index);
			Lock lock = locks.get(index);
			if (lock.tryLock()) {
				try {
					return connection.getResponse(request);
				} catch (UnavailableConnectionException e) {
					String errorMessage = String.format(
							"Thread failed to process request %s through connection %s.",
							request, connection);
					logger.warn(errorMessage, e);
					if (e.getTimeout() < timeout) {
						timeout = e.getTimeout();
					}
					triedIndices.add(index);
					if (e instanceof PermanentlyUnavailableConnectionException) {
						synchronized (deadIndices) {
							deadIndices.add(index);
						}
					} else {
						allDead = false;
					}
				} finally {
					lock.unlock();
				}
			}
		}
		if (allDead) {
			throw new PermanentlyUnavailableConnectionException();
		}
		throw new UnavailableConnectionException(timeout);
	}
}
