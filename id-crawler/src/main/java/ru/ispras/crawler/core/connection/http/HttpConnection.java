package ru.ispras.crawler.core.connection.http;

import java.io.IOException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.HttpClientUtils;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.ConnectionCreationException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpClientFactory;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpExceptionHandler;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpRequestConverter;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpResponseConverter;
import ru.ispras.crawler.core.connection.http.ext.IHttpClientFactory;
import ru.ispras.crawler.core.connection.http.ext.IHttpExceptionHandler;
import ru.ispras.crawler.core.connection.http.ext.IHttpRequestConverter;
import ru.ispras.crawler.core.connection.http.ext.IHttpResponseConverter;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * Realization of the {@link IConnection} for Http protocol.
 * 
 * @author Alexander, Ivan Andrianov
 * 
 */
public class HttpConnection<CC extends HttpConnectionConfiguration> implements IConnection<HttpRequest, HttpResponse> {
	private final IHttpRequestConverter requestConverter;
	private final HttpClient httpClient;
	private final IHttpResponseConverter responseConverter;
	private final IHttpExceptionHandler exceptionHandler;

	public HttpConnection(CC configuration) throws ConnectionCreationException {
		this(new DefaultHttpRequestConverter(), new DefaultHttpClientFactory(), configuration,
				new DefaultHttpResponseConverter(), new DefaultHttpExceptionHandler());
	}

	public HttpConnection(IHttpRequestConverter requestConverter, IHttpClientFactory<? super CC> factory,
			CC configuration, IHttpResponseConverter responseConverter, IHttpExceptionHandler exceptionHandler) throws ConnectionCreationException {
		this.requestConverter = requestConverter;
		httpClient = factory.create(configuration);
		this.responseConverter = responseConverter;
		this.exceptionHandler = exceptionHandler;
	}
	
	public HttpConnection(IHttpRequestConverter requestConverter, IHttpClientFactory<? super CC> factory,
			CC configuration, IHttpResponseConverter responseConverter) throws ConnectionCreationException {
		this(requestConverter, factory, configuration, responseConverter, new DefaultHttpExceptionHandler());
	}

	@Override
	public synchronized HttpResponse getResponse(HttpRequest request) throws UnavailableConnectionException {
		HttpUriRequest uriRequest = requestConverter.convert(request);
		org.apache.http.HttpResponse response = execute(uriRequest);
		try {
			return responseConverter.convert(uriRequest, response);
		} finally {
			HttpClientUtils.closeQuietly(response);
		}
	}

	private org.apache.http.HttpResponse execute(HttpUriRequest uriRequest) throws UnavailableConnectionException {
		try {
			return httpClient.execute(uriRequest);
		} catch (IOException e) {
			exceptionHandler.handle(uriRequest, e);
			throw new IllegalStateException("Exception handler error");
		}
	}
}
