package ru.ispras.crawler.core.datamodel.hunch;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} for downloading Hunch item.
 * Example: https://hunch.com/item/hn_12345 (item info), https://hunch.com/feed/ws/itemratings/?result_id=hn_12345 (item ratings)
 */
public class HunchItemRequest implements IRequest {

	private final long id;

	public HunchItemRequest(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		HunchItemRequest other = (HunchItemRequest) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return getClass().getName()+":[id=" + id + "]";
	}
}
