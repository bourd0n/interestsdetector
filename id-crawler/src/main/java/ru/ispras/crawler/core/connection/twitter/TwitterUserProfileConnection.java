package ru.ispras.crawler.core.connection.twitter;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserProfileRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.User;

/**
 * {@link IConnection} which returns Twitter user profile for every given {@link TwitterUserProfileRequest}.
 *
 * See https://dev.twitter.com/rest/reference/get/users/show
 *
 * @author Ivan Andrianov
 * 
 */
public class TwitterUserProfileConnection extends
		AbstractTwitterConnection<TwitterUserProfileRequest, BasicResponse<User>> {
	private static final String METHOD_BASE = "users/show.json?%s";
	
	public TwitterUserProfileConnection(IConnection<HttpRequest, HttpResponse> twitterHttpConnection) {
		super(twitterHttpConnection);
	}

	@Override
	public BasicResponse<User> getResponse(TwitterUserProfileRequest request) throws UnavailableConnectionException {
		String method = String.format(METHOD_BASE, getIdOrScreenNameParameter(request));
		return new BasicResponse<User>(getHttpResponse(method, User.class));
	}
	
	private String getIdOrScreenNameParameter(TwitterUserProfileRequest userRequest) {
		if (userRequest.isScreenNameRequest()) {
			return String.format("screen_name=%s", userRequest.getScreenName());
		}
		return getIdParameter(userRequest);
	}
}
