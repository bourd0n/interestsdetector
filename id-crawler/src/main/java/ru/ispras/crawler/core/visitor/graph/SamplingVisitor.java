package ru.ispras.crawler.core.visitor.graph;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;
import ru.ispras.crawler.core.visitor.IVisitor;
import ru.ispras.crawler.core.visitor.graph.sampler.IGraphSampler;

/**
 * {@link IVisitor} implementation which stores sets of graph nodes which were
 * processed successfully, unsuccessfully, are in processing now, candidates for
 * processing.
 * <p>
 * This class tries to reach the given node limit (if any) by applying the given
 * {@link IGraphSampler}. On success it feeds candidates set with node
 * neighbours extracted through {@link IResponseNeighbourExtractor}. On both success and
 * failure it uses needed number of nodes to reach node limit from the
 * candidates set as continuation {@link IRequest}s.
 * 
 * @author Ivan Andrianov
 */
public class SamplingVisitor<Req extends IRequest, Resp extends IResponse, ID> implements IVisitor<Req, Resp> {

	private final Set<ID> processingNodes = new HashSet<>();
	private final Set<ID> successNodes = new HashSet<>();
	private final Set<ID> failNodes = new HashSet<>();
	private final Set<ID> candidateNodes = new HashSet<>();

	private final IGraphSampler<ID> sampler;
	private final IRequestIdConverter<Req, ID> requestConverter;
	private final IResponseNeighbourExtractor<Resp, ID> neighbourExtractor;
	private final Class<Req> requestType;
	private final int nodeLimit;

	public SamplingVisitor(IGraphSampler<ID> sampler, IRequestIdConverter<Req, ID> requestConverter,
			IResponseNeighbourExtractor<Resp, ID> neighbourExtractor, Class<Req> requestType) {
		this(sampler, requestConverter, neighbourExtractor, requestType, Integer.MAX_VALUE);
	}

	public SamplingVisitor(IGraphSampler<ID> sampler, IRequestIdConverter<Req, ID> requestConverter,
			IResponseNeighbourExtractor<Resp, ID> neighbourExtractor, Class<Req> requestType, int nodeLimit) {
		this.sampler = sampler;
		this.requestConverter = requestConverter;
		this.neighbourExtractor = neighbourExtractor;
		this.requestType = requestType;
		this.nodeLimit = nodeLimit;
	}

	@Override
	public synchronized Collection<Req> visit(Req request, Resp response) {
		ID node = requestConverter.getId(request);
		markNodeAsSuccessful(node);

		Collection<ID> neighbourNodes = sampler.getNextNodes(node, neighbourExtractor.getNeighbours(response));
		addNeighboursToCandidates(neighbourNodes);
		return getNextProcessingNodes();
	}

	@Override
	public synchronized Collection<Req> visit(Req request, Throwable problem) {
		markNodeAsFailed(requestConverter.getId(request));
		return getNextProcessingNodes();
	}

	private void markNodeAsSuccessful(ID node) {
		processingNodes.remove(node);
		successNodes.add(node);
	}

	private void markNodeAsFailed(ID node) {
		processingNodes.remove(node);
		failNodes.add(node);
	}

	private void addNeighboursToCandidates(Collection<ID> neighbourNodes) {
		final Set<ID> addedNodes = new HashSet<>(neighbourNodes);
		addedNodes.removeAll(successNodes);
		addedNodes.removeAll(failNodes);
		addedNodes.removeAll(processingNodes);
		candidateNodes.addAll(addedNodes);
	}

	private Collection<Req> getNextProcessingNodes() {
		Set<Req> result = new HashSet<>();
		Iterator<ID> it = candidateNodes.iterator();
		int limit = nodeLimit - successNodes.size();

		while (it.hasNext() && (processingNodes.size() < limit)) {
			ID candidateNode = it.next();
			it.remove();
			processingNodes.add(candidateNode);
			result.add(requestConverter.getRequest(candidateNode));
		}

		return result;
	}

	@Override
	public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
		return Collections.singleton(requestType);
	}
}
