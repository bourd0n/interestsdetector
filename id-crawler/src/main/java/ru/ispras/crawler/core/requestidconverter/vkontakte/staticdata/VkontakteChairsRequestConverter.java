package ru.ispras.crawler.core.requestidconverter.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteChairsRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 03.06.14
 * Time: 16:23
 */
public class VkontakteChairsRequestConverter implements IRequestIdConverter<VkontakteChairsRequest, Long> {
    @Override
    public VkontakteChairsRequest getRequest(Long aLong) {
        return new VkontakteChairsRequest(aLong);
    }

    @Override
    public Long getId(VkontakteChairsRequest request) {
        return request.getFacultyId();
    }
}
