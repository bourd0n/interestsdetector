package ru.ispras.crawler.core.connection.http.ext;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import ru.ispras.crawler.core.connection.http.HttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.http.HttpProxyDescription;

/**
 * {@link IHttpClientFactory} implementation which creates
 * {@link DefaultHttpClient} and sets up timeouts and proxy info from the given
 * {@link HttpConnectionConfiguration}.
 * 
 * @author Ivan Andrianov
 * 
 */
public class DefaultHttpClientFactory implements IHttpClientFactory<HttpConnectionConfiguration> {
	@Override
	public HttpClient create(HttpConnectionConfiguration configuration) {
		final DefaultHttpClient httpClient = new DefaultHttpClient();
		final HttpParams params = httpClient.getParams();

		HttpConnectionParams.setConnectionTimeout(params, configuration.getConnectionTimeout());
		HttpConnectionParams.setSoTimeout(params, configuration.getSocketTimeout());

		HttpProxyDescription proxy = configuration.getProxy();
		if (proxy != null) {
			String ip = proxy.getIp();
			int port = proxy.getPort();

			params.setParameter(ConnRoutePNames.DEFAULT_PROXY, new HttpHost(ip, port));

			String login = proxy.getLogin();
			String password = proxy.getPassword();

			if ((login != null) && (password != null)) {
				httpClient.getCredentialsProvider().setCredentials(new AuthScope(ip, port),
						new UsernamePasswordCredentials(login, password));
			}
		}

		return httpClient;
	}
}
