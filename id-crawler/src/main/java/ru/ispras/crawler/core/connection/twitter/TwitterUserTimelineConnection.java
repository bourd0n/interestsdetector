package ru.ispras.crawler.core.connection.twitter;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserTimelineRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.Status;

/**
 * {@link IConnection} which returns Twitter user timeline (recent
 * tweets) for every given {@link TwitterUserTimelineRequest}.
 *
 * See https://dev.twitter.com/rest/reference/get/statuses/user_timeline
 *
 * @author Ivan Andrianov
 * 
 */
public class TwitterUserTimelineConnection extends
		AbstractTwitterConnection<TwitterUserTimelineRequest, BasicResponse<List<Status>>> {
	private static final String METHOD_BASE = "statuses/user_timeline.json?count=200&trim_user=true&include_rts=true&exclude_replies=true&%s%s";

	public TwitterUserTimelineConnection(IConnection<HttpRequest, HttpResponse> twitterHttpConnection) {
		super(twitterHttpConnection);
	}

	@Override
	public BasicResponse<List<Status>> getResponse(TwitterUserTimelineRequest request) throws UnavailableConnectionException {
		Long maxId = null;
		List<Status> timeline = new ArrayList<Status>();
		while (true) {
			String method = String.format(METHOD_BASE,
					getIdParameter(request), getMaxIdParameter(maxId));
			List<Status> response = getHttpResponse(method, new TypeReference<List<Status>>() {});

			timeline.addAll(response);

			if (timeline.size() >= request.getLimit() || response.isEmpty()) {
				break;
			}
			Status oldestTweet = response.get(response.size() - 1);
			maxId = oldestTweet.getId() - 1;
		}
		return new BasicResponse<List<Status>>(timeline);
	}
}
