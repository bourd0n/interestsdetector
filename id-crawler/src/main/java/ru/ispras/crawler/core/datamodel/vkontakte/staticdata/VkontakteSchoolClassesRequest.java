package ru.ispras.crawler.core.datamodel.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Request for https://vk.com/dev/database.getSchoolClasses
 *  
 * @author Ivan Andrianov
 */
public class VkontakteSchoolClassesRequest implements IRequest {
	private final long countryId;

	public VkontakteSchoolClassesRequest(long countryId) {
		this.countryId = countryId;
	}
	public long getCountryId() {
		return countryId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (countryId ^ (countryId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		VkontakteSchoolClassesRequest other = (VkontakteSchoolClassesRequest) obj;
		return countryId == other.countryId;
	}

	@Override
	public String toString() {
		return "VkontakteSchoolClassesRequest [countryId=" + countryId + "]";
	}
}
