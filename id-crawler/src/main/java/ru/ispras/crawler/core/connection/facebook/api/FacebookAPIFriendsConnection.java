package ru.ispras.crawler.core.connection.facebook.api;

import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.facebook.api.utils.UserToLongConverter;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIFriendsRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.User;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * {@link IConnection} for downloading friends list of given Facebook user via Facebook API.
 * Example: https://graph.facebook.com/100001698473676/friends
 *
 * @author andrey g
 *
 */
public class FacebookAPIFriendsConnection extends
		AbstractFacebookAPIConnection<FacebookAPIFriendsRequest, BasicResponse<List<Long>>> {

	public FacebookAPIFriendsConnection(IConnection<HttpRequest, HttpResponse> facebookAPIHttpConnection) {
		super(facebookAPIHttpConnection);
	}

	@Override
	public BasicResponse<List<Long>> getResponse(FacebookAPIFriendsRequest request) throws UnavailableConnectionException {
		return new BasicResponse<List<Long>>(UserToLongConverter.getLongList(getPagedResponse(request, User.class)));
	}
}
