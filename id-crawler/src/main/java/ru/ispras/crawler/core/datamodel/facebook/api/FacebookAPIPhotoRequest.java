package ru.ispras.crawler.core.datamodel.facebook.api;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} for downloading photo object via Facebook API
 * Example: https://graph.facebook.com/182443505155597
 *
 * @author andrey g
 *
 */
public class FacebookAPIPhotoRequest extends AbstractFacebookAPIObjectRequest {

	public FacebookAPIPhotoRequest(long id, String parameters) {
		super(id, parameters);
	}

	public FacebookAPIPhotoRequest(long id) {
		super(id);
	}

}
