package ru.ispras.crawler.core.connection.hunch;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserFollowingRequest;

/**
 * {@link IConnection} for downloading list of followees for given Hunch user
 * Example: https://hunch.com/ndrey/following
 */
public class HunchUserFollowingConnection extends AbstractHunchUserListConnection<HunchUserFollowingRequest> {

	private final String METHOD = "%s/following/";
	
	public HunchUserFollowingConnection(IConnection<HttpRequest, HttpResponse> hunchHttpConnection) {
		super(hunchHttpConnection);
	}

	@Override
	protected String getMethod(HunchUserFollowingRequest request) {
		final String screenName = request.getScreenName();
		return String.format(METHOD, screenName);
	}
}
