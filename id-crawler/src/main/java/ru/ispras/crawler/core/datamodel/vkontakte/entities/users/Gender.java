package ru.ispras.crawler.core.datamodel.vkontakte.entities.users;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 14.05.14
 * Time: 19:30
 */
public enum Gender {
    MALE, FEMALE, UNKNOWN
}
