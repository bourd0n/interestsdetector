package ru.ispras.crawler.core.datamodel.zvents;

import java.net.URL;

/**
 * {@link ZventsRequest} for downloading Zvents event.
 * <p>
 * Example: http://www.zvents.com/watsonville_ca/events/show/371697916-santa-cruz-symphony-season-opening-concert
 * 
 * @author sysoev
 */
public class EventZventsRequest extends ZventsRequest {

	private String eventId;
	private String locationId;
	
	public EventZventsRequest(URL url, String eventId, String locationId) {
		super(url);
		this.eventId = eventId;
		this.locationId = locationId;
	}

	public String getEventId() {
		return eventId;
	}
	
	public String getLocationId() {
		return locationId;
	}
	
	@Override
	public int hashCode() {
		return eventId.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EventZventsRequest) {
			return eventId.equals(((EventZventsRequest)obj).eventId);
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "Event. id: " + eventId + ", location: " + locationId + ", " + super.toString();
	}
}
