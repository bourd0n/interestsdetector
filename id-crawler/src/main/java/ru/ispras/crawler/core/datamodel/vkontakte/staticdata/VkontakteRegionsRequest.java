package ru.ispras.crawler.core.datamodel.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 28.05.14
 * Time: 15:20
 *
 * Request for https://vk.com/dev/database.getRegions
 */
public class VkontakteRegionsRequest  implements IRequest {
    private final long countryId;

    public VkontakteRegionsRequest(long countryId) {
        this.countryId = countryId;
    }

    public long getCountryId() {
        return countryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VkontakteRegionsRequest)) return false;

        VkontakteRegionsRequest that = (VkontakteRegionsRequest) o;

        if (countryId != that.countryId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (countryId ^ (countryId >>> 32));
    }

    @Override
	public String toString() {
		return "VkontakteRegionsRequest [countryId=" + countryId + "]";
	}
}
