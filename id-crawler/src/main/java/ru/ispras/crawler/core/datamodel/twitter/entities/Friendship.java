/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.twitter.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Yusuke Yamamoto - yusuke at mac.com, andrey g
 * @since Twitter4J 2.1.9
 */
public class Friendship {
    private final long id;
    private final String name;
    private final String screenName;
    private final boolean following;
    private final boolean followedBy;

    @JsonCreator
    public Friendship(
    		@JsonProperty("id") long id,
    		@JsonProperty("name") String name,
    		@JsonProperty("screen_name") String screenName,
    		@JsonProperty("following") boolean following,
    		@JsonProperty("followes_by") boolean followedBy) {
		this.id = id;
		this.name = name;
		this.screenName = screenName;
		this.following = following;
		this.followedBy = followedBy;
	}
    
    @JsonProperty("id")
    public long getId() {
        return id;
    }
    
    @JsonProperty("name")
    public String getName() {
        return name;
    }
    
    @JsonProperty("screen_name")
    public String getScreenName() {
        return screenName;
    }
    
    @JsonProperty("following")
    public boolean isFollowing() {
        return following;
    }
    
    @JsonProperty("followes_by")
    public boolean isFollowedBy() {
        return followedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Friendship that = (Friendship) o;

        if (followedBy != that.followedBy) return false;
        if (following != that.following) return false;
        if (id != that.id) return false;
        if (!name.equals(that.name)) return false;
        if (!screenName.equals(that.screenName)) return false;

        return true;
    }
    
    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (screenName != null ? screenName.hashCode() : 0);
        result = 31 * result + (following ? 1 : 0);
        result = 31 * result + (followedBy ? 1 : 0);
        return result;
    }

}
