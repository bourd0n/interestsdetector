package ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 16.05.14
 * Time: 17:47
 */
public class Graffiti extends Attachment {
    private final Long id;
    private final Long ownerId;
    private final String photo200;
    private final String photo586;

    @JsonCreator
    public Graffiti(
            @JsonProperty("id") Long id,
            @JsonProperty("owner_id") Long ownerId,
            @JsonProperty("photo_200") String photo200,
            @JsonProperty("photo_586") String photo586) {
        this.id = id;
        this.ownerId = ownerId;
        this.photo200 = photo200;
        this.photo586 = photo586;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("owner_id")
    public Long getOwnerId() {
        return ownerId;
    }

    @JsonProperty("photo_200")
    public String getPhoto200() {
        return photo200;
    }

    @JsonProperty("photo_586")
    public String getPhoto586() {
        return photo586;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ownerId == null) ? 0 : ownerId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Graffiti other = (Graffiti) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ownerId == null) {
			if (other.ownerId != null)
				return false;
		} else if (!ownerId.equals(other.ownerId))
			return false;
		return true;
	}
}

