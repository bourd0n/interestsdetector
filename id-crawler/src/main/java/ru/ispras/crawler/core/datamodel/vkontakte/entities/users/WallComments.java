package ru.ispras.crawler.core.datamodel.vkontakte.entities.users;

/**
 * Created by padre on 14.07.14
 */
public enum WallComments {
    ENABLE, DISABLE;

    public static WallComments intToWallComments(Integer number) {
       if(number != null && number == 1) return ENABLE;
       return DISABLE;
    }

    public static int wallCommentsToInt(WallComments status) {
        if(status == ENABLE) return 1;
        return 0;
    }

}
