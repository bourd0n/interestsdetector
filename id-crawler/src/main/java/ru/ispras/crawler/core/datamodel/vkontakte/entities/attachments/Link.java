package ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 16.05.14
 * Time: 17:47
 */
public class Link extends Attachment {
    private final String url;
    private final String title;
    private final String description;
    private final String imageSrc;
    private final String previewPage;
    private final String previewUrl;

    @JsonCreator
    public Link(
            @JsonProperty("url") String url,
            @JsonProperty("title") String title,
            @JsonProperty("description") String description,
            @JsonProperty("image_src") String imageSrc,
            @JsonProperty("preview_page") String previewPage,
            @JsonProperty("preview_url")  String previewUrl) {
        this.url = url;
        this.title = title;
        this.description = description;
        this.imageSrc = imageSrc;
        this.previewPage = previewPage;
        this.previewUrl = previewUrl;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("image_src")
    public String getImageSrc() {
        return imageSrc;
    }

    @JsonProperty("preview_page")
    public String getPreviewPage() {
        return previewPage;
    }

    @JsonProperty("preview_url")
    public String getPreviewUrl() {
        return previewUrl;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Link other = (Link) obj;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}
}
