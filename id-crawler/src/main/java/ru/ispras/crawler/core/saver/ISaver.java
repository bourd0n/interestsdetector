package ru.ispras.crawler.core.saver;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * Saves {@link IRequest} and its associated {@link IResponse} somewhere (e.g.,
 * database).
 * <p>
 * Implementations are expected to be thread-safe.
 * 
 * @author Ivan Andrianov
 */
public interface ISaver<Req extends IRequest, Resp extends IResponse> {
	void save(Req request, Resp response);
}
