package ru.ispras.crawler.core.connection;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.datamodel.grouping.GroupingRequest;
import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;
import ru.ispras.modis.utils.concurrent.DaemonThreadFactory;

/**
 * {@link IConnection} that manages its own thread pool to process
 * {@link GroupingRequest}s and return corresponding {@link GroupingResponse}s.
 * Default thread pool size is 50.
 * 
 * @author Ivan Andrianov
 * 
 */
public class GroupingConnection implements IConnection<GroupingRequest, GroupingResponse> {
	/**
	 * Configuration of {@link GroupingConnection} instances.
	 * <p>
	 * Provides a type-safe way to setup needed {@link IConnection} instances.
	 * 
	 * @author Ivan Andrianov
	 * 
	 */
	public static class Configuration {
		private final Map<Class<? extends IRequest>, IConnection<?, ?>> connections = new HashMap<>();

		public <Req extends IRequest> void put(Class<Req> type, IConnection<? super Req, ?> connection) {
			connections.put(type, connection);
		}
	}

	private final Map<Class<? extends IRequest>, IConnection<?, ?>> connections;
	private final ExecutorService threadPool;

	public GroupingConnection(Configuration configuration) {
		this(configuration, 50);
	}

	public GroupingConnection(Configuration configuration, int numberOfThreads) {
		this.connections = new HashMap<>(configuration.connections);
		threadPool = Executors.newFixedThreadPool(numberOfThreads, new DaemonThreadFactory());
	}

	@Override
	public GroupingResponse getResponse(GroupingRequest request) throws UnavailableConnectionException {
		List<? extends IRequest> requests = request.getRequests();
		Map<Class<? extends IRequest>, Future<IResponse>> tasks = submitTasks(requests);
		Map<Class<? extends IRequest>, IResponse> responses = getTasksResponses(tasks);
		return new GroupingResponse(responses);
	}

	private Map<Class<? extends IRequest>, Future<IResponse>> submitTasks(List<? extends IRequest> requests) {
		Map<Class<? extends IRequest>, Future<IResponse>> result = new HashMap<>();
		for (IRequest request : requests) {
			submitTask(result, request);
		}
		return result;
	}

	private void submitTask(Map<Class<? extends IRequest>, Future<IResponse>> tasks, final IRequest request) {
		Class<? extends IRequest> requestType = request.getClass();
		final IConnection<IRequest, ?> connection = getConnection(requestType);
		Future<IResponse> task = threadPool.submit(new Callable<IResponse>() {
			@Override
			public IResponse call() throws Exception {
				return connection.getResponse(request);
			}
		});
		tasks.put(requestType, task);
	}

	@SuppressWarnings("unchecked")
	private IConnection<IRequest, ?> getConnection(Class<? extends IRequest> requestType) {
		IConnection<IRequest, ?> result = (IConnection<IRequest, ?>) connections.get(requestType);
		if (result == null) {
			throw new IllegalStateException("No connection specified for request type: " + requestType.getName());
		}
		return result;
	}

	private Map<Class<? extends IRequest>, IResponse> getTasksResponses(
			Map<Class<? extends IRequest>, Future<IResponse>> tasks) throws UnavailableConnectionException {
		try {
			Map<Class<? extends IRequest>, IResponse> result = new HashMap<>();
			for (Map.Entry<Class<? extends IRequest>, Future<IResponse>> entry : tasks.entrySet()) {
				result.put(entry.getKey(), entry.getValue().get());
			}
			return result;
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		} catch (ExecutionException e) {
			Throwable cause = e.getCause();
			if (cause instanceof UnavailableConnectionException) {
				throw (UnavailableConnectionException) cause;
			}
			throw new RuntimeException(cause);
		}
	}
}
