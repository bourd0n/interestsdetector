package ru.ispras.crawler.core.datamodel.twitter;

import java.util.List;

import ru.ispras.crawler.core.datamodel.grouping.GroupingRequest;

/**
 * {@link GroupingRequest} subclass for {@link AbstractTwitterUserRequest}
 * grouping which provides {@link #hashCode()} and {@link #equals(Object)}
 * implementations based on user id.
 * 
 * @author Ivan Andrianov
 * 
 */
public abstract class TwitterUserGroupingRequest extends GroupingRequest {
	private final long id;

	public TwitterUserGroupingRequest(long id, AbstractTwitterUserRequest... requests) {
		super(requests);
		this.id = id;
	}

	public TwitterUserGroupingRequest(long id, List<? extends AbstractTwitterUserRequest> requests) {
		super(requests);
		this.id = id;
	}

	public long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TwitterUserGroupingRequest other = (TwitterUserGroupingRequest) obj;
		return id == other.id;
	}
	
	@Override
	public String toString() {
		return getClass().getCanonicalName() + "["  + id + "]";
	}
}
