package ru.ispras.crawler.core.connection;

import org.apache.log4j.Logger;

import ru.ispras.crawler.core.connection.exception.PermanentlyUnavailableConnectionException;
import ru.ispras.crawler.core.connection.exception.TargetUnreachableException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * {@link IConnection} wrapper implementation which sleeps to the given
 * timestamp on {@link UnavailableConnectionException} and retries to process
 * the given {@link IRequest}. Number of tries can be specified or 10 will be
 * used as default.
 * 
 * @author Ivan Andrianov
 */
public class DelayedConnection<Req extends IRequest, Resp extends IResponse> implements IConnection<Req, Resp> {
	private static final Logger logger = Logger.getLogger(DelayedConnection.class);

	private final IConnection<Req, Resp> wrapped;
	private final int numberOfTries;

	public DelayedConnection(IConnection<Req, Resp> wrapped) {
		this(wrapped, 10);
	}

	public DelayedConnection(IConnection<Req, Resp> wrapped, int numberOfTries) {
		this.wrapped = wrapped;
		this.numberOfTries = numberOfTries;
	}

	@Override
	public Resp getResponse(Req request) throws PermanentlyUnavailableConnectionException {
		for (int i = 0; i < numberOfTries; i++) {
			try {
				return wrapped.getResponse(request);
			} catch (PermanentlyUnavailableConnectionException e) {
				throw e;
			} catch (UnavailableConnectionException e) {
				String errorMessage = String.format("Thread failed to process request %s.",
						request);
				logger.warn(errorMessage, e);
				long interval = e.getTimeout() - System.currentTimeMillis();
				if (interval > 0) {
					String infoMessage = String.format(
							"Thread goes to sleep for %sms during processing of request %s.",
							 interval, request);
					logger.info(infoMessage);
					try {
						Thread.sleep(interval);
					} catch (InterruptedException ie) {
						throw new TargetUnreachableException(ie);
					}
				}
			}
		}
		throw new TargetUnreachableException(String.format("%d tries have failed during processing of request %s",
				numberOfTries, request));
	}
}
