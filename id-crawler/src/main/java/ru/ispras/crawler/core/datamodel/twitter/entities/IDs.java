/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.twitter.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A data class representing array of numeric IDs.
 *
 * @author Yusuke Yamamoto - yusuke at mac.com, andrey g
 */
public class IDs {

	private final List<Long> ids;
	private final long previousCursor;
	private final long nextCursor;


	@JsonCreator
    public IDs(
    		@JsonProperty("ids") List<Long> ids,
    		@JsonProperty("previous_cursor") long previousCursor,
    		@JsonProperty("next_cursor") long nextCursor) {
		this.ids = ids;
		this.previousCursor = previousCursor;
		this.nextCursor = nextCursor;
	}
		
    public boolean hasPrevious() {
        return 0 != previousCursor;
    }
 
    @JsonProperty("previous_cursor")
    public long getPreviousCursor() {
        return previousCursor;
    }

    @JsonProperty("ids")
	public List<Long> getIds() {
		return ids;
	}

	public boolean hasNext() {
        return 0 != nextCursor;
    }

	@JsonProperty("next_cursor")
	public long getNextCursor() {
        return nextCursor;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ids == null) ? 0 : ids.hashCode());
		result = prime * result + (int) (nextCursor ^ (nextCursor >>> 32));
		result = prime * result
				+ (int) (previousCursor ^ (previousCursor >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IDs other = (IDs) obj;
		if (ids == null) {
			if (other.ids != null)
				return false;
		} else if (!ids.equals(other.ids))
			return false;
		if (nextCursor != other.nextCursor)
			return false;
		if (previousCursor != other.previousCursor)
			return false;
		return true;
	}
}