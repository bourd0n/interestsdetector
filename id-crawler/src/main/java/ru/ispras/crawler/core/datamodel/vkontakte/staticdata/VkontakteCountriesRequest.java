package ru.ispras.crawler.core.datamodel.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 22.05.14
 * Time: 15:38
 *
 * Request for https://vk.com/dev/database.getCountries
 */
public class VkontakteCountriesRequest implements IRequest {
	@Override
	public int hashCode() {
		return 13;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
	    return (obj != null) && (getClass() == obj.getClass());
	}

	@Override
	public String toString() {
		return "VkontakteCountriesRequest []";
	}
}
