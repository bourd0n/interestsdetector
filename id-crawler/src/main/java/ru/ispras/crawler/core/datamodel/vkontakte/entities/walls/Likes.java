package ru.ispras.crawler.core.datamodel.vkontakte.entities.walls;

import static ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils.booleanToFlag;
import static ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils.flagToBoolean;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public  class Likes {
    private final int count;
    private final Boolean userLike;
    private final Boolean canLike;
    private final Boolean canPublish;

    @JsonCreator
    public Likes(
            @JsonProperty("count") int count,
            @JsonProperty("user_likes") int userLike,
            @JsonProperty("can_like") int canLike,
            @JsonProperty("can_publish") int canPublish) {
        this.count = count;
        this.userLike = flagToBoolean(userLike);
        this.canLike = flagToBoolean(canLike);
        this.canPublish = flagToBoolean(canPublish);
    }

    @JsonProperty("count")
    public int getCount() {
        return count;
    }

    @JsonIgnore
    public Boolean getUserLike() {
        return userLike;
    }
    
    @JsonProperty("user_likes")
    public Integer getUserLikeInt() {
        return booleanToFlag(userLike);
    }

    @JsonIgnore
    public Boolean getCanLike() {
        return canLike;
    }
    
    @JsonProperty("can_like")
    public Integer getCanLikeInt() {
        return booleanToFlag(canLike);
    }

    @JsonIgnore
    public Boolean getCanPublish() {
        return canPublish;
    }
    
    @JsonProperty("can_publish")
    public Integer getCanPublishInt() {
        return booleanToFlag(canPublish);
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + count;
		result = prime * result
				+ ((userLike == null) ? 0 : userLike.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Likes other = (Likes) obj;
		if (count != other.count)
			return false;
		if (userLike == null) {
			if (other.userLike != null)
				return false;
		} else if (!userLike.equals(other.userLike))
			return false;
		return true;
	}
}
