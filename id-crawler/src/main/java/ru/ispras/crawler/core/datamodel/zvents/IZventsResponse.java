package ru.ispras.crawler.core.datamodel.zvents;

import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * Marker interface for Zvents-related responses.
 * 
 * @author sysoev
 */
public interface IZventsResponse extends IResponse {
}