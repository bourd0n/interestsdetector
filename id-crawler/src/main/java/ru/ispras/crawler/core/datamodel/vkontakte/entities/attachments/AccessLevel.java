package ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments;

/**
 * Created by padre on 14.07.14
 */
public enum AccessLevel {
    EVERYONE, MEMBERONLY, HEADONLY, UNKNOWN;

    public static AccessLevel intToAccessLevel(Integer number) {
        if(number == null) return UNKNOWN;
        switch (number) {
            case 0: return HEADONLY;
            case 1: return MEMBERONLY;
            case 2: return EVERYONE;
            default: return UNKNOWN;
        }
    }

    public static Integer AccessLevelToInt (AccessLevel status) {
        switch (status) {
            case HEADONLY: return 0;
            case MEMBERONLY: return 1;
            case EVERYONE: return 2;
            default: return null;
        }
    }
}
