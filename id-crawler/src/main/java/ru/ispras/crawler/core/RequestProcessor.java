package ru.ispras.crawler.core;

import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.saver.ISaver;
import ru.ispras.crawler.core.visitor.IVisitor;

/**
 * Class to process requests of a single type.
 * <p>
 * Each incoming request is added to the queue; requests are extracted from the queue
 * and fulfilled by threads from pool.
 * <p>
 * Each request is
 * <ul>
 * <li> processed by corresponding {@link IConnection}
 * <li> received response, together with initial request are sent to {@link IVisitor}
 * <li> requests, returned by the {@link IVisitor} are sent to the crawler.
 * </ul>
 * 
 * @author sysoev
 */
class RequestProcessor<Req extends IRequest, Resp extends IResponse> {
	private static final Logger logger = Logger.getLogger(RequestProcessor.class);
	
	private final IConnection<? super Req, Resp> connection;
	private final IVisitor<? super Req, ? super Resp> visitor;
	private final ISaver<? super Req, ? super Resp> saver;
	private final ThreadPoolExecutor threadPool;
	private Crawler crawler;
	
	public RequestProcessor(Crawler crawler,
			IConnection<? super Req, Resp> connection,
			IVisitor<? super Req, ? super Resp> visitor,
			ISaver<? super Req, ? super Resp> saver,
			int numThreads) {
		this.crawler = crawler;
		this.connection = connection;
		this.visitor = visitor;
		this.saver = saver;
		this.threadPool = new ThreadPoolExecutor(
				numThreads, numThreads, 0L, TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>(), Executors.defaultThreadFactory());
	}

	/**
	 * Return true if there are no active tasks running and there are no tasks
	 * scheduled for running. 
	 */
	public boolean isEmpty() {
		// no active tasks and no tasks waiting for execution
		return threadPool.getActiveCount() == 0 && threadPool.getQueue().isEmpty();
	}

	/**
	 * Process provided request.
	 */
	public void processRequest(final Req request) {
		logger.trace("Request processor: " + this + ". Queue size: " + threadPool.getQueue().size());

		threadPool.execute(new Runnable() {
			@Override
			public void run() {
				try {
					Resp response = connection.getResponse(request);
					visit(request, response);
					save(request, response);
				} catch (Exception ce) {
					logger.error("Failed to process request " + request, ce);
					visit(request, ce);
				}
			}

			private void visit(Req request, Resp response) {
				try {
					Collection<? extends IRequest> newRequests = visitor.visit(request, response);
					crawler.addRequests(newRequests);
				} catch (Exception ve) {
					logger.error("Failed to visit request " + request, ve);
				}
			}

			private void save(Req request, Resp response) {
				try {
					saver.save(request, response);
				} catch (Exception se) {
					logger.error("Failed to save request " + request, se);
				}
			}

			private void visit(Req request, Exception ce) {
				try {
					Collection<? extends IRequest> newRequests = visitor.visit(request, ce);
					crawler.addRequests(newRequests);
				} catch (Exception ve) {
					logger.error("Failed to visit request " + request, ve);
				}
			}
		});
	}

	/**
	 * Shutdown crawling threads and close visitor (if it is {@link IClosable}).
	 */
	public void shutdown() {
		threadPool.shutdownNow();
	}
}
