package ru.ispras.crawler.core.requestidconverter.vkontakte;

import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteGroupMembersRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class VkontakteGroupMembersRequestIdConverter implements
		IRequestIdConverter<VkontakteGroupMembersRequest, Long> {

	@Override
	public Long getId(VkontakteGroupMembersRequest request) {
		return request.getGroupId();
	}

	@Override
	public VkontakteGroupMembersRequest getRequest(Long id) {
		return new VkontakteGroupMembersRequest(id);
	}

}
