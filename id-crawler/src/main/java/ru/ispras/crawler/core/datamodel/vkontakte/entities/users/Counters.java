package ru.ispras.crawler.core.datamodel.vkontakte.entities.users;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 14.05.14
 * Time: 21:26
 */
public class Counters {
    private final int albums;
    private final int videos;
    private final int audios;
    private final int photos;
    private final int notes;
    private final int friends;
    private final int groups;
    private final int onlineFriends;
    private final int mutualFriends;
    private final int userVideos;
    private final int followers;
    private final int userPhotos;
    private final int subscriptions;

    @JsonCreator
    public Counters(
            @JsonProperty("albums") int albums,
            @JsonProperty("videos") int videos,
            @JsonProperty("audios") int audios,
            @JsonProperty("photos") int photos,
            @JsonProperty("notes") int notes,
            @JsonProperty("friends") int friends,
            @JsonProperty("groups") int groups,
            @JsonProperty("online_friends") int onlineFriends,
            @JsonProperty("mutual_friends") int mutualFriends,
            @JsonProperty("user_videos") int userVideos,
            @JsonProperty("followers") int followers,
            @JsonProperty("user_photos") int userPhotos,
            @JsonProperty("subscriptions") int subscriptions) {
        this.albums = albums;
        this.videos = videos;
        this.audios = audios;
        this.photos = photos;
        this.notes = notes;
        this.friends = friends;
        this.groups = groups;
        this.onlineFriends = onlineFriends;
        this.mutualFriends = mutualFriends;
        this.userVideos = userVideos;
        this.followers = followers;
        this.userPhotos = userPhotos;
        this.subscriptions = subscriptions;
    }

    @JsonProperty("albums")
    public int getAlbums() {
        return albums;
    }

    @JsonProperty("videos")
    public int getVideos() {
        return videos;
    }

    @JsonProperty("audios")
    public int getAudios() {
        return audios;
    }

    @JsonProperty("photos")
    public int getPhotos() {
        return photos;
    }

    @JsonProperty("notes")
    public int getNotes() {
        return notes;
    }

    @JsonProperty("friends")
    public int getFriends() {
        return friends;
    }

    @JsonProperty("groups")
    public int getGroups() {
        return groups;
    }

    @JsonProperty("online_friends")
    public int getOnlineFriends() {
        return onlineFriends;
    }

    @JsonProperty("mutual_friends")
    public int getMutualFriends() {
        return mutualFriends;
    }

    @JsonProperty("user_videos")
    public int getUserVideos() {
        return userVideos;
    }

    @JsonProperty("followers")
    public int getFollowers() {
        return followers;
    }

    @JsonProperty("user_photos")
    public int getUserPhotos() {
        return userPhotos;
    }

    @JsonProperty("subscriptions")
    public int getSubscriptions() {
        return subscriptions;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + albums;
		result = prime * result + audios;
		result = prime * result + followers;
		result = prime * result + friends;
		result = prime * result + groups;
		result = prime * result + mutualFriends;
		result = prime * result + notes;
		result = prime * result + onlineFriends;
		result = prime * result + photos;
		result = prime * result + subscriptions;
		result = prime * result + userPhotos;
		result = prime * result + userVideos;
		result = prime * result + videos;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Counters other = (Counters) obj;
		if (albums != other.albums)
			return false;
		if (audios != other.audios)
			return false;
		if (followers != other.followers)
			return false;
		if (friends != other.friends)
			return false;
		if (groups != other.groups)
			return false;
		if (mutualFriends != other.mutualFriends)
			return false;
		if (notes != other.notes)
			return false;
		if (onlineFriends != other.onlineFriends)
			return false;
		if (photos != other.photos)
			return false;
		if (subscriptions != other.subscriptions)
			return false;
		if (userPhotos != other.userPhotos)
			return false;
		if (userVideos != other.userVideos)
			return false;
		if (videos != other.videos)
			return false;
		return true;
	}
}
