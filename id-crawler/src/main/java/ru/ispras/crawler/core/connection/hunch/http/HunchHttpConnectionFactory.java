package ru.ispras.crawler.core.connection.hunch.http;

import ru.ispras.crawler.core.connection.IConfigurableConnectionFactory;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.RateLimitConnection;
import ru.ispras.crawler.core.connection.exception.ConnectionCreationException;
import ru.ispras.crawler.core.connection.http.HttpConnection;
import ru.ispras.crawler.core.connection.http.auth.cookie.CookieAuthHttpClientFactory;
import ru.ispras.crawler.core.connection.http.auth.cookie.CookieAuthHttpResourceProfile;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpRequestConverter;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpResponseConverter;
import ru.ispras.crawler.core.connection.hunch.HunchUtils;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * Hunch {@link IConfigurableConnectionFactory} implementation.
 * 
 * @author Ivan Andrianov
 * 
 */
public class HunchHttpConnectionFactory implements
		IConfigurableConnectionFactory<HttpRequest, HttpResponse, HunchHttpConnectionConfiguration> {
	private static final CookieAuthHttpResourceProfile resourceProfile = new CookieAuthHttpResourceProfile(
			HunchUtils.BASE_URL + "people/login/", "email_or_username", "password", "session");

	@Override
	public IConnection<HttpRequest, HttpResponse> create(HunchHttpConnectionConfiguration configuration)
			throws ConnectionCreationException {
		return new RateLimitConnection<>(new HttpConnection<>(new DefaultHttpRequestConverter(),
				new CookieAuthHttpClientFactory(resourceProfile), configuration, new DefaultHttpResponseConverter()));
	}
}
