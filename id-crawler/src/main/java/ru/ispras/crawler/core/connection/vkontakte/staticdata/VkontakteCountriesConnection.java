package ru.ispras.crawler.core.connection.vkontakte.staticdata;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteCountriesRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 28.05.14
 * Time: 15:00
 */

/**
 * see description of connection in https://vk.com/dev/database.getCountries
 */
public class VkontakteCountriesConnection extends AbstractVkontakteIdTitleConnection<VkontakteCountriesRequest> {

    public VkontakteCountriesConnection(IConnection<HttpRequest, HttpResponse> httpConnection) {
        super(httpConnection);
    }


    @Override
    protected String getMethod(VkontakteCountriesRequest request) {
        return "database.getCountries?need_all=1";
    }


}
