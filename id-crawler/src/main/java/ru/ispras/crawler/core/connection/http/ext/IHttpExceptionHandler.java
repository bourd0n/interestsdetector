package ru.ispras.crawler.core.connection.http.ext;

import java.io.IOException;

import org.apache.http.client.methods.HttpUriRequest;

import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;

/**
 * Analyzes {@link IOException} arised during fetching HTTP response and throws 
 * {@link UnavailableConnectionException} or {@linkRuntimeException}
 * 
 * @author andrey
 *
 */
public interface IHttpExceptionHandler {
	/**
	 * Analyzes {@link IOException} arised during {@paramref uriRequest} processing.
	 * This method have to throw {@link UnavailableConnectionException} or {@linkRuntimeException} 
	 * 
	 * @param uriRequest request
	 * @param e an exception to be handle
	 * @throws UnavailableConnectionException
	 */
	public void handle(HttpUriRequest uriRequest, IOException e)  throws UnavailableConnectionException;
}
