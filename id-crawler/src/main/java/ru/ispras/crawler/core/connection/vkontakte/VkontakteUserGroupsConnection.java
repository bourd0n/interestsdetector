package ru.ispras.crawler.core.connection.vkontakte;

import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteUserGroupsRequest;

/**
 * Connection for https://vk.com/dev/groups.get method.
 * Requires access token
 * 
 * @author andrey
 *
 */
public class VkontakteUserGroupsConnection implements IConnection<VkontakteUserGroupsRequest, BasicResponse<List<Long>>> {

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	public VkontakteUserGroupsConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}

	@Override
	public BasicResponse<List<Long>> getResponse(
			VkontakteUserGroupsRequest request)
			throws UnavailableConnectionException {
		String url = "groups.get?user_id="+request.getUserId();
		List<Long> response = VkontakteConnectionUtils.getPagedResponse(url, 1000, request.getLimit(), httpConnection, Long.class);
		return new BasicResponse<List<Long>>(response);
	}
}
