package ru.ispras.crawler.core.datamodel.zvents;

import java.net.URL;

/**
 * {@link ZventsRequest} for downloading Zvents venue.
 * <p>
 * Example: http://www.zvents.com/watsonville_ca/venues/show/8394-henry-j-mello-center-for-the-performing-arts
 * 
 * @author sysoev
 */
public class VenueZventsRequest extends ZventsRequest {

	private String locationId;
	
	public VenueZventsRequest(URL url, String locationId) {
		super(url);
		this.locationId = locationId;
	}
	
	public String getLocationId() {
		return locationId;
	}
	
	@Override
	public int hashCode() {
		return locationId.hashCode();
	}
	
	@Override
	public boolean equals(Object other) {
		if (other instanceof VenueZventsRequest) {
			return locationId.equals(((VenueZventsRequest)other).locationId);
		}
		return false;
	}
	
	@Override
	public String toString() {
		return "Venue. id: " + locationId + ", " + super.toString();
	}
}
