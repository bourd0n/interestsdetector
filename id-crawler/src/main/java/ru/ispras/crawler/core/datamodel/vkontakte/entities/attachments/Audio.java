package ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 16.05.14
 * Time: 17:46
 */
public class Audio extends Attachment{
    private final Long id;
    private final Long ownerId;
    private final String artist;
    private final int duration;
    private final String url;
    private final Long lyricsId;
    private final Long albumId;
    private final Long genreId;

    @JsonCreator
    public Audio(
            @JsonProperty("id") Long id,
            @JsonProperty("owner_id") Long ownerId,
            @JsonProperty("artist") String artist,
            @JsonProperty("duration") int duration,
            @JsonProperty("url") String url,
            @JsonProperty("lyrics_id") Long lyricsId,
            @JsonProperty("album_id") Long albumId,
            @JsonProperty("genre_id") Long genreId) {
        this.id = id;
        this.ownerId = ownerId;
        this.artist = artist;
        this.duration = duration;
        this.url = url;
        this.lyricsId = lyricsId;
        this.albumId = albumId;
        this.genreId = genreId;
    }
    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("owner_id")
    public Long getOwnerId() {
        return ownerId;
    }

    @JsonProperty("artist")
    public String getArtist() {
        return artist;
    }

    @JsonProperty("duration")
    public int getDuration() {
        return duration;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("lyrics_id")
    public Long getLyricsId() {
        return lyricsId;
    }

    @JsonProperty("album_id")
    public Long getAlbumId() {
        return albumId;
    }

    @JsonProperty("genre_id")
    public Long getGenreId() {
        return genreId;
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ownerId == null) ? 0 : ownerId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Audio other = (Audio) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ownerId == null) {
			if (other.ownerId != null)
				return false;
		} else if (!ownerId.equals(other.ownerId))
			return false;
		return true;
	}
}
