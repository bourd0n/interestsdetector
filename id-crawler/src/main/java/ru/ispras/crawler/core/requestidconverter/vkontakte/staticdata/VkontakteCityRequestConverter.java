package ru.ispras.crawler.core.requestidconverter.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteCitiesRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 03.06.14
 * Time: 16:44
 */
public class VkontakteCityRequestConverter implements IRequestIdConverter<VkontakteCitiesRequest, Long> {
    @Override
    public VkontakteCitiesRequest getRequest(Long aLong) {
        return new VkontakteCitiesRequest(aLong);
    }

    @Override
    public Long getId(VkontakteCitiesRequest request) {
        return request.getCountryId();
    }
}
