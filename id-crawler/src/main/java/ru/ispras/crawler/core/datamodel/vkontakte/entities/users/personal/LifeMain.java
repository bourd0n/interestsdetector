package ru.ispras.crawler.core.datamodel.vkontakte.entities.users.personal;

/**
 * Created by padre on 14.07.14
 */
public enum LifeMain {
    FAMILY, CAREER, ENTERTAINMENTS, SCIENCE, IMPROWINGTHEWORLD, SELFDEVELOPMENT, ART, POWER, UNKNOWN;

    public static LifeMain intToLifeMain(Integer number) {
        if (number == null) return UNKNOWN;
        switch (number) {
            case 1: return FAMILY;
            case 2: return CAREER;
            case 3: return ENTERTAINMENTS;
            case 4: return SCIENCE;
            case 5: return IMPROWINGTHEWORLD;
            case 6: return SELFDEVELOPMENT;
            case 7: return ART;
            case 8: return POWER;
            default: return UNKNOWN;
        }
    }

    public static int LifeMainToint(LifeMain type) {
        switch (type) {
            case FAMILY: return 1;
            case CAREER: return 2;
            case ENTERTAINMENTS: return 3;
            case SCIENCE: return 4;
            case IMPROWINGTHEWORLD: return 5;
            case SELFDEVELOPMENT: return 6;
            case ART: return 7;
            case POWER: return 8;
            default: return 0;
        }
    }
}
