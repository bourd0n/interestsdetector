package ru.ispras.crawler.core.connection.hunch;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserItemRatingsRequest;
import ru.ispras.crawler.core.datamodel.hunch.entities.ItemRating;

/**
 *
 * {@link IConnection} for downloading list of items, rated by given Hunch user
 * Example: https://hunch.com/ndrey/ratings
 */
public class HunchUserItemsConnection implements IConnection<HunchUserItemRatingsRequest, BasicResponse<List<ItemRating>>> {
	
	// get user rated items
	private static final String RATED_ITEMS_METHOD = "%s/ratings/";

	/*
	 * These selectors are used to parse rated items
	 */
	private static final String ITEMS_SELECTOR = "ul.results > li.media-box.widget";
	private static final String ITEM_LINK_SELECTOR = "div.text > h4 > a";
	private static final String ITEM_RATING_SELECTOR = "div.text > div.br-spl > span.stars > span.si-u";

	private static final Pattern extractIdPattern = Pattern.compile("/item/hn_(\\d+)/");
	private static final Pattern extractRatingFromClass = Pattern.compile("si([1-5]+)");

	private final IConnection<HttpRequest, HttpResponse> hunchHttpConnection;
	
	public HunchUserItemsConnection(IConnection<HttpRequest, HttpResponse> hunchHttpConnection) {
		this.hunchHttpConnection = hunchHttpConnection;
	}

	@Override
	public BasicResponse<List<ItemRating>> getResponse(HunchUserItemRatingsRequest request) throws UnavailableConnectionException {
		final String screeName = request.getScreenName();
		return new BasicResponse<List<ItemRating>>(downloadRatedItemsList(screeName));
	}

	private List<ItemRating> downloadRatedItemsList(String screenName) throws UnavailableConnectionException {
		final String url = String.format(RATED_ITEMS_METHOD, screenName);
		
		return HunchUtils.getPagedItems(url, new HunchUtils.IHunchPagedListPageProcessor<ItemRating>() {
			@Override
			public List<ItemRating> processPage(Document page) {
				return parseItemsRatingsPage(page);
			}
		}, hunchHttpConnection);
	}
		
	private List<ItemRating> parseItemsRatingsPage(Document page) {
		final List<ItemRating> ratings = new ArrayList<ItemRating>();
		final Elements itemElemets = page.select(ITEMS_SELECTOR);
		for (Element itemElement : itemElemets) {
			ItemRating itemInfo = getItemInfo(itemElement);
			if (itemInfo != null) {
				ratings.add(itemInfo);
			}
		}
		return ratings;
	}
	
	private ItemRating getItemInfo(Element itemElement) {
		Element linkElement = itemElement.select(ITEM_LINK_SELECTOR).first();
		if (linkElement != null) {
			long id = getHunchIdFromUrl(linkElement.attr("href"));
			int rating = getItemRating(itemElement);
			return new ItemRating(id, rating);
		}
		return null;
	}
	
	private long getHunchIdFromUrl(String url) {
		Matcher matcher = extractIdPattern.matcher(url);
		if (matcher.find()) {
			return Integer.parseInt(matcher.group(1));
		}
		return 0;
	}
	
	private int getItemRating(Element itemElement) {
		Element ratingElement = itemElement.select(ITEM_RATING_SELECTOR).first();
		if (ratingElement != null) {
			Set<String> classes = ratingElement.classNames();
			for (String className : classes) {
				Matcher matcher = extractRatingFromClass.matcher(className);
				if (matcher.find()) {
					return Integer.parseInt(matcher.group(1));
				}
			}
		}
		return 0;
	}
}
