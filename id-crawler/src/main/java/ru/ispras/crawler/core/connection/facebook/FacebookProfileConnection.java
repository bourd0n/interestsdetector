package ru.ispras.crawler.core.connection.facebook;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.TargetUnreachableException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.facebook.FacebookResponse;
import ru.ispras.crawler.core.datamodel.facebook.FacebookUserProfileRequest;
import ru.ispras.crawler.core.datamodel.facebook.entities.Profile;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * {@link IConnection} for downloading profile for given Facebook user
 * Example: https://m.facebook.com/profile.php&id=100001698473676&v=info
 *
 * @author andrey g
 *
 */
public class FacebookProfileConnection extends
		AbstractFacebookConnection<FacebookUserProfileRequest, FacebookResponse<Profile>> {
	private final static String BASE_METHOD_ID = "profile.php?id=%d&v=info";
	private final static String BASE_METHOD_SCREEN_NAME = "%s?v=info";

	private final FacebookProfileParser profileParser = new FacebookProfileParser();
		
	public FacebookProfileConnection(IConnection<HttpRequest, HttpResponse> facebookHttpConnection) {
		super(facebookHttpConnection);
	}

	@Override
	public FacebookResponse<Profile> getResponse(FacebookUserProfileRequest request) throws UnavailableConnectionException {
		String method;
		Long userId = null; //unknown
		if (request.isScreenNameRequest()){
			method = String.format(BASE_METHOD_SCREEN_NAME, request.getScreenName());
		}else{
			userId = request.getId();
			method = String.format(BASE_METHOD_ID, userId);
		}
		String htmlPage = getHttpResponse(method);
		Profile profile = profileParser.parseProfile(htmlPage, userId);
		if (profile != null) {
			return new FacebookResponse<Profile>(profile, htmlPage);
		} else {
			throw new TargetUnreachableException(
					"No profile received or profile cannot be extracted from given link.");
		}
	}
}
