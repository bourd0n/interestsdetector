package ru.ispras.crawler.core.requestidconverter.vkontakte;

import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteGroupTopicsRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class VkontakteGroupTopicsRequestIdConverter implements
		IRequestIdConverter<VkontakteGroupTopicsRequest, Long> {

	@Override
	public Long getId(VkontakteGroupTopicsRequest request) {
		return request.getGroupId();
	}

	@Override
	public VkontakteGroupTopicsRequest getRequest(Long id) {
		return new VkontakteGroupTopicsRequest(id);
	}
}
