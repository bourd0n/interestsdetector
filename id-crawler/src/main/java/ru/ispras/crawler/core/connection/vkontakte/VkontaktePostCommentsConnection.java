package ru.ispras.crawler.core.connection.vkontakte;

import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontaktePostCommentsRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.SortOrder;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.walls.Comment;

/**
 * Connection for https://vk.com/dev/wall.getComments method
 *  
 * @author andrey
 *
 */
public class VkontaktePostCommentsConnection implements IConnection<VkontaktePostCommentsRequest, BasicResponse<List<Comment>>> {

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	public VkontaktePostCommentsConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}

	private String getMethod(VkontaktePostCommentsRequest request) {
		return "wall.getComments?owner_id="+request.getOwnerId()+
				"&post_id="+request.getPostId()+
				"&sort="+toString(request.getSort());
	}
	
	private String toString(SortOrder sort) {
		return (sort == SortOrder.ASC) ? "asc" : "desc";
	}
	
	@Override
	public BasicResponse<List<Comment>> getResponse(VkontaktePostCommentsRequest request)
			throws UnavailableConnectionException {
		String url = getMethod(request);
		List<Comment> response = VkontakteConnectionUtils.getPagedResponse(url, 100, request.getLimit(), httpConnection, Comment.class);
		return new BasicResponse<List<Comment>>(response);
	}	
}
