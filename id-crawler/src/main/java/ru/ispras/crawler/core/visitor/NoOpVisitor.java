package ru.ispras.crawler.core.visitor;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * {@link IVisitor} implementation which always produces no continuation
 * {@link IRequest}s.
 * 
 * @author Ivan Andrianov
 */
public class NoOpVisitor<Req extends IRequest, Resp extends IResponse> implements IVisitor<Req, Resp> {
	@Override
	public Collection<? extends IRequest> visit(Req request, Resp response) {
		return Collections.emptySet();
	}

	@Override
	public Collection<? extends IRequest> visit(Req request, Throwable problem) {
		return Collections.emptySet();
	}

	@Override
	public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
		return Collections.emptySet();
	}
}
