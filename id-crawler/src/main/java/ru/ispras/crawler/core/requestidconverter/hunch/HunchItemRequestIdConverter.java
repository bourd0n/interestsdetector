package ru.ispras.crawler.core.requestidconverter.hunch;

import ru.ispras.crawler.core.datamodel.hunch.HunchItemRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class HunchItemRequestIdConverter implements IRequestIdConverter<HunchItemRequest, Long> {
	@Override
	public Long getId(HunchItemRequest request) {
		return request.getId();
	}

	@Override
	public HunchItemRequest getRequest(Long id) {
		return new HunchItemRequest(id);
	}
}
