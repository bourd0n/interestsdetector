package ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 16.05.14
 * Time: 17:46
 * To change this template use File | Settings | File Templates.
 */
public class Video extends Attachment {
    private final Long id;
    private final Long ownerId;
    private final String title;
    private final String description;
    private final int duration;
    private final String  link;
    private final String  photo130;
    private final String  photo320;
    private final String photo640;
    private final Long date;
    private final int views;
    private final int comments;
    private final String  player;

    @JsonCreator
    public Video(
            @JsonProperty("id") Long id,
            @JsonProperty("owner_id") Long ownerId,
            @JsonProperty("title") String title,
            @JsonProperty("description") String description,
            @JsonProperty("duration") int duration,
            @JsonProperty("link") String link,
            @JsonProperty("photo_130") String photo130,
            @JsonProperty("photo_320") String photo320,
            @JsonProperty("photo_640") String photo640,
            @JsonProperty("date") Long date,
            @JsonProperty("views") int views,
            @JsonProperty("comments") int comments,
            @JsonProperty("player") String player) {
        this.id = id;
        this.ownerId = ownerId;
        this.title = title;
        this.description = description;
        this.duration = duration;
        this.link = link;
        this.photo130 = photo130;
        this.photo320 = photo320;
        this.photo640 = photo640;
        this.date = date;
        this.views = views;
        this.comments = comments;
        this.player = player;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("owner_id")
    public Long getOwnerId() {
        return ownerId;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("duration")
    public int getDuration() {
        return duration;
    }

    @JsonProperty("link")
    public String getLink() {
        return link;
    }

    @JsonProperty("photo_130")
    public String getPhoto130() {
        return photo130;
    }

    @JsonProperty("photo_320")
    public String getPhoto320() {
        return photo320;
    }

    @JsonProperty("photo_640")
    public String getPhoto640() {
        return photo640;
    }

    @JsonProperty("date")
    public Long getDate() {
        return date;
    }

    @JsonProperty("views")
    public int getViews() {
        return views;
    }

    @JsonProperty("comments")
    public int getComments() {
        return comments;
    }

    @JsonProperty("player")
    public String getPlayer() {
        return player;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ownerId == null) ? 0 : ownerId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Video other = (Video) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ownerId == null) {
			if (other.ownerId != null)
				return false;
		} else if (!ownerId.equals(other.ownerId))
			return false;
		return true;
	}
}
