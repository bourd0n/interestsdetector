package ru.ispras.crawler.core.datamodel.vkontakte;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Request for a single user.
 *
 * Batch connection for https://vk.com/dev/users.get should be used
 *
 * @author andrey
 *
 */
public class VkontakteUserRequest implements IRequest {
	private final long id;

	public VkontakteUserRequest(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VkontakteUserRequest other = (VkontakteUserRequest) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.valueOf(id);
	}
	
}
