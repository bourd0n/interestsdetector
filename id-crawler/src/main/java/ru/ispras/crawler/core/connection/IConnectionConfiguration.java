package ru.ispras.crawler.core.connection;

/**
 * Abstraction for entity classes which will be used for creating
 * {@link IConnection} instances
 * 
 * @author Alexander
 * 
 */
public interface IConnectionConfiguration {

}
