package ru.ispras.crawler.core.requestidconverter.vkontakte;

import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteGroupRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class VkontakteSingleGroupRequestIdConverter implements
		IRequestIdConverter<VkontakteGroupRequest, Long> {

	@Override
	public Long getId(VkontakteGroupRequest request) {
		return request.getId();
	}

	@Override
	public VkontakteGroupRequest getRequest(Long id) {
		return new VkontakteGroupRequest(id);
	}
}
