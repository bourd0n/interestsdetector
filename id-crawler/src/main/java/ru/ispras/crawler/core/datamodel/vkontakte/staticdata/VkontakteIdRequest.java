package ru.ispras.crawler.core.datamodel.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 10.06.14
 * Time: 18:44
 *
 * Request that contains id
 */
public class VkontakteIdRequest implements IRequest {
    private final long id;

    public VkontakteIdRequest(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VkontakteIdRequest)) return false;

        VkontakteIdRequest vkontakteIdRequest = (VkontakteIdRequest) o;

        if (id != vkontakteIdRequest.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
