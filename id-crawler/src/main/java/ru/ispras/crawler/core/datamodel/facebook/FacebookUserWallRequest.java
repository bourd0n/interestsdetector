package ru.ispras.crawler.core.datamodel.facebook;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} for downloading given user's wall.
 * Example: https://m.facebook.com/wall.php?id=100001698473676&time=2147483647
 *
 * @author andrey g
 */
public class FacebookUserWallRequest extends AbstractFacebookUserRequest {
	public FacebookUserWallRequest(long id) {
		super(id);
	}
}
