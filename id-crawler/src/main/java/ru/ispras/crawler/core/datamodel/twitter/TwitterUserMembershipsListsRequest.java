package ru.ispras.crawler.core.datamodel.twitter;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} for downloading memberships in lists of given user
 *
 * See https://dev.twitter.com/rest/reference/get/lists/memberships
 */
public class TwitterUserMembershipsListsRequest extends
		AbstractTwitterUserRequest {
	public TwitterUserMembershipsListsRequest(long id) {
		super(id);
	}
}
