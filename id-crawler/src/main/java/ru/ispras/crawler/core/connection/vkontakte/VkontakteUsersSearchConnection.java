package ru.ispras.crawler.core.connection.vkontakte;

import static ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils.encodeURL;
import static ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils.genderToInt;

import java.util.ArrayList;
import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteUsersSearchRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.ResponseList;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.users.User;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.users.UserQuery;

/**
 * Connection for https://vk.com/dev/users.search method.
 * Requires access token.
 * 
 * The method itself returns not full profiles, hence the result is a list of user's ids.
 * Use https://vk.com/dev/users.get method to retrieve full user's profiles
 * 
 * @author andrey
 *
 */
public class VkontakteUsersSearchConnection implements IConnection<VkontakteUsersSearchRequest, BasicResponse<List<Long>>> {
	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	public VkontakteUsersSearchConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}

	private String getMethod(VkontakteUsersSearchRequest request) {
		UserQuery query = request.getQuery();
		String method = "users.search?count=1000";

		if (query.getQuery() != null){
			method+="&q="+encodeURL(query.getQuery());
		}
		if (query.getAgeFrom() != null){
			method+="&age_from="+query.getAgeFrom();
		}
		if (query.getAgeTo() != null){
			method+="&age_to="+query.getAgeTo();
		}
		if (query.getBirthDay() != null){
			method+="&birth_day="+query.getBirthDay();
		}
		if (query.getBirthMonth() != null){
			method+="&birth_month="+query.getBirthMonth();
		}
		if (query.getBirthYear() != null){
			method+="&birth_year="+query.getBirthYear();
		}
		if (query.getCityId() != null){
			method+="&city="+query.getCityId();
		}
		if (query.getCompany() != null){
			method+="&company="+encodeURL(query.getCompany());
		}
		if (query.getCountryId() != null){
			method+="&country="+query.getCountryId();
		}
		if (query.getGroupId() != null){
			method+="&group_id="+query.getGroupId();
		}
		if (query.getHometownId() != null){
			method+="&hometown="+query.getHometownId();
		}
		if (query.getInterests() != null){
			method+="&interests="+encodeURL(query.getInterests());
		}
		if (query.getPosition() != null){
			method+="&position="+encodeURL(query.getPosition());
		}
		if (query.getReligion() != null){
			method+="&religion="+encodeURL(query.getReligion());
		}
		if (query.getSchoolCityId() != null){
			method+="&school_city="+query.getSchoolCityId();
		}
		if (query.getSchoolClass() != null){
			method+="&school_class="+query.getSchoolClass();
		}
		if (query.getSchoolCountryId() != null){
			method+="&school_country="+query.getSchoolCountryId();
		}
		if (query.getSchoolId() != null){
			method+="&school="+query.getSchoolId();
		}
		if (query.getSchoolYear() != null){
			method+="&school_year="+query.getSchoolYear();
		}
		if (query.getGender() != null){
			method+="&sex="+genderToInt(query.getGender());
		}
		if (query.getStatus() != null){
			method+="&status="+query.getStatus().getValue();
		}
		if (query.getUniversityChairId() != null){
			method+="&university_chair="+query.getUniversityChairId();
		}
		if (query.getUniversityCountryId() != null){
			method+="&university_country="+query.getUniversityCountryId();
		}
		if (query.getUniversityFacultyId() != null){
			method+="&university_faculty="+query.getUniversityFacultyId();
		}
		if (query.getUniversityId() != null){
			method+="&university="+query.getUniversityId();
		}
		if (query.getUniversityYear() != null){
			method+="&university_year="+query.getUniversityYear();
		}
		method+="&sort="+query.getSort().getValue();
		return method;
	}

	@Override
	public BasicResponse<List<Long>> getResponse(
			VkontakteUsersSearchRequest request)
			throws UnavailableConnectionException {
		String url = getMethod(request);
		ResponseList<User> response = VkontakteConnectionUtils.getSingleResponseList(url, httpConnection, User.class);
		List<Long> ids = new ArrayList<Long>();
		for (User user: response.getItems()){
			ids.add(user.getId());
		}
		return new BasicResponse<List<Long>>(ids);
	}
}
