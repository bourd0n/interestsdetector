package ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 10.06.14
 * Time: 18:27
 */
public class Chair {
    private final long id;
    private final String title;
    private final Long facultyId;

    @JsonCreator
    public Chair(
            @JsonProperty("id") long id,
            @JsonProperty("title") String title,
            @JsonProperty("faculty_id") Long facultiesId) {
        this.id = id;
        this.title = title;
        this.facultyId = facultiesId;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("faculty_id")
    public Long getFacultyId() {
        return facultyId;
    }
}
