package ru.ispras.crawler.core.saver;

import org.apache.commons.collections15.Predicate;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.saver.ISaver;


/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 06.06.14
 * Time: 15:48
 */

/**
 * The aim of this class is to prevent us from saving garbage response, e.q. the empty requests.
 * For this purposes one may use predicate.
 * @param <Req> type of request
 * @param <Resp> type of response
 */
public class ConditionalSaver<Req extends IRequest, Resp extends IResponse> implements ISaver<Req, Resp> {

    private final ISaver<Req, Resp> saver;
    private final Predicate<Resp> predicate;

    /**
     *
     * @param saver instance of ISaver. It save the data that is worth to save
     * @param predicate take into input response and return true if and only if the data is worth to save
     */
    public ConditionalSaver(ISaver<Req, Resp> saver, Predicate<Resp> predicate) {
        this.saver = saver;
        this.predicate = predicate;
    }

    @Override
    public void save(Req request, Resp response) {
        if (predicate.evaluate(response)) {
            saver.save(request, response);
        }
    }
}