package ru.ispras.crawler.core.visitor.graph;

import java.util.Collection;

import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * Converts {@link IResponse}s into collections of node neighbours identifiers.
 * 
 * @author andrey g
 */
public interface IResponseNeighbourExtractor<Resp extends IResponse, ID> {
	public Collection<ID> getNeighbours(Resp response);
}
