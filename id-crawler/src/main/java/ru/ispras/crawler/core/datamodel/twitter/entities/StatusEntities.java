package ru.ispras.crawler.core.datamodel.twitter.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class represents entities that appear in Status  
 * 
 * @author andrey g
 *
 */
public class StatusEntities {
    private final List<UserMentionEntity> userMentionEntities;
    private final List<URLEntity> urlEntities;
    private final List<HashtagEntity> hashtagEntities;
    private final List<MediaEntity> mediaEntities;
    private final List<SymbolEntity> symbolEntities;
    
    @JsonCreator
	public StatusEntities(
			@JsonProperty("user_mentions") List<UserMentionEntity> userMentionEntities,
			@JsonProperty("urls") List<URLEntity> urlEntities,
			@JsonProperty("hashtags") List<HashtagEntity> hashtagEntities,
			@JsonProperty("media") List<MediaEntity> mediaEntities,
			@JsonProperty("symbols") List<SymbolEntity> symbolEntities) {
		this.userMentionEntities = userMentionEntities;
		this.urlEntities = urlEntities;
		this.hashtagEntities = hashtagEntities;
		this.mediaEntities = mediaEntities;
		this.symbolEntities = symbolEntities;
	}    
    	
	@JsonProperty("user_mentions")
	public List<UserMentionEntity> getUserMentionEntities() {
		return userMentionEntities;
	}
	
	@JsonProperty("urls")
	public List<URLEntity> getUrlEntities() {
		return urlEntities;
	}
	
	@JsonProperty("hashtags")
	public List<HashtagEntity> getHashtagEntities() {
		return hashtagEntities;
	}
	
	@JsonProperty("media")
	public List<MediaEntity> getMediaEntities() {
		return mediaEntities;
	}
	
	@JsonProperty("symbols")
	public List<SymbolEntity> getSymbolEntities() {
		return symbolEntities;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((hashtagEntities == null) ? 0 : hashtagEntities.hashCode());
		result = prime * result
				+ ((mediaEntities == null) ? 0 : mediaEntities.hashCode());
		result = prime * result
				+ ((symbolEntities == null) ? 0 : symbolEntities.hashCode());
		result = prime * result
				+ ((urlEntities == null) ? 0 : urlEntities.hashCode());
		result = prime
				* result
				+ ((userMentionEntities == null) ? 0 : userMentionEntities
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StatusEntities other = (StatusEntities) obj;
		if (hashtagEntities == null) {
			if (other.hashtagEntities != null)
				return false;
		} else if (!hashtagEntities.equals(other.hashtagEntities))
			return false;
		if (mediaEntities == null) {
			if (other.mediaEntities != null)
				return false;
		} else if (!mediaEntities.equals(other.mediaEntities))
			return false;
		if (symbolEntities == null) {
			if (other.symbolEntities != null)
				return false;
		} else if (!symbolEntities.equals(other.symbolEntities))
			return false;
		if (urlEntities == null) {
			if (other.urlEntities != null)
				return false;
		} else if (!urlEntities.equals(other.urlEntities))
			return false;
		if (userMentionEntities == null) {
			if (other.userMentionEntities != null)
				return false;
		} else if (!userMentionEntities.equals(other.userMentionEntities))
			return false;
		return true;
	}

}
