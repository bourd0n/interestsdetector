package ru.ispras.crawler.core.datamodel.vkontakte.entities.users;

/**
 * Created by padre on 14.07.14
 */

public enum Online {
    ONLINE, OFFLINE;

    public static Online intToOnline(Integer number) {
        if(number != null && number == 1) return ONLINE;
        return OFFLINE;
    }

    public static int onlineToInt(Online status) {
        if(status == ONLINE) return 1;
        return 0;
    }
}
