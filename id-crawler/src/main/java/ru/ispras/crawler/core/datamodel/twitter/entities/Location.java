/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.twitter.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Yusuke Yamamoto - yusuke at mac.com, andrey g
 */
public class Location {
    private final int woeid;
    private final String countryName;
    private final String countryCode;
    private final String name;
    private final String url;
    private final PlaceType placeType;

    @JsonCreator
    public Location(
    		@JsonProperty("woeid") int woeid,
    		@JsonProperty("country") String countryName,
    		@JsonProperty("countryCode") String countryCode,
    		@JsonProperty("name") String name,
    		@JsonProperty("url") String url,
    		@JsonProperty("placeType") PlaceType placeType) {
		super();
		this.woeid = woeid;
		this.countryName = countryName;
		this.countryCode = countryCode;
		this.name = name;
		this.url = url;
		this.placeType = placeType;
	}
    
    public static class PlaceType{
    	private final String name;
    	private final int code;
    	
    	@JsonCreator    	
		public PlaceType(
				@JsonProperty("name") String name,
				@JsonProperty("code") int code) {
			this.name = name;
			this.code = code;
		}
    	
    	@JsonProperty("name") 
		public String getName() {
			return name;
		}
    	
    	@JsonProperty("code")
		public int getCode() {
			return code;
		}	
    }


    @JsonProperty("woeid")
    public int getWoeid() {
        return woeid;
    }

    @JsonProperty("country")
    public String getCountryName() {
        return countryName;
    }
    
    @JsonProperty("countryCode")
    public String getCountryCode() {
        return countryCode;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("url") 
    public String getURL() {
        return url;
    }

    @JsonProperty("placeType")
	public PlaceType getPlaceType() {
		return placeType;
	}
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Location)) return false;

        Location that = (Location) o;

        if (woeid != that.woeid) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return woeid;
    }
}
