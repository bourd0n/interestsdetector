package ru.ispras.crawler.core.requestidconverter.vkontakte;

import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteTopicCommentsRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class VkontakteTopicCommentsRequestIdConverter implements
		IRequestIdConverter<VkontakteTopicCommentsRequest, String> {

	@Override
	public String getId(VkontakteTopicCommentsRequest request) {
		return request.getGroupId()+"_"+request.getTopicId();
	}

	@Override
	public VkontakteTopicCommentsRequest getRequest(String id) {
		String elems[] = id.split("_");
		return new VkontakteTopicCommentsRequest(Long.parseLong(elems[0]), Long.parseLong(elems[1]));
	}

}
