package ru.ispras.crawler.core.datamodel.vkontakte;

import org.apache.commons.lang.Validate;

/**
 * Request for https://vk.com/dev/places.search method
 * 
 * @author andrey
 *
 */
public class VkontaktePlacesSearchRequest extends AbstractVkontakteLimitedRequest {
	
	public static enum Radius{
		R_300M("1"), R_2400M("2"), R_18KM("3"), R_150KM("4");
		
		private final String value;
		
		private Radius(String value){
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}
	
	private final String query;
	private final double latitude;
	private final double longitude;
	private final Radius radius;
	
	public VkontaktePlacesSearchRequest(String query, double latitude,
			double longitude, Radius radius, int limit) {
		super(limit);
		Validate.notNull(radius);
		this.query = query;
		this.latitude = latitude;
		this.longitude = longitude;
		this.radius = radius;
	}
	
	public VkontaktePlacesSearchRequest(String query, double latitude,
			double longitude, Radius radius) {
		super();
		Validate.notNull(radius);
		this.query = query;
		this.latitude = latitude;
		this.longitude = longitude;
		this.radius = radius;
	}
	
	public VkontaktePlacesSearchRequest(double latitude,
			double longitude, Radius radius, int limit) {
		this(null, latitude, longitude, radius, limit);
	}
	
	public VkontaktePlacesSearchRequest(double latitude,
			double longitude, Radius radius) {
		this(null, latitude, longitude, radius);
	}
	
	public VkontaktePlacesSearchRequest(String query, double latitude,
			double longitude, int limit) {
		this(query, latitude, longitude, Radius.R_300M, limit);
	}
	
	public VkontaktePlacesSearchRequest(String query, double latitude,
			double longitude) {
		this(query, latitude, longitude, Radius.R_300M);
	}
	
	public VkontaktePlacesSearchRequest(double latitude,
			double longitude, int limit) {
		this(null, latitude, longitude, Radius.R_300M, limit);
	}
	
	public VkontaktePlacesSearchRequest(double latitude,
			double longitude) {
		this(null, latitude, longitude, Radius.R_300M);
	}

	
	public String getQuery() {
		return query;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public Radius getRadius() {
		return radius;
	}

	@Override
	public String toString() {
		return "VkontaktePlacesSearchRequest [query=" + query + ", latitude="
				+ latitude + ", longitude=" + longitude + ", radius=" + radius
				+ "]";
	}
}
