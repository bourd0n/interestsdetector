/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.twitter.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author Yusuke Yamamoto - yusuke at mac.com, andrey g
 * @since Twitter4J 2.1.1
 */
public class Place{
    private final String name;
    private final String streetAddress;
    private final String countryCode;
    private final String id;
    private final String country;
    private final String placeType;
    private final String url;
    private final String fullName;
    private final PolygonGeoLocation boundingBox;
    private final List<Place> containedWithIn;

    @JsonCreator
    public Place(
    		@JsonProperty("name") String name,
    		@JsonProperty("street_address") String streetAddress,
    		@JsonProperty("country_code") String countryCode,
    		@JsonProperty("id") String id,
    		@JsonProperty("country") String country,
    		@JsonProperty("place_type") String placeType,
    		@JsonProperty("url") String url,
    		@JsonProperty("full_name") String fullName,
    		@JsonProperty("bounding_box") PolygonGeoLocation boundingBox,
    		@JsonProperty("contained_within") List<Place> containedWithIn) {
		this.name = name;
		this.streetAddress = streetAddress;
		this.countryCode = countryCode;
		this.id = id;
		this.country = country;
		this.placeType = placeType;
		this.url = url;
		this.fullName = fullName;
		this.boundingBox = boundingBox;
		this.containedWithIn = containedWithIn;
	}

    @JsonProperty("name")
    public String getName() {
        return name;
    }
   
    @JsonProperty("street_address")
    public String getStreetAddress() {
        return streetAddress;
    }
    
    @JsonProperty("country_code")
    public String getCountryCode() {
        return countryCode;
    }
    
    @JsonProperty("id")
    public String getId() {
        return id;
    }
    
    @JsonProperty("country")
    public String getCountry() {
        return country;
    }
    
    @JsonProperty("place_type")
    public String getPlaceType() {
        return placeType;
    }
    
    @JsonProperty("url")
    public String getURL() {
        return url;
    }
    
    @JsonProperty("full_name")
    public String getFullName() {
        return fullName;
    }
    
    @JsonProperty("bounding_box")
    public PolygonGeoLocation getBoundingBox() {
        return boundingBox;
    }
    
    @JsonProperty("contained_within")
    public List<Place> getContainedWithIn() {
        return containedWithIn;
    }

    @Override
	public boolean equals(Object obj) {
        if (null == obj) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return obj instanceof Place && ((Place) obj).getId().equals(this.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
