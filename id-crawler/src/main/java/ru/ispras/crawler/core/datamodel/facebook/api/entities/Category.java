/*
 * Copyright 2012 Ryuji Yamashita
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.facebook.api.entities;

import java.util.Date;

import ru.ispras.crawler.core.datamodel.facebook.api.utils.FacebookAPIDateConverter;
import ru.ispras.crawler.core.datamodel.utils.DateConverter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ryuji Yamashita - roundrop at gmail.com, andrey g
 */
public class Category {
	private final String id;
	private final String name;
	private final String category;
	private final Date createdTime;

	private final static DateConverter dateConverter = new FacebookAPIDateConverter();
	
	@JsonCreator
	public Category(
			@JsonProperty("id") String id,
			@JsonProperty("name") String name,
			@JsonProperty("category") String category,
			@JsonProperty("created_time") String createdTime) {
		this.id = id;
		this.name = name;
		this.category = category;
		this.createdTime = dateConverter.getDate(createdTime);
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("category")
	public String getCategory() {
		return category;
	}

	@JsonProperty("created_time")
	public String getCreatedTimeString() {
		return dateConverter.getString(createdTime);
	}
	
	@JsonIgnore
	public Date getCreatedTime() {
		return createdTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
