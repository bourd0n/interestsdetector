/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.ispras.crawler.core.datamodel.twitter.entities;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Yusuke Yamamoto - yusuke at mac.com
 * @since Twitter4J 2.2.3
 */
public class MediaEntity {
    private final long id;
    private final String url;
    private final String mediaURL;
    private final String mediaURLHttps;
    private final String expandedURL;
    private final String displayURL;
    private final Sizes sizes;
    private final String type;
    private final List<Integer> indices;
    
    @JsonCreator
    public MediaEntity(
		   @JsonProperty("id") long id, 
		   @JsonProperty("url") String url, 
		   @JsonProperty("media_url") String mediaURL,
		   @JsonProperty("media_url_https") String mediaURLHttps,
		   @JsonProperty("expanded_url") String expandedURL,
		   @JsonProperty("display_url") String displayURL,
		   @JsonProperty("sizes") Sizes sizes,
		   @JsonProperty("type") String type,
		   @JsonProperty("indices") List<Integer> indices) {
		this.id = id;
		this.url = url;
		this.mediaURL = mediaURL;
		this.mediaURLHttps = mediaURLHttps;
		this.expandedURL = expandedURL;
		this.displayURL = displayURL;
		this.sizes = sizes;
		this.type = type;
		this.indices = indices;
	}
    
    @JsonProperty("indices")
	public List<Integer> getIndices() {
		return indices;
	}

	@JsonProperty("id") 
	public long getId() {
        return id;
    }

	@JsonProperty("media_url")
    public String getMediaURL() {
        return mediaURL;
    }

	@JsonProperty("media_url_https")
    public String getMediaURLHttps() {
        return mediaURLHttps;
    }

	@JsonProperty("url")
    public String getURL() {
        return url;
    }

	@JsonProperty("display_url")
    public String getDisplayURL() {
        return displayURL;
    }

	@JsonProperty("expanded_url")
    public String getExpandedURL() {
        return expandedURL;
    }
    
	@JsonProperty("sizes")
    public Sizes getSizes() {
        return sizes;
    }

	@JsonProperty("type")
    public String getType() {
        return type;
    }

	/**
	 * Sizes are part of MediaEntity
	 * @author andrey g
	 *
	 */
	public static class Sizes {
		private final Size large;
		private final Size medium;
		private final Size small;
		private final Size thumb;
		
		@JsonCreator	
		public Sizes(
				@JsonProperty("large") Size large,
				@JsonProperty("medium") Size medium,
				@JsonProperty("small") Size small,
				@JsonProperty("thumb") Size thumb) {
			this.large = large;
			this.medium = medium;
			this.small = small;
			this.thumb = thumb;
		}
		
		@JsonProperty("large")
		public Size getLarge() {
			return large;
		}
		
		@JsonProperty("medium")
		public Size getMedium() {
			return medium;
		}
		
		@JsonProperty("small")
		public Size getSmall() {
			return small;
		}
		
		@JsonProperty("thumb")
		public Size getThumb() {
			return thumb;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((large == null) ? 0 : large.hashCode());
			result = prime * result
					+ ((medium == null) ? 0 : medium.hashCode());
			result = prime * result + ((small == null) ? 0 : small.hashCode());
			result = prime * result + ((thumb == null) ? 0 : thumb.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Sizes other = (Sizes) obj;
			if (large == null) {
				if (other.large != null)
					return false;
			} else if (!large.equals(other.large))
				return false;
			if (medium == null) {
				if (other.medium != null)
					return false;
			} else if (!medium.equals(other.medium))
				return false;
			if (small == null) {
				if (other.small != null)
					return false;
			} else if (!small.equals(other.small))
				return false;
			if (thumb == null) {
				if (other.thumb != null)
					return false;
			} else if (!thumb.equals(other.thumb))
				return false;
			return true;
		}
	}
	
	/**
	 * Size is part of Sizes
	 * @author andrey g
	 *
	 */
    public static class Size {
        private final int width;
        private final int height;
        private final String resize;
        
        @JsonCreator
		public Size(
				@JsonProperty("width") int width,
				@JsonProperty("height") int height,
				@JsonProperty("resize") String resize) {
			this.width = width;
			this.height = height;
			this.resize = resize;
		}

        @JsonProperty("width")
        public int getWidth() {
            return width;
        }
        
        @JsonProperty("height")
        public int getHeight() {
            return height;
        }
        
        @JsonProperty("resize")
        public String getResize() {
            return resize;
        }

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + height;
			result = prime * result
					+ ((resize == null) ? 0 : resize.hashCode());
			result = prime * result + width;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Size other = (Size) obj;
			if (height != other.height)
				return false;
			if (resize == null) {
				if (other.resize != null)
					return false;
			} else if (!resize.equals(other.resize))
				return false;
			if (width != other.width)
				return false;
			return true;
		}
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MediaEntity)) return false;

        MediaEntity that = (MediaEntity) o;

        if (id != that.id) return false;

        return true;
    }
    
    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
