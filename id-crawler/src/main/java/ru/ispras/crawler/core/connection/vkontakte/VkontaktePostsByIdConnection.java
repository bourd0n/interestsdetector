package ru.ispras.crawler.core.connection.vkontakte;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteBatchResponseUtils;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.batch.BasicBatchRequest;
import ru.ispras.crawler.core.datamodel.batch.BasicBatchResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontaktePostRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.walls.Post;

/**
 * Connection for https://vk.com/dev/wall.getById method
 * 
 * @author andrey
 *
 */
public class VkontaktePostsByIdConnection
		implements IConnection<BasicBatchRequest<VkontaktePostRequest>, BasicBatchResponse<VkontaktePostRequest, BasicResponse<Post>>> {

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	private static final VkontakteBatchResponseUtils<VkontaktePostRequest, String, Post> utils = 
			new VkontakteBatchResponseUtils<VkontaktePostRequest, String, Post>(){
				@Override
				protected String getId(VkontaktePostRequest req) {
					return req.getOwnerId()+"_"+req.getPostId();
				}

				@Override
				protected String getId(Post obj) {
					return obj.getOwnerId()+"_"+obj.getId();
				}
	};
	
	public VkontaktePostsByIdConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}
	
	private String getMethod(List<String> ids) {
		return "wall.getById?posts="+StringUtils.join(ids, ',')+
				"&copy_history_depth=2";
	}

	@Override
	public BasicBatchResponse<VkontaktePostRequest, BasicResponse<Post>> getResponse(
			BasicBatchRequest<VkontaktePostRequest> request)
			throws UnavailableConnectionException {
		String url = getMethod(utils.getIds(request.getRequests()));
		List<Post> response = VkontakteConnectionUtils.getSingleListResponse(url, httpConnection, Post.class);
		return utils.getBasicBatchResponse(request.getRequests(), response);
	}


}
