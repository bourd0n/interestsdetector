package ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 16.05.14
 * Time: 17:48
 */
public class PhotosList extends Attachment{
    private final List<Integer> ids;

    @JsonCreator
    public PhotosList(
            @JsonProperty("photos_list")  List<Integer> ids) {
        this.ids = ids;
    }

    @JsonProperty("photos_list")
    public List<Integer> getIds() {
        return ids;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ids == null) ? 0 : ids.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhotosList other = (PhotosList) obj;
		if (ids == null) {
			if (other.ids != null)
				return false;
		} else if (!ids.equals(other.ids))
			return false;
		return true;
	}
}
