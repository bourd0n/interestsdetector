package ru.ispras.crawler.core.requestidconverter.facebook;

import ru.ispras.crawler.core.datamodel.facebook.AbstractFacebookUserRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public abstract class AbstractFacebookUserRequestIdConverter<Req extends AbstractFacebookUserRequest> implements
		IRequestIdConverter<Req, Long> {
	@Override
	public Long getId(Req request) {
		return request.getId();
	}
}
