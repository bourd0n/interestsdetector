package ru.ispras.crawler.core.requestidconverter.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteFacultiesRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 06.06.14
 * Time: 15:10
 */
public class VkontakteFacultiesRequestConverter implements IRequestIdConverter<VkontakteFacultiesRequest, Long> {
    @Override
    public Long getId(VkontakteFacultiesRequest request) {
        return request.getUniversityId();
    }

    @Override
    public VkontakteFacultiesRequest getRequest(Long aLong) {
        return new VkontakteFacultiesRequest(aLong);
    }
}
