package ru.ispras.crawler.core.datamodel.facebook;

import ru.ispras.crawler.core.datamodel.IRequest;
/**
 * {@link IRequest} for downloading friends list of given Facebook user
 * Example: https://m.facebook.com/friends/?id=100001698473676&f=0
 *
 * @author andrey g
 *
 */
public class FacebookUserFriendsRequest extends AbstractFacebookUserRequest {
	public FacebookUserFriendsRequest(long id) {
		super(id);
	}
}
