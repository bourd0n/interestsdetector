package ru.ispras.crawler.core.connection.http.ext;

import org.apache.http.client.methods.HttpUriRequest;

import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * Converts HTTP Client's {@link HttpUriRequest} and
 * {@link org.apache.http.HttpResponse} into {@link HttpResponse}.
 * 
 * @author Ivan Andrianov
 * 
 */
public interface IHttpResponseConverter {
	HttpResponse convert(HttpUriRequest request, org.apache.http.HttpResponse response)
			throws UnavailableConnectionException;
}
