package ru.ispras.crawler.core.connection.zvents;

import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.zvents.VenueZventsRequest;
import ru.ispras.crawler.core.datamodel.zvents.VenueZventsResponse;
import ru.ispras.modis.geo.Coordinate;

/**
 * Connection to download and process Zvents venue page.
 * 
 * @author sysoev
 */
public class VenueZventsConnection extends ZventsConnection<VenueZventsRequest, VenueZventsResponse> {

	private static final Pattern LAT_PATTERN = Pattern.compile("\"lat\":([^,]+),");
	private static final Pattern LON_PATTERN = Pattern.compile("\"lng\":([^,]+),");
	
	private static final Pattern LAT_URL_PATTERN = Pattern.compile("latitude=([^&]+)&");
	private static final Pattern LON_URL_PATTERN = Pattern.compile("longitude=([^&]+)&");
	
	public VenueZventsConnection(IConnection<? super HttpRequest, ? extends HttpResponse> httpConnection) {
		super(httpConnection);
	}

	@Override
	protected VenueZventsResponse convertToResponse(Document doc, String html, VenueZventsRequest request) {
		
		final String id = request.getLocationId();
		final URL url = request.getURL();
		
		final Element descriptionColumn = doc.getElementsByClass("description_column").first(); 
		final String name = descriptionColumn.getElementsByTag("h1").first().text();
		
		final String address = descriptionColumn.getElementsByClass("address").first().text();
		final Coordinate coordinate = extractCoordinate(html);
		return new VenueZventsResponse(id, url, name, address, coordinate);
	}
	
	private Coordinate extractCoordinate(String html) {
		for (String line : html.split("\n")) {
			if (line.contains("options.venueMarkerData")) {
				return extractCoordinates(line, LAT_PATTERN, LON_PATTERN);
			} else if (line.contains("latitude=") && line.contains("longitude=")) {
				return extractCoordinates(line, LAT_URL_PATTERN, LON_URL_PATTERN);
			}
		}
		return null;
	}
	
	private Coordinate extractCoordinates(
			String line, Pattern latitudePattern, Pattern longitudePattern) {
		final Double lat = extractFirstGroupAsDouble(line, latitudePattern);
		final Double lon = extractFirstGroupAsDouble(line, longitudePattern);
		if (lat != null && lon != null) {
			return new Coordinate(lat, lon);
		} else {
			return null;
		}
	}
	
	private static Double extractFirstGroupAsDouble(String line, Pattern pattern) {
		final Matcher matcher = pattern.matcher(line);
		if (matcher.find()) {
			final String first = matcher.group(1);
			return Double.parseDouble(first);
		}
		return null;
	}
}
