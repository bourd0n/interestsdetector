package ru.ispras.crawler.core.requestidconverter.facebook.api;

import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIFriendsRequest;

public class FacebookAPIFriendsRequestIdConverter extends
		AbstractFacebookAPIRequestIdConverter<FacebookAPIFriendsRequest> {
	@Override
	public FacebookAPIFriendsRequest getRequest(Long id) {
		return new FacebookAPIFriendsRequest(id);
	}
}
