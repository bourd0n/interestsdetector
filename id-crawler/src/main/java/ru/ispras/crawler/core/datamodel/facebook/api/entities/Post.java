/*
 * Copyright 2012 Ryuji Yamashita
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.facebook.api.entities;

import java.util.Date;
import java.util.List;
import java.util.Map;

import ru.ispras.crawler.core.datamodel.facebook.api.utils.FacebookAPIDateConverter;
import ru.ispras.crawler.core.datamodel.utils.DateConverter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ryuji Yamashita - roundrop at gmail.com, andrey g
 */
public class Post {
	private final String id;
	private final Category from;
	private final ResponseList<IdNameEntity> to;
	private final String message;
	private final List<Tag> messageTags;
	private final String picture;
	private final String link;
	private final String name;
	private final String caption;
	private final String description;
	private final String source;
	private final List<Post.Property> properties;
	private final String icon;
	private final List<Post.Action> actions;
	private final Privacy privacy;
	private final String type;
	private final Shares shares;
	private final ResponseList<Category> likes;
	private final Place place;
	private final String statusType;
	private final String story;
	private final Map<String, List<Tag>> storyTags;
	private final ResponseList<IdNameEntity> withTags;
	private final ResponseList<Comment> comments;
	private final String objectId;
	private final Application application;
	private final Date createdTime;
	private final Date updatedTime;
	private final Boolean isPublished;
	private final Integer scheduledPublishTime;
	private final Targeting targeting;

	private final static DateConverter dateConverter = new FacebookAPIDateConverter();
	
	@JsonCreator
	public Post(
			@JsonProperty("id") String id,
			@JsonProperty("from") Category from,
			@JsonProperty("to") ResponseList<IdNameEntity> to,
			@JsonProperty("message") String message,
			@JsonProperty("message_tags") List<Tag> messageTags,
			@JsonProperty("picture") String picture,
			@JsonProperty("link") String link,
			@JsonProperty("name") String name,
			@JsonProperty("caption") String caption,
			@JsonProperty("description") String description,
			@JsonProperty("source") String source,
			@JsonProperty("properties") List<Property> properties,
			@JsonProperty("icon") String icon,
			@JsonProperty("actions") List<Action> actions,
			@JsonProperty("privacy") Privacy privacy,
			@JsonProperty("type") String type,
			@JsonProperty("shares") Shares shares,
			@JsonProperty("likes") ResponseList<Category> likes,
			@JsonProperty("place") Place place,
			@JsonProperty("statusType") String statusType,
			@JsonProperty("story") String story,
			@JsonProperty("story_tags") Map<String, List<Tag>> storyTags,
			@JsonProperty("with_tags") ResponseList<IdNameEntity> withTags,
			@JsonProperty("comments") ResponseList<Comment> comments,
			@JsonProperty("object_id") String objectId,
			@JsonProperty("application") Application application,
			@JsonProperty("created_time") String createdTime,
			@JsonProperty("updated_time") String updatedTime,
			@JsonProperty("is_published") Boolean isPublished,
			@JsonProperty("scheduled_publish_time") Integer scheduledPublishTime,
			@JsonProperty("targeting") Targeting targeting) {
		this.id = id;
		this.from = from;
		this.to = to;
		this.message = message;
		this.messageTags = messageTags;
		this.picture = picture;
		this.link = link;
		this.name = name;
		this.caption = caption;
		this.description = description;
		this.source = source;
		this.properties = properties;
		this.icon = icon;
		this.actions = actions;
		this.privacy = privacy;
		this.type = type;
		this.shares = shares;
		this.likes = likes;
		this.place = place;
		this.statusType = statusType;
		this.story = story;
		this.storyTags = storyTags;
		this.withTags = withTags;
		this.comments = comments;
		this.objectId = objectId;
		this.application = application;
		this.createdTime = dateConverter.getDate(createdTime);
		this.updatedTime = dateConverter.getDate(updatedTime);
		this.isPublished = isPublished;
		this.scheduledPublishTime = scheduledPublishTime;
		this.targeting = targeting;
	}
	
	@JsonProperty("id")
	public String getId() {
		return id;
	}
	
	@JsonProperty("from")
	public Category getFrom() {
		return from;
	}
	
	@JsonProperty("to")
	public ResponseList<IdNameEntity> getTo() {
		return to;
	}
	
	@JsonProperty("message")
	public String getMessage() {
		return message;
	}
	
	@JsonProperty("message_tags")
	public List<Tag> getMessageTags() {
		return messageTags;
	}
	
	@JsonProperty("picture")
	public String getPicture() {
		return picture;
	}
	
	@JsonProperty("link")
	public String getLink() {
		return link;
	}
	
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	
	@JsonProperty("caption")
	public String getCaption() {
		return caption;
	}
	
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}
	
	@JsonProperty("source")
	public String getSource() {
		return source;
	}
	
	@JsonProperty("properties")
	public List<Post.Property> getProperties() {
		return properties;
	}
	
	@JsonProperty("icon")
	public String getIcon() {
		return icon;
	}
	
	@JsonProperty("actions")
	public List<Post.Action> getActions() {
		return actions;
	}
	
	@JsonProperty("privacy")
	public Privacy getPrivacy() {
		return privacy;
	}
	
	@JsonProperty("type")
	public String getType() {
		return type;
	}
	
	@JsonProperty("shares")
	public Shares getShares() {
		return shares;
	}

	@JsonProperty("likes")
	public ResponseList<Category> getLikes() {
		return likes;
	}
	
	@JsonProperty("place") 
	public Place getPlace() {
		return place;
	}
	
	@JsonProperty("statusType")
	public String getStatusType() {
		return statusType;
	}
	
	@JsonProperty("story")
	public String getStory() {
		return story;
	}
	
	@JsonProperty("story_tags")
	public Map<String, List<Tag>> getStoryTags() {
		return storyTags;
	}
	
	@JsonProperty("with_tags")
	public ResponseList<IdNameEntity> getWithTags() {
		return withTags;
	}
	
	@JsonProperty("comments")
	public ResponseList<Comment> getComments() {
		return comments;
	}
	
	@JsonProperty("object_id")
	public String getObjectId() {
		return objectId;
	}
	
	@JsonProperty("application")
	public Application getApplication() {
		return application;
	}

	@JsonProperty("created_time")
	public String getCreatedTimeString() {
		return dateConverter.getString(createdTime);
	}
	
	@JsonIgnore
	public Date getCreatedTime() {
		return createdTime;
	}

	@JsonProperty("updated_time")
	public String getUpdatedTimeString() {
		return dateConverter.getString(updatedTime);
	}
	
	@JsonIgnore
	public Date getUpdatedTime() {
		return updatedTime;
	}
	@JsonProperty("is_published")
	public Boolean isPublished() {
		return isPublished;
	}
	@JsonProperty("scheduled_publish_time")
	public Integer getScheduledPublishTime() {
		return scheduledPublishTime;
	}

	@JsonProperty("targeting")
	public Targeting getTargeting() {
		return targeting;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Post other = (Post) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public static class Property {

		private final String name;
		private final String text;
		private final String href;

		@JsonCreator
		public Property(@JsonProperty("name") String name,
				@JsonProperty("text") String text,
				@JsonProperty("href") String href) {
			this.name = name;
			this.text = text;
			this.href = href;
		}

		@JsonProperty("name")
		public String getName() {
			return name;
		}

		@JsonProperty("text")
		public String getText() {
			return text;
		}

		@JsonProperty("href")
		public String getHref() {
			return href;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((href == null) ? 0 : href.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((text == null) ? 0 : text.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Property other = (Property) obj;
			if (href == null) {
				if (other.href != null)
					return false;
			} else if (!href.equals(other.href))
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			if (text == null) {
				if (other.text != null)
					return false;
			} else if (!text.equals(other.text))
				return false;
			return true;
		}
	}

	public static class Action {
		private final String name;
		private final String link;

		@JsonCreator
		public Action(@JsonProperty("name") String name,
				@JsonProperty("link") String link) {
			this.name = name;
			this.link = link;
		}

		@JsonProperty("name")
		public String getName() {
			return name;
		}

		@JsonProperty("link")
		public String getLink() {
			return link;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((link == null) ? 0 : link.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Action other = (Action) obj;
			if (link == null) {
				if (other.link != null)
					return false;
			} else if (!link.equals(other.link))
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}
	}

	public static class Shares {
		private final Integer count;

		@JsonCreator
		public Shares(@JsonProperty("count") Integer count) {
			this.count = count;
		}

		@JsonProperty("count")
		public Integer getCount() {
			return count;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((count == null) ? 0 : count.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Shares other = (Shares) obj;
			if (count == null) {
				if (other.count != null)
					return false;
			} else if (!count.equals(other.count))
				return false;
			return true;
		}
	}

}
