package ru.ispras.crawler.core.datamodel.vkontakte.entities;

/**
 * 
 * @author andrey
 *
 */
public enum SortOrder{
	ASC, DESC
}
