package ru.ispras.crawler.core.visitor.graph.sampler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * {@link IGraphSampler} implementation for Forest Fire random walk.
 * 
 * @author Ivan Andrianov
 */
public class ForestFireSampler<T> implements IGraphSampler<T> {
	private static final Random random = new Random();

	private final Set<T> burntNeighbours = new HashSet<T>();

	private final int neighboursLimit;
	private final double burningProbability;

	public ForestFireSampler() {
		this(10000, 0.6);
	}

	public ForestFireSampler(int neighboursLimit, double burningProbability) {
		this.neighboursLimit = neighboursLimit;
		this.burningProbability = burningProbability;
	}

	@Override
	public Collection<T> getNextNodes(T node, Collection<T> neighbours) {
		Collection<T> result = new ArrayList<T>();
	
		double burningProbability = getBurningProbability(neighbours.size());
	
		for (T neighbour: neighbours){
			if (shouldBeBurnt(burningProbability)) {
				if (!burntNeighbours.contains(neighbour)) {
					burntNeighbours.add(neighbour);
					result.add(neighbour);
				}
			}
		}
		return result;
	}

	private double getBurningProbability(int neighboursSize) {
		return neighboursSize <= neighboursLimit ? burningProbability : burningProbability
				* neighboursLimit / neighboursSize;
	}

	private boolean shouldBeBurnt(double burningProbability) {
		return random.nextDouble() <= burningProbability;
	}
}
