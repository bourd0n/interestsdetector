/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.twitter.entities;



import java.util.Date;

import ru.ispras.crawler.core.datamodel.twitter.utils.TwitterDateConverter;
import ru.ispras.crawler.core.datamodel.utils.DateConverter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A data class representing Basic user information element
 *
 * @author Yusuke Yamamoto - yusuke at mac.com, andrey g
 */
public class User{

    private final long id;
    private final String name;
    private final String screenName;
    private final String location;
    private final String description;
    private final UserEntities entities;
    private final boolean isContributorsEnabled;
    private final String profileImageUrl;
    private final String profileImageUrlHttps;
    private final String url;
    private final boolean isProtected;
    private final int followersCount;
    private final Status status;
    private final String profileBackgroundColor;
    private final String profileTextColor;
    private final String profileLinkColor;
    private final String profileSidebarFillColor;
    private final String profileSidebarBorderColor;
    private final boolean profileUseBackgroundImage;
    private final int friendsCount;
    private final Date createdAt;
    private final int favouritesCount;
    private final int utcOffset;
    private final String timeZone;
    private final String profileBackgroundImageUrl;
    private final String profileBackgroundImageUrlHttps;
    private final String profileBannerImageUrl;
    private final boolean profileBackgroundTiled;
    private final String lang;
    private final int statusesCount;
    private final boolean isGeoEnabled;
    private final boolean isVerified;
    private final boolean isTranslator;
    private final int listedCount;
    private final boolean isFollowRequestSent;

    private final static DateConverter dateConverter = new TwitterDateConverter();
    
    @JsonCreator
    public User(
		   		@JsonProperty("id") long id, 
		   		@JsonProperty("name") String name,
		   		@JsonProperty("screen_name") String screenName,
		   		@JsonProperty("location") String location,
		   		@JsonProperty("description") String description,
		   		@JsonProperty("entities") UserEntities entities,
		   		@JsonProperty("contributors_enabled") boolean isContributorsEnabled,
		   		@JsonProperty("profile_image_url") String profileImageUrl,
		   		@JsonProperty("profile_image_url_https") String profileImageUrlHttps,
		   		@JsonProperty("url") String url,
		   		@JsonProperty("protected") boolean isProtected,
		   		@JsonProperty("followers_count") int followersCount,
		   		@JsonProperty("status") Status status,
		   		@JsonProperty("profile_background_color") String profileBackgroundColor,
		   		@JsonProperty("profile_text_color") String profileTextColor,
		   		@JsonProperty("profile_link_color") String profileLinkColor,
		   		@JsonProperty("profile_sidebar_fill_color") String profileSidebarFillColor,
		   		@JsonProperty("profile_sidebar_border_color") String profileSidebarBorderColor,
		   		@JsonProperty("profile_use_background_image") boolean profileUseBackgroundImage,
		   		@JsonProperty("friends_count") int friendsCount,
		   		@JsonProperty("created_at") String createdAt,
		   		@JsonProperty("favourites_count") int favouritesCount,
		   		@JsonProperty("utc_offset") int utcOffset,
		   		@JsonProperty("time_zone") String timeZone,
		   		@JsonProperty("profile_background_image_url") String profileBackgroundImageUrl,
		   		@JsonProperty("profile_background_image_url_https") String profileBackgroundImageUrlHttps,
		   		@JsonProperty("profile_banner_url") String profileBannerImageUrl,
		   		@JsonProperty("profile_background_tile") boolean profileBackgroundTiled,
		   		@JsonProperty("lang") String lang,
		   		@JsonProperty("statuses_count") int statusesCount,
		   		@JsonProperty("geo_enabled") boolean isGeoEnabled,
		   		@JsonProperty("verified") boolean isVerified,
		   		@JsonProperty("is_translator") boolean isTranslator,
		   		@JsonProperty("listed_count") int listedCount,
		   		@JsonProperty("follow_request_sent") boolean isFollowRequestSent) {
		this.id = id;
		this.name = name;
		this.screenName = screenName;
		this.location = location;
		this.description = description;
		this.entities = entities;
		this.isContributorsEnabled = isContributorsEnabled;
		this.profileImageUrl = profileImageUrl;
		this.profileImageUrlHttps = profileImageUrlHttps;
		this.url = url;
		this.isProtected = isProtected;
		this.followersCount = followersCount;
		this.status = status;
		this.profileBackgroundColor = profileBackgroundColor;
		this.profileTextColor = profileTextColor;
		this.profileLinkColor = profileLinkColor;
		this.profileSidebarFillColor = profileSidebarFillColor;
		this.profileSidebarBorderColor = profileSidebarBorderColor;
		this.profileUseBackgroundImage = profileUseBackgroundImage;
		this.friendsCount = friendsCount;
		this.createdAt = dateConverter.getDate(createdAt);
		this.favouritesCount = favouritesCount;
		this.utcOffset = utcOffset;
		this.timeZone = timeZone;
		this.profileBackgroundImageUrl = profileBackgroundImageUrl;
		this.profileBackgroundImageUrlHttps = profileBackgroundImageUrlHttps;
		this.profileBannerImageUrl = profileBannerImageUrl;
		this.profileBackgroundTiled = profileBackgroundTiled;
		this.lang = lang;
		this.statusesCount = statusesCount;
		this.isGeoEnabled = isGeoEnabled;
		this.isVerified = isVerified;
		this.isTranslator = isTranslator;
		this.listedCount = listedCount;
		this.isFollowRequestSent = isFollowRequestSent;
	}

    @JsonProperty("id")
    public long getId() {
		return id;
	}

    @JsonProperty("name")
    public String getName() {
		return name;
	}

    @JsonProperty("screen_name") 
    public String getScreenName() {
		return screenName;
	}

    @JsonProperty("location")
    public String getLocation() {
		return location;
	}
    
    @JsonProperty("description")
    public String getDescription() {
		return description;
	}

    @JsonProperty("entities")
    public UserEntities getEntities() {
		return entities;
	}

    @JsonProperty("contributors_enabled")
    public boolean isContributorsEnabled() {
		return isContributorsEnabled;
	}
    
    @JsonProperty("profile_image_url")
    public String getProfileImageUrl() {
		return profileImageUrl;
	}

    @JsonProperty("profile_image_url_https")
    public String getProfileImageUrlHttps() {
		return profileImageUrlHttps;
	}

    @JsonProperty("url")
    public String getUrl() {
		return url;
	}

    @JsonProperty("protected")
    public boolean isProtected() {
		return isProtected;
	}

    @JsonProperty("followers_count")
    public int getFollowersCount() {
		return followersCount;
	}
    
    @JsonProperty("status")
    public Status getStatus() {
		return status;
	}

    @JsonProperty("profile_background_color")
    public String getProfileBackgroundColor() {
		return profileBackgroundColor;
	}

    @JsonProperty("profile_text_color")
    public String getProfileTextColor() {
		return profileTextColor;
	}

    @JsonProperty("profile_link_color")
	public String getProfileLinkColor() {
		return profileLinkColor;
	}

    @JsonProperty("profile_sidebar_fill_color")
	public String getProfileSidebarFillColor() {
		return profileSidebarFillColor;
	}

    @JsonProperty("profile_sidebar_border_color") 
	public String getProfileSidebarBorderColor() {
		return profileSidebarBorderColor;
	}

	@JsonProperty("profile_use_background_image")
	public boolean isProfileUseBackgroundImage() {
		return profileUseBackgroundImage;
	}

	@JsonProperty("friends_count")
	public int getFriendsCount() {
		return friendsCount;
	}

	@JsonIgnore
	public Date getCreatedAt() {
		return createdAt;
	}
	
	@JsonProperty("created_at")
	public String getCreatedAtString() {
		return dateConverter.getString(createdAt);
	}
	
	@JsonProperty("favourites_count")
	public int getFavouritesCount() {
		return favouritesCount;
	}

	@JsonProperty("utc_offset")
	public int getUtcOffset() {
		return utcOffset;
	}

	@JsonProperty("time_zone")
	public String getTimeZone() {
		return timeZone;
	}

	@JsonProperty("profile_background_image_url")
	public String getProfileBackgroundImageUrl() {
		return profileBackgroundImageUrl;
	}

	@JsonProperty("profile_background_image_url_https")
	public String getProfileBackgroundImageUrlHttps() {
		return profileBackgroundImageUrlHttps;
	}

	@JsonProperty("profile_banner_url")
	public String getProfileBannerImageUrl() {
		return profileBannerImageUrl;
	}

	@JsonProperty("profile_background_tile")
	public boolean isProfileBackgroundTiled() {
		return profileBackgroundTiled;
	}
	
	@JsonProperty("lang") 
	public String getLang() {
		return lang;
	}

	@JsonProperty("statuses_count")
	public int getStatusesCount() {
		return statusesCount;
	}

	@JsonProperty("geo_enabled")
	public boolean isGeoEnabled() {
		return isGeoEnabled;
	}

	@JsonProperty("verified")
	public boolean isVerified() {
		return isVerified;
	}

	@JsonProperty("is_translator")
	public boolean isTranslator() {
		return isTranslator;
	}

	@JsonProperty("listed_count")
	public int getListedCount() {
		return listedCount;
	}

	@JsonProperty("follow_request_sent")
	public boolean isFollowRequestSent() {
		return isFollowRequestSent;
	}

	@Override
	public int hashCode() {
        return (int) id;
    }
    
	@Override
    public boolean equals(Object obj) {
        if (null == obj) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return obj instanceof User && ((User) obj).getId() == this.id;
    }
}
