package ru.ispras.crawler.core.datamodel.facebook;

import org.apache.commons.lang.Validate;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 *  Abstract {@link IRequest} that is processed via parsing Facebook HTML pages (https://m.facebook.com/...)
 */
public abstract class AbstractFacebookUserRequest implements IRequest {

	private final Long id;

	public AbstractFacebookUserRequest(long id) {
		Validate.isTrue(id > 0, "id should be > 0");
		this.id = id;
	}
	
	protected AbstractFacebookUserRequest() {
		id = null;
	}

	public long getId() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		AbstractFacebookUserRequest other = (AbstractFacebookUserRequest) obj;
		return id == other.id;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + ":[userId=" + id + "]";
	}
}
