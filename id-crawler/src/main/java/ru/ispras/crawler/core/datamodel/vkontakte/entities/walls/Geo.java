package ru.ispras.crawler.core.datamodel.vkontakte.entities.walls;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by padre on 14.07.14
 */
public class Geo {


    private final String type;
    private final String coordinates;
    private final Place place;

    @JsonCreator
    public Geo(
            @JsonProperty("type") String type,
            @JsonProperty("coordinates") String coordinates,
            @JsonProperty("place") Place place) {
        this.type = type;
        this.coordinates = coordinates;
        this.place = place;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("coordinates")
    public String getCoordinates() {
        return coordinates;
    }

    @JsonProperty("place")
    public Place getPlace() {
        return place;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((coordinates == null) ? 0 : coordinates.hashCode());
        result = prime * result + ((place == null) ? 0 : place.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Geo other = (Geo) obj;
        if (coordinates == null) {
            if (other.coordinates != null)
                return false;
        } else if (!coordinates.equals(other.coordinates))
            return false;
        if (place == null) {
            if (other.place != null)
                return false;
        } else if (!place.equals(other.place))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }
}
