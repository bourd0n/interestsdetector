package ru.ispras.crawler.core.datamodel.vkontakte.entities.walls;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 19.05.14
 * Time: 18:15
 */

import java.util.List;

import ru.ispras.crawler.core.datamodel.vkontakte.entities.groups.Group;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.users.User;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * see description in https://vk.com/dev/wall.getReposts
 */
public class Repost {
    private final List<Post> posts;
    private final List<User> profiles;
    private final List<Group> groups;

    @JsonCreator
    public Repost(
            @JsonProperty("items") List<Post> posts,
            @JsonProperty("profiles") List<User> profiles,
            @JsonProperty("groups") List<Group> groups) {
        this.posts = posts;
        this.profiles = profiles;
        this.groups = groups;
    }

    @JsonProperty("items")
    public List<Post> getPosts() {
        return posts;
    }

    @JsonProperty("profiles")
    public List<User> getProfiles() {
        return profiles;
    }

    @JsonProperty("groups")
    public List<Group> getGroups() {
        return groups;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((groups == null) ? 0 : groups.hashCode());
		result = prime * result + ((posts == null) ? 0 : posts.hashCode());
		result = prime * result
				+ ((profiles == null) ? 0 : profiles.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Repost other = (Repost) obj;
		if (groups == null) {
			if (other.groups != null)
				return false;
		} else if (!groups.equals(other.groups))
			return false;
		if (posts == null) {
			if (other.posts != null)
				return false;
		} else if (!posts.equals(other.posts))
			return false;
		if (profiles == null) {
			if (other.profiles != null)
				return false;
		} else if (!profiles.equals(other.profiles))
			return false;
		return true;
	}
}
