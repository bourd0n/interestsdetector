package ru.ispras.crawler.core.datamodel.vkontakte.entities.stats;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 19.05.14
 * Time: 18:51
 */
public class Stats {
    private final Day day;
    private final int views;
    private final int visitors;
    private final int reach;
    private final int reachSubscribers;
    private final VisitorsByProperty visitorsByGender;
    private final VisitorsByProperty visitorsByAge;
    private final VisitorsByProperty visitorsByAgeGender;
    private final VisitorsByPCities visitorsByPCities;
    private final VisitorsByPCountries visitorsByPCountries;

    @JsonCreator
    public Stats(
            @JsonProperty("day") String day,
            @JsonProperty("views") int views,
            @JsonProperty("visitors") int visitors,
            @JsonProperty("reach") int reach,
            @JsonProperty("reach_subscribers") int reachSubscribers,
            @JsonProperty("sex") VisitorsByProperty visitorsByGender,
            @JsonProperty("age") VisitorsByProperty visitorsByAge,
            @JsonProperty("sex_age") VisitorsByProperty visitorsByAgeGender,
            @JsonProperty("cities") VisitorsByPCities visitorsByPCities,
            @JsonProperty("countries") VisitorsByPCountries visitorsByPCountries) {
        this.day = new Day(day);
        this.views = views;
        this.visitors = visitors;
        this.reach = reach;
        this.reachSubscribers = reachSubscribers;
        this.visitorsByGender = visitorsByGender;
        this.visitorsByAge = visitorsByAge;
        this.visitorsByAgeGender = visitorsByAgeGender;
        this.visitorsByPCities = visitorsByPCities;
        this.visitorsByPCountries = visitorsByPCountries;
    }

    @JsonProperty("day")
    public String getDayString() {
        return day.toString();
    }

    @JsonIgnore
    public Day getDay() {
        return day;
    }

    @JsonProperty("views")
    public int getViews() {
        return views;
    }

    @JsonProperty("visitors")
    public int getVisitors() {
        return visitors;
    }

    @JsonProperty("reach")
    public int getReach() {
        return reach;
    }

    @JsonProperty("reach_subscribers")
    public int getReachSubscribers() {
        return reachSubscribers;
    }

    @JsonProperty("sex")
    public VisitorsByProperty getVisitorsByGender() {
        return visitorsByGender;
    }

    @JsonProperty("age")
    public VisitorsByProperty getVisitorsByAge() {
        return visitorsByAge;
    }

    @JsonProperty("sex_age")
    public VisitorsByProperty getVisitorsByAgeGender() {
        return visitorsByAgeGender;
    }

    @JsonProperty("cities")
    public VisitorsByPCities getVisitorsByPCities() {
        return visitorsByPCities;
    }

    @JsonProperty("countries")
    public VisitorsByPCountries getVisitorsByPCountries() {
        return visitorsByPCountries;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result + reach;
		result = prime * result + reachSubscribers;
		result = prime * result + views;
		result = prime * result + visitors;
		result = prime * result
				+ ((visitorsByAge == null) ? 0 : visitorsByAge.hashCode());
		result = prime
				* result
				+ ((visitorsByAgeGender == null) ? 0 : visitorsByAgeGender
						.hashCode());
		result = prime
				* result
				+ ((visitorsByGender == null) ? 0 : visitorsByGender.hashCode());
		result = prime
				* result
				+ ((visitorsByPCities == null) ? 0 : visitorsByPCities
						.hashCode());
		result = prime
				* result
				+ ((visitorsByPCountries == null) ? 0 : visitorsByPCountries
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stats other = (Stats) obj;
		if (day == null) {
			if (other.day != null)
				return false;
		} else if (!day.equals(other.day))
			return false;
		if (reach != other.reach)
			return false;
		if (reachSubscribers != other.reachSubscribers)
			return false;
		if (views != other.views)
			return false;
		if (visitors != other.visitors)
			return false;
		if (visitorsByAge == null) {
			if (other.visitorsByAge != null)
				return false;
		} else if (!visitorsByAge.equals(other.visitorsByAge))
			return false;
		if (visitorsByAgeGender == null) {
			if (other.visitorsByAgeGender != null)
				return false;
		} else if (!visitorsByAgeGender.equals(other.visitorsByAgeGender))
			return false;
		if (visitorsByGender == null) {
			if (other.visitorsByGender != null)
				return false;
		} else if (!visitorsByGender.equals(other.visitorsByGender))
			return false;
		if (visitorsByPCities == null) {
			if (other.visitorsByPCities != null)
				return false;
		} else if (!visitorsByPCities.equals(other.visitorsByPCities))
			return false;
		if (visitorsByPCountries == null) {
			if (other.visitorsByPCountries != null)
				return false;
		} else if (!visitorsByPCountries.equals(other.visitorsByPCountries))
			return false;
		return true;
	}
}
