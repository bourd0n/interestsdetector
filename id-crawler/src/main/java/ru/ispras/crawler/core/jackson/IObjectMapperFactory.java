package ru.ispras.crawler.core.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Factory for creating Jackson object mappers
 *
 * Created with IntelliJ IDEA.
 * User: andrey
 * Date: 10/31/14
 * Time: 12:15 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IObjectMapperFactory {
    public ObjectMapper create();
}
