package ru.ispras.crawler.core.connection.zvents;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.zvents.ZventsRequest;
import ru.ispras.crawler.core.datamodel.zvents.IZventsResponse;

/**
 * Basic class for connections, dealing with Zvents.
 * 
 * @author sysoev
 */
public abstract class ZventsConnection<Req extends ZventsRequest, Resp extends IZventsResponse> implements
		IConnection<Req, Resp> {
	
	private final IConnection<? super HttpRequest, ? extends HttpResponse> httpConnection;

	public ZventsConnection(IConnection<? super HttpRequest, ? extends HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}

	@Override
	public Resp getResponse(Req request) throws UnavailableConnectionException {
		final HttpRequest httpRequest = new HttpRequest(request.getURL());
		final String htmlData = httpConnection.getResponse(httpRequest).getString();
		final Document doc = Jsoup.parse(htmlData, "http://zvents.com/");
		return convertToResponse(doc, htmlData, request);
	}

	protected abstract Resp convertToResponse(Document doc, String htmlData, Req request);
}
