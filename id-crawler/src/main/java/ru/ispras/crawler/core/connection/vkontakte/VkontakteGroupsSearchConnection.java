package ru.ispras.crawler.core.connection.vkontakte;

import java.util.ArrayList;
import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteGroupsSearchRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.ResponseList;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.groups.Group;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.groups.GroupQuery;
import ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils;

/**
 * Connection for https://vk.com/dev/groups.search method.
 * Requires access token.
 * 
 * The method itself returns not full information about groups, hence the result is a list of group's ids.
 * Use https://vk.com/dev/groups.getById method to retrieve full information about groups
 * 
 * @author andrey
 *
 */
public class VkontakteGroupsSearchConnection implements IConnection<VkontakteGroupsSearchRequest, BasicResponse<List<Long>>> {

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	public VkontakteGroupsSearchConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}

	private String getMethod(VkontakteGroupsSearchRequest request) {
		GroupQuery query = request.getQuery();
		
		String method="groups.search?q="+VkontakteConnectionUtils.encodeURL(query.getQuery());
		
		if (query.getCityId() != null){
			method+="&city_id="+query.getCityId();
		}
		if (query.getCountryId() != null){
			method+="&country_id="+query.getCountryId();
		}
		if (query.getFuture() != null){
			method+="&future="+VkontakteUtils.booleanToFlag(query.getFuture());
		}
		if (query.getType() != null){
			method+="&type="+query.getType().getValue();
		}
		method+="&sort="+query.getSort().getValue();
		method+="&count=1000";
		return method;
	}

	@Override
	public BasicResponse<List<Long>> getResponse(
			VkontakteGroupsSearchRequest request)
			throws UnavailableConnectionException {
		String url = getMethod(request);
		ResponseList<Group> response = VkontakteConnectionUtils.getSingleResponseList(url, httpConnection, Group.class);
		List<Long> ids = new ArrayList<Long>();
		for (Group group: response.getItems()){
			ids.add(group.getId());
		}
		return new BasicResponse<List<Long>>(ids);
	}	
}
