/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.twitter.entities;

import java.util.Date;
import java.util.List;

import ru.ispras.crawler.core.datamodel.twitter.utils.TwitterDateConverter;
import ru.ispras.crawler.core.datamodel.utils.DateConverter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A data class representing one single status of a user.
 *
 * @author Yusuke Yamamoto - yusuke at mac.com, andrey g
 */
public class Status {
    private final Date createdAt;
    private final long id;
    private final String text;
    private final String source;
    private final boolean isTruncated;
    private final long inReplyToStatusId;
    private final long inReplyToUserId;
    private final boolean isFavorited;
    private final boolean isRetweeted;
    private final int favoritesCount;
    private final String inReplyToScreenName;
    private final PointGeoLocation geoLocation;
    private final Place place;
    private final long retweetCount;
    private final boolean isPossiblySensitive;
    private final String lang;
    private final List<Long> contributorsIDs;
    private final Status retweetedStatus;
    private final StatusEntities entities;
    private final User user;
    
    private final static DateConverter dateConverter = new TwitterDateConverter();
   
    @JsonCreator
    public Status(
    			@JsonProperty("created_at") String createdAt,
    			@JsonProperty("id") long id,
    			@JsonProperty("text") String text,
    			@JsonProperty("source") String source,
    			@JsonProperty("truncated") boolean isTruncated,
    			@JsonProperty("in_reply_to_status_id") long inReplyToStatusId,
    			@JsonProperty("in_reply_to_user_id") long inReplyToUserId,
    			@JsonProperty("favorited") boolean isFavorited,
    			@JsonProperty("retweeted") boolean isRetweeted,
    			@JsonProperty("favourites_count") int favoritesCount,
    			@JsonProperty("in_reply_to_screen_name") String inReplyToScreenName,
    			@JsonProperty("coordinates") PointGeoLocation geoLocation,
    			@JsonProperty("place") Place place,
    			@JsonProperty("retweet_count") long retweetCount,
    			@JsonProperty("possibly_sensitive") boolean isPossiblySensitive,
    			@JsonProperty("lang") String lang,
    			@JsonProperty("contributors") List<Long> contributorsIDs,
    			@JsonProperty("retweeted_status") Status retweetedStatus,
    			@JsonProperty("entities") StatusEntities entities,
    			@JsonProperty("user") User user) {
		this.createdAt = dateConverter.getDate(createdAt);
		this.id = id;
		this.text = text;
		this.source = source;
		this.isTruncated = isTruncated;
		this.inReplyToStatusId = inReplyToStatusId;
		this.inReplyToUserId = inReplyToUserId;
		this.isFavorited = isFavorited;
		this.isRetweeted = isRetweeted;
		this.favoritesCount = favoritesCount;
		this.inReplyToScreenName = inReplyToScreenName;
		this.geoLocation = geoLocation;
		this.place = place;
		this.retweetCount = retweetCount;
		this.isPossiblySensitive = isPossiblySensitive;
		this.lang = lang;
		this.contributorsIDs = contributorsIDs;
		this.retweetedStatus = retweetedStatus;
		this.entities = entities;
		this.user = user;
	}

    @JsonIgnore
    public Date getCreatedAt() {
		return createdAt;
	}
    
    @JsonProperty("created_at")
    public String getCreatedAtString() {
		return dateConverter.getString(createdAt);
	}
    
    @JsonProperty("id")
	public long getId() {
		return id;
	}
	
    @JsonProperty("text")
	public String getText() {
		return text;
	}

    @JsonProperty("source")
	public String getSource() {
		return source;
	}
    
    @JsonProperty("truncated")
	public boolean isTruncated() {
		return isTruncated;
	}
    
    @JsonProperty("in_reply_to_status_id")
	public long getInReplyToStatusId() {
		return inReplyToStatusId;
	}

    @JsonProperty("in_reply_to_user_id")
	public long getInReplyToUserId() {
		return inReplyToUserId;
	}
    
    @JsonProperty("favorited")
	public boolean isFavorited() {
		return isFavorited;
	}

    @JsonProperty("retweeted")
	public boolean isRetweeted() {
		return isRetweeted;
	}

    @JsonProperty("favourites_count")
	public int getFavoritesCount() {
		return favoritesCount;
	}

    @JsonProperty("in_reply_to_screen_name")
	public String getInReplyToScreenName() {
		return inReplyToScreenName;
	}

    @JsonProperty("coordinates")
	public PointGeoLocation getGeoLocation() {
		return geoLocation;
	}

    @JsonProperty("place")
	public Place getPlace() {
		return place;
	}

    @JsonProperty("retweet_count")
	public long getRetweetCount() {
		return retweetCount;
	}

    @JsonProperty("possibly_sensitive")
	public boolean isPossiblySensitive() {
		return isPossiblySensitive;
	}

    @JsonProperty("lang")
	public String getLang() {
		return lang;
	}

    @JsonProperty("contributors")
	public List<Long> getContributorsIDs() {
		return contributorsIDs;
	}

    @JsonProperty("retweeted_status")
	public Status getRetweetedStatus() {
		return retweetedStatus;
	}

    @JsonProperty("entities")
	public StatusEntities getEntities() {
		return entities;
	}

    @JsonProperty("user")
	public User getUser() {
		return user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Status other = (Status) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
