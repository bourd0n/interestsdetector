package ru.ispras.crawler.core.connection.vkontakte.http;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import org.apache.http.client.methods.HttpUriRequest;

import ru.ispras.crawler.core.connection.exception.PermanentlyUnavailableConnectionException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpResponseConverter;
import ru.ispras.crawler.core.connection.http.ext.IHttpResponseConverter;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This converter analyzes response and throws UnavailableConnectionException in the following cases:
 * 1) wrapped connection throws UnavailableConnectionException (i.e. some low-level problems)
 * 2) error in JSON parsing
 * 3) JSON object from response contains the "error" field
 * 
 * @author andrey
 *
 */
public class VkontakteHttpResponseConverter implements IHttpResponseConverter {

	public VkontakteHttpResponseConverter(
			IHttpResponseConverter wrappedConverter) {
		this.wrappedConverter = wrappedConverter;
	}
	
	public VkontakteHttpResponseConverter() {
		this(new DefaultHttpResponseConverter());
	}
	
	private final IHttpResponseConverter wrappedConverter;
	

	/**
	 * https://vk.com/dev/errors
	 * 
	 * 1 - unknown, 6 - too much requests per second, 9 - to much of the one type, 10 - internal server error
	 */
	private final static Collection<Integer> nonPermanentErrorCodes = Arrays.asList(1, 6, 9, 10); 
	private final static long TIMEOUT = 1 * 60 * 60 * 1000;//FIXME 1 hour. Random magic number 
	
	private final static ObjectMapper mapper = new ObjectMapper();

	@Override
	public HttpResponse convert(HttpUriRequest request,
			org.apache.http.HttpResponse response)
			throws UnavailableConnectionException {
		HttpResponse httpResponse = wrappedConverter.convert(request, response);
		analyze(request, httpResponse);
		return httpResponse;
	}

	private void analyze(HttpUriRequest request, HttpResponse httpResponse)
			throws UnavailableConnectionException {
		String response = httpResponse.getString();
		try {
			JsonNode json = mapper.readTree(response);
			JsonNode error = json.get("error");
			//response has an error field => throw exception
			if (error != null) {
				Integer code = Integer.parseInt(json.get("error").get("error_code").toString());
				String errorMessage = String.format("Failed to process %s. Message: %s", request,
						error.get("error_msg").toString());
				
				if (nonPermanentErrorCodes.contains(code)){
					throw new UnavailableConnectionException(errorMessage, System.currentTimeMillis() + TIMEOUT);
				}
				throw new PermanentlyUnavailableConnectionException(errorMessage);
			}

		} catch (IOException e) {
			throw new PermanentlyUnavailableConnectionException(String.format(
					"Failed to process %s. %s", request, "JSON parsing error"), e);
		}
	}
}
