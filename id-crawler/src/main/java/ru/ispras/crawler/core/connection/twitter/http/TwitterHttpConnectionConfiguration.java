package ru.ispras.crawler.core.connection.twitter.http;

import javax.xml.bind.annotation.XmlRootElement;

import ru.ispras.crawler.core.connection.http.auth.oauth.OAuthHttpConnectionConfiguration;

@XmlRootElement(name = "twitter")
public class TwitterHttpConnectionConfiguration extends OAuthHttpConnectionConfiguration {
	public TwitterHttpConnectionConfiguration() {
	}

	public TwitterHttpConnectionConfiguration(String consumerKey, String consumerSecret, String accessToken,
			String tokenSecret) {
		super(consumerKey, consumerSecret, accessToken, tokenSecret);
	}
}
