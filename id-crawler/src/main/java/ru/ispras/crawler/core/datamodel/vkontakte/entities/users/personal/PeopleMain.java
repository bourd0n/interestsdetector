package ru.ispras.crawler.core.datamodel.vkontakte.entities.users.personal;

/**
 * Created by padre on 14.07.14
 */

/**
 * see description in  https://vk.com/dev/fields_2   people_main
 */
public enum PeopleMain {
    INTELLECT, HONESTY, HEALTH, POWER, COURAGE, HUMOUR, UNKNOWN;

    public static PeopleMain intToPeopleMain(Integer number) {
        if (number == null) return UNKNOWN;
        switch (number){
            case 1: return INTELLECT;
            case 2: return HONESTY;
            case 3: return HEALTH;
            case 4: return POWER;
            case 5: return COURAGE;
            case 6: return HUMOUR;
            default: return UNKNOWN;
        }
    }

    public static int peopleMainToInt(PeopleMain type) {
        switch (type) {
            case INTELLECT: return 1;
            case HONESTY: return 2;
            case HEALTH: return 3;
            case POWER: return 4;
            case COURAGE: return 5;
            case HUMOUR: return 6;
            default: return 0;
        }
    }
}
