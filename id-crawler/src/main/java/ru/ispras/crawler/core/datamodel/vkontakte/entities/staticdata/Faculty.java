package ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 10.06.14
 * Time: 18:24
 */
public class Faculty {
    private final long id;
    private final String title;
    private final Long universityId;

    @JsonCreator
    public Faculty(
            @JsonProperty("id") long id,
            @JsonProperty("title") String title,
            @JsonProperty("university_id") Long universityId) {
        this.id = id;
        this.title = title;
        this.universityId = universityId;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("university_id")
    public Long getUniversityId() {
        return universityId;
    }
}
