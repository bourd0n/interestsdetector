package ru.ispras.crawler.core.datamodel;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * {@link IResponse} that contains one object inside
 * @param <T>
 */
public class BasicResponse<T> implements IResponse {
	private final T object;

	@JsonCreator
	public BasicResponse(@JsonProperty("object") T object) {
		this.object = object;
	}

	@JsonProperty("object")
	public T getObject() {
		return object;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((object == null) ? 0 : object.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		@SuppressWarnings("unchecked")
		BasicResponse<T> other = (BasicResponse<T>) obj;
		if (object == null) {
			return other.object == null;
		}
		return object.equals(other.object);
	}
}
