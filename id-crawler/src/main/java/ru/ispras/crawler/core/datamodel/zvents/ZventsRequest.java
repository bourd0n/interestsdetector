package ru.ispras.crawler.core.datamodel.zvents;

import java.net.URL;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Basic class for Zvents-related requests. Contains URL of the requested page. 
 * 
 * @author sysoev
 */
public abstract class ZventsRequest implements IRequest {
	
	private URL url;
	
	public ZventsRequest(URL url) {
		this.url = url;
	}
	
	public URL getURL() {
		return url;
	}
	
	@Override
	public String toString() {
		return url.toString();
	}
}
