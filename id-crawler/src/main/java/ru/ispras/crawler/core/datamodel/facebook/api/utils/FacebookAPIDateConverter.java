package ru.ispras.crawler.core.datamodel.facebook.api.utils;

import java.text.SimpleDateFormat;

import ru.ispras.crawler.core.datamodel.utils.DateConverter;

/**
 * {@link DateConverter} subclass specifying {@link SimpleDateFormat} used by
 * Facebook API.
 * 
 * @author Ivan Andrianov
 * 
 */
public class FacebookAPIDateConverter extends DateConverter {
	public FacebookAPIDateConverter() {
		super(new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ"));
									 
	}
}
