package ru.ispras.crawler.core.saver.mongodb;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.util.JSON;

/**
 * Utility class for using {@link ObjectMapper} and {@link JSON} functionality
 * in one call.
 * 
 * @author Ivan Andrianov
 */
public class JacksonMongoDBUtils {
    private final ObjectMapper objectMapper;

    public JacksonMongoDBUtils(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }


    public Object toMongoDB(Object object) {
		try {
			return JSON.parse(objectMapper.writeValueAsString(object));
		} catch (JsonProcessingException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public <T> T fromMongoDB(Object object, TypeReference<T> typeRef) {
		try {
			return objectMapper.readValue(JSON.serialize(object), typeRef);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public <T> T fromMongoDB(Object object, Class<T> type) {
		try {
			return objectMapper.readValue(JSON.serialize(object), type);
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}
}
