package ru.ispras.crawler.core.connection.exception;

import ru.ispras.crawler.core.connection.IConnection;

/**
 * Exception thrown when {@link IConnection} can not be created.
 * 
 * @author Ivan Andrianov
 * 
 */
public class ConnectionCreationException extends Exception {
	private static final long serialVersionUID = 1904768904474768842L;

	public ConnectionCreationException() {
	}

	public ConnectionCreationException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConnectionCreationException(String message) {
		super(message);
	}

	public ConnectionCreationException(Throwable cause) {
		super(cause);
	}
}
