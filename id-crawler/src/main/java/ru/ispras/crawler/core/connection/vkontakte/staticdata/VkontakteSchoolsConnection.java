package ru.ispras.crawler.core.connection.vkontakte.staticdata;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 28.05.14
 * Time: 15:39
 */

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteSchoolsRequest;

/**
 * see description in https://vk.com/dev/database.getSchools
 */
public class VkontakteSchoolsConnection  extends AbstractVkontakteIdTitleConnection<VkontakteSchoolsRequest> {
    public VkontakteSchoolsConnection(IConnection<HttpRequest, HttpResponse> httpConnection) {
        super(httpConnection);
    }

    @Override
    protected String getMethod(VkontakteSchoolsRequest request) {
        return "database.getSchools?city_id=" + request.getCityId();
    }
}
