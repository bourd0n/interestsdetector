/*
 * Copyright 2012 Ryuji Yamashita
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.facebook.api.entities;

import java.util.Date;
import java.util.List;

import ru.ispras.crawler.core.datamodel.facebook.api.utils.FacebookAPIDateConverter;
import ru.ispras.crawler.core.datamodel.utils.DateConverter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author Ryuji Yamashita - roundrop at gmail.com
 */
public class Video{
    private final String id;
    private final Category from;
    private final ResponseList<Tag> tags;
    private final String name;
    private final String description;
    private final String picture;
    private final String embedHtml;
    private final List<Video.Format> format;
    private final String icon;
    private final String source;
    private final Date createdTime;
    private final Date updatedTime;
    private final ResponseList<Comment> comments;
    private final String link;

    private final static DateConverter dateConverter = new FacebookAPIDateConverter();
    
    @JsonCreator
    public Video(
    		@JsonProperty("id") String id, 
    		@JsonProperty("from") Category from,
    		@JsonProperty("tags") ResponseList<Tag> tags,
    		@JsonProperty("name") String name,
    		@JsonProperty("description") String description,
    		@JsonProperty("picture") String picture,
    		@JsonProperty("embed_html") String embedHtml,
    		@JsonProperty("format") List<Format> format,
    		@JsonProperty("icon") String icon,
    		@JsonProperty("source") String source,
    		@JsonProperty("created_time") String createdTime,
    		@JsonProperty("updated_time") String updatedTime,
    		@JsonProperty("comments") ResponseList<Comment> comments,
    		@JsonProperty("link") String link) {
		this.id = id;
		this.from = from;
		this.tags = tags;
		this.name = name;
		this.description = description;
		this.picture = picture;
		this.embedHtml = embedHtml;
		this.format = format;
		this.icon = icon;
		this.source = source;
		this.createdTime = dateConverter.getDate(createdTime);
		this.updatedTime = dateConverter.getDate(updatedTime);
		this.comments = comments;
		this.link = link;
	}

    @JsonProperty("id")
	public String getId() {
        return id;
    }

    @JsonProperty("from")
    public Category getFrom() {
        return from;
    }

    @JsonProperty("tags")
    public ResponseList<Tag> getTags() {
        return tags;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("picture")
    public String getPicture() {
        return picture;
    }

    @JsonProperty("embed_html")
    public String getEmbedHtml() {
        return embedHtml;
    }
    @JsonProperty("format")
    public List<Format> getFormat() {
        return format;
    }

    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    @JsonProperty("source")
    public String getSource() {
        return source;
    }
    
    @JsonProperty("created_time")
    public String getCreatedTimeString() {
        return dateConverter.getString(createdTime);
    }
    
    @JsonIgnore
    public Date getCreatedTime() {
        return createdTime;
    }

    @JsonProperty("updated_time")
    public String getUpdatedTimeString() {
        return dateConverter.getString(updatedTime);
    }
    
    @JsonIgnore
    public Date getUpdatedTime() {
        return updatedTime;
    }

    @JsonProperty("comments")
    public ResponseList<Comment> getComments() {
        return comments;
    }

    @JsonProperty("link")
    public String getLink() {
        return link;
    }

    public static class Format{
        private final String embedHtml;
        private final String filter;
        private final Integer height;
        private final Integer width;
        private final String picture;

        @JsonCreator
        public Format(
        		@JsonProperty("embed_html") String embedHtml,
        		@JsonProperty("filter") String filter,
        		@JsonProperty("height") Integer height,
        		@JsonProperty("width") Integer width,
        		@JsonProperty("picture") String picture) {
			this.embedHtml = embedHtml;
			this.filter = filter;
			this.height = height;
			this.width = width;
			this.picture = picture;
		}

        @JsonProperty("embed_html")
		public String getEmbedHtml() {
            return embedHtml;
        }
        
        @JsonProperty("filter")
        public String getFilter() {
            return filter;
        }
        
        @JsonProperty("height")
        public Integer getHeight() {
            return height;
        }
        
        @JsonProperty("width")
        public Integer getWidth() {
            return width;
        }
        
        @JsonProperty("picture")
        public String getPicture() {
            return picture;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Format)) return false;

            Format that = (Format) o;

            if (embedHtml != null ? !embedHtml.equals(that.embedHtml) : that.embedHtml != null) return false;
            if (filter != null ? !filter.equals(that.filter) : that.filter != null) return false;
            if (height != null ? !height.equals(that.height) : that.height != null) return false;
            if (picture != null ? !picture.equals(that.picture) : that.picture != null) return false;
            if (width != null ? !width.equals(that.width) : that.width != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = embedHtml != null ? embedHtml.hashCode() : 0;
            result = 31 * result + (filter != null ? filter.hashCode() : 0);
            result = 31 * result + (height != null ? height.hashCode() : 0);
            result = 31 * result + (width != null ? width.hashCode() : 0);
            result = 31 * result + (picture != null ? picture.hashCode() : 0);
            return result;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Video other = (Video) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
}
