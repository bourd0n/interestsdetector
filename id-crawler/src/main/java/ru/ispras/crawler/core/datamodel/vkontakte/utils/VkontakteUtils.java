package ru.ispras.crawler.core.datamodel.vkontakte.utils;

import ru.ispras.crawler.core.datamodel.vkontakte.entities.users.Gender;
/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 15.05.14
 * Time: 19:14
 */
public class VkontakteUtils {
    public static Boolean flagToBoolean(Integer flag) {
    	if (flag == null){
    		return null;
    	}
        return flag == 1;
    }

    public static Integer booleanToFlag(Boolean value) {
    	if (value == null){
    		return null;
    	}
        if (value) return 1;
        else return 0;
    }
    
    public static Gender intToGender(Integer gender){
    	if (gender == null){
    		return null;
    	}
        if (gender == 1)
            return Gender.FEMALE;
        else if (gender == 2)
            return Gender.MALE;
        else
            return Gender.UNKNOWN;
    }

    public static Integer genderToInt(Gender gender) {
    	if (gender == null) {
    		return null;
    	}
        if (gender == Gender.MALE) return 2;
        if (gender == Gender.FEMALE) return 1;
        return 0;
    }
}
