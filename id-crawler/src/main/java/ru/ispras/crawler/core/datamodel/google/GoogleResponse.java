package ru.ispras.crawler.core.datamodel.google;

import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * {@link IResponse} from google.com that contains number of results returned for given request
 */
public class GoogleResponse implements IResponse {
	private final long numberOfResults;

	public GoogleResponse(long numberOfResults) {
		this.numberOfResults = numberOfResults;
	}

	public long getNumberOfResults() {
		return numberOfResults;
	}
}
