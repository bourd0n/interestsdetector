package ru.ispras.crawler.core.datamodel.facebook.api;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} to be processed using Facebook API (https://graph.facebook.com/...)
 */
public class FacebookAPIRequest implements IRequest {
	private long id;
	private String parameters;
	private String path;
	
	public FacebookAPIRequest(long id, String path, String parameters) {
		this.id = id;
		this.parameters = parameters;
		this.path = path;
	}
	
	public long getId() {
		return id;
	}
	public String getParameters() {
		return parameters;
	}
	public String getPath() {
		return path;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + ":[id=" + id + "]";
	}
}
