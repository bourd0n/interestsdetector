package ru.ispras.crawler.core.connection.zvents;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.zvents.EventZventsRequest;
import ru.ispras.crawler.core.datamodel.zvents.EventZventsResponse;
import ru.ispras.crawler.core.datamodel.zvents.EventZventsResponse.Review;

/**
 * Connection to download and process Zvents event page.
 * 
 * @author sysoev
 */
public class EventZventsConnection extends ZventsConnection<EventZventsRequest, EventZventsResponse> {

	public EventZventsConnection(IConnection<? super HttpRequest, ? extends HttpResponse> httpConnection) {
		super(httpConnection);
	}

	@Override
	protected EventZventsResponse convertToResponse(
			Document doc, String html, EventZventsRequest request) {
		final Element descriptionColumn = doc.getElementsByClass("description_column").first();
		final String name = descriptionColumn.getElementsByClass("name").first().text();
		final String description = descriptionColumn.getElementsByClass("description").first().text();
		
		final Element dateElement = descriptionColumn.getElementsByClass("dtstart").first();
		final String time = dateElement.text();
		final String date = dateElement.attr("title");
		final Calendar calendar = parseCalendar(date);
		
		final Collection<Review> reviews = new ArrayList<>();
		
		for (Element reviewTitle : doc.getElementsByClass("review_headline")) {
			final String title = reviewTitle.text();
			final String author = reviewTitle.previousElementSibling().text();
			final String text = reviewTitle.nextElementSibling().text();
			reviews.add(new Review(title, text, author));
		}

		return new EventZventsResponse(request.getEventId(), request.getLocationId(), request.getURL(),
				name, time + "\n" + description, calendar, reviews);
	}

	private GregorianCalendar parseCalendar(String date) {
		try {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(date).toGregorianCalendar();
		} catch (DatatypeConfigurationException e) {
			throw new RuntimeException(e);
		}
	}
}
