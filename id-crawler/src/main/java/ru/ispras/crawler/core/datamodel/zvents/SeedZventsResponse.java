package ru.ispras.crawler.core.datamodel.zvents;

import java.net.URL;

/**
 * Response, representing information from Zvents seed page. Contains link 
 * to search results page for given seed.
 * 
 * @author sysoev
 */
public class SeedZventsResponse implements IZventsResponse {

	private final URL moreEventsLink;
	
	public SeedZventsResponse(final URL url) {
		super();
		this.moreEventsLink = url;
	}

	public URL getMoreEventsLink() {
		return moreEventsLink;
	}
	
	public boolean hasMoreEventsLink() {
		return moreEventsLink != null;
	}
}
