package ru.ispras.crawler.core.connection.http.ext;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.protocol.HTTP;

import ru.ispras.crawler.core.connection.exception.PermanentlyUnavailableConnectionException;
import ru.ispras.crawler.core.connection.exception.TargetUnreachableException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * {@link IHttpResponseConverter} implementation which treats any status code >=
 * 300 except 403 as a {@link TargetUnreachableException}.
 * <p>
 * 403 Forbidden is treated as {@link PermanentlyUnavailableConnectionException}.
 * <p>
 * Note that this class reads the whole {@link HttpEntity#getContent()} into
 * byte array. Therefore it is not suited for huge downloads and streaming.
 * 
 * @author Ivan Andrianov
 */
public class DefaultHttpResponseConverter implements IHttpResponseConverter {
	private static final int FORBIDDEN = 403;

	@Override
	public HttpResponse convert(HttpUriRequest request, org.apache.http.HttpResponse response)
			throws UnavailableConnectionException {
		HttpEntity entity = checkEntityExists(request.getURI(), response);
		checkFailedStatus(request.getURI(), response.getStatusLine());
		byte[] bytes = getBytes(request.getURI(), entity);
		Charset charset = getCharset(entity);
		return new HttpResponse(bytes, charset);
	}

	private HttpEntity checkEntityExists(URI uri, org.apache.http.HttpResponse response) {
		HttpEntity entity = response.getEntity();
		if (entity == null) {
			throw new TargetUnreachableException(String.format("Failed to process %s: no entity.", uri));
		}
		return entity;
	}

	private void checkFailedStatus(URI uri, StatusLine statusLine) throws UnavailableConnectionException {
		int statusCode = statusLine.getStatusCode();
		if (statusCode >= 300) {
			String errorMessage = String.format("Failed to process %s: %d %s.", uri, statusCode,
					statusLine.getReasonPhrase());
			if (statusCode == FORBIDDEN) {
				throw new PermanentlyUnavailableConnectionException(errorMessage);
			}
			throw new TargetUnreachableException(errorMessage);
		}
	}

	private byte[] getBytes(URI uri, HttpEntity entity) {
		try {
			InputStream in = entity.getContent();
			return IOUtils.toByteArray(in);
		} catch (IOException e) {
			throw new TargetUnreachableException(String.format("Failed to process %s.", uri), e);
		}
	}

	private Charset getCharset(HttpEntity entity) {
		Charset charset = null;
		try {
			ContentType contentType = ContentType.getOrDefault(entity);
			charset = contentType.getCharset();
		} catch (UnsupportedCharsetException ignore) {
		}

		if (charset == null) {
			charset = HTTP.DEF_CONTENT_CHARSET;
		}
		return charset;
	}
}
