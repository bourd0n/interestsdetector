package ru.ispras.crawler.core.datamodel.vkontakte;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.groups.GroupQuery;

/**
 * Request for https://vk.com/dev/groups.search method
 * 
 * @author andrey
 *
 */
public class VkontakteGroupsSearchRequest implements IRequest {
	private final GroupQuery query;

	public VkontakteGroupsSearchRequest(GroupQuery query) {
		super();
		this.query = new GroupQuery(query);
	}

	public GroupQuery getQuery() {
		return query;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((query == null) ? 0 : query.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VkontakteGroupsSearchRequest other = (VkontakteGroupsSearchRequest) obj;
		if (query == null) {
			if (other.query != null)
				return false;
		} else if (!query.equals(other.query))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VkontakteGroupsSearchRequest [query=" + query + "]";
	}
}
