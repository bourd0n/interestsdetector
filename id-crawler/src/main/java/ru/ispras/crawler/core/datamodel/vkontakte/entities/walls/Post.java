package ru.ispras.crawler.core.datamodel.vkontakte.entities.walls;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 15.05.14
 * Time: 18:56
 */


import java.util.List;

import ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments.Attachment;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * description of the field in https://vk.com/dev/post
 */
public class Post {
	public static class Comments {
	    private final int count;
	    private final int canPost;

	    @JsonCreator
	    public Comments(
	            @JsonProperty("count") int count,
	            @JsonProperty("can_post") int canPost) {
	        this.count = count;
	        this.canPost = canPost;
	    }

		@JsonProperty("count")
		public int getCount() {
			return count;
		}

		@JsonProperty("can_post")
		public int getCanPost() {
			return canPost;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + count;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Comments other = (Comments) obj;
			if (count != other.count)
				return false;
			return true;
		}
	}

    private final long id;
    private final Long ownerId;
    private final Long fromId;
    private final Long date;
    private final String text;
    private final Long replayOunerId;
    private final Long replyPostId;
    private final Long friendsOnly;
    private final Comments comments;
    private final Likes likes;
    private final Reposts reposts;
    private final String postType;
    private final Source postSource;
    private final List<Attachment> attachments;
    private final Geo geo;
    private final Long signerId;
    private final List<Post> copyHistory;

    @JsonCreator
    public Post(
            @JsonProperty("id") long id,
            @JsonProperty("owner_id") Long ownerId,
            @JsonProperty("from_id") Long fromId,
            @JsonProperty("date") Long date,
            @JsonProperty("text") String text,
            @JsonProperty("reply_owner_id") Long replayOunerId,
            @JsonProperty("reply_post_id") Long replyPostId,
            @JsonProperty("friends_only") Long friendsOnly,
            @JsonProperty("comments") Comments comments,
            @JsonProperty("likes") Likes likes,
            @JsonProperty("reposts") Reposts reposts,
            @JsonProperty("post_type") String postType,
            @JsonProperty("post_source") Source postSource,
            @JsonProperty("attachments") List<Attachment> attachments,
            @JsonProperty("geo") Geo geo,
            @JsonProperty("signer_id") Long signerId,
            @JsonProperty("copy_history") List<Post> copyHistory) {
        this.id = id;
        this.ownerId = ownerId;
        this.fromId = fromId;
        this.date = date;
        this.text = text;
        this.replayOunerId = replayOunerId;
        this.replyPostId = replyPostId;
        this.friendsOnly = friendsOnly;
        this.comments = comments;
        this.likes = likes;
        this.reposts = reposts;
        this.postType = postType;
        this.postSource = postSource;
        this.attachments = attachments;
        this.geo = geo;
        this.signerId = signerId;
        this.copyHistory = copyHistory;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("owner_id")
    public Long getOwnerId() {
        return ownerId;
    }

    @JsonProperty("from_id")
    public Long getFromId() {
        return fromId;
    }

    @JsonProperty("date")
    public Long getDate() {
        return date;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("reply_owner_id")
    public Long getReplayOunerId() {
        return replayOunerId;
    }

    @JsonProperty("reply_post_id")
    public Long getReplyPostId() {
        return replyPostId;
    }

    @JsonProperty("friends_only")
    public Long getFriendsOnly() {
        return friendsOnly;
    }

    @JsonProperty("comments")
    public Comments getComments() {
        return comments;
    }

    @JsonProperty("likes")
    public Likes getLikes() {
        return likes;
    }

    @JsonProperty("reposts")
    public Reposts getReposts() {
        return reposts;
    }

    @JsonProperty("post_type")
    public String getPostType() {
        return postType;
    }

    @JsonProperty("post_source")
    public Source getPostSource() {
        return postSource;
    }

    @JsonProperty("attachments")
    public List<Attachment> getAttachments() {
        return attachments;
    }

    @JsonProperty("geo")
    public Geo getGeo() {
        return geo;
    }

    @JsonProperty("signer_id")
    public Long getSignerId() {
        return signerId;
    }

    @JsonProperty("copy_history")
    public List<Post> getCopyHistory() {
        return copyHistory;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (fromId ^ (fromId >>> 32));
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + (int) (ownerId ^ (ownerId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Post other = (Post) obj;
		if (fromId != other.fromId)
			return false;
		if (id != other.id)
			return false;
		if (ownerId != other.ownerId)
			return false;
		return true;
	}
}
