package ru.ispras.crawler.core.requestidconverter.vkontakte;

import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteFriendsRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class VkontakteFriendsRequestIdConverter implements
		IRequestIdConverter<VkontakteFriendsRequest, Long> {

	@Override
	public Long getId(VkontakteFriendsRequest request) {
		return request.getUserId();
	}

	@Override
	public VkontakteFriendsRequest getRequest(Long id) {
		return new VkontakteFriendsRequest(id);
	}

}
