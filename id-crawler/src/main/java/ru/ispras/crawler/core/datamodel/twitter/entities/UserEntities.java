package ru.ispras.crawler.core.datamodel.twitter.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class represents entities that appear in Status  
 * @author andrey g
 */
public class UserEntities {
    private final StatusEntities description;
    private final StatusEntities url;
    
    @JsonCreator
	public UserEntities(
			@JsonProperty("url") StatusEntities url,
			@JsonProperty("description") StatusEntities description) {
		this.description = description;
		this.url = url;
	}    

	@JsonProperty("url")
	public StatusEntities getUrl() {
		return url;
	}
	
	@JsonProperty("description")
	public StatusEntities getDescription() {
		return description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserEntities other = (UserEntities) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}    
}
