package ru.ispras.crawler.core.connection.exception;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Thrown when {@link IRequest} cannot be done due to target issue.
 * 
 * @author Alexander
 * 
 */
public class TargetUnreachableException extends RuntimeException {

	private static final long serialVersionUID = 7572147297559483296L;

	public TargetUnreachableException() {
	}

	public TargetUnreachableException(String message) {
		super(message);
	}

	public TargetUnreachableException(Throwable cause) {
		super(cause);
	}

	public TargetUnreachableException(String message, Throwable cause) {
		super(message, cause);
	}
}
