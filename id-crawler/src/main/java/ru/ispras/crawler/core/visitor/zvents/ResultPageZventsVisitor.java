package ru.ispras.crawler.core.visitor.zvents;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.zvents.EventZventsRequest;
import ru.ispras.crawler.core.datamodel.zvents.ResultPageZventsRequest;
import ru.ispras.crawler.core.datamodel.zvents.ResultPageZventsResponse;
import ru.ispras.crawler.core.datamodel.zvents.ResultPageZventsResponse.EventUrlId;
import ru.ispras.crawler.core.datamodel.zvents.ResultPageZventsResponse.VenueUrlId;
import ru.ispras.crawler.core.datamodel.zvents.VenueZventsRequest;
import ru.ispras.crawler.core.datamodel.zvents.ZventsRequest;
import ru.ispras.crawler.core.visitor.AbstractNonFailingVisitor;

/**
 * Visitor, which processes search result pages and extracts the following continuation
 * requests:
 * <ul>
 * <li> {@link ResultPageZventsRequest} for next result page, if present; </li>
 * <li> {@link VenueZventsRequest}s for venues on current result page;</li>
 * <li> {@link EventZventsRequest}s for events on current result page.</li>
 * </ul>
 * 
 * @author sysoev
 */
public class ResultPageZventsVisitor extends
		AbstractNonFailingVisitor<ResultPageZventsRequest, ResultPageZventsResponse> {

	private static final Logger logger = Logger.getLogger(ResultPageZventsRequest.class);
	
	private Set<ZventsRequest> zventRequests = new HashSet<ZventsRequest>();
	
	@Override
	public Collection<? extends IRequest> visit(ResultPageZventsRequest request, ResultPageZventsResponse response) {
		final Collection<IRequest> result = new HashSet<>();
		if (response.hasNextPageUrl()) {
			result.add(new ResultPageZventsRequest(response.getNextPageUrl()));
		}
		for (VenueUrlId venue : response.getVenues()) {
			addRequest(result, new VenueZventsRequest(venue.getUrl(), venue.getId()));
		}
		for (EventUrlId event : response.getEvents()) {
			addRequest(result, new EventZventsRequest(event.getUrl(), event.getEventId(), event.getVenueId()));
		}
		return result;
	}

	private synchronized void addRequest(
			Collection<IRequest> requests,
			ZventsRequest request) {
		if (!zventRequests.contains(request)) {
			requests.add(request);
			zventRequests.add(request);
			logger.debug(request.toString());
		}
		else {
			logger.debug("DUPLICATED " + request.toString());
		}
	}
	
	// -------------------------------------------------------------------------------------
	
	@Override
	public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
		final Set<Class<? extends IRequest>> result = new HashSet<Class<? extends IRequest>>();
		result.add(ResultPageZventsRequest.class);
		result.add(VenueZventsRequest.class);
		result.add(EventZventsRequest.class);
		return result;
	}
}
