package ru.ispras.crawler.core.requestidconverter.hunch;

import ru.ispras.crawler.core.datamodel.hunch.HunchUserProfileRequest;

public class HunchUserProfileRequestIdConverter extends AbstractHunchUserRequestIdConverter<HunchUserProfileRequest> {
	@Override
	public HunchUserProfileRequest getRequest(String screenName) {
		return new HunchUserProfileRequest(screenName);
	}
}
