package ru.ispras.crawler.core.connection.vkontakte;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteGroupTopicsRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.groups.Topic;

/**
 * Connection for https://vk.com/dev/board.getTopics method.
 * Requires access token
 * 
 * @author andrey
 *
 */
public class VkontakteGroupTopicsConnection implements IConnection<VkontakteGroupTopicsRequest, BasicResponse<List<Topic>>> {

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	public VkontakteGroupTopicsConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}
	
	private String getMethod(VkontakteGroupTopicsRequest request) {
		String method = "board.getTopics?group_id="+request.getGroupId();
		if (request.getTopicsIds().size() > 0){
			method = method+"&topic_ids="+StringUtils.join(request.getTopicsIds(),',');
		}
		if (request.getOrder() != null){
			method = method+"&order="+request.getOrder().getValue();
		}
		return method;
	}
	
	@Override
	public BasicResponse<List<Topic>> getResponse(
			VkontakteGroupTopicsRequest request)
			throws UnavailableConnectionException {
		String url = getMethod(request);
		List<Topic> response = VkontakteConnectionUtils.getPagedResponse(url, 100, request.getLimit(), httpConnection, Topic.class);
		return new BasicResponse<List<Topic>>(response);
	}

}
