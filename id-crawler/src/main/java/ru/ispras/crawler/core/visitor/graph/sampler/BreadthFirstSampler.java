package ru.ispras.crawler.core.visitor.graph.sampler;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * {@link IGraphSampler} implementation for breadth-first search.
 * 
 * @author Alexander, andrey g
 */
public class BreadthFirstSampler<T> implements IGraphSampler<T> {

	private final Map<T, Integer> node2Depth = new HashMap<>();

	private final int maxDepth;
	private final Map<Integer, Integer> depth2Limit;

	/**
	 * @param maxDepth
	 *            - neighborhood to crawl
	 */
	public BreadthFirstSampler(int maxDepth) {
		this(maxDepth, new HashMap<Integer, Integer>());
	}

	/**
	 * 
	 * @param maxDepth
	 *            - neighborhood to crawl
	 * @param depth2Limit
	 *            - requests limit for each depth
	 */
	public BreadthFirstSampler(int maxDepth, Map<Integer, Integer> depth2Limit) {
		this.maxDepth = maxDepth;
		this.depth2Limit = depth2Limit;
	}

	@Override
	public Collection<T> getNextNodes(T node, Collection<T> neighbours) {
		int depth = getNodeDepth(node);
		if (depth == maxDepth) {
			return Collections.emptyList();
		}
		return getNextProcessingNodes(neighbours, depth);
	}

	private int getNodeDepth(T node) {
		Integer depth = node2Depth.get(node);
		if (depth == null) {
			// new node
			depth = 0;
			node2Depth.put(node, depth);
		}
		return depth;
	}

	private Collection<T> getNextProcessingNodes(Collection<T> neighbours, int depth) {
		List<T> result = new LinkedList<T>();
		Integer limit = depth2Limit.get(depth);
		int c = 0;
		for (T neighbour : neighbours) {
			if (limit != null && c >= limit) {
				break;
			}
			if (!node2Depth.containsKey(neighbour)) {
				result.add(neighbour);
				node2Depth.put(neighbour, depth + 1);
				c++;
			}
		}
		return result;
	}
}
