package ru.ispras.crawler.core.datamodel.grouping;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} which groups a number of different-typed {@link IRequest}s.
 * 
 * @author Ivan Andrianov
 * 
 */
public abstract class GroupingRequest implements IRequest {
	private final List<? extends IRequest> requests;

	public GroupingRequest(IRequest... requests) {
		this(Arrays.asList(requests));
	}

	public GroupingRequest(List<? extends IRequest> requests) {
		checkNoDuplicateTypes(requests);
		this.requests = Collections.unmodifiableList(requests);
	}

	private void checkNoDuplicateTypes(List<? extends IRequest> requests) {
		Set<Class<? extends IRequest>> requestTypes = new HashSet<>();
		for (IRequest request : requests) {
			Class<? extends IRequest> requestType = request.getClass();
			if (requestTypes.contains(requestType)) {
				throw new IllegalArgumentException("Duplicated request type: "
						+ requestType.getCanonicalName());
			}
			requestTypes.add(requestType);
		}
	}

	public List<? extends IRequest> getRequests() {
		return requests;
	}

	@SuppressWarnings("unchecked")
	public <Req extends IRequest> Req getRequest(Class<Req> clazz) {
		for (IRequest request : requests) {
			if (request.getClass() == clazz) {
				return (Req) request;
			}
		}
		return null;
	}
}
