package ru.ispras.crawler.core.connection.http;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ru.ispras.crawler.core.connection.IConnectionConfiguration;

/**
 * Entity class for storing configuration for {@link HttpConnection}
 * 
 * @author Alexander
 * 
 */
@XmlRootElement(name = "http")
public class HttpConnectionConfiguration implements IConnectionConfiguration {
	private HttpProxyDescription proxy;
	private Integer connectionTimeout;
	private Integer socketTimeout;

	public HttpConnectionConfiguration(HttpProxyDescription proxy, Integer connectionTimeout, Integer socketTimeout) {
		this.proxy = proxy;
		this.connectionTimeout = (connectionTimeout == null) || (connectionTimeout <= 0) ? 10000 : connectionTimeout;
		this.socketTimeout = (socketTimeout == null) || (socketTimeout <= 0) ? 10000 : socketTimeout;
	}

	public HttpConnectionConfiguration() {
		this(null, null, null);
	}

	public HttpConnectionConfiguration(HttpProxyDescription proxy) {
		this(proxy, null, null);
	}

	@XmlElement
	public HttpProxyDescription getProxy() {
		return proxy;
	}

	public void setProxy(HttpProxyDescription proxy) {
		this.proxy = proxy;
	}

	@XmlElement
	public Integer getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(Integer connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}

	@XmlElement
	public Integer getSocketTimeout() {
		return socketTimeout;
	}

	public void setSocketTimeout(Integer socketTimeout) {
		this.socketTimeout = socketTimeout;
	}
}
