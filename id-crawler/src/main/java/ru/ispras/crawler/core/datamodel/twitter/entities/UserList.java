/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.twitter.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A data class representing Basic list information element
 *
 * @author Dan Checkoway - dcheckoway at gmail.com, andrey g
 */
public class UserList {
	private final int id;
	private final String name;
	private final String fullName;
	private final String slug;
	private final String description;
	private final int subscriberCount;
	private final int memberCount;
	private final String uri;
	private final String mode;
	private final User user;
	private final boolean following;
    
	@JsonCreator
	public UserList(
				@JsonProperty("id") int id,
				@JsonProperty("name") String name,
				@JsonProperty("full_name") String fullName,
				@JsonProperty("slug") String slug,
				@JsonProperty("description") String description,
				@JsonProperty("subscriber_count") int subscriberCount,
				@JsonProperty("member_count") int memberCount,
				@JsonProperty("uri") String uri,
				@JsonProperty("mode") String mode,
				@JsonProperty("user") User user,
				@JsonProperty("following") boolean following) {
		this.id = id;
		this.name = name;
		this.fullName = fullName;
		this.slug = slug;
		this.description = description;
		this.subscriberCount = subscriberCount;
		this.memberCount = memberCount;
		this.uri = uri;
		this.mode = mode;
		this.user = user;
		this.following = following;
	}

	@JsonProperty("id")
	public int getId() {
		return id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("full_name")
	public String getFullName() {
		return fullName;
	}

	@JsonProperty("slug")
	public String getSlug() {
		return slug;
	}
   
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("subscriber_count")
	public int getSubscriberCount() {
		return subscriberCount;
	}

	@JsonProperty("member_count")
	public int getMemberCount() {
		return memberCount;
	}

	@JsonProperty("uri")
	public String getUri() {
		return uri;
	}

	@JsonProperty("mode")
	public String getMode() {
		return mode;
	}

	@JsonProperty("user")
	public User getUser() {
		return user;
	}

	@JsonProperty("following")
	public boolean isFollowing() {
		return following;
	}

	@Override
	public int hashCode() {
		return id;
	}
    
	@Override
	public boolean equals(Object obj) {
		if (null == obj) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		return obj instanceof UserList && ((UserList) obj).getId() == this.id;
	}
}
