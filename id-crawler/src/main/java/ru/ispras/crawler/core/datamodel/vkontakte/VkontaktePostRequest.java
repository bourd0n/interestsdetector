package ru.ispras.crawler.core.datamodel.vkontakte;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Request for a single post.
 *
 * Batch connection for https://vk.com/dev/wall.getById should be used
 *
 * @author andrey
 *
 */
public class VkontaktePostRequest implements IRequest {
	private final long ownerId;
	private final long postId;
	
	public VkontaktePostRequest(long ownerId, long postId) {
		this.ownerId = ownerId;
		this.postId = postId;
	}
	
	public long getOwnerId() {
		return ownerId;
	}
	
	public long getPostId() {
		return postId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (ownerId ^ (ownerId >>> 32));
		result = prime * result + (int) (postId ^ (postId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VkontaktePostRequest other = (VkontaktePostRequest) obj;
		if (ownerId != other.ownerId)
			return false;
		if (postId != other.postId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return ownerId+"_"+postId;
	}
	
}
