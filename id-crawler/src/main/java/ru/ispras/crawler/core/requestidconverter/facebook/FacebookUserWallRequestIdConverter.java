package ru.ispras.crawler.core.requestidconverter.facebook;

import ru.ispras.crawler.core.datamodel.facebook.FacebookUserWallRequest;

public class FacebookUserWallRequestIdConverter extends AbstractFacebookUserRequestIdConverter<FacebookUserWallRequest> {
	@Override
	public FacebookUserWallRequest getRequest(Long id) {
		return new FacebookUserWallRequest(id);
	}
}
