package ru.ispras.crawler.core.datamodel.twitter.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Symbol entity representation
 * @author andrey g
 *
 */
public class SymbolEntity {
	private final String text;
    private final List<Integer> indices;

	@JsonCreator
	public SymbolEntity(
			@JsonProperty("text") String text,
			@JsonProperty("indices") List<Integer> indices) {
		this.text = text;
		this.indices = indices;
	}
    
    @JsonProperty("text")
    public String getText() {
        return text;
    }
    
    @JsonProperty("indices")
	public List<Integer> getIndices() {
		return indices;
	}
   
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SymbolEntity that = (SymbolEntity) o;

        if (text != null ? !text.equals(that.text) : that.text != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return text != null ? text.hashCode() : 0;
    }
	
}
