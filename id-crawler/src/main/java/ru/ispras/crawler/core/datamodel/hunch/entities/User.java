package ru.ispras.crawler.core.datamodel.hunch.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
	private final String hunchId;
	private final boolean influencer;
	private final String bio;
	private final List<String> info;
	private final String website;
	private final String twitter;
	private final String facebook;
	
	@JsonCreator
	public User(
			@JsonProperty("hunchId") String hunchId,
			@JsonProperty("influencer") boolean influencer,
			@JsonProperty("bio") String bio,
			@JsonProperty("info") List<String> info,
			@JsonProperty("website") String website,
			@JsonProperty("twitter") String twitter,
			@JsonProperty("facebook") String facebook) {
		this.hunchId = hunchId;
		this.influencer = influencer;
		this.bio = bio;
		this.info = info;
		this.website = website;
		this.twitter = twitter;
		this.facebook = facebook;
	}

	@JsonProperty("hunchId")
	public String getHunchId() {
		return hunchId;
	}

	@JsonProperty("influencer")
	public boolean isInfluencer() {
		return influencer;
	}

	@JsonProperty("bio")
	public String getBio() {
		return bio;
	}

	@JsonProperty("info")
	public List<String> getInfo() {
		return info;
	}

	@JsonProperty("website")
	public String getWebsite() {
		return website;
	}

	@JsonProperty("twitter")
	public String getTwitter() {
		return twitter;
	}

	@JsonProperty("facebook")
	public String getFacebook() {
		return facebook;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hunchId == null) ? 0 : hunchId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (hunchId == null) {
			if (other.hunchId != null)
				return false;
		} else if (!hunchId.equals(other.hunchId))
			return false;
		return true;
	}
}
