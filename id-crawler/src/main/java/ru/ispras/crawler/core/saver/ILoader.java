package ru.ispras.crawler.core.saver;

import java.util.Map;
import java.util.NoSuchElementException;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * Loads {@link IResponse}s corresponding to the given {@link IRequest}s or
 * iterate over {@link IRequest}-{@link IResponse} pairs by using storage (e.g.,
 * database) in which {@link ISaver} saved them.
 * <p>
 * Implementations are expected to be thread-safe.
 * 
 * @author Ivan Andrianov
 */
public interface ILoader<Req extends IRequest, Resp extends IResponse> {
	/**
	 * Loads {@link IResponse} corresponding to the given {@link IRequest} in
	 * the storage.
	 * 
	 * @throws NoSuchElementException
	 *             if no {@link IResponse} found in the storage.
	 */
	Resp load(Req request);

	/**
	 * Iterates over all {@link IRequest}-{@link IResponse} pairs in the
	 * storage.
	 */
	Iterable<Map.Entry<Req, Resp>> load();
}
