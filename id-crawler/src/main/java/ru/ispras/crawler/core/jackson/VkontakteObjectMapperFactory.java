package ru.ispras.crawler.core.jackson;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Factory for Object Mapper that is used in Vkontakte connections/savers/loaders
 *
 * Default {@link DefaultObjectMapperFactory} is extended by one property:
 * Deserialization Feature ACCEPT_SINGLE_VALUE_AS_ARRAY is set to true:
 *  => Collection deserializers will try to handle non-array values as if they had "implicit" surrounding JSON array.
 *
 * Created with IntelliJ IDEA.
 * User: andrey
 * Date: 10/31/14
 * Time: 12:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class VkontakteObjectMapperFactory implements IObjectMapperFactory {
    @Override
    public ObjectMapper create() {
        return new DefaultObjectMapperFactory().create().
               configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    }
}
