package ru.ispras.crawler.core.datamodel.vkontakte.entities.groups;

/**
 * Created by padre on 14.07.14
 */
public enum GroupType {
    GROUP, PAGE, EVENT, UNKNOWN;

    public static GroupType stringToType(String type) {
        switch (type) {
            case "group": return GROUP;
            case "page":  return PAGE;
            case "event": return EVENT;
            default: return UNKNOWN;
        }
    }


    public static String typeToString(GroupType type) {
        switch (type) {
            case GROUP : return "group";
            case PAGE :  return "page";
            case EVENT : return "event";
            default: return "unknown";
        }
    }
}
