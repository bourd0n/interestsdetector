package ru.ispras.crawler.core.connection.vkontakte;

import static ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils.encodeURL;

import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontaktePlacesSearchRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.walls.Place;

/**
 * Connection for https://vk.com/dev/places.search method.
 * Requires access token
 * 
 * @author andrey
 *
 */
public class VkontaktePlacesSearchConnection implements IConnection<VkontaktePlacesSearchRequest, BasicResponse<List<Place>>> {

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	public VkontaktePlacesSearchConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}
	
	private String getMethod(VkontaktePlacesSearchRequest request) {
		String method = "places.search?latitude="+request.getLatitude()+
				"&longitude="+request.getLongitude()+
				"&radius="+request.getRadius().getValue();
		if (request.getQuery() != null){
			method = method+"&q="+encodeURL(request.getQuery());
		}
		return method;
	}

	@Override
	public BasicResponse<List<Place>> getResponse(
			VkontaktePlacesSearchRequest request)
			throws UnavailableConnectionException {
		String url = getMethod(request);
		List<Place> response = VkontakteConnectionUtils.getPagedResponse(url, 1000, request.getLimit(), httpConnection, Place.class);
		return new BasicResponse<List<Place>>(response);
	}

}
