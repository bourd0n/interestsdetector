package ru.ispras.crawler.core.connection.vkontakte.staticdata;

import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.IdTitle;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 28.05.14
 * Time: 14:37
 */

/**
 * Abstract {@link IConnection} which contains common Vkontakte-related utils for downloading static data.
 * @param <Request>
 */
public abstract class AbstractVkontakteIdTitleConnection<Request extends IRequest>  implements IConnection<Request, BasicResponse<List<IdTitle>>> {
    private final IConnection<HttpRequest, HttpResponse> httpConnection;

    protected AbstractVkontakteIdTitleConnection(IConnection<HttpRequest, HttpResponse> httpConnection) {
        this.httpConnection = httpConnection;
    }

    /**
    *
    * @param request information of request
    * @return part of request string from (not include) method up to count, thus if you want request
    * "https://api.vk.com/method/database.getCountries?need_all=1"
    * this function should return "database.getCountries?need_all=1"
    */
    protected abstract String getMethod(Request request);

    @Override
    public BasicResponse<List<IdTitle>> getResponse(Request request) throws UnavailableConnectionException {
        String url = getMethod(request);
		List<IdTitle> response = VkontakteConnectionUtils.getPagedResponse(url, 1000, Integer.MAX_VALUE,
				httpConnection, IdTitle.class);
        return new BasicResponse<>(response);
    }
}
