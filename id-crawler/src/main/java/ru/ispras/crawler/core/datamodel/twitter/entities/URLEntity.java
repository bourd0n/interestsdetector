/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.twitter.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A data class representing one single URL entity.
 *
 * @author Mocel - mocel at guma.jp, andrey g
 * @since Twitter4J 2.1.9
 */
public class URLEntity{

    private final String url;
    private final String expandedURL;
    private final String displayURL;
    private final List<Integer> indices;

    @JsonCreator
    public URLEntity(
    		@JsonProperty("url") String url,
    		@JsonProperty("expanded_url") String expandedURL,
    		@JsonProperty("display_url") String displayURL,
    		@JsonProperty("indices") List<Integer> indices) {
		this.url = url;
		this.expandedURL = expandedURL;
		this.displayURL = displayURL;
		this.indices = indices;
	}

    public String getText() {
        return url;
    }

    @JsonProperty("url")
    public String getURL() {
        return url;
    }

    @JsonProperty("expanded_url")
    public String getExpandedURL() {
        return expandedURL;
    }

    @JsonProperty("display_url")
    public String getDisplayURL() {
        return displayURL;
    }

    @JsonProperty("indices")
	public List<Integer> getIndices() {
		return indices;
	}
    
    @Override
	public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        URLEntity that = (URLEntity) o;

        if (displayURL != null ? !displayURL.equals(that.displayURL) : that.displayURL != null) return false;
        if (expandedURL != null ? !expandedURL.equals(that.expandedURL) : that.expandedURL != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;

        return true;
    }
    
    @Override
    public int hashCode() {
        int result = url != null ? url.hashCode() : 0;
        result = 31 * result + (expandedURL != null ? expandedURL.hashCode() : 0);
        result = 31 * result + (displayURL != null ? displayURL.hashCode() : 0);
        return result;
    }
}
