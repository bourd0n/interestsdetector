package ru.ispras.crawler.core.connection.facebook.api;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.User;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * {@link IConnection} for downloading profile for given Facebook user via Facebook API.
 * Example: https://graph.facebook.com/100001698473676
 *
 * @author andrey g
 *
 */
public class FacebookAPIProfileConnection extends AbstractFacebookAPIObjectConnection<User>{

	public FacebookAPIProfileConnection(
			IConnection<HttpRequest, HttpResponse> facebookAPIHttpConnection) {
		super(facebookAPIHttpConnection, User.class);
	}

}
