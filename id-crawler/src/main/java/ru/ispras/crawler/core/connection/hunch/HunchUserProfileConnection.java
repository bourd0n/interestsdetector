package ru.ispras.crawler.core.connection.hunch;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserProfileRequest;
import ru.ispras.crawler.core.datamodel.hunch.entities.User;

/**
 *
 * {@link IConnection} for downloading profile of given Hunch user
 *
 * Example: https://hunch.com/ndrey/profile
 */
public class HunchUserProfileConnection implements IConnection<HunchUserProfileRequest, BasicResponse<User>> {

	private final static String PROFILE_METHOD = "%s/profile/";
	
	private final static String INFO_CONTAINER_SELECTOR = "div.tight-widget";
	private final static String INFO_ITEM_SELECTOR = "p.dim";
	private final static String WEBSITE_SELECTOR = "div.dim.prose > a[rel=nofollow]";
	private final static String FACEBOOK_AND_TWITTER_SELECTOR = "div.dim.prose > p.small > a[href]";
	private final static String INFLUENCER_SELECTOR = "a.picture-frame > s.influencer";
	private final static String BIOGRAPHY_SELECTOR = "div.dim.prose > p:not([.small])";
	
	private final static Pattern facebookScreenNamePattern = Pattern.compile("^(http|https)://(www\\.)*facebook.com/([^\\s]+)");
	private final static Pattern twitterScreenNamePattern = Pattern.compile("^(http|https)://(www\\.)*twitter.com/([^\\s]+)");
	
	private final IConnection<HttpRequest, HttpResponse> hunchHttpConnection;
	
	
	public HunchUserProfileConnection(IConnection<HttpRequest, HttpResponse> hunchHttpConnection) {
		this.hunchHttpConnection = hunchHttpConnection;
	}

	@Override
	public BasicResponse<User> getResponse(HunchUserProfileRequest request) throws UnavailableConnectionException {
		
		final String hunchId = request.getScreenName();
		final String method = String.format(PROFILE_METHOD, hunchId);
		final Document doc = HunchUtils.getDocument(method, hunchHttpConnection);
		
		List<String> info = getInfo(doc);
		
		String website = getWebsite(doc);
		boolean influencer = isEnfluencer(doc);
		         
		String bio = getBiography(doc);
				
		String[] facebookAndTwitter = addFacebookTwitterNames(doc);
		
		return new BasicResponse<User>(new User(hunchId, influencer, bio, info, website,
									facebookAndTwitter[1], facebookAndTwitter[0]));
	}
	
	private List<String> getInfo(Document profile) {
		List<String> info = new ArrayList<String>();
		Element infoContainerElement = profile.select(INFO_CONTAINER_SELECTOR).first();
		if (infoContainerElement != null) {
			Elements infoElements = infoContainerElement.select(INFO_ITEM_SELECTOR);
			for (Element infoElement : infoElements) {
				info.add(infoElement.text());
			}
		}
		return info;
	}
	
	private String getWebsite(Document profile) {
		Element websiteElement = profile.select(WEBSITE_SELECTOR).first();
		if (websiteElement != null) {
			return websiteElement.attr("href");
		}
		return null;
	}
	
	private boolean isEnfluencer(Document profile) {
		return profile.select(INFLUENCER_SELECTOR).first() != null;
	}
	
	private String getBiography(Document profile) {
		final Element bioElement = profile.select(BIOGRAPHY_SELECTOR).first();
		if (bioElement != null && !bioElement.text().isEmpty()) {
			return bioElement.text();
		}
		return null;
	}
	
	private String[] addFacebookTwitterNames(Document profile) {
		final String[] result = {null, null};
		final Elements linkElements = profile.select(FACEBOOK_AND_TWITTER_SELECTOR);
		for (Element linkElement : linkElements) {
			final String href = linkElement.attr("href");
			Matcher matcher = facebookScreenNamePattern.matcher(href);
			if (matcher.find()) {
				result[0] = matcher.group(3);
				continue;
			}
			
			matcher = twitterScreenNamePattern.matcher(href);
			if (matcher.find()) {
				result[1] = matcher.group(3);
			}
		}
		return result;
	}
}
