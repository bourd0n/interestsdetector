package ru.ispras.crawler.core.datamodel.vkontakte;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Created with IntelliJ IDEA.
 * User: andrey
 * Date: 9/1/14
 * Time: 5:07 PM
 * {link @IRequest} for https://vk.com/dev/utils.resolveScreenName
 */
public class VkontakteResolveScreenNameRequest implements IRequest {
    private final String screenName;

    public VkontakteResolveScreenNameRequest(String screenName) {
        this.screenName = screenName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VkontakteResolveScreenNameRequest that = (VkontakteResolveScreenNameRequest) o;

        if (screenName != null ? !screenName.equals(that.screenName) : that.screenName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return screenName != null ? screenName.hashCode() : 0;
    }

    public String getScreenName() {
        return screenName;
    }
}
