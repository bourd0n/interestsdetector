package ru.ispras.crawler.core.requestidconverter.vkontakte;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteIsMemberRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class VkontakteIsMemberRequestIdConverter implements
		IRequestIdConverter<VkontakteIsMemberRequest, String> {

	@Override
	public String getId(VkontakteIsMemberRequest request) {
		return request.getGroupId()+"_"+StringUtils.join(request.getUserIds(), ',');
	}

	@Override
	public VkontakteIsMemberRequest getRequest(String id) {
		String[] elems = id.split("_");
		String[] users = elems[1].split(",");
		return new VkontakteIsMemberRequest(Long.parseLong(elems[0]), getList(users));
	}

	private List<Long> getList(String[] users) {
		List<Long> result = new ArrayList<>();
		for (String user: users){
			result.add(Long.parseLong(user));
		}
		return result;
	}
}
