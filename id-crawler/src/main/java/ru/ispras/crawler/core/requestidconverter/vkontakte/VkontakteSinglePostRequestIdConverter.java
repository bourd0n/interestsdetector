package ru.ispras.crawler.core.requestidconverter.vkontakte;

import ru.ispras.crawler.core.datamodel.vkontakte.VkontaktePostRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class VkontakteSinglePostRequestIdConverter implements
		IRequestIdConverter<VkontaktePostRequest, String> {

	@Override
	public String getId(VkontaktePostRequest request) {
		return request.getOwnerId()+"_"+request.getPostId();
	}

	@Override
	public VkontaktePostRequest getRequest(String id) {
		String[] elems = id.split("_");
		return new VkontaktePostRequest(Long.parseLong(elems[0]), Long.parseLong(elems[1]));
	}
}
