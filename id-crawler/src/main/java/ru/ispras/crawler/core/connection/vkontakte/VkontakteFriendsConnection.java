package ru.ispras.crawler.core.connection.vkontakte;

import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteFriendsRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.ResponseList;

/**
 * Connection for https://vk.com/dev/friends.get method
 * 
 * @author andrey
 *
 */
public class VkontakteFriendsConnection implements IConnection<VkontakteFriendsRequest, BasicResponse<List<Long>>> {

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	public VkontakteFriendsConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}
	
	@Override
	public BasicResponse<List<Long>> getResponse(VkontakteFriendsRequest request)
			throws UnavailableConnectionException {
		String url = "friends.get?user_id="+request.getUserId();
		ResponseList<Long> response = VkontakteConnectionUtils.getSingleResponseList(url, httpConnection, Long.class);
		return new BasicResponse<List<Long>>(response.getItems());
	}
	
}
