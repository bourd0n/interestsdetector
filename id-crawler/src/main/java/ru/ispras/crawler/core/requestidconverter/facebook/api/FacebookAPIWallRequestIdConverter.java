package ru.ispras.crawler.core.requestidconverter.facebook.api;

import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIWallRequest;

public class FacebookAPIWallRequestIdConverter extends AbstractFacebookAPIRequestIdConverter<FacebookAPIWallRequest> {
	@Override
	public FacebookAPIWallRequest getRequest(Long id) {
		return new FacebookAPIWallRequest(id);
	}
}
