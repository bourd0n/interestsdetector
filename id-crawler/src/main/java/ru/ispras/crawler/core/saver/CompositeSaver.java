package ru.ispras.crawler.core.saver;

import java.util.Arrays;
import java.util.List;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * {@link ISaver} implementation which encapsulates a list of {@link ISaver}s
 * and translates all calls to them.
 * 
 * @author Ivan Andrianov
 */
public class CompositeSaver<Req extends IRequest, Resp extends IResponse> implements ISaver<Req, Resp> {
	private final List<? extends ISaver<? super Req, ? super Resp>> savers;

	@SafeVarargs
	public CompositeSaver(ISaver<? super Req, ? super Resp>... savers) {
		this(Arrays.asList(savers));
	}

	public CompositeSaver(List<? extends ISaver<? super Req, ? super Resp>> savers) {
		this.savers = savers;
	}

	@Override
	public void save(Req request, Resp response) {
		for (ISaver<? super Req, ? super Resp> saver : savers) {
			saver.save(request, response);
		}
	}
}
