package ru.ispras.crawler.core.connection.twitter;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterSearchTweetsRequest;
import ru.ispras.crawler.core.datamodel.twitter.TwitterSearchTweetsRequest.LocationQuery;
import ru.ispras.crawler.core.datamodel.twitter.entities.SearchResult;
import ru.ispras.crawler.core.datamodel.twitter.entities.SearchResult.SearchMetadata;
import ru.ispras.crawler.core.datamodel.twitter.entities.Status;

/**
 * {@link IConnection} which returns list of Tweets corresponding to the given
 * query for every given {@link TwitterSearchTweetsRequest}.
 *
 * See https://dev.twitter.com/rest/reference/get/search/tweets
 * 
 * @author Ivan Andrianov
 * 
 */
public class TwitterSearchTweetsConnection extends
		AbstractTwitterConnection<TwitterSearchTweetsRequest, BasicResponse<List<Status>>> {
	private static final String METHOD = "search/tweets.json";
	private static final String METHOD_BASE = METHOD + "?count=100&%s%s%s";

	public TwitterSearchTweetsConnection(IConnection<HttpRequest, HttpResponse> twitterHttpConnection) {
		super(twitterHttpConnection);
	}

	@Override
	public BasicResponse<List<Status>> getResponse(TwitterSearchTweetsRequest request) throws UnavailableConnectionException {
		String method = String.format(METHOD_BASE, getTextQueryParameter(request),
				getLocationQueryParameter(request),
				getLanguageParameter(request));
		List<Status> tweets = new ArrayList<Status>();
		while (true) {
			SearchResult response = getHttpResponse(method, SearchResult.class);
			tweets.addAll(response.getStatuses());

			SearchMetadata metadata = response.getSearchMetadata();
			String nextResultsQuery = metadata.getNextResults();
			if (tweets.size() >= request.getLimit() || nextResultsQuery == null) {
				break;
			}
			method = METHOD + nextResultsQuery;
		}
		return new BasicResponse<List<Status>>(tweets);
	}

	private String getTextQueryParameter(TwitterSearchTweetsRequest searchTweetsRequest) {
		try {
			String encodedQuery = URLEncoder.encode(searchTweetsRequest.getTextQuery(), "UTF-8");
			return String.format("q=%s", encodedQuery);
		} catch (UnsupportedEncodingException e) {
			// Impossible in fact.
			throw new RuntimeException(e);
		}
	}

	private String getLocationQueryParameter(TwitterSearchTweetsRequest searchTweetsRequest) {
		LocationQuery locationQuery = searchTweetsRequest.getLocationQuery();
		return locationQuery != null ? String.format(Locale.ENGLISH, "&geocode=%f,%f,%dkm",
				locationQuery.getLatitude(), locationQuery.getLongitude(),
				locationQuery.getKilometerRadius()) : "";
	}

	private String getLanguageParameter(TwitterSearchTweetsRequest searchTweetsRequest) {
		String language = searchTweetsRequest.getLanguage();
		return language != null ? String.format("&lang=%s", language) : "";
	}
}
