package ru.ispras.crawler.core.saver.mongodb;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;
import ru.ispras.crawler.core.saver.ILoader;
import ru.ispras.crawler.core.saver.ISaver;
import ru.ispras.crawler.core.saver.mongodb.responseconverter.IMongoDBResponseConverter;
import ru.ispras.modis.utils.collections.AbstractChunkedIterator;

import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

/**
 * {@link ISaver} and {@link ILoader} implementation for MongoDB storage.
 * <p>
 * This class works with the only {@link DBCollection} and stores all the data
 * in three columns: ID, CHUNK and VALUE. ID field represents a {@link IRequest}
 * according to the given {@link IRequestIdConverter}, VALUE field represents a
 * {@link IResponse} according to given {@link IMongoDBResponseConverter}, CHUNK
 * field contains a number of corresponding value element. An index is provided
 * on ID and CHUNK fields for faster lookups.
 * 
 * @author Ivan Andrianov
 */
public class MongoDBSaverLoader<Req extends IRequest, Resp extends IResponse, ID> implements ISaver<Req, Resp>,
		ILoader<Req, Resp> {

	private static class DBObjectChunkedByIdIterator extends AbstractChunkedIterator<DBObject> {
		public DBObjectChunkedByIdIterator(Iterator<DBObject> iterator) {
			super(iterator);
		}

		@Override
		protected boolean areInTheSameChunk(DBObject first, DBObject second) {
			return first.get(ID).equals(second.get(ID));
		}
	}

	private class RequestResponseIterator implements Iterator<Map.Entry<Req, Resp>> {
		private final Iterator<List<DBObject>> it;

		public RequestResponseIterator(Iterator<List<DBObject>> it) {
			this.it = it;
		}

		@Override
		public boolean hasNext() {
			return it.hasNext();
		}

		@Override
		public Map.Entry<Req, Resp> next() {
			List<DBObject> objects = it.next();
			return getEntry(objects);
		}

		@SuppressWarnings("unchecked")
		private Map.Entry<Req, Resp> getEntry(List<DBObject> objects) {
			final Req request = requestConverter.getRequest((ID) objects.get(0).get(ID));
			List<DBObject> values = getValues(objects);
			final Resp response = responseConverter.getResponse(values);

			return new Map.Entry<Req, Resp>() {
				@Override
				public Req getKey() {
					return request;
				}

				@Override
				public Resp getValue() {
					return response;
				}

				@Override
				public Resp setValue(Resp value) {
					throw new UnsupportedOperationException();
				}
			};
		}

		private List<DBObject> getValues(List<DBObject> objects) {
			List<DBObject> values = new ArrayList<>();
			for (DBObject object : objects) {
				values.add((DBObject) object.get(VALUE));
			}
			return values;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
	}

	private static final String ID = "id";
	private static final String CHUNK = "chunk";
	private static final String VALUE = "value";

	private final DBCollection collection;
	private final IRequestIdConverter<Req, ID> requestConverter;
	private final IMongoDBResponseConverter<Resp> responseConverter;

	public MongoDBSaverLoader(DBCollection collection, IRequestIdConverter<Req, ID> requestConverter,
			IMongoDBResponseConverter<Resp> responseConverter) {
		this.collection = collection;
		this.requestConverter = requestConverter;
		this.responseConverter = responseConverter;
		ensureIndex();
	}

	private void ensureIndex() {
		DBObject index = new BasicDBObject();
		index.put(ID, 1);
		index.put(CHUNK, 1);

		collection.createIndex(index, ID, true);
	}

	@Override
	public void save(Req request, Resp response) {
		ID id = requestConverter.getId(request);
		List<DBObject> values = responseConverter.getValues(response);

		for (int i = 0; i < values.size(); i++) {
			DBObject value = values.get(i);

			DBObject object = new BasicDBObject();
			object.put(ID, id);
			object.put(CHUNK, i);
			object.put(VALUE, value);

			collection.save(object);
		}
	}

	@Override
	public Resp load(Req request) {
		DBObject query = new BasicDBObject();
		query.put(ID, requestConverter.getId(request));
		DBObject order = new BasicDBObject();
		order.put(CHUNK, 1);

		DBCursor cursor = collection.find(query).sort(order);
		Map.Entry<Req, Resp> entry = getRequestResponseIterator(cursor).next();
		return entry.getValue();
	}

	@Override
	public Iterable<Map.Entry<Req, Resp>> load() {
		return new Iterable<Map.Entry<Req, Resp>>() {
			@Override
			public Iterator<Map.Entry<Req, Resp>> iterator() {
				DBObject order = new BasicDBObject();
				order.put(ID, 1);
				order.put(CHUNK, 1);

				DBCursor cursor = collection.find().addOption(Bytes.QUERYOPTION_NOTIMEOUT).sort(order);
				return getRequestResponseIterator(cursor);
			}
		};
	}

	private RequestResponseIterator getRequestResponseIterator(DBCursor cursor) {
		return new RequestResponseIterator(new DBObjectChunkedByIdIterator(cursor));
	}
}
