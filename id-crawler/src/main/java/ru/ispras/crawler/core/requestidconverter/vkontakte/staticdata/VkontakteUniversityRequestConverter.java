package ru.ispras.crawler.core.requestidconverter.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteUniversitiesRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 03.06.14
 * Time: 17:00
 */
public class VkontakteUniversityRequestConverter implements IRequestIdConverter<VkontakteUniversitiesRequest, Long> {
    @Override
    public Long getId(VkontakteUniversitiesRequest request) {
        return request.getCityId();
    }

    @Override
    public VkontakteUniversitiesRequest getRequest(Long aLong) {
        return new VkontakteUniversitiesRequest(aLong);
    }
}
