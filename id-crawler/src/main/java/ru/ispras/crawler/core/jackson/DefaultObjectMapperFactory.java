package ru.ispras.crawler.core.jackson;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Factory provides Jsckson Object mappers with default configuration:
 * Deserialization Feature FAIL_ON_UNKNOWN_PROPERTIES is set to false
 *  => Unknown properties in JSON representation (ones that do not map to constructed object's property) are ignored
 * Serialization Inclusion is set as Include.NON_NULL
 *  => values with null value will not appear in the JSON representation
 *
 * Created with IntelliJ IDEA.
 * User: andrey
 * Date: 10/31/14
 * Time: 12:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class DefaultObjectMapperFactory implements IObjectMapperFactory {
    @Override
    public ObjectMapper create() {
       return new ObjectMapper().
              configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES , false).
              setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }
}
