package ru.ispras.crawler.core.connection;

import ru.ispras.crawler.core.connection.exception.ConnectionCreationException;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * Creates {@link IConnection} by using given {@link IConnectionConfiguration}.
 * 
 * @author Ivan Andrianov
 */
public interface IConfigurableConnectionFactory<Req extends IRequest, Resp extends IResponse, CC extends IConnectionConfiguration> {
	IConnection<Req, Resp> create(CC configuration) throws ConnectionCreationException;
}
