package ru.ispras.crawler.core.connection.facebook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.facebook.FacebookUserWallRequest;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

// This class is package visible until there is any IWallItemProcessor implementation.

/**
 * {@link IConnection} for downloading given user's wall.
 * Wall is a list of posts.
 * Example: https://m.facebook.com/wall.php?id=100001698473676&time=2147483647
 *
 * @author andrey g
 */
class FacebookUserWallConnection extends
		AbstractFacebookConnection<FacebookUserWallRequest, BasicResponse<List<FacebookUserWallConnection.WallPost>>> {
	
	//entity for response
	public static class WallPost {
		//TODO
	}
	
	private static interface IWallItemProcessor {
		public <T> WallPost processItem(List<T> targetIds, String text);
	}

	private static final String BASE_METHOD = "wall.php?id=%d&time=%d";
	private static final int MAX_TIMESTAMP = Integer.MAX_VALUE;

	private static final Pattern moreItemsPattern = Pattern.compile("time=([0-9]+)&");

	private static final Map<String, IWallItemProcessor> itemType2Processor;

	static {
		itemType2Processor = new HashMap<String, IWallItemProcessor>();
		// TODO #2005, #2006.
	}

	public FacebookUserWallConnection(IConnection<HttpRequest, HttpResponse> facebookHttpConnection) {
		super(facebookHttpConnection);
	}

	@Override
	public BasicResponse<List<WallPost>> getResponse(FacebookUserWallRequest request) throws UnavailableConnectionException {
		long userId = request.getId();
		int timestamp = MAX_TIMESTAMP;
		List<WallPost> wallItems = new ArrayList<WallPost>();
		while (true) {
			String htmlPage = getHttpResponse(String.format(BASE_METHOD, userId, timestamp));
			Document doc = Jsoup.parse(htmlPage);
			addWallItemsFromDocument(wallItems, doc);
			Element moreItemsDiv = doc.getElementById("m_more_item");
			if (moreItemsDiv == null) {
				break;
			}
			timestamp = getNextTimestamp(moreItemsDiv);
		}
		return new BasicResponse<List<WallPost>>(wallItems);
	}

	private void addWallItemsFromDocument(List<WallPost> wallItems, Document doc) {
		Elements storyDivs = doc.getElementsByClass("story");
		for (Element storyDiv : storyDivs) {
			addWallItemFromStoryDiv(wallItems, storyDiv);
		}
	}

	//TODO 
	private void addWallItemFromStoryDiv(List<WallPost> wallItems, Element storyDiv) {
		//temp: JSONObject was here
		Map<String, Object> ftObject = null;//(JSONObject) JSONValue.parse(storyDiv.attr("data-ft"));
		@SuppressWarnings("null")
		String type = (String) ftObject.get("ft_story_name");
		IWallItemProcessor processor = itemType2Processor.get(type);
		if (processor != null) {
			List<Long> targetIds = getTargetIds((String) ftObject.get("fbid"));
			String text = storyDiv.getElementsByClass("fcg").text();
			WallPost wallItem = processor.processItem(targetIds, text);
			@SuppressWarnings("unused")
			int pubTime = (Integer) ftObject.get("pub_time");
			//setters
			//wallItem.put("pub_time", pubTime);
			//wallItem.put("type", type);
			wallItems.add(wallItem);
		}
	}

	private List<Long> getTargetIds(String fbId) {
		List<Long> result = new ArrayList<Long>();
		String[] fbIdParts = fbId.split(",");
		for (String fbIdPart : fbIdParts) {
			result.add(Long.parseLong(fbIdPart));
		}
		return result;
	}

	private int getNextTimestamp(Element moreItemsDiv) {
		Element moreItemsLink = moreItemsDiv.getElementsByTag("a").first();
		Matcher moreItemsMatcher = moreItemsPattern.matcher(moreItemsLink.attr("href"));
		moreItemsMatcher.find();
		return Integer.parseInt(moreItemsMatcher.group(1));
	}
	
}
