/*
 * Copyright 2012 Ryuji Yamashita
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.facebook.api.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * @author Ryuji Yamashita - roundrop at gmail.com, andrey g
 */
public final class Picture{

    private final String url;
    private final Boolean isSilhouette;

    @JsonCreator
    public Picture(
    		@JsonProperty("url") String url,
    		@JsonProperty("is_silhouette") Boolean isSilhouette) {
		this.url = url;
		this.isSilhouette = isSilhouette;
	}

    @JsonProperty("url")
	public String getURL() {
        return url;
    }

    @JsonProperty("is_silhouette")
	public Boolean isSilhouette() {
        return isSilhouette;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((isSilhouette == null) ? 0 : isSilhouette.hashCode());
        result = prime * result + ((url == null) ? 0 : url.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Picture other = (Picture) obj;
        if (isSilhouette == null) {
            if (other.isSilhouette != null)
                return false;
        } else if (!isSilhouette.equals(other.isSilhouette))
            return false;
        if (url == null) {
            if (other.url != null)
                return false;
        } else if (!url.equals(other.url))
            return false;
        return true;
    }

}
