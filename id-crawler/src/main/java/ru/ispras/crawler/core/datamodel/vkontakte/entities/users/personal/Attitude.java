package ru.ispras.crawler.core.datamodel.vkontakte.entities.users.personal;

/**
 * Created by padre on 14.07.14
 */

/**
 * attitude to smoke and alcohol
 * see description in https://vk.com/dev/fields_2
 */
public enum Attitude {
    VERYNEGATIVE, NEGATIVE, NEUTRAL, COMPROMISE, POSITIVE, UNKNOWN;

    public static Attitude intToAttitude(Integer number) {
        if (number == null) return UNKNOWN;
        switch (number){
            case 1: return VERYNEGATIVE;
            case 2: return NEGATIVE;
            case 3: return NEUTRAL;
            case 4: return COMPROMISE;
            case 5: return POSITIVE;
            default: return UNKNOWN;
        }
    }

    public static Integer attitudeToInt(Attitude type) {
        switch (type){
            case VERYNEGATIVE: return 1;
            case NEGATIVE: return 2;
            case NEUTRAL: return 3;
            case COMPROMISE: return 4;
            case POSITIVE: return 5;
            default: return 0;
        }
    }
}
