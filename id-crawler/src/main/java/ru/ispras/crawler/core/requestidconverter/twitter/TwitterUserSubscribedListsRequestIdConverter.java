package ru.ispras.crawler.core.requestidconverter.twitter;

import ru.ispras.crawler.core.datamodel.twitter.TwitterUserSubscribedListsRequest;

public class TwitterUserSubscribedListsRequestIdConverter extends
		AbstractTwitterUserRequestIdConverter<TwitterUserSubscribedListsRequest> {
	@Override
	public TwitterUserSubscribedListsRequest getRequest(Long id) {
		return new TwitterUserSubscribedListsRequest(id);
	}
}
