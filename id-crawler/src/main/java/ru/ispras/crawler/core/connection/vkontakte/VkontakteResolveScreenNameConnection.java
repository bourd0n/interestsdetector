package ru.ispras.crawler.core.connection.vkontakte;

import org.apache.commons.lang.StringUtils;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteBatchResponseUtils;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.batch.BasicBatchRequest;
import ru.ispras.crawler.core.datamodel.batch.BasicBatchResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteResolveScreenNameRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteUserRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.ResolveScreenNameResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.users.User;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: andrey
 * Date: 9/1/14
 * Time: 5:05 PM
 * Connection for https://vk.com/dev/utils.resolveScreenName
 * Allows to get vkontakte user id by screen name
 */
public class VkontakteResolveScreenNameConnection implements IConnection<VkontakteResolveScreenNameRequest,BasicResponse<ResolveScreenNameResponse>> {

    private final IConnection<HttpRequest, HttpResponse> httpConnection;

    public VkontakteResolveScreenNameConnection(
            IConnection<HttpRequest, HttpResponse> httpConnection) {
        this.httpConnection = httpConnection;
    }

    private String getMethod(String screenName) {
        return "utils.resolveScreenName?screen_name="+ screenName;
    }

    @Override
    public BasicResponse<ResolveScreenNameResponse> getResponse(VkontakteResolveScreenNameRequest request)
            throws UnavailableConnectionException {
        String url = getMethod(request.getScreenName());
        ResolveScreenNameResponse response = VkontakteConnectionUtils.getSingleResponse(url, httpConnection, ResolveScreenNameResponse.class);
        return new BasicResponse<>(response);
    }
}
