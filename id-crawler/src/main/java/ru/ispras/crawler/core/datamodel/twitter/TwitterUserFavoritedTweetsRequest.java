package ru.ispras.crawler.core.datamodel.twitter;

import ru.ispras.crawler.core.datamodel.IRequest;
/**
 *  {@link IRequest} for downloading favorite Tweets for given user
 *
 *  See https://dev.twitter.com/rest/reference/get/favorites/list
 */
public class TwitterUserFavoritedTweetsRequest extends
		AbstractTwitterUserRequest {

	private final int limit;

	public TwitterUserFavoritedTweetsRequest(long id) {
		this(id, Integer.MAX_VALUE);
	}

	public TwitterUserFavoritedTweetsRequest(long id, int limit) {
		super(id);
		this.limit = limit;
	}

	public int getLimit() {
		return limit;
	}
}
