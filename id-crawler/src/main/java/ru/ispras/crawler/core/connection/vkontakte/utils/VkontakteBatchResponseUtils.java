package ru.ispras.crawler.core.connection.vkontakte.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.batch.BasicBatchResponse;

/**
 * An abstract Batch Basic Response utils. This class is used for batch API requests
 * @author andrey
 *
 * @param <Req> request type
 * @param <ID> id type
 * @param <T> entity type
 */
public abstract class VkontakteBatchResponseUtils<Req extends IRequest, ID, T> {
	
	/**
	 * Extracts List of ids from list of requests
	 * @param requests
	 * @return
	 */
	public List<ID> getIds(List<Req> requests){
		List<ID> result = new ArrayList<>();
		for (Req req: requests){
			result.add(getId(req));
		}
		return result;
	}
	
	/**
	 * Returns BasicBatchResponse given list of requests and list of objects from response
	 * @param requests
	 * @param objects
	 * @return
	 */
	public BasicBatchResponse<Req, BasicResponse<T>> getBasicBatchResponse(List<Req> requests, List<T> objects){
		Map<ID, T> id2obj = getObjectMap(objects);
		Map<Req, BasicResponse<T>> req2resp = getRequestToResponseMap(requests, id2obj);
		return new BasicBatchResponse<>(req2resp);
	}

	protected abstract ID getId(Req req);

	protected abstract ID getId(T obj);

	private Map<Req, BasicResponse<T>> getRequestToResponseMap(
			List<Req> requests, Map<ID, T> id2obj) {
		Map<Req, BasicResponse<T>> map = new HashMap<>();
		for (Req req: requests){
			ID id = getId(req);
			T obj = id2obj.get(id);
			if (obj != null){
				map.put(req, new BasicResponse<>(obj));
			}
		}
		return map;
	}
	
	private Map<ID, T> getObjectMap(List<T> objects) {
		Map<ID, T> result = new HashMap<>();
		for (T obj: objects){
			result.put(getId(obj), obj);
		}
		return result;
	}
}
