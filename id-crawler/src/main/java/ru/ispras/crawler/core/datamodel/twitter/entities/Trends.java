/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.twitter.entities;

import java.util.Date;
import java.util.List;

import ru.ispras.crawler.core.datamodel.twitter.utils.TwitterDateConverter;
import ru.ispras.crawler.core.datamodel.utils.DateConverter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A data class representing Trends.
 *
 * @author Yusuke Yamamoto - yusuke at mac.com, andrey g
 * @since Twitter4J 2.0.2
 */
public final class Trends {
    private final Date asOf;
    private final Date createdAt;
    private final List<Trend> trends;
    private final List<Location> locations;
    
    private final static DateConverter dateConverter = new TwitterDateConverter();
    
    @JsonCreator
    public Trends(
    			@JsonProperty("as_of") String asOf,
    			@JsonProperty("created_at") String createdAt,
    			@JsonProperty("locations") List<Location> locations,
    			@JsonProperty("trends") List<Trend> trends) {
        this.asOf = dateConverter.getDate(asOf);
        this.locations = locations;
        this.trends = trends;
        this.createdAt = dateConverter.getDate(createdAt);
    }
    
    
    /**
     * A data class representing Trend.
     *
     * @author Yusuke Yamamoto - yusuke at mac.com, andrey g
     * @since Twitter4J 2.0.2
     */
    public class Trend {
        private final String name;
        private final String url;
        private final String query;
        
        public Trend(
        			@JsonProperty("name") String name,
        			@JsonProperty("url") String url,
        			@JsonProperty("query") String query) {
    		this.name = name;
    		this.url = url;
    		this.query = query;
    	}

        @JsonProperty("name")
    	public String getName() {
            return name;
        }
        @JsonProperty("url")
        public String getURL() {
            return url;
        }

        @JsonProperty("query")
        public String getQuery() {
            return query;
        }
        
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Trend)) return false;

            Trend trend = (Trend) o;

            if (!name.equals(trend.getName())) return false;
            if (query != null ? !query.equals(trend.getQuery()) : trend.getQuery() != null)
                return false;
            if (url != null ? !url.equals(trend.getURL()) : trend.getURL() != null)
                return false;

            return true;
        }
        
        @Override
        public int hashCode() {
            int result = name.hashCode();
            result = 31 * result + (url != null ? url.hashCode() : 0);
            result = 31 * result + (query != null ? query.hashCode() : 0);
            return result;
        }
    }    

    @JsonIgnore
    public Date getAsOf() {
		return asOf;
	}

    @JsonIgnore
	public Date getCreatedAt() {
		return createdAt;
	}
	
	@JsonProperty("as_of") 
    public String getAsOfString() {
		return dateConverter.getString(asOf);
	}

    @JsonProperty("created_at")
	public String getCreatedAtString() {
		return dateConverter.getString(createdAt);
	}

    @JsonProperty("trends")
	public List<Trend> getTrends() {
		return trends;
	}

	@JsonProperty("locations")
	public List<Location> getLocations() {
		return locations;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((asOf == null) ? 0 : asOf.hashCode());
		result = prime * result
				+ ((createdAt == null) ? 0 : createdAt.hashCode());
		result = prime * result
				+ ((locations == null) ? 0 : locations.hashCode());
		result = prime * result + ((trends == null) ? 0 : trends.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trends other = (Trends) obj;
		if (asOf == null) {
			if (other.asOf != null)
				return false;
		} else if (!asOf.equals(other.asOf))
			return false;
		if (createdAt == null) {
			if (other.createdAt != null)
				return false;
		} else if (!createdAt.equals(other.createdAt))
			return false;
		if (locations == null) {
			if (other.locations != null)
				return false;
		} else if (!locations.equals(other.locations))
			return false;
		if (trends == null) {
			if (other.trends != null)
				return false;
		} else if (!trends.equals(other.trends))
			return false;
		return true;
	}

}
