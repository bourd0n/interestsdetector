package ru.ispras.crawler.core.visitor;

import java.util.Collection;
import java.util.Set;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * Interface to collect responses for requests.
 * <p>
 * Implementations are expected to be thread-safe.
 * 
 * @author sysoev
 */
public interface IVisitor<Req extends IRequest, Resp extends IResponse> {
	/**
	 * Process the provided response for the request. As a result create a number
	 * of additional requests, that should be crawled.
	 * <p>
	 * Visitor can throw any {@link RuntimeException}s - in such case request and response
	 * will be ignored and there will be no continuation requests. 
	 */
	public Collection<? extends IRequest> visit(Req request, Resp response);

	/**
	 * React on the problem, occurred during request execution. As a result create a number
	 * of additional requests, that should be crawled.
	 * <p>
	 * Visitor can throw any {@link RuntimeException}s - in such case there
	 * will be no continuation requests. 
	 */
	public Collection<? extends IRequest> visit(Req request, Throwable problem);

	/**
	 * Get types of produced requests.
	 */
	public Set<? extends Class<? extends IRequest>> getProducedRequestTypes();
}
