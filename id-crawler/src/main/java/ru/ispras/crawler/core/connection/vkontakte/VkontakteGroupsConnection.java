package ru.ispras.crawler.core.connection.vkontakte;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteBatchResponseUtils;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.batch.BasicBatchRequest;
import ru.ispras.crawler.core.datamodel.batch.BasicBatchResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteGroupRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.groups.Group;

/**
 * Connection for https://vk.com/dev/groups.getById method
 * @author andrey
 *
 */
public class VkontakteGroupsConnection implements 
		IConnection<BasicBatchRequest<VkontakteGroupRequest>, BasicBatchResponse<VkontakteGroupRequest, BasicResponse<Group>>> {

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	private static final VkontakteBatchResponseUtils<VkontakteGroupRequest, Long, Group> utils = new VkontakteBatchResponseUtils<VkontakteGroupRequest, Long, Group>(){
		@Override
		protected Long getId(VkontakteGroupRequest req) {
			return req.getId();
		}
		@Override
		protected Long getId(Group obj) {
			return obj.getId();
		}
	};
	
	public VkontakteGroupsConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}

	private String getMethod(List<Long> ids) {
		return "groups.getById?group_ids="+StringUtils.join(ids,',')+
				"&fields=city,country,place,description,wiki_page,members_count,counters," +
				"start_date,finish_date,activity,status,contacts,links,fixed_post,verified,site";
	}

	@Override
	public BasicBatchResponse<VkontakteGroupRequest, BasicResponse<Group>> getResponse(BasicBatchRequest<VkontakteGroupRequest> request)
			throws UnavailableConnectionException {
		String url = getMethod(utils.getIds(request.getRequests()));
		List<Group> response = VkontakteConnectionUtils.getSingleListResponse(url, httpConnection, Group.class);
		return utils.getBasicBatchResponse(request.getRequests(), response);
	}
}
