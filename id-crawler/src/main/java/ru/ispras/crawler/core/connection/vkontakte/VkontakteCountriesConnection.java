package ru.ispras.crawler.core.connection.vkontakte;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.IdTitle;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteCountriesRequest;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 27.05.14
 * Time: 20:39
 * see https://vk.com/dev/database.getCountries
 */
public class VkontakteCountriesConnection implements IConnection<VkontakteCountriesRequest, BasicResponse<List<IdTitle>>> {

    private final IConnection<HttpRequest, HttpResponse> httpConnection;

    public VkontakteCountriesConnection(IConnection<HttpRequest, HttpResponse> httpConnection) {
        this.httpConnection = httpConnection;
    }

    private String getMethod(VkontakteCountriesRequest request) {
        return "database.getCountries?need_all=1";
    }

    @Override
    public BasicResponse<List<IdTitle>> getResponse(VkontakteCountriesRequest request) throws UnavailableConnectionException {
        String url = getMethod(request);
        List<IdTitle> response = VkontakteConnectionUtils.getPagedResponse(url, 1000, 1000, httpConnection, IdTitle.class);
        return new BasicResponse<List<IdTitle>>(response);
    }
}
