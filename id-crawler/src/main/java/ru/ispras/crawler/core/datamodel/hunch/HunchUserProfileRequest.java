package ru.ispras.crawler.core.datamodel.hunch;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 *
 * {@link IRequest} for downloading profile of given Hunch user.
 *
 * Example: https://hunch.com/ndrey/profile
 */
public class HunchUserProfileRequest extends AbstractHunchUserRequest {
	public HunchUserProfileRequest(String screenName) {
		super(screenName);
	}
}
