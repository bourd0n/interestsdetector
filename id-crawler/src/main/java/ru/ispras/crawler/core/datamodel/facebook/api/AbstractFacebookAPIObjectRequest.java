package ru.ispras.crawler.core.datamodel.facebook.api;
import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Abstract {@link IRequest} that is processed via Facebook API (https://graph.facebook.com/...).
 *
 * @author andrey g
 *
 */
abstract public class AbstractFacebookAPIObjectRequest extends FacebookAPIRequest {

	public AbstractFacebookAPIObjectRequest(long id) {
		this(id, "");
	}
	
	public AbstractFacebookAPIObjectRequest(long id, String parameters) {
		super(id, "", parameters);
	}
}
