/*
 * Copyright 2012 Ryuji Yamashita
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.facebook.api.entities;

import java.util.Date;

import ru.ispras.crawler.core.datamodel.facebook.api.utils.FacebookAPIDateConverter;
import ru.ispras.crawler.core.datamodel.utils.DateConverter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ryuji Yamashita - roundrop at gmail.com, andrey g
 */
public final class Comment {
	private final String id;
	private final Category from;
	private final String message;
	private final Boolean canRemove;
	private final Date createdTime;
	private final Integer likeCount;
	private final Boolean isUserLikes;
	
	private final static DateConverter dateConverter = new FacebookAPIDateConverter();

	@JsonCreator
	public Comment(
			@JsonProperty("id") String id,
			@JsonProperty("from") Category from,
			@JsonProperty("message") String message,
			@JsonProperty("can_remove") Boolean canRemove,
			@JsonProperty("created_time") String createdTime,
			@JsonProperty("like_count") Integer likeCount,
			@JsonProperty("user_likes") Boolean isUserLikes) {
		this.id = id;
		this.from = from;
		this.message = message;
		this.canRemove = canRemove;
		this.createdTime = dateConverter.getDate(createdTime);
		this.likeCount = likeCount;
		this.isUserLikes = isUserLikes;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("from")
	public Category getFrom() {
		return from;
	}

	@JsonProperty("message")
	public String getMessage() {
		return message;
	}

	@JsonProperty("can_remove")
	public Boolean canRemove() {
		return canRemove;
	}

	@JsonProperty("created_time")
	public String getCreatedTimeString() {
		return dateConverter.getString(createdTime);
	}

	@JsonIgnore
	public Date getCreatedTime() {
		return createdTime;
	}

	@JsonProperty("like_count")
	public Integer getLikeCount() {
		return likeCount;
	}

	@JsonProperty("user_likes")
	public Boolean isUserLikes() {
		return isUserLikes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
