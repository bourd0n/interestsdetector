package ru.ispras.crawler.core.datamodel.hunch;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} for downloading list of followees for given Hunch user
 * Example: https://hunch.com/ndrey/following
 */
public class HunchUserFollowingRequest extends AbstractHunchUserRequest {
	public HunchUserFollowingRequest(String screenName) {
		super(screenName);
	}
}
