package ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 16.05.14
 * Time: 17:41
 */

@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property="type")
@JsonSubTypes({
        @JsonSubTypes.Type(value=Photo.class, name="photo"),
        @JsonSubTypes.Type(value=PostedPhoto.class, name="posted_photo"),
        @JsonSubTypes.Type(value=Video.class, name="video"),
        @JsonSubTypes.Type(value=Audio.class, name="audio"),
        @JsonSubTypes.Type(value=Doc.class, name="doc"),
        @JsonSubTypes.Type(value=Graffiti.class, name="graffiti"),
        @JsonSubTypes.Type(value=Link.class, name="link"),
        @JsonSubTypes.Type(value=Note.class, name="note"),
        @JsonSubTypes.Type(value=Application.class, name="app"),
        @JsonSubTypes.Type(value=Poll.class, name="poll"),
        @JsonSubTypes.Type(value=Page.class, name="page"),
        @JsonSubTypes.Type(value=Album.class, name="album"),
        @JsonSubTypes.Type(value=PhotosList.class, name="photos_list")
})
/**
 * see descriptions in https://vk.com/dev/attachments_w
 */
public abstract class Attachment {
}
