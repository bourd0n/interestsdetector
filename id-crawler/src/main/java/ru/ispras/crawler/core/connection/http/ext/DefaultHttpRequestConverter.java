package ru.ispras.crawler.core.connection.http.ext;

import java.net.URI;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;

import ru.ispras.crawler.core.datamodel.http.HttpRequest;

/**
 * {@link IHttpRequestConverter} implementation which creates {@link HttpGet}
 * from the {@link URI} specified in the given {@link HttpRequest}.
 * 
 * @author Ivan Andrianov
 * 
 */
public class DefaultHttpRequestConverter implements IHttpRequestConverter {
	@Override
	public HttpUriRequest convert(HttpRequest request) {
		return new HttpGet(request.getURL().toExternalForm());
	}
}
