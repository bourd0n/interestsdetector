package ru.ispras.crawler.core.requestidconverter.twitter;

import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFollowersRequest;

public class TwitterUserFollowersRequestIdConverter extends
		AbstractTwitterUserRequestIdConverter<TwitterUserFollowersRequest> {
	@Override
	public TwitterUserFollowersRequest getRequest(Long id) {
		return new TwitterUserFollowersRequest(id);
	}
}
