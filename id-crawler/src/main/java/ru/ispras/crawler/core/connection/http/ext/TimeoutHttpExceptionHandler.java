package ru.ispras.crawler.core.connection.http.ext;

import java.io.IOException;
import java.net.SocketTimeoutException;

import org.apache.commons.lang.Validate;
import org.apache.http.client.methods.HttpUriRequest;

import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;

/**
 * Throws {@link UnavailableConnectionException} if {@link SocketTimeoutException} arises
 * and executes wrapped handler otherwise
 * @author andrey
 *
 */
public class TimeoutHttpExceptionHandler implements IHttpExceptionHandler{

	private final long tryAfter;
	private final IHttpExceptionHandler wrappedHandler;		

	/**
	 * @param wrappedHandler exception handler that is executed if there is not SocketTimeoutException
	 * @param tryAfter tryAfter timeout for {@link UnavailableConnectionException} (in ms.). Should be >0
	 */
	public TimeoutHttpExceptionHandler(IHttpExceptionHandler wrappedHandler, long tryAfter) {
		Validate.isTrue(tryAfter > 0);
		Validate.notNull(wrappedHandler);
		this.tryAfter = tryAfter;
		this.wrappedHandler = wrappedHandler;
	}

	/**
	 * Try connection after 5 sec. 
	 */
	public TimeoutHttpExceptionHandler() {
		this(new DefaultHttpExceptionHandler(), 5000);
	}
	
	public TimeoutHttpExceptionHandler(long tryAfter) {
		this(new DefaultHttpExceptionHandler(), tryAfter);
	}

	@Override
	public void handle(HttpUriRequest uriRequest, IOException e) throws UnavailableConnectionException {
		if (e instanceof SocketTimeoutException){
			throw new UnavailableConnectionException(String.format("Timeout error while processing request %s.", 
					uriRequest.getURI()), e, System.currentTimeMillis() + tryAfter);	
		}
		wrappedHandler.handle(uriRequest, e);
	}

}
