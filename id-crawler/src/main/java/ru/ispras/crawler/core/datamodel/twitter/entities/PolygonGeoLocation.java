package ru.ispras.crawler.core.datamodel.twitter.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PolygonGeoLocation extends AbstractGeoLocation {
	private final List<List<List<Double>>> coordinates;
	
	
	@JsonCreator
	public PolygonGeoLocation(
			@JsonProperty("type") String type, 
			@JsonProperty("coordinates") List<List<List<Double>>> coordinates) {
		super(type);
		this.coordinates = coordinates;
	}

	@JsonProperty("coordinates")
	public List<List<List<Double>>> getCoordinates() {
		return coordinates;
	}	
}
