package ru.ispras.crawler.core.datamodel.hunch;

import ru.ispras.crawler.core.datamodel.IRequest;
/**
 * {@link IRequest} for downloading list of followers for given user
 * Example: https://hunch.com/ndrey/followers
 */
public class HunchUserFollowersRequest extends AbstractHunchUserRequest {
	public HunchUserFollowersRequest(String screenName) {
		super(screenName);
	}
}
