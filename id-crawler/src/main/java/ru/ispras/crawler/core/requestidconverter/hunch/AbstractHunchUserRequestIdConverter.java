package ru.ispras.crawler.core.requestidconverter.hunch;

import ru.ispras.crawler.core.datamodel.hunch.AbstractHunchUserRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public abstract class AbstractHunchUserRequestIdConverter<Req extends AbstractHunchUserRequest> implements
		IRequestIdConverter<Req, String> {
	@Override
	public String getId(Req request) {
		return request.getScreenName();
	}
}
