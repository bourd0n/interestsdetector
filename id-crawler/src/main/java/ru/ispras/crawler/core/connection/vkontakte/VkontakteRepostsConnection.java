package ru.ispras.crawler.core.connection.vkontakte;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteRepostsRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.walls.Repost;

/**
 * Connection for https://vk.com/dev/wall.getReposts method
 * 
 * @author andrey
 *
 */
public class VkontakteRepostsConnection implements IConnection<VkontakteRepostsRequest, BasicResponse<Repost>> {

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	public VkontakteRepostsConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}
	
	private String getMethod(VkontakteRepostsRequest request) {
		return "wall.getReposts?owner_id="+request.getOwnerId()+
				"&post_id="+request.getPostId();
	}
	
	@Override
	public BasicResponse<Repost> getResponse(VkontakteRepostsRequest request)
			throws UnavailableConnectionException {
		String url = getMethod(request);
		Repost response = VkontakteConnectionUtils.getSingleResponse(url, httpConnection, Repost.class);
		return new BasicResponse<Repost>(response);
	}

}
