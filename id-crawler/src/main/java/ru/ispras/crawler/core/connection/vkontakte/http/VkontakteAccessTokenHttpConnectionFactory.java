package ru.ispras.crawler.core.connection.vkontakte.http;

import ru.ispras.crawler.core.connection.IConfigurableConnectionFactory;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.ConnectionCreationException;
import ru.ispras.crawler.core.connection.http.HttpConnection;
import ru.ispras.crawler.core.connection.http.auth.accesstoken.AccessTokenHttpRequestConverter;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpClientFactory;
import ru.ispras.crawler.core.connection.http.ext.TimeoutHttpExceptionHandler;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * Vkontakte API {@link IConfigurableConnectionFactory} implementation
 * for connections that require access token authorization.
 *
 * @author andrey g
 *
 */
public class VkontakteAccessTokenHttpConnectionFactory implements
IConfigurableConnectionFactory<HttpRequest, HttpResponse, VkontakteHttpConnectionConfiguration>{

	@Override
	public IConnection<HttpRequest, HttpResponse> create(
			VkontakteHttpConnectionConfiguration configuration)
			throws ConnectionCreationException {
		return new HttpConnection<>(new AccessTokenHttpRequestConverter(configuration, "access_token"), new DefaultHttpClientFactory(),
				configuration, new VkontakteHttpResponseConverter(), new TimeoutHttpExceptionHandler());
	}

}
