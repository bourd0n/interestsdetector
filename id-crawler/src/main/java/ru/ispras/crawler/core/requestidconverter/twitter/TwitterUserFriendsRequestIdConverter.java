package ru.ispras.crawler.core.requestidconverter.twitter;

import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFriendsRequest;

public class TwitterUserFriendsRequestIdConverter extends
		AbstractTwitterUserRequestIdConverter<TwitterUserFriendsRequest> {
	@Override
	public TwitterUserFriendsRequest getRequest(Long id) {
		return new TwitterUserFriendsRequest(id);
	}
}
