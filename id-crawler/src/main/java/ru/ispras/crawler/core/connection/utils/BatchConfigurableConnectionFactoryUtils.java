package ru.ispras.crawler.core.connection.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ru.ispras.crawler.core.connection.IConfigurableConnectionFactory;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.IConnectionConfiguration;
import ru.ispras.crawler.core.connection.RoundRobinConnection;
import ru.ispras.crawler.core.connection.exception.ConnectionCreationException;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * Utility class for batch creation of {@link IConnection}s from the given
 * {@link IConnectionConfiguration}s by using given
 * {@link IConfigurableConnectionFactory}. All created {@link IConnection}s are
 * wrapped by {@link RoundRobinConnection}.
 * <p>
 * Note that all {@link ConnectionCreationException}s are logged and not
 * propagated.
 * 
 * @author Ivan Andrianov
 */
public class BatchConfigurableConnectionFactoryUtils {
	private static Logger logger = Logger.getLogger(BatchConfigurableConnectionFactoryUtils.class);

	public static <Req extends IRequest, Resp extends IResponse, CC extends IConnectionConfiguration>
			IConnection<Req, Resp> create(
					IConfigurableConnectionFactory<Req, Resp, CC> factory, Iterable<CC> configurations) {
		List<IConnection<Req, Resp>> connections = new ArrayList<>();
		for (CC configuration : configurations) {
			try {
				connections.add(factory.create(configuration));
			} catch (ConnectionCreationException e) {
				logger.warn("Failed to create connection", e);
			}
		}
		return new RoundRobinConnection<Req, Resp>(connections);
	}
}
