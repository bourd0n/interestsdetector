package ru.ispras.crawler.core.connection.vkontakte.staticdata;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 28.05.14
 * Time: 15:42
 */

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteFacultiesRequest;

/**
 * see description in https://vk.com/dev/database.getFaculties
 */
public class VkontakteFacultiesConnection extends AbstractVkontakteIdTitleConnection<VkontakteFacultiesRequest>  {
    public VkontakteFacultiesConnection(IConnection<HttpRequest, HttpResponse> httpConnection) {
        super(httpConnection);
    }

    @Override
    protected String getMethod(VkontakteFacultiesRequest request) {
        return "database.getFaculties?university_id=" + request.getUniversityId();
    }
}
