package ru.ispras.crawler.core.connection.vkontakte.staticdata;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 28.05.14
 * Time: 15:46
 */

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteChairsRequest;

/**
 * see description in https://vk.com/dev/database.getChairs
 */

public class VkontakteChairsConnection extends AbstractVkontakteIdTitleConnection<VkontakteChairsRequest> {
    public VkontakteChairsConnection(IConnection<HttpRequest, HttpResponse> httpConnection) {
        super(httpConnection);
    }

    @Override
    protected String getMethod(VkontakteChairsRequest request) {
        return "database.getChairs?faculty_id=" + request.getFacultyId();
    }
}
