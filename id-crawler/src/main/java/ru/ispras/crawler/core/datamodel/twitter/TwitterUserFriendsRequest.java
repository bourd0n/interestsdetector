package ru.ispras.crawler.core.datamodel.twitter;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} for downloading Twitter user friends.
 *
 * See https://dev.twitter.com/rest/reference/get/friends/ids
 *
 * @author Ivan Andrianov
 * 
 */
public class TwitterUserFriendsRequest extends AbstractTwitterUserNeigboursRequest {
	public TwitterUserFriendsRequest(long id) {
		super(id);
	}
	
	public TwitterUserFriendsRequest(long id, int limit) {
		super(id, limit);
	}
}
