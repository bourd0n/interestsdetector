package ru.ispras.crawler.core.saver;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * {@link ISaver} implementation which does nothing.
 * 
 * @author Ivan Andrianov
 */
public class NoOpSaver<Req extends IRequest, Resp extends IResponse> implements ISaver<Req, Resp> {
	@Override
	public void save(Req request, Resp response) {
		// Nothing to do.
	}
}
