package ru.ispras.crawler.core.datamodel.google;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} for google.com. Contains query
 * Example: http://www.google.com/search?q=Crawler
 */
public class GoogleRequest implements IRequest {

	private String query;

	public GoogleRequest(String query) {
		this.query = query;
	}

	public String getQuery() {
		return query;
	}
	
	@Override
	public String toString() {
		return getClass().getName()+":[query=" + query + "]";
	}
}
