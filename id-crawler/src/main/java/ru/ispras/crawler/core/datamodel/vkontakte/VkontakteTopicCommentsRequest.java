package ru.ispras.crawler.core.datamodel.vkontakte;

import org.apache.commons.lang.Validate;

import ru.ispras.crawler.core.datamodel.vkontakte.entities.SortOrder;

/**
 * Request for https://vk.com/dev/board.getComments method
 * 
 * @author andrey
 *
 */
public class VkontakteTopicCommentsRequest extends AbstractVkontakteLimitedRequest {
	private final long groupId;
	private final long topicId;
	private final SortOrder sort;
	private final boolean needLikes;
	
	public VkontakteTopicCommentsRequest(long groupId, long topicId, SortOrder sort,
			boolean needLikes, int limit) {
		super(limit);
		Validate.notNull(sort);
		this.groupId = groupId;
		this.topicId = topicId;
		this.sort = sort;
		this.needLikes = needLikes;
	}
	
	public VkontakteTopicCommentsRequest(long groupId, long topicId, SortOrder sort,
			boolean needLikes) {
		super();
		Validate.notNull(sort);
		this.groupId = groupId;
		this.topicId = topicId;
		this.sort = sort;
		this.needLikes = needLikes;
	}
	
	public VkontakteTopicCommentsRequest(long groupId, long topicId, SortOrder sort) {
		this(groupId, topicId, sort, false);
	}
	
	public VkontakteTopicCommentsRequest(long groupId, long topicId) {
		this(groupId, topicId, SortOrder.ASC);
	}
	
	public long getGroupId() {
		return groupId;
	}
	
	public long getTopicId() {
		return topicId;
	}
	
	public SortOrder getSort() {
		return sort;
	}
	
	public Boolean getNeedLikes() {
		return needLikes;
	}

	@Override
	public String toString() {
		return "VkontakteTopicCommentsRequest [groupId=" + groupId
				+ ", topicId=" + topicId + ", sort=" + sort + ", needLikes="
				+ needLikes + "]";
	}	

}
