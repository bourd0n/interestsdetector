package ru.ispras.crawler.core.datamodel.vkontakte.entities.users;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 14.05.14
 * Time: 18:58
 */

import static ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils.booleanToFlag;
import static ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils.flagToBoolean;
import static ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils.genderToInt;
import static ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils.intToGender;

import java.util.List;

import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.IdTitle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.users.personal.Personal;
import ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils;

/** Holds the information about vk.com use
 * see description of this data structure in https://vk.com/dev/fields
 */
public class User {
    private final long id;
    private final String firstName;
    private final String lastName;
    private final Deactivated deactivated;
    private final Hidden hidden;
    private final Boolean verified;
    private final Boolean blacklisted;
    private final Gender gender;
    private final Date bday;
    private final IdTitle city; // the description of this field is wrong
    private final IdTitle country;
    private final String homeTown;
    private final String photo50;
    private final String photo100;
    private final String photo200orig;
    private final String photo200;
    private final String photo400orig;
    private final String photoMax;
    private final String photoMaxOrig;
    private final Online online;
    private final List<Long> lists;
    private final String domain;
    private final Boolean hasMobile;
    private final String mobileNumber;
    private final String homeNumber;
    private final Education education;
    private final List<University> universities;
    private final List<School> schools;
    private final String status;
    private final LastSeen lastSeen;
    private final Integer followersCount;
    private final Integer commonCount;
    private final Counters counters;
    private final Occupation occupation;
    private final String nickname;
    private final List<Relative> relatives;
    private final Relation relation;
    private final Personal personal;
    private final String skype;
    private final String twitter;
    private final String site;
    private final WallComments wallComments;
    private final String  activities;
    private final String  interests;
    private final String  music;
    private final String  movies;
    private final String  tv;
    private final String  books;
    private final String  games;
    private final String  about;
    private final String  quotes;
    private final Boolean canPost;
    private final Boolean canSeeAllPosts;
    private final Boolean canWritePrivateMessages;
    private final Integer timeZone;
    private final String screenName;


    @JsonCreator
    public User(
            @JsonProperty("id") long id,
            @JsonProperty("first_name") String firstName,
            @JsonProperty("last_name") String lastName,
            @JsonProperty("deactivated") String deactivated,
            @JsonProperty("hidden") Integer hidden,
            @JsonProperty("verified") Integer verified,
            @JsonProperty("blacklisted")  Integer blacklisted,
            @JsonProperty("sex")  Integer sex,
            @JsonProperty("bdate") String bday,
            @JsonProperty("city") IdTitle city,
            @JsonProperty("country")  IdTitle country,
            @JsonProperty("home_town")  String homeTown,
            @JsonProperty("photo_50") String photo50,
            @JsonProperty("photo_100") String photo100,
            @JsonProperty("photo_200_orig") String photo200orig,
            @JsonProperty("photo_200") String photo200,
            @JsonProperty("photo_400_orig") String photo400orig,
            @JsonProperty("photo_max") String photoMax,
            @JsonProperty("photo_max_orig") String photoMaxOrig,
            @JsonProperty("online") Integer online,
            @JsonProperty("lists") List<Long> lists,
            @JsonProperty("domain") String domain,
            @JsonProperty("has_mobile") Integer hasMobile,
            @JsonProperty("mobile_phone") String mobileNumber,
            @JsonProperty("home_phone") String homeNumber,

            @JsonProperty("university") int university,
            @JsonProperty("university_name") String universityName,
            @JsonProperty("faculty") int faculty,
            @JsonProperty("faculty_name") String facultyName,
            @JsonProperty("graduation") int graduation,

            @JsonProperty("universities") List<University> universities,
            @JsonProperty("schools") List<School> schools,
            @JsonProperty("status") String status,
            @JsonProperty("last_seen") LastSeen lastSeen,
            @JsonProperty("followers_count") Integer followersCount, // i do not sure that it field is ever presented
            @JsonProperty("common_count") Integer commonCount,
            @JsonProperty("counters") Counters counters,
            @JsonProperty("occupation") Occupation occupation,
            @JsonProperty("nickname") String nickname,
            @JsonProperty("relatives") List<Relative> relatives,
            @JsonProperty("relation") Integer relation,
            @JsonProperty("personal") List<Personal> personal,
            @JsonProperty("skype") String skype,
            @JsonProperty("twitter") String twitter,
            @JsonProperty("site") String site,
            @JsonProperty("wall_comments") Integer wallComments,
            @JsonProperty("activities") String activities,
            @JsonProperty("interests") String interests,
            @JsonProperty("music") String music,
            @JsonProperty("movies") String movies,
            @JsonProperty("tv") String tv,
            @JsonProperty("books") String books,
            @JsonProperty("games") String games,
            @JsonProperty("about") String about,
            @JsonProperty("quotes") String quotes,
            @JsonProperty("can_post") Integer canPost,
            @JsonProperty("can_see_all_posts") Integer canSeeAllPosts,
            @JsonProperty("can_write_private_message") Integer canWritePrivateMessages,
            @JsonProperty("timezone") Integer timeZone,
            @JsonProperty("screen_name") String screenName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.hidden = Hidden.intToHidden(hidden);
        this.verified = flagToBoolean(verified);
        this.blacklisted = flagToBoolean(blacklisted);
        this.gender = intToGender(sex);
        if (bday == null){
        	this.bday = null;
        }else{
        	this.bday = new Date(bday);
        }
        this.deactivated = Deactivated.stringToDeactevated(deactivated);
        this.city = city;
        this.country = country;
        this.homeTown = homeTown;
        this.photo50 = photo50;
        this.photo100 = photo100;
        this.photo200orig = photo200orig;
        this.photo200 = photo200;
        this.photo400orig = photo400orig;
        this.photoMax = photoMax;
        this.photoMaxOrig = photoMaxOrig;
        this.online = Online.intToOnline(online);
        this.lists = lists;
        this.domain = domain;
        this.hasMobile = flagToBoolean(hasMobile);
        this.mobileNumber = mobileNumber;
        this.homeNumber = homeNumber;
        this.education = new Education(university, universityName, faculty, facultyName, graduation);
        this.universities = universities;
        this.schools = schools;
        this.status = status;
        this.lastSeen = lastSeen;
        this.followersCount = followersCount;
        this.commonCount = commonCount;
        this.counters = counters;
        this.occupation = occupation;
        this.nickname = nickname;
        this.relatives = relatives;
        this.relation = Relation.intToRelation(relation);
        if (personal == null || personal.size() != 1){
        	this.personal = null;
        }else{
        	this.personal = personal.get(0);	
        }
        this.skype = skype;
        this.twitter = twitter;
        this.site = site;
        this.wallComments = WallComments.intToWallComments(wallComments);
        this.activities = activities;
        this.interests = interests;
        this.music = music;
        this.movies = movies;
        this.tv = tv;
        this.books = books;
        this.games = games;
        this.about = about;
        this.quotes = quotes;
        this.canPost = VkontakteUtils.flagToBoolean(canPost);
        this.canSeeAllPosts = VkontakteUtils.flagToBoolean(canSeeAllPosts);
        this.canWritePrivateMessages = VkontakteUtils.flagToBoolean(canWritePrivateMessages);
        this.timeZone = timeZone;
        this.screenName = screenName;
    }

    @JsonProperty("photo_max")
    public String getPhotoMax() {
        return photoMax;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("first_name")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("last_name")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("deactivated")
    public String getDeactivatedString() { return Deactivated.deactevatedToString(deactivated);}

    @JsonIgnore
    public Deactivated getDeactivated() { return deactivated;}

    @JsonProperty("hidden")
    public Integer getHiddenIndex() {return Hidden.hiddenToInt(hidden);}

    @JsonIgnore
    public Hidden getHidden() {return hidden;}

    @JsonProperty("verified")
    public Integer getVerified() {
        return booleanToFlag(verified);
    }

    @JsonProperty("blacklisted")
    public Integer getBlacklisted() {
        return booleanToFlag(blacklisted);
    }

    @JsonIgnore
    public Gender getGender() {
        return gender;
    }

    @JsonProperty("sex")
    public Integer getGenderId() {
    	return genderToInt(gender);
    }

    @JsonIgnore
    public Date getBday() {
        return bday;
    }
    
    @JsonProperty("bdate")
    public String getBdayString() {
    	if (bday == null) {
    		return null;	
    	}
        return bday.getString();
    }

    @JsonProperty("city")
    public IdTitle getCity() {
        return city;
    }

    @JsonProperty("country")
    public IdTitle getCountry() {
        return country;
    }

    @JsonProperty("home_town")
    public String getHomeTown() {
        return homeTown;
    }

    @JsonProperty("photo_50")
    public String getPhoto50() {
        return photo50;
    }

    @JsonProperty("photo_100")
    public String getPhoto100() {
        return photo100;
    }

    @JsonProperty("photo_200_orig")
    public String getPhoto200orig() {
        return photo200orig;
    }

    @JsonProperty("photo_200")
    public String getPhoto200() {
        return photo200;
    }

    @JsonProperty("photo_400_orig")
    public String getPhoto400orig() {
        return photo400orig;
    }

    @JsonProperty("photo_max_orig")
    public String getPhotoMaxOrig() {
        return photoMaxOrig;
    }

    @JsonProperty("online")
    public Integer getOnlineIndex() {return Online.onlineToInt(online);}

    @JsonIgnore
    public Online getOnline() {return online;}

    @JsonProperty("lists")
    public List<Long> getLists() {
        return lists;
    }

    @JsonProperty("domain")
    public String getDomain() {
        return domain;
    }

    @JsonProperty("has_mobile")
    public Integer getHasMobile() {
        return booleanToFlag(hasMobile);
    }

    @JsonProperty("mobile_phone")
    public String getMobileNumber() {
        return mobileNumber;
    }

    @JsonProperty("home_phone")
    public String getHomeNumber() {
        return homeNumber;
    }

    @JsonIgnore
    public Education getEducation() {
        return education;
    }

    @JsonProperty("university")
    public int getUniversity() {
        return education.getUniversity();
    }

    @JsonProperty("university_name")
    public String getUniversityName() {
        return education.getUniversityName();
    }

    @JsonProperty("faculty")
    public int getFaculty() {
        return education.getFaculty();
    }

    @JsonProperty("faculty_name")
    public String getFacultyName() {
        return education.getFacultyName();
    }

    @JsonProperty("graduation")
    public int getGraduation() {
        return education.getGraduation();
    }

    @JsonProperty("universities")
    public List<University> getUniversities() {
        return universities;
    }

    @JsonProperty("schools")
    public List<School> getSchools() {
        return schools;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("last_seen")
    public LastSeen getLastSeen() {
        return lastSeen;
    }

    @JsonProperty("followers_count")
    public Integer getFollowersCount() {
        return followersCount;
    }

    @JsonProperty("common_count")
    public Integer getCommonCount() {
        return commonCount;
    }

    @JsonProperty("counters")
    public Counters getCounters() {
        return counters;
    }

    @JsonProperty("occupation")
    public Occupation getOccupation() {
        return occupation;
    }

    @JsonProperty("nickname")
    public String getNickname() {
        return nickname;
    }

    @JsonProperty("relatives")
    public List<Relative> getRelatives() {
        return relatives;
    }

    @JsonProperty("relation")
    public Integer getRelationIndex() { return Relation.relationToInt(relation);}

    @JsonIgnore
    public Relation getRelation() { return relation;}

    @JsonProperty("personal")
    public Personal getPersonal() {
        return personal;
    }

    @JsonProperty("skype")
    public String getSkype() {
        return skype;
    }

    @JsonProperty("twitter")
    public String getTwitter() {
        return twitter;
    }

    @JsonProperty("site")
    public String getSite() {
        return site;
    }

    @JsonProperty("wall_comments")
    public Integer getWallCommentsIndex() {
        return WallComments.wallCommentsToInt(wallComments);
    }

    @JsonIgnore
    public WallComments getWallComments() {
        return wallComments;
    }

    @JsonProperty("activities")
    public String getActivities() {
        return activities;
    }

    @JsonProperty("interests")
    public String getInterests() {
        return interests;
    }

    @JsonProperty("music")
    public String getMusic() {
        return music;
    }

    @JsonProperty("movies")
    public String getMovies() {
        return movies;
    }

    @JsonProperty("tv")
    public String getTv() {
        return tv;
    }

    @JsonProperty("books")
    public String getBooks() {
        return books;
    }

    @JsonProperty("games")
    public String getGames() {
        return games;
    }

    @JsonProperty("about")
    public String getAbout() {
        return about;
    }

    @JsonProperty("quotes")
    public String getQuotes() {
        return quotes;
    }

    @JsonProperty("can_post")
    public Integer getCanPostIndex() {
        return VkontakteUtils.booleanToFlag(canPost);
    }

    @JsonIgnore
    public Boolean getCanPost() {
        return canPost;
    }

    @JsonProperty("can_see_all_posts")
    public Integer getCanSeeAllPostsIndex() {
        return VkontakteUtils.booleanToFlag(canSeeAllPosts);
    }

    @JsonIgnore
    public Boolean getCanSeeAllPosts() {
        return canSeeAllPosts;
    }



    @JsonProperty("can_write_private_message")
    public Integer getCanWritePrivateMessagesIndex() {
        return VkontakteUtils.booleanToFlag(canWritePrivateMessages);
    }

    @JsonIgnore
    public Boolean getCanWritePrivateMessages() {
        return canWritePrivateMessages;
    }

    @JsonProperty("timezone")
    public Integer getTimeZone() {
        return timeZone;
    }

    @JsonProperty("screen_name")
    public String getScreenName() {
        return screenName;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
