package ru.ispras.crawler.core.requestidconverter.facebook.api;

import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIProfileRequest;

public class FacebookAPIProfileRequestIdConverter extends
		AbstractFacebookAPIRequestIdConverter<FacebookAPIProfileRequest> {
	@Override
	public FacebookAPIProfileRequest getRequest(Long id) {
		return new FacebookAPIProfileRequest(id);
	}
}
