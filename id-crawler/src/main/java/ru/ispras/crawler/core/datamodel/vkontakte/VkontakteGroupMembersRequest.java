package ru.ispras.crawler.core.datamodel.vkontakte;


import org.apache.commons.lang.Validate;

import ru.ispras.crawler.core.datamodel.vkontakte.entities.SortOrder;

/**
 * Request for https://vk.com/dev/groups.getMembers method
 * 
 * @author andrey
 *
 */
public class VkontakteGroupMembersRequest extends AbstractVkontakteLimitedRequest {
	private final long groupId;
	private final SortOrder sort;
	
	public VkontakteGroupMembersRequest(long groupId, SortOrder sort) {
		super();
		Validate.notNull(sort);
		this.groupId = groupId;
		this.sort = sort;
	}
	
	public VkontakteGroupMembersRequest(long groupId, SortOrder sort, int limit) {
		super(limit);
		Validate.notNull(sort);
		this.groupId = groupId;
		this.sort = sort;
	}
	
	public VkontakteGroupMembersRequest(long groupId) {
		this(groupId, SortOrder.ASC); 
	}
	
	public VkontakteGroupMembersRequest(long groupId, int limit) {
		this(groupId, SortOrder.ASC, limit); 
	}

	public long getGroupId() {
		return groupId;
	}

	public SortOrder getSort() {
		return sort;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (groupId ^ (groupId >>> 32));
		result = prime * result + ((sort == null) ? 0 : sort.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VkontakteGroupMembersRequest other = (VkontakteGroupMembersRequest) obj;
		if (groupId != other.groupId)
			return false;
		if (sort != other.sort)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VkontakteGroupMembersRequest [groupId=" + groupId + ", sort="
				+ sort + "]";
	}
}
