package ru.ispras.crawler.core.requestidconverter.vkontakte;

import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteUserGroupsRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class VkontakteUserGroupsRequestIdConverter implements
		IRequestIdConverter<VkontakteUserGroupsRequest, Long> {

	@Override
	public Long getId(VkontakteUserGroupsRequest request) {
		return request.getUserId();
	}

	@Override
	public VkontakteUserGroupsRequest getRequest(Long id) {
		return new VkontakteUserGroupsRequest(id);
	}

}
