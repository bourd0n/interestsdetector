package ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import static ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils.flagToBoolean;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 16.05.14
 * Time: 17:48
 */

/**
 * see description in https://vk.com/dev/page
 */
public class Page  extends Attachment {
    private final Long id;
    private final Long groupId;
    private final Long creatorId;
    private final String title;
    private final Boolean currentUserCanEdit;
    private final Boolean currentUserCanEditAccess;
    private final AccessLevel whoCanView;
    private final AccessLevel whoCanEdit;
    private final Long edited;
    private final Long created;
    private final Long editorId;
    private final int views;
    private final String parent;
    private final String parent2;
    private final String source;
    private final String html;
    private final String viewUrl;

    @JsonCreator
    public Page(
            @JsonProperty("id") Long id,
            @JsonProperty("group_id") Long groupId,
            @JsonProperty("creator_id") Long creatorId,
            @JsonProperty("title") String title,
            @JsonProperty("current_user_can_edit") int currentUserCanEdit,
            @JsonProperty("current_user_can_edit_access") int currentUserCanEditAccess,
            @JsonProperty("who_can_view") int whoCanView,
            @JsonProperty("who_can_edit") int whoCanEdit,
            @JsonProperty("edited") Long edited,
            @JsonProperty("created") Long created,
            @JsonProperty("editor_id") Long editorId,
            @JsonProperty("views") int views,
            @JsonProperty("parent") String parent,
            @JsonProperty("parent2") String parent2,
            @JsonProperty("source") String source,
            @JsonProperty("html") String html,
            @JsonProperty("view_url") String viewUrl) {
        this.id = id;
        this.groupId = groupId;
        this.creatorId = creatorId;
        this.title = title;
        this.currentUserCanEdit = flagToBoolean(currentUserCanEdit);
        this.currentUserCanEditAccess = flagToBoolean(currentUserCanEditAccess);
        this.whoCanView = AccessLevel.intToAccessLevel(whoCanView);
        this.whoCanEdit = AccessLevel.intToAccessLevel(whoCanEdit);
        this.edited = edited;
        this.created = created;
        this.editorId = editorId;
        this.views = views;
        this.parent = parent;
        this.parent2 = parent2;
        this.source = source;
        this.html = html;
        this.viewUrl = viewUrl;
    }
    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("group_id")
    public Long getGroupId() {
        return groupId;
    }

    @JsonProperty("creator_id")
    public Long getCreatorId() {
        return creatorId;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("current_user_can_edit")
    public Boolean getCurrentUserCanEdit() {
        return currentUserCanEdit;
    }

    @JsonProperty("current_user_can_edit_access")
    public Boolean getCurrentUserCanEditAccess() {
        return currentUserCanEditAccess;
    }

    @JsonProperty("who_can_view")
    public int getWhoCanViewIndex() {
        return AccessLevel.AccessLevelToInt(whoCanView);
    }

    @JsonIgnore
    public AccessLevel getWhoCanView() {
        return whoCanView;
    }

    @JsonProperty("who_can_edit")
    public int getWhoCanEditIndex() {
        return AccessLevel.AccessLevelToInt(whoCanEdit);
    }

    @JsonIgnore
    public AccessLevel getWhoCanEdit() {
        return whoCanEdit;
    }

    @JsonProperty("edited")
    public Long getEdited() {
        return edited;
    }

    @JsonProperty("created")
    public Long getCreated() {
        return created;
    }

    @JsonProperty("editor_id")
    public Long getEditorId() {
        return editorId;
    }

    @JsonProperty("views")
    public int getViews() {
        return views;
    }

    @JsonProperty("parent")
    public String getParent() {
        return parent;
    }

    @JsonProperty("parent2")
    public String getParent2() {
        return parent2;
    }

    @JsonProperty("source")
    public String getSource() {
        return source;
    }

    @JsonProperty("html")
    public String getHtml() {
        return html;
    }

    @JsonProperty("view_url")
    public String getViewUrl() {
        return viewUrl;
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creatorId == null) ? 0 : creatorId.hashCode());
		result = prime * result
				+ ((editorId == null) ? 0 : editorId.hashCode());
		result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Page other = (Page) obj;
		if (creatorId == null) {
			if (other.creatorId != null)
				return false;
		} else if (!creatorId.equals(other.creatorId))
			return false;
		if (editorId == null) {
			if (other.editorId != null)
				return false;
		} else if (!editorId.equals(other.editorId))
			return false;
		if (groupId == null) {
			if (other.groupId != null)
				return false;
		} else if (!groupId.equals(other.groupId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}  
}
