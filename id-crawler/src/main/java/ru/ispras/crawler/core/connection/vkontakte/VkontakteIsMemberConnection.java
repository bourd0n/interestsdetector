package ru.ispras.crawler.core.connection.vkontakte;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteIsMemberRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.Member;

/**
 * Connection for https://vk.com/dev/groups.getMembers method
 * 
 * @author andrey
 *
 */
public class VkontakteIsMemberConnection implements IConnection<VkontakteIsMemberRequest, BasicResponse<List<Member>>> {

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	public VkontakteIsMemberConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}
	
	private String getMethod(VkontakteIsMemberRequest request) {
		return "groups.isMember?group_id="+request.getGroupId()+
				"&user_ids="+StringUtils.join(request.getUserIds(),',');
	}
	
	@Override
	public BasicResponse<List<Member>> getResponse(
			VkontakteIsMemberRequest request)
			throws UnavailableConnectionException {
		String url = getMethod(request);
		List<Member> response = VkontakteConnectionUtils.getSingleListResponse(url, httpConnection, Member.class);
		return new BasicResponse<List<Member>>(response);
	}

}
