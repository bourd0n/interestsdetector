package ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 10.06.14
 * Time: 18:20
 */
public class School {
    private final long id;
    private final String title;
    private final Long cityId;

    @JsonCreator
    public School(
            @JsonProperty("id") long id,
            @JsonProperty("title") String title,
            @JsonProperty("city_id") Long cityId) {
        this.id = id;
        this.title = title;
        this.cityId = cityId;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("city_id")
    public Long getCityId() {
        return cityId;
    }


}
