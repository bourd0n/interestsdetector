package ru.ispras.crawler.core.connection.http.auth.accesstoken;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ru.ispras.crawler.core.connection.http.HttpConnectionConfiguration;
import ru.ispras.crawler.core.connection.http.HttpProxyDescription;

/**
 * 
 * @author andrey
 * 
 */
@XmlRootElement(name = "accessToken")
public class AccessTokenConfiguration extends HttpConnectionConfiguration {
	private String token;

	public AccessTokenConfiguration() {
	}

	public AccessTokenConfiguration(HttpProxyDescription proxy, String token) {
		super(proxy);
		this.token = token;
	}

	public AccessTokenConfiguration(HttpProxyDescription proxy, Integer connectionTimeout,
			Integer socketTimeout, String token) {
		super(proxy, connectionTimeout, socketTimeout);
		this.token = token;
	}

	public AccessTokenConfiguration(String token) {
		this.token = token;
	}

	@XmlElement(required = true)
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
