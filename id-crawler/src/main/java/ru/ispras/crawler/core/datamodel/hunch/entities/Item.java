package ru.ispras.crawler.core.datamodel.hunch.entities;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Item {
	private final long id;
	private final String title;
	private final List<String> tags;
	private final List<String> domains;
	private final String pictureLink;
	private final float averageRating;
	private final Map<String, String> info;
	
	@JsonCreator
	public Item(
			@JsonProperty("id") long id,
			@JsonProperty("title") String title,
			@JsonProperty("tags") List<String> tags,
			@JsonProperty("domains") List<String> domains,
			@JsonProperty("pictureLink") String pictureLink,
			@JsonProperty("averageRating") float averageRating,
			@JsonProperty("info") Map<String, String> info) {
		this.id = id;
		this.title = title;
		this.tags = tags;
		this.domains = domains;
		this.pictureLink = pictureLink;
		this.averageRating = averageRating;
		this.info = info;
	}

	@JsonProperty("id")
	public long getId() {
		return id;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("tags")
	public List<String> getTags() {
		return tags;
	}

	@JsonProperty("domains")
	public List<String> getDomains() {
		return domains;
	}

	@JsonProperty("pictureLink")
	public String getPictureLink() {
		return pictureLink;
	}

	@JsonProperty("averageRating")
	public float getAverageRating() {
		return averageRating;
	}

	@JsonProperty("info")
	public Map<String, String> getInfo() {
		return info;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
