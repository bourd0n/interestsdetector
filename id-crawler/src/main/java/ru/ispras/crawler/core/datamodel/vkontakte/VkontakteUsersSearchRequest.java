package ru.ispras.crawler.core.datamodel.vkontakte;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.users.UserQuery;

/**
 * Request for https://vk.com/dev/users.search method
 * 
 * @author andrey
 *
 */
public class VkontakteUsersSearchRequest implements IRequest {
	private final UserQuery query;

	public VkontakteUsersSearchRequest(UserQuery query) {
		super();
		this.query = new UserQuery(query);
	}

	public UserQuery getQuery() {
		return query;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((query == null) ? 0 : query.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VkontakteUsersSearchRequest other = (VkontakteUsersSearchRequest) obj;
		if (query == null) {
			if (other.query != null)
				return false;
		} else if (!query.equals(other.query))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VkontakteUsersSearchRequest [query=" + query + "]";
	}

}