package ru.ispras.crawler.core.connection.facebook.api.http;

import ru.ispras.crawler.core.connection.IConfigurableConnectionFactory;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.ConnectionCreationException;
import ru.ispras.crawler.core.connection.http.HttpConnection;
import ru.ispras.crawler.core.connection.http.auth.accesstoken.AccessTokenHttpRequestConverter;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpClientFactory;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpResponseConverter;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * Facebook API {@link IConfigurableConnectionFactory} implementation.
 * 
 * @author Ivan Andrianov
 * 
 */
public class FacebookAPIHttpConnectionFactory implements
		IConfigurableConnectionFactory<HttpRequest, HttpResponse, FacebookAPIHttpConnectionConfiguration> {
	@Override
	public IConnection<HttpRequest, HttpResponse> create(FacebookAPIHttpConnectionConfiguration configuration)
			throws ConnectionCreationException {
		return new HttpConnection<>(new AccessTokenHttpRequestConverter(configuration, "access_token"), new DefaultHttpClientFactory(),
				configuration, new DefaultHttpResponseConverter());
	}
}
