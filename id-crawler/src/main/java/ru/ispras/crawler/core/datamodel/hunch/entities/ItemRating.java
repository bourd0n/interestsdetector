package ru.ispras.crawler.core.datamodel.hunch.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ItemRating {
	private final long id;
	private final int rating;
	
	@JsonCreator
	public ItemRating(
			@JsonProperty("id") long id,
			@JsonProperty("rating") int rating) {
		this.id = id;
		this.rating = rating;
	}

	@JsonProperty("id")
	public long getId() {
		return id;
	}

	@JsonProperty("rating")
	public int getRating() {
		return rating;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + rating;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemRating other = (ItemRating) obj;
		if (id != other.id)
			return false;
		if (rating != other.rating)
			return false;
		return true;
	}
}
