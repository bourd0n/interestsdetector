package ru.ispras.crawler.core.datamodel.vkontakte.entities.users;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 15.05.14
 * Time: 13:12
 */
public class LastSeen {
    private final Long time;
    private final int platform;

    @JsonCreator
    LastSeen(
            @JsonProperty("time") Long time,
            @JsonProperty("platform") int platform) {
        this.time = time;
        this.platform = platform;
    }

    @JsonProperty("time")
    public Long getTime() {
        return time;
    }

    @JsonProperty("platform")
    public int getPlatform() {
        return platform;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + platform;
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LastSeen other = (LastSeen) obj;
		if (platform != other.platform)
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}  
}
