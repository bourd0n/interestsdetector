package ru.ispras.crawler.core.datamodel.hunch;

import ru.ispras.crawler.core.datamodel.IRequest;
/**
 *
 * {@link IRequest} for downloading list of items, rated by given Hunch user.
 * Example: https://hunch.com/ndrey/ratings
 */
public class HunchUserItemRatingsRequest extends AbstractHunchUserRequest {
	public HunchUserItemRatingsRequest(String screenName) {
		super(screenName);
	}
}
