package ru.ispras.crawler.core.datamodel.hunch;

import org.apache.commons.lang.Validate;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Abstract {@link IRequest} that is processed via parsing Hunch HTML pages related to user.
 */
public abstract class AbstractHunchUserRequest implements IRequest {
	private final String screenName;

	public AbstractHunchUserRequest(String screenName) {
		Validate.notNull(screenName, "Screen name should not be null.");
		this.screenName = screenName;
	}

	public String getScreenName() {
		return screenName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + screenName.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		AbstractHunchUserRequest other = (AbstractHunchUserRequest) obj;
		return screenName.equals(other.screenName);
	}
	
	@Override
	public String toString() {
		return getClass().getName()+":[screenName=" + screenName + "]";
	}
}
