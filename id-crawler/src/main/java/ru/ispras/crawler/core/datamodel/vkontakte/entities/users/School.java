package ru.ispras.crawler.core.datamodel.vkontakte.entities.users;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 15.05.14
 * Time: 13:04
 * To change this template use File | Settings | File Templates.
 */
public class School {
    private final Long id;
    private final int country;
    private final int city;
    private final String name;
    private final int yearFrom;
    private final int yearTo;
    private final int yearGraduated;
    private final String numberOfClass;
    private final String speciality;
    private final int type;
    private final String typeString;

    @JsonCreator
    public School(
            @JsonProperty("id") Long id,
            @JsonProperty("country") int country,
            @JsonProperty("city") int city,
            @JsonProperty("name") String name,
            @JsonProperty("year_from") int yearFrom,
            @JsonProperty("year_to") int yearTo,
            @JsonProperty("year_graduated") int yearGraduated,
            @JsonProperty("class") String numberOfClass,
            @JsonProperty("speciality") String speciality,
            @JsonProperty("type") int type,
            @JsonProperty("type_str") String typeString) {
        this.id = id;
        this.country = country;
        this.city = city;
        this.name = name;
        this.yearFrom = yearFrom;
        this.yearTo = yearTo;
        this.yearGraduated = yearGraduated;
        this.numberOfClass = numberOfClass;
        this.speciality = speciality;
        this.type = type;
        this.typeString = typeString;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("country")
    public int getCountry() {
        return country;
    }

    @JsonProperty("city")
    public int getCity() {
        return city;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("year_from")
    public int getYearFrom() {
        return yearFrom;
    }

    @JsonProperty("year_to")
    public int getYearTo() {
        return yearTo;
    }

    @JsonProperty("year_graduated")
    public int getYearGraduated() {
        return yearGraduated;
    }

    @JsonProperty("class")
    public String getNumberOfClass() {
        return numberOfClass;
    }

    @JsonProperty("speciality")
    public String getSpeciality() {
        return speciality;
    }

    @JsonProperty("type")
    public int getType() {
        return type;
    }

    @JsonProperty("type_str")
    public String getTypeString() {
        return typeString;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + city;
		result = prime * result + country;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((numberOfClass == null) ? 0 : numberOfClass.hashCode());
		result = prime * result
				+ ((speciality == null) ? 0 : speciality.hashCode());
		result = prime * result + type;
		result = prime * result
				+ ((typeString == null) ? 0 : typeString.hashCode());
		result = prime * result + yearFrom;
		result = prime * result + yearGraduated;
		result = prime * result + yearTo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		School other = (School) obj;
		if (city != other.city)
			return false;
		if (country != other.country)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (numberOfClass == null) {
			if (other.numberOfClass != null)
				return false;
		} else if (!numberOfClass.equals(other.numberOfClass))
			return false;
		if (speciality == null) {
			if (other.speciality != null)
				return false;
		} else if (!speciality.equals(other.speciality))
			return false;
		if (type != other.type)
			return false;
		if (typeString == null) {
			if (other.typeString != null)
				return false;
		} else if (!typeString.equals(other.typeString))
			return false;
		if (yearFrom != other.yearFrom)
			return false;
		if (yearGraduated != other.yearGraduated)
			return false;
		if (yearTo != other.yearTo)
			return false;
		return true;
	}
}
