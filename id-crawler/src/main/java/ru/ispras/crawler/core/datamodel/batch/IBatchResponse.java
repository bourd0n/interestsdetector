package ru.ispras.crawler.core.datamodel.batch;

import java.util.Map;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * Abstraction for entity classes which stores information about some collection
 * of responses to be performed at once
 * <p>
 * Implementations are expected to be immutable.
 * 
 * @author andrey g
 */
public interface IBatchResponse<Req extends IRequest, Resp extends IResponse> extends IResponse {
	Map<Req, Resp> getRequestToResponseMap();
}
