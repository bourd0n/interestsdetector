package ru.ispras.crawler.core.datamodel.vkontakte.entities.stats;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 19.05.14
 * Time: 19:03
 */
public class VisitorsByProperty {
    private final int visitors;
    private final String value;

    @JsonCreator
    public VisitorsByProperty(
            @JsonProperty("visitors") int visitors,
            @JsonProperty("value") String value) {
        this.visitors = visitors;
        this.value = value;
    }

    @JsonProperty("visitors")
    public int getVisitors() {
        return visitors;
    }

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + visitors;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VisitorsByProperty other = (VisitorsByProperty) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		if (visitors != other.visitors)
			return false;
		return true;
	}
}
