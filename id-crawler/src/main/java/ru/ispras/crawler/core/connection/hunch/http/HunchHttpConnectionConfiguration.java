package ru.ispras.crawler.core.connection.hunch.http;

import javax.xml.bind.annotation.XmlRootElement;

import ru.ispras.crawler.core.connection.http.auth.cookie.CookieAuthHttpConnectionConfiguration;

@XmlRootElement(name = "hunch")
public class HunchHttpConnectionConfiguration extends CookieAuthHttpConnectionConfiguration {
	public HunchHttpConnectionConfiguration() {
	}

	public HunchHttpConnectionConfiguration(String login, String password) {
		super(login, password);
	}
}
