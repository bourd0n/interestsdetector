package ru.ispras.crawler.core.requestidconverter.facebook;

import ru.ispras.crawler.core.datamodel.facebook.FacebookUserFavoritesRequest;

public class FacebookUserFavoritesRequestIdConverter extends
		AbstractFacebookUserRequestIdConverter<FacebookUserFavoritesRequest> {
	@Override
	public FacebookUserFavoritesRequest getRequest(Long id) {
		return new FacebookUserFavoritesRequest(id);
	}
}
