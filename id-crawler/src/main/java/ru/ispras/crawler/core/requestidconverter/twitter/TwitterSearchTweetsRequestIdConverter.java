package ru.ispras.crawler.core.requestidconverter.twitter;

import ru.ispras.crawler.core.datamodel.twitter.TwitterSearchTweetsRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class TwitterSearchTweetsRequestIdConverter implements IRequestIdConverter<TwitterSearchTweetsRequest, String> {
	@Override
	public String getId(TwitterSearchTweetsRequest request) {
		return request.getTextQuery();
	}

	@Override
	public TwitterSearchTweetsRequest getRequest(String textQuery) {
		return new TwitterSearchTweetsRequest(textQuery);
	}
}
