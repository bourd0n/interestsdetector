/*
 * Copyright 2012 Ryuji Yamashita
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.facebook.api.entities;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import ru.ispras.crawler.core.datamodel.facebook.api.utils.FacebookAPIDateConverter;
import ru.ispras.crawler.core.datamodel.utils.DateConverter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A data class representing Basic user information element
 *
 * @author Ryuji Yamashita - roundrop at gmail.com
 */
public class User {
 
    private final String id;
    private final String name;
    private final String firstName;
    private final String middleName;
    private final String lastName;
    private final String gender;
    private final Locale locale;
    private final List<IdNameEntity> languages;
    private final String link;
    private final String username;
    private final String thirdPartyId;
    private final Boolean installed;
    private final Double timezone;
    private final Date updatedTime;
    private final Boolean verified;
    private final String bio;
    private final String birthday;
    private final Cover cover;
    private final List<User.Education> education;
    private final String email;
    private final IdNameEntity hometown;
    private final List<String> interestedIn;
    private final IdNameEntity location;
    private final String political;
    private final List<IdNameEntity> favoriteAthletes;
    private final List<IdNameEntity> favoriteTeams;
    private final Picture picture;
    private final String quotes;
    private final String relationshipStatus;
    private final String religion;
    private final IdNameEntity significantOther;
    private final User.VideoUploadLimits videoUploadLimits;
    private final String website;
    private final List<User.Work> work;
    
    private final static DateConverter dateConverter = new FacebookAPIDateConverter();
    
    @JsonCreator
    public User(
    		@JsonProperty("id") String id,
    		@JsonProperty("name") String name,
    		@JsonProperty("first_name") String firstName,
    		@JsonProperty("middle_name") String middleName,
    		@JsonProperty("last_name") String lastName,
    		@JsonProperty("gender") String gender,
    		@JsonProperty("locale") String locale,
    		@JsonProperty("languages") List<IdNameEntity> languages,
    		@JsonProperty("link") String link,
    		@JsonProperty("username") String username,
    		@JsonProperty("third_party_id") String thirdPartyId,
    		@JsonProperty("installed") Boolean installed,
    		@JsonProperty("timezone") Double timezone,
    		@JsonProperty("updated_time") String updatedTime,
    		@JsonProperty("verified") Boolean verified,
    		@JsonProperty("bio") String bio,
    		@JsonProperty("birthday") String birthday,
    		@JsonProperty("cover") Cover cover,
    		@JsonProperty("education") List<Education> education,
    		@JsonProperty("email") String email,
    		@JsonProperty("hometown") IdNameEntity hometown,
    		@JsonProperty("interested_in") List<String> interestedIn,
    		@JsonProperty("location") IdNameEntity location,
    		@JsonProperty("political") String political,
    		@JsonProperty("favorite_athletes") List<IdNameEntity> favoriteAthletes,
    		@JsonProperty("favorite_teams") List<IdNameEntity> favoriteTeams,
    		@JsonProperty("picture") Picture picture, 
    		@JsonProperty("quotes") String quotes,
    		@JsonProperty("relationship_status") String relationshipStatus,
    		@JsonProperty("religion") String religion,
    		@JsonProperty("significant_other") IdNameEntity significantOther,
    		@JsonProperty("video_upload_limits") VideoUploadLimits videoUploadLimits,
    		@JsonProperty("website") String website,
 			@JsonProperty("work") List<Work> work) {
 		this.id = id;
 		this.name = name;
 		this.firstName = firstName;
 		this.middleName = middleName;
 		this.lastName = lastName;
 		this.gender = gender;
 		this.locale = parseLocale(locale);
 		this.languages = languages;
 		this.link = link;
 		this.username = username;
 		this.thirdPartyId = thirdPartyId;
 		this.installed = installed;
 		this.timezone = timezone;
 		this.updatedTime = dateConverter.getDate(updatedTime);
 		this.verified = verified;
 		this.bio = bio;
 		this.birthday = birthday;
 		this.cover = cover;
 		this.education = education;
 		this.email = email;
 		this.hometown = hometown;
 		this.interestedIn = interestedIn;
 		this.location = location;
 		this.political = political;
 		this.favoriteAthletes = favoriteAthletes;
 		this.favoriteTeams = favoriteTeams;
 		this.picture = picture;
 		this.quotes = quotes;
 		this.relationshipStatus = relationshipStatus;
 		this.religion = religion;
 		this.significantOther = significantOther;
 		this.videoUploadLimits = videoUploadLimits;
 		this.website = website;
 		this.work = work;
 	}
 
    @JsonProperty("id")
	public String getId() {
        return id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("first_name") 
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("middle_name")
    public String getMiddleName() {
        return middleName;
    }

    @JsonProperty("last_name")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    @JsonIgnore
    public Locale getLocale() {
        return locale;
    }
    
	@JsonProperty("locale")    
	public String getLocaleString(){
		if (locale == null){
			return null;
		}
		return locale.getLanguage()+"_"+locale.getCountry();
	}

	@JsonProperty("languages")
    public List<IdNameEntity> getLanguages() {
        return languages;
    }
	
	@JsonProperty("link")
    public String getLink() {
        return link;
    }
	
	@JsonProperty("username") 
    public String getUsername() {
        return username;
    }
	
	@JsonProperty("third_party_id")
    public String getThirdPartyId() {
        return thirdPartyId;
    }
	
	@JsonProperty("installed")
    public Boolean isInstalled() {
        return installed;
    }
	
	@JsonProperty("timezone")
    public Double getTimezone() {
        return timezone;
    }

    @JsonIgnore
    public Date getUpdatedTime() {
        return updatedTime;
    }
    
    @JsonProperty("updated_time")
    public String getUpdatedTimeString() {
        return dateConverter.getString(updatedTime);
    }
    
    @JsonProperty("verified")
    public Boolean isVerified() {
        return verified;
    }
    
    @JsonProperty("bio")
    public String getBio() {
        return bio;
    }

    @JsonProperty("birthday")
    public String getBirthday() {
        return birthday;
    }
    
    @JsonProperty("cover")
    public Cover getCover() {
        return cover;
    }
    
    @JsonProperty("education")
    public List<Education> getEducation() {
        return education;
    }
    
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }
    
    @JsonProperty("hometown")
    public IdNameEntity getHometown() {
        return hometown;
    }
    
    @JsonProperty("interested_in")
    public List<String> getInterestedIn() {
        return interestedIn;
    }
    
    @JsonProperty("location")
    public IdNameEntity getLocation() {
        return location;
    }
    
    @JsonProperty("political")
    public String getPolitical() {
        return political;
    }

    @JsonProperty("favorite_athletes")
    public List<IdNameEntity> getFavoriteAthletes() {
        return favoriteAthletes;
    }

    @JsonProperty("favorite_teams")
    public List<IdNameEntity> getFavoriteTeams() {
        return favoriteTeams;
    }

    @JsonProperty("picture")
    public Picture getPicture() {
        return picture;
    }
    
    @JsonProperty("quotes")
    public String getQuotes() {
        return quotes;
    }
    
    @JsonProperty("relationship_status")
    public String getRelationshipStatus() {
        return relationshipStatus;
    }
    
    @JsonProperty("religion")
    public String getReligion() {
        return religion;
    }
    
    @JsonProperty("significant_other")
    public IdNameEntity getSignificantOther() {
        return significantOther;
    }
    
    @JsonProperty("video_upload_limits")
    public User.VideoUploadLimits getVideoUploadLimits() {
        return videoUploadLimits;
    }
    
    @JsonProperty("website")
    public String getWebsite() {
        return website;
    }
    
    @JsonProperty("work")
    public List<Work> getWork() {
        return work;
    }
   
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (null == obj) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return obj instanceof User && ((User) obj).getId().equals(this.id);
    }
    
	//from facebook4j    
	private Locale parseLocale(String locale){
		if (locale == null){
			return null;
		}
		String[] _locale = locale.split("_");
	    String language = _locale[0];
	    String country = _locale[1];
		return new Locale(language, country);
	}

   public static class Education{
        private final IdNameEntity year;
        private final String type;
        private final IdNameEntity school;
        private final IdNameEntity degree;
        private final List<IdNameEntity> concentration;
        private final List<EducationClass> classes;
        private final List<IdNameEntity> with;
  
        @JsonCreator
        public Education(
        		@JsonProperty("year") IdNameEntity year,
        		@JsonProperty("type") String type,
        		@JsonProperty("school") IdNameEntity school,
        		@JsonProperty("degree") IdNameEntity degree,
        		@JsonProperty("concentration") List<IdNameEntity> concentration,
        		@JsonProperty("classes") List<EducationClass> classes,
        		@JsonProperty("with") List<IdNameEntity> with) {
			this.year = year;
			this.type = type;
			this.school = school;
			this.degree = degree;
			this.concentration = concentration;
			this.classes = classes;
			this.with = with;
		}

        @JsonProperty("year")
		public IdNameEntity getYear() {
            return year;
        }
        
        @JsonProperty("type")
        public String getType() {
            return type;
        }
        
        @JsonProperty("school")
        public IdNameEntity getSchool() {
            return school;
        }
        
        @JsonProperty("degree")
        public IdNameEntity getDegree() {
            return degree;
        }
        
        @JsonProperty("concentration")
        public List<IdNameEntity> getConcentration() {
            return concentration;
        }
        
        @JsonProperty("classes")
        public List<EducationClass> getClasses() {
            return classes;
        }
        
        @JsonProperty("with")
        public List<IdNameEntity> getWith() {
            return with;
        }

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((classes == null) ? 0 : classes.hashCode());
			result = prime * result
					+ ((concentration == null) ? 0 : concentration.hashCode());
			result = prime * result
					+ ((degree == null) ? 0 : degree.hashCode());
			result = prime * result
					+ ((school == null) ? 0 : school.hashCode());
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			result = prime * result + ((with == null) ? 0 : with.hashCode());
			result = prime * result + ((year == null) ? 0 : year.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Education other = (Education) obj;
			if (classes == null) {
				if (other.classes != null)
					return false;
			} else if (!classes.equals(other.classes))
				return false;
			if (concentration == null) {
				if (other.concentration != null)
					return false;
			} else if (!concentration.equals(other.concentration))
				return false;
			if (degree == null) {
				if (other.degree != null)
					return false;
			} else if (!degree.equals(other.degree))
				return false;
			if (school == null) {
				if (other.school != null)
					return false;
			} else if (!school.equals(other.school))
				return false;
			if (type == null) {
				if (other.type != null)
					return false;
			} else if (!type.equals(other.type))
				return false;
			if (with == null) {
				if (other.with != null)
					return false;
			} else if (!with.equals(other.with))
				return false;
			if (year == null) {
				if (other.year != null)
					return false;
			} else if (!year.equals(other.year))
				return false;
			return true;
		}

    }

    public static class EducationClass{
        private final List<IdNameEntity> with;
        private final String description;

        
        @JsonCreator
        public EducationClass(
        		@JsonProperty("with") List<IdNameEntity> with,
        		@JsonProperty("description") String description) {
			this.with = with;
			this.description = description;
		}

        @JsonProperty("with")
		public List<IdNameEntity> getWith() {
            return with;
        }

        @JsonProperty("description")
        public String getDescription() {
            return description;
        }

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((description == null) ? 0 : description.hashCode());
			result = prime * result + ((with == null) ? 0 : with.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			EducationClass other = (EducationClass) obj;
			if (description == null) {
				if (other.description != null)
					return false;
			} else if (!description.equals(other.description))
				return false;
			if (with == null) {
				if (other.with != null)
					return false;
			} else if (!with.equals(other.with))
				return false;
			return true;
		}
    }

    public static class VideoUploadLimits {
        private final long length;
        private final long size;

        @JsonCreator
        public VideoUploadLimits(
        		@JsonProperty("length") long length,
        		@JsonProperty("size") long size) {
        	this.length = length;
        	this.size = size;
        }        
        
        @JsonProperty("length")
        public long getLength() {
            return length;
        }

        @JsonProperty("size")
		public long getSize() {
            return size;
        }

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (length ^ (length >>> 32));
			result = prime * result + (int) (size ^ (size >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			VideoUploadLimits other = (VideoUploadLimits) obj;
			if (length != other.length)
				return false;
			if (size != other.size)
				return false;
			return true;
		}
    }

    public static class Work {
        private final IdNameEntity employer;
        private final IdNameEntity location;
        private final IdNameEntity position;
        private final String startDate;
        private final String endDate;

        @JsonCreator
        public Work(
        		@JsonProperty("employer") IdNameEntity employer,
        		@JsonProperty("location") IdNameEntity location,
        		@JsonProperty("position") IdNameEntity position,
        		@JsonProperty("start_date") String startDate,
        		@JsonProperty("end_date") String endDate) {
			this.employer = employer;
			this.location = location;
			this.position = position;
			this.startDate = startDate;
			this.endDate = endDate;
		}

        @JsonProperty("employer")
		public IdNameEntity getEmployer() {
            return employer;
        }

        @JsonProperty("location")
        public IdNameEntity getLocation() {
            return location;
        }

        @JsonProperty("position")
        public IdNameEntity getPosition() {
            return position;
        }

        @JsonProperty("start_date")
        public String getStartDate() {
            return startDate;
        }

        @JsonProperty("end_date")
        public String getEndDate() {
            return endDate;
        }

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((employer == null) ? 0 : employer.hashCode());
			result = prime * result
					+ ((endDate == null) ? 0 : endDate.hashCode());
			result = prime * result
					+ ((location == null) ? 0 : location.hashCode());
			result = prime * result
					+ ((position == null) ? 0 : position.hashCode());
			result = prime * result
					+ ((startDate == null) ? 0 : startDate.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Work other = (Work) obj;
			if (employer == null) {
				if (other.employer != null)
					return false;
			} else if (!employer.equals(other.employer))
				return false;
			if (endDate == null) {
				if (other.endDate != null)
					return false;
			} else if (!endDate.equals(other.endDate))
				return false;
			if (location == null) {
				if (other.location != null)
					return false;
			} else if (!location.equals(other.location))
				return false;
			if (position == null) {
				if (other.position != null)
					return false;
			} else if (!position.equals(other.position))
				return false;
			if (startDate == null) {
				if (other.startDate != null)
					return false;
			} else if (!startDate.equals(other.startDate))
				return false;
			return true;
		}
    }

}
