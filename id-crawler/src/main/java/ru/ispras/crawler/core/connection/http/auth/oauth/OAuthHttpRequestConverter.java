package ru.ispras.crawler.core.connection.http.auth.oauth;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.apache.http.client.methods.HttpUriRequest;

import ru.ispras.crawler.core.connection.exception.TargetUnreachableException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpRequestConverter;
import ru.ispras.crawler.core.connection.http.ext.IHttpRequestConverter;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;

/**
 * {@link IHttpRequestConverter} implementation which uses other
 * {@link IHttpRequestConverter} and signs the resulting {@link HttpUriRequest}
 * using {@link OAuthConsumer}.
 * 
 * @author Ivan Andrianov
 * 
 */
public class OAuthHttpRequestConverter implements IHttpRequestConverter {
	private final IHttpRequestConverter wrapped;
	private final OAuthConsumer consumer;

	public OAuthHttpRequestConverter(OAuthHttpConnectionConfiguration configuration) {
		this(new DefaultHttpRequestConverter(), configuration);
	}

	public OAuthHttpRequestConverter(IHttpRequestConverter wrapped, OAuthHttpConnectionConfiguration configuration) {
		this.wrapped = wrapped;
		consumer = new CommonsHttpOAuthConsumer(configuration.getConsumerKey(), configuration.getConsumerSecret());
		consumer.setTokenWithSecret(configuration.getAccessToken(), configuration.getTokenSecret());
	}

	@Override
	public HttpUriRequest convert(HttpRequest request) throws UnavailableConnectionException {
		HttpUriRequest result = wrapped.convert(request);
		sign(result);
		return result;
	}

	private void sign(HttpUriRequest request) {
		try {
			consumer.sign(request);
		} catch (OAuthMessageSignerException | OAuthCommunicationException e) {
			throw new TargetUnreachableException(e);
		} catch (OAuthExpectationFailedException e) {
			// thrown when consumer is incorrectly constructed
			throw new IllegalArgumentException(e);
		}
	}
}
