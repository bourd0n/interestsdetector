/*
 * Copyright 2012 Ryuji Yamashita
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.facebook.api.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ryuji Yamashita - roundrop at gmail.com, andrey g
 */
public class Paging {
	private final Cursors cursors;
	private final String previous;
	private final String next;

	@JsonCreator
	public Paging(
			@JsonProperty("cursors") Cursors cursors,
			@JsonProperty("previous") String previous,
			@JsonProperty("next") String next) {
		this.cursors = cursors;
		this.previous = previous;
		this.next = next;
	}

	@JsonProperty("previous")
	public String getPrevious() {
		return previous;
	}

	@JsonProperty("next")
	public String getNext() {
		return next;
	}

	@JsonProperty("cursors")
	public Cursors getCursors() {
		return cursors;
	}
	
	

	public static class Cursors {
		private final String after;
		private final String before;

		@JsonCreator
		public Cursors(
				@JsonProperty("after") String after,
				@JsonProperty("before") String before) {
			this.after = after;
			this.before = before;
		}

		@JsonProperty("after")
		public String getAfter() {
			return after;
		}

		@JsonProperty("before")
		public String getBefore() {
			return before;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((after == null) ? 0 : after.hashCode());
			result = prime * result
					+ ((before == null) ? 0 : before.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Cursors other = (Cursors) obj;
			if (after == null) {
				if (other.after != null)
					return false;
			} else if (!after.equals(other.after))
				return false;
			if (before == null) {
				if (other.before != null)
					return false;
			} else if (!before.equals(other.before))
				return false;
			return true;
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cursors == null) ? 0 : cursors.hashCode());
		result = prime * result + ((next == null) ? 0 : next.hashCode());
		result = prime * result
				+ ((previous == null) ? 0 : previous.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Paging other = (Paging) obj;
		if (cursors == null) {
			if (other.cursors != null)
				return false;
		} else if (!cursors.equals(other.cursors))
			return false;
		if (next == null) {
			if (other.next != null)
				return false;
		} else if (!next.equals(other.next))
			return false;
		if (previous == null) {
			if (other.previous != null)
				return false;
		} else if (!previous.equals(other.previous))
			return false;
		return true;
	}
}
