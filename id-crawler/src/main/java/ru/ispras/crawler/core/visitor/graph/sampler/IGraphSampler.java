package ru.ispras.crawler.core.visitor.graph.sampler;

import java.util.Collection;

/**
 * Filters neighbours collection for the given node according to some sampling
 * algorithm.
 * 
 * @author andrey g
 */
public interface IGraphSampler<T> {
	public Collection<T> getNextNodes(T node, Collection<T> neighbours);
}
