/*
 * Copyright 2012 Ryuji Yamashita
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.facebook.api.entities;

import java.util.Date;

import ru.ispras.crawler.core.datamodel.facebook.api.utils.FacebookAPIDateConverter;
import ru.ispras.crawler.core.datamodel.utils.DateConverter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ryuji Yamashita - roundrop at gmail.com, andrey g
 */
public class Link {

	private final String id;
	private final IdNameEntity from;
	private final String link;
	private final String name;
	private final ResponseList<Category> likes;
	private final ResponseList<Comment> comments;
	private final String description;
	private final String icon;
	private final String picture;
	private final String message;
	private final Date createdTime;
	private final String type;
	private final Privacy privacy;
	
	private final static DateConverter dateConverter = new FacebookAPIDateConverter();

	@JsonCreator
	public Link(
			@JsonProperty("id") String id,
			@JsonProperty("from") IdNameEntity from,
			@JsonProperty("link") String link,
			@JsonProperty("name") String name,
			@JsonProperty("likes") ResponseList<Category> likes,
			@JsonProperty("comments") ResponseList<Comment> comments,
			@JsonProperty("description") String description,
			@JsonProperty("icon") String icon,
			@JsonProperty("picture") String picture,
			@JsonProperty("message") String message,
			@JsonProperty("created_time") Date createdTime,
			@JsonProperty("type") String type,
			@JsonProperty("privacy") Privacy privacy) {
		this.id = id;
		this.from = from;
		this.link = link;
		this.name = name;
		this.likes = likes;
		this.comments = comments;
		this.description = description;
		this.icon = icon;
		this.picture = picture;
		this.message = message;
		this.createdTime = createdTime;
		this.type = type;
		this.privacy = privacy;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("from")
	public IdNameEntity getFrom() {
		return from;
	}

	@JsonProperty("link")
	public String getLink() {
		return link;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("likes")
	public ResponseList<Category> getLikes() {
		return likes;
	}

	@JsonProperty("comments")
	public ResponseList<Comment> getComments() {
		return comments;
	}
	
	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("icon")
	public String getIcon() {
		return icon;
	}

	@JsonProperty("picture")
	public String getPicture() {
		return picture;
	}

	@JsonProperty("message")
	public String getMessage() {
		return message;
	}
	@JsonProperty("created_time")
	public String getCreatedTimeString() {
		return dateConverter.getString(createdTime);
	}
	
	@JsonIgnore
	public Date getCreatedTime() {
		return createdTime;
	}
	@JsonProperty("type")
	public String getType() {
		return type;
	}
	@JsonProperty("privacy")
	public Privacy getPrivacy() {
		return privacy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Link other = (Link) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
