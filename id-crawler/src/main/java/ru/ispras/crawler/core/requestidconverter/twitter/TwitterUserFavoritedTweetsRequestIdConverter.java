package ru.ispras.crawler.core.requestidconverter.twitter;

import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFavoritedTweetsRequest;

public class TwitterUserFavoritedTweetsRequestIdConverter extends
		AbstractTwitterUserRequestIdConverter<TwitterUserFavoritedTweetsRequest> {
	@Override
	public TwitterUserFavoritedTweetsRequest getRequest(Long id) {
		return new TwitterUserFavoritedTweetsRequest(id);
	}
}
