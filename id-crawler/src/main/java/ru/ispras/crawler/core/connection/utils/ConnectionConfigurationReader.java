package ru.ispras.crawler.core.connection.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import ru.ispras.crawler.core.connection.IConnectionConfiguration;
import ru.ispras.modis.utils.xml.JaxbChunkXmlFactory;
import ru.ispras.modis.utils.xml.JaxbChunkXmlParser;

/**
 * File-based reader of {@link IConnectionConfiguration}s.
 * 
 * @author Ivan Andrianov
 * 
 */
public class ConnectionConfigurationReader {
	public static <CC extends IConnectionConfiguration> Iterable<CC> getConfigurations(Class<CC> c,
			File file) throws IOException {
		try {
			JaxbChunkXmlParser<CC> parser = new JaxbChunkXmlFactory(c).createParser(c);
			//hack for web
			return parser.parse(new BufferedInputStream(ConnectionConfigurationReader.class.getResourceAsStream("/" + file.getName())));
		} catch (JAXBException e) {
			throw new IOException(e);
		} catch (XMLStreamException e) {
			throw new IOException(e);
		}
	}

	public static <CC extends IConnectionConfiguration> Iterable<CC> getConfigurations(Class<CC> c,
			String fileName) throws IOException {
		return getConfigurations(c, new File(fileName));
	}
}
