package ru.ispras.crawler.core.datamodel.twitter.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class represents Twitter API search result page 
 * 
 * @author andrey
 *
 */
public class SearchResult {
	private final SearchMetadata searchMetadata;
	private final List<Status> statuses;
	
	@JsonCreator
	public SearchResult(
			@JsonProperty("search_metadata") SearchMetadata searchMetadata,
			@JsonProperty("statuses") List<Status> statuses) {
		this.searchMetadata = searchMetadata;
		this.statuses = statuses;
	}

	/**
	 * This class represents metadata part of SearchResult
	 * 
	 * @author andrey g
	 *
	 */
	public static class SearchMetadata{
		private final long maxId;
		private final long sinceId;
		private final String refreshUrl;
		private final String nextResults;
		private final int count;
		private final double completedIn;
		private final String query;
		
		@JsonCreator
		public SearchMetadata(
				@JsonProperty("max_id") long maxId,
				@JsonProperty("since_id") long sinceId,
				@JsonProperty("refresh_url") String refreshUrl,
				@JsonProperty("next_results") String nextResults,
				@JsonProperty("count") int count,
				@JsonProperty("completed_in") double completedIn,
				@JsonProperty("query") String query) {
			this.maxId = maxId;
			this.sinceId = sinceId;
			this.refreshUrl = refreshUrl;
			this.nextResults = nextResults;
			this.count = count;
			this.completedIn = completedIn;
			this.query = query;
		}

		@JsonProperty("max_id")
		public long getMaxId() {
			return maxId;
		}

		@JsonProperty("since_id")
		public long getSinceId() {
			return sinceId;
		}

		@JsonProperty("refresh_url")
		public String getRefreshUrl() {
			return refreshUrl;
		}

		@JsonProperty("next_results")
		public String getNextResults() {
			return nextResults;
		}

		@JsonProperty("count")
		public int getCount() {
			return count;
		}

		@JsonProperty("completed_in")
		public double getCompletedIn() {
			return completedIn;
		}

		@JsonProperty("query")
		public String getQuery() {
			return query;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(completedIn);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			result = prime * result + count;
			result = prime * result + (int) (maxId ^ (maxId >>> 32));
			result = prime * result
					+ ((nextResults == null) ? 0 : nextResults.hashCode());
			result = prime * result + ((query == null) ? 0 : query.hashCode());
			result = prime * result
					+ ((refreshUrl == null) ? 0 : refreshUrl.hashCode());
			result = prime * result + (int) (sinceId ^ (sinceId >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			SearchMetadata other = (SearchMetadata) obj;
			if (Double.doubleToLongBits(completedIn) != Double
					.doubleToLongBits(other.completedIn))
				return false;
			if (count != other.count)
				return false;
			if (maxId != other.maxId)
				return false;
			if (nextResults == null) {
				if (other.nextResults != null)
					return false;
			} else if (!nextResults.equals(other.nextResults))
				return false;
			if (query == null) {
				if (other.query != null)
					return false;
			} else if (!query.equals(other.query))
				return false;
			if (refreshUrl == null) {
				if (other.refreshUrl != null)
					return false;
			} else if (!refreshUrl.equals(other.refreshUrl))
				return false;
			if (sinceId != other.sinceId)
				return false;
			return true;
		}
		
	}

	@JsonProperty("search_metadata")
	public SearchMetadata getSearchMetadata() {
		return searchMetadata;
	}

	@JsonProperty("statuses")
	public List<Status> getStatuses() {
		return statuses;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((searchMetadata == null) ? 0 : searchMetadata.hashCode());
		result = prime * result
				+ ((statuses == null) ? 0 : statuses.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SearchResult other = (SearchResult) obj;
		if (searchMetadata == null) {
			if (other.searchMetadata != null)
				return false;
		} else if (!searchMetadata.equals(other.searchMetadata))
			return false;
		if (statuses == null) {
			if (other.statuses != null)
				return false;
		} else if (!statuses.equals(other.statuses))
			return false;
		return true;
	}

}
