package ru.ispras.crawler.core.requestidconverter.hunch;

import ru.ispras.crawler.core.datamodel.hunch.HunchUserFollowersRequest;

public class HunchUserFollowersRequestIdConverter extends
		AbstractHunchUserRequestIdConverter<HunchUserFollowersRequest> {
	@Override
	public HunchUserFollowersRequest getRequest(String screenName) {
		return new HunchUserFollowersRequest(screenName);
	}
}
