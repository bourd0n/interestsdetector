package ru.ispras.crawler.core.saver.mongodb.responseconverter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.Validate;

import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.saver.responsechunker.IResponseChunker;

import com.mongodb.DBObject;

/**
 * A wrapper {@link IMongoDBResponseConverter} which works through given
 * {@link IResponseChunker} and one element list
 * {@link IMongoDBResponseConverter}.
 * <p>
 * This class chunks {@link IResponse}, applies given
 * {@link IMongoDBResponseConverter} to every chunk in
 * {@link #getValues(IResponse)} and collects results.
 * <p>
 * This class applies given {@link IMongoDBResponseConverter} for every
 * {@link DBObject} (as one element list) and merges received {@link IResponse}
 * chunks into one.
 * 
 * @author Ivan Andrianov
 */
public class ChunkingMongoDBResponseConverter<Resp extends IResponse> implements IMongoDBResponseConverter<Resp> {
	private final IResponseChunker<Resp> responseChunker;
	private final IMongoDBResponseConverter<Resp> responseConverter;

	public ChunkingMongoDBResponseConverter(IResponseChunker<Resp> responseChunker,
			IMongoDBResponseConverter<Resp> responseConverter) {
		this.responseChunker = responseChunker;
		this.responseConverter = responseConverter;
	}

	@Override
	public List<DBObject> getValues(Resp response) {
		List<Resp> responseChunks = responseChunker.split(response);
		List<DBObject> result = new ArrayList<>();
		for (Resp responseChunk : responseChunks) {
			List<DBObject> values = responseConverter.getValues(responseChunk);
			Validate.isTrue(values.size() == 1);
			result.add(values.get(0));
		}
		return result;
	}

	@Override
	public Resp getResponse(List<DBObject> values) {
		List<Resp> responseChunks = new ArrayList<>();
		for (DBObject value : values) {
			responseChunks.add(responseConverter.getResponse(Collections.singletonList(value)));
		}
		return responseChunker.merge(responseChunks);
	}
}
