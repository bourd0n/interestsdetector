package ru.ispras.crawler.core.datamodel.twitter;

import ru.ispras.crawler.core.datamodel.IRequest;
/**
 * {@link IRequest} for downloading lists that given user is subscribed to.
 *
 * See https://dev.twitter.com/rest/reference/get/lists/list
 */
public class TwitterUserSubscribedListsRequest extends
		AbstractTwitterUserRequest {

	public TwitterUserSubscribedListsRequest(long id) {
		super(id);
	}
}
