package ru.ispras.crawler.core.datamodel.vkontakte.entities.users.personal;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 15.05.14
 * Time: 15:02
 */

/**
 * see description in https://vk.com/dev/fields_2
 */
public class Personal {
    private final Political political;
    private final List<String> langs;
    private final String religion;
    private final String inspiredBy;
    private final PeopleMain peopleMain;
    private final LifeMain lifeMain;
    private final Attitude smocking;
    private final Attitude alcohol;

    @JsonCreator
    public Personal(
            @JsonProperty("political")  int political,
            @JsonProperty("langs") List<String> langs,
            @JsonProperty("religion") String religion,
            @JsonProperty("inspired_by") String inspiredBy,
            @JsonProperty("people_main") int peopleMain,
            @JsonProperty("life_main")  int lifeMain,
            @JsonProperty("smoking")  int smocking,
            @JsonProperty("alcohol") int alcohol) {
        this.political = Political.intToPolitical(political);
        this.langs = langs;
        this.religion = religion;
        this.inspiredBy = inspiredBy;
        this.peopleMain = PeopleMain.intToPeopleMain(peopleMain);
        this.lifeMain = LifeMain.intToLifeMain(lifeMain);
        this.smocking = Attitude.intToAttitude(smocking);
        this.alcohol = Attitude.intToAttitude(alcohol);
    }

    @JsonProperty("political")
    public int getPoliticalIndex() {
        return Political.politicalToInt(political);
    }

    @JsonIgnore
    public Political getPolitical() {
        return political;
    }

    @JsonProperty("langs")
    public List<String> getLangs() {
        return langs;
    }

    @JsonProperty("religion")
    public String getReligion() {
        return religion;
    }

    @JsonProperty("inspired_by")
    public String getInspiredBy() {
        return inspiredBy;
    }

    @JsonProperty("people_main")
    public int getPeopleMainIndex() {
        return PeopleMain.peopleMainToInt(peopleMain);
    }

    @JsonIgnore
    public PeopleMain getPeopleMain() {
        return peopleMain;
    }

    @JsonProperty("life_main")
    public int getLifeMainIndex() {
        return LifeMain.LifeMainToint(lifeMain);
    }

    @JsonIgnore
    public LifeMain getLifeMain() {
        return lifeMain;
    }

    @JsonProperty("smoking")
    public int getSmockingIndex() {
        return Attitude.attitudeToInt(smocking);
    }

    @JsonIgnore
    public Attitude getSmocking() {
        return smocking;
    }

    @JsonProperty("alcohol")
    public int getAlcoholIndex() {
        return Attitude.attitudeToInt(alcohol);
    }

    @JsonIgnore
    public Attitude getAlcohol() {
        return alcohol;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + alcohol.hashCode();
		result = prime * result
				+ ((inspiredBy == null) ? 0 : inspiredBy.hashCode());
		result = prime * result + ((langs == null) ? 0 : langs.hashCode());
		result = prime * result + lifeMain.hashCode();
		result = prime * result + peopleMain.hashCode();
		result = prime * result + Political.politicalToInt(political);
		result = prime * result
				+ ((religion == null) ? 0 : religion.hashCode());
		result = prime * result + smocking.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personal other = (Personal) obj;
		if (alcohol != other.alcohol)
			return false;
		if (inspiredBy == null) {
			if (other.inspiredBy != null)
				return false;
		} else if (!inspiredBy.equals(other.inspiredBy))
			return false;
		if (langs == null) {
			if (other.langs != null)
				return false;
		} else if (!langs.equals(other.langs))
			return false;
		if (lifeMain != other.lifeMain)
			return false;
		if (peopleMain != other.peopleMain)
			return false;
		if (political != other.political)
			return false;
		if (religion == null) {
			if (other.religion != null)
				return false;
		} else if (!religion.equals(other.religion))
			return false;
		if (smocking != other.smocking)
			return false;
		return true;
	}

}
