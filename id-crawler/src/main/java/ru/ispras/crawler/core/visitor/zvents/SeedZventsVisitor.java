package ru.ispras.crawler.core.visitor.zvents;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.zvents.ResultPageZventsRequest;
import ru.ispras.crawler.core.datamodel.zvents.SeedZventsRequest;
import ru.ispras.crawler.core.datamodel.zvents.SeedZventsResponse;
import ru.ispras.crawler.core.visitor.AbstractNonFailingVisitor;

/**
 * Visitor which processes seed pages. Its only goal is to extract "More events in ..."
 * link and to create {@link ResultPageZventsRequest} from it.
 * 
 * @author sysoev
 */
public class SeedZventsVisitor extends AbstractNonFailingVisitor<SeedZventsRequest, SeedZventsResponse> {

	@Override
	public Collection<? extends IRequest> visit(SeedZventsRequest request, SeedZventsResponse response) {
		final Collection<IRequest> result = new ArrayList<>(1);
		if (response.hasMoreEventsLink()) {
			result.add(new ResultPageZventsRequest(response.getMoreEventsLink()));
		}
		return result;
	}

	@Override
	public Set<? extends Class<? extends IRequest>> getProducedRequestTypes() {
		return Collections.singleton(ResultPageZventsRequest.class);
	}
}
