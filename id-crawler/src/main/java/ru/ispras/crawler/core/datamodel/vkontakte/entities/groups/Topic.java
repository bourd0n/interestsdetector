package ru.ispras.crawler.core.datamodel.vkontakte.entities.groups;

import ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Topic {
	private final Long id;
	private final String title;
	private final Long created;
	private final Long createdBy;
	private final Long updated;
	private final Long updatedBy;
	private final Boolean isClosed;
	private final Boolean isFixed;
	private final Integer comments;
	
	@JsonCreator
	public Topic(
			@JsonProperty("id") Long id, 
			@JsonProperty("title") String title, 
			@JsonProperty("created") Long created, 
			@JsonProperty("created_by") Long createdBy,
			@JsonProperty("updated") Long updated, 
			@JsonProperty("updated_by") Long updatedBy, 
			@JsonProperty("is_closed") Integer isClosed, 
			@JsonProperty("is_fixed") Integer isFixed,
			@JsonProperty("comments") Integer comments) {
		this.id = id;
		this.title = title;
		this.created = created;
		this.createdBy = createdBy;
		this.updated = updated;
		this.updatedBy = updatedBy;
		this.isClosed = VkontakteUtils.flagToBoolean(isClosed);
		this.isFixed = VkontakteUtils.flagToBoolean(isFixed);
		this.comments = comments;
	}

	@JsonProperty("id")
	public Long getTid() {
		return id;
	}

	@JsonProperty("title")
	public String getTitle() {
		return title;
	}

	@JsonProperty("created")
	public Long getCreated() {
		return created;
	}

	@JsonProperty("created_by")
	public Long getCreatedBy() {
		return createdBy;
	}

	@JsonProperty("updated")
	public Long getUpdated() {
		return updated;
	}

	@JsonProperty("updated_by")
	public Long getUpdatedBy() {
		return updatedBy;
	}

	@JsonIgnore
	public Boolean getIsClosed() {
		return isClosed;
	}
	
	@JsonProperty("is_closed")
	public Integer getIsClosedInt() {
		return VkontakteUtils.booleanToFlag(isClosed);
	}

	@JsonIgnore
	public Boolean getIsFixed() {
		return isFixed;
	}
	
	@JsonProperty("is_fixed")
	public Integer getIsFixedInt() {
		return VkontakteUtils.booleanToFlag(isFixed);
	}

	@JsonProperty("comments")
	public Integer getComments() {
		return comments;
	}
}
