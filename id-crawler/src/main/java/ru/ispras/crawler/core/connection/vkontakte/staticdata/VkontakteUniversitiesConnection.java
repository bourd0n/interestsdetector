package ru.ispras.crawler.core.connection.vkontakte.staticdata;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 28.05.14
 * Time: 15:26
 */

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteUniversitiesRequest;

/**
 * see description in https://vk.com/dev/database.getUniversities
 */
public class VkontakteUniversitiesConnection extends AbstractVkontakteIdTitleConnection<VkontakteUniversitiesRequest> {
    public VkontakteUniversitiesConnection(IConnection<HttpRequest, HttpResponse> httpConnection) {
        super(httpConnection);
    }

    @Override
    protected String getMethod(VkontakteUniversitiesRequest request) {
        return "database.getUniversities?city_id=" + request.getCityId();
    }
}
