package ru.ispras.crawler.core.connection.facebook.api;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.TargetUnreachableException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIPaggedRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.Paging;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.ResponseList;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.ispras.crawler.core.jackson.DefaultObjectMapperFactory;

/**
 * Abstract {@link IConnection} which contains common FacebookAPI-related utils for downloading (https://graph.facebook.com/...)
 *
 * @author andrey g
 * 
 */
public abstract class AbstractFacebookAPIConnection<Req extends FacebookAPIRequest, Resp extends IResponse> implements
		IConnection<Req, Resp> {
	private static final Logger logger = Logger.getLogger(AbstractFacebookAPIConnection.class);

	private static final String BASE_URL = "https://graph.facebook.com/";

	private final IConnection<HttpRequest, HttpResponse> facebookAPIHttpConnection;
	
	private static final ObjectMapper objectMapper = new DefaultObjectMapperFactory().create();
	
	public AbstractFacebookAPIConnection(IConnection<HttpRequest, HttpResponse> facebookAPIHttpConnection) {
		this.facebookAPIHttpConnection = facebookAPIHttpConnection;
	}

	private String getHttpResponse(String url) throws UnavailableConnectionException {
		try {
			logger.info("Facebook API request: " + url);
			HttpRequest httpRequest = new HttpRequest(new URL(url));
			HttpResponse httpResponse = facebookAPIHttpConnection.getResponse(httpRequest);
			return httpResponse.getString();
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException(e);
		}
	}

	protected <T> List<T> getPagedResponse(FacebookAPIPaggedRequest request, Class<T> clazz)
			throws UnavailableConnectionException {
		List<T> result = new ArrayList<T>();
		String url = getUrl(request);
		int limit = request.getPagesLimit();
		while (limit != 0) {
			limit--;
			
			JavaType type = objectMapper.getTypeFactory().constructParametricType(ResponseList.class, clazz);
			ResponseList<T> response = getSingleHttpResponse(url, type);
			if (response.getData() != null){
				result.addAll(response.getData());	
			}
			
			Paging paging = response.getPaging();
			if (paging != null && paging.getNext() != null){
				url = paging.getNext();
			} else {
				break;
			}
		}
		return result;
	}

	protected <T> T getSingleResponse(FacebookAPIRequest request, Class<T> clazz) throws UnavailableConnectionException {
		String httpResponse = getHttpResponse(getUrl(request));
		T result = null;
		try {
			result = objectMapper.readValue(httpResponse, clazz);
		} catch (JsonParseException e) {
			throw new TargetUnreachableException(e);
		} catch (JsonMappingException e) {
			throw new IllegalStateException(e);
		} catch (IOException e) {
			throw new TargetUnreachableException(e);
		}
		return result;
	}
	
	
	private <T> T getSingleHttpResponse(String method, JavaType type) throws UnavailableConnectionException {
		String httpResponse = getHttpResponse(method);
		try {
			return objectMapper.readValue(httpResponse, type);
		} catch (JsonParseException e) {
			throw new TargetUnreachableException(e);
		} catch (JsonMappingException e) {
			throw new IllegalStateException(e);
		} catch (IOException e) {
			throw new TargetUnreachableException(e);
		}
	}	
	
	private String getUrl(FacebookAPIRequest request) {
		String res = BASE_URL + Long.toString(request.getId());
		String path = request.getPath();
		String parameters = request.getParameters();
		if (!path.isEmpty()) {
			res += "/" + path;
		}
		if (!parameters.isEmpty()) {
			res += "?" + parameters;
		}
		return res;
	}
}
