package ru.ispras.crawler.core.datamodel.batch;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * An implementation of {@link IResponse} that just stores request-to-response map
 *
 * @author andrey
 *
 * @param <T>
 */
public class BasicBatchResponse<Req extends IRequest, Resp extends IResponse> implements IBatchResponse<Req, Resp> {

	private final Map<Req, Resp> map;
	
	public BasicBatchResponse(Map<Req, Resp> map){
		this.map = new HashMap<>(map);
	}

	@Override
	public Map<Req, Resp> getRequestToResponseMap() {
		return Collections.unmodifiableMap(map);
	}
}
