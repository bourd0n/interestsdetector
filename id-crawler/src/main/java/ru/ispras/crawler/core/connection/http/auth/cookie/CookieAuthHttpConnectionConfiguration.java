package ru.ispras.crawler.core.connection.http.auth.cookie;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ru.ispras.crawler.core.connection.http.HttpConnectionConfiguration;

@XmlRootElement(name = "cookie-auth-http")
public class CookieAuthHttpConnectionConfiguration extends HttpConnectionConfiguration {
	private String login;
	private String password;

	public CookieAuthHttpConnectionConfiguration(String login, String password) {
		this.login = login;
		this.password = password;
	}

	public CookieAuthHttpConnectionConfiguration() {
	}

	@XmlElement(required = true)
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@XmlElement(required = true)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
