package ru.ispras.crawler.core.connection.http.auth.cookie;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import ru.ispras.crawler.core.connection.exception.ConnectionCreationException;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpClientFactory;
import ru.ispras.crawler.core.connection.http.ext.IHttpClientFactory;

/**
 * {@link IHttpClientFactory} implementation which uses other
 * {@link IHttpClientFactory} and uses resulting {@link HttpClient} to
 * authenticate using login form described by the given
 * {@link CookieAuthHttpResourceProfile}.
 * 
 * @author Ivan Andrianov
 */
public class CookieAuthHttpClientFactory implements IHttpClientFactory<CookieAuthHttpConnectionConfiguration> {
	private final IHttpClientFactory<? super CookieAuthHttpConnectionConfiguration> wrapped;
	private final CookieAuthHttpResourceProfile resourceProfile;

	public CookieAuthHttpClientFactory(CookieAuthHttpResourceProfile resourceProfile) {
		this(new DefaultHttpClientFactory(), resourceProfile);
	}

	public CookieAuthHttpClientFactory(IHttpClientFactory<? super CookieAuthHttpConnectionConfiguration> wrapped,
			CookieAuthHttpResourceProfile resourceProfile) {
		this.wrapped = wrapped;
		this.resourceProfile = resourceProfile;
	}

	@Override
	public HttpClient create(CookieAuthHttpConnectionConfiguration configuration) throws ConnectionCreationException {
		HttpClient result = wrapped.create(configuration);
		authenticate(result, configuration);
		return result;
	}

	private void authenticate(HttpClient client, CookieAuthHttpConnectionConfiguration configuration)
			throws ConnectionCreationException {
		getLoginForm(client);
		postLoginForm(client, configuration);
		checkCookiePresent(client);
	}

	private void getLoginForm(HttpClient client) throws ConnectionCreationException {
		try {
			HttpResponse getResponse = client.execute(new HttpGet(resourceProfile.getLoginPage()));
			HttpClientUtils.closeQuietly(getResponse);
		} catch (IOException e) {
			throw new ConnectionCreationException(String.format("Failed to login through %s: form GET failed.",
					resourceProfile.getLoginPage()), e);
		}
	}

	private void postLoginForm(HttpClient client, CookieAuthHttpConnectionConfiguration configuration)
			throws ConnectionCreationException {
		try {
			HttpPost post = new HttpPost(resourceProfile.getLoginPage());

			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair(resourceProfile.getLoginFieldName(), configuration.getLogin()));
			nvps.add(new BasicNameValuePair(resourceProfile.getPasswordFieldName(), configuration.getPassword()));

			post.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));

			HttpResponse postResponse = client.execute(post);
			HttpClientUtils.closeQuietly(postResponse);
		} catch (IOException e) {
			throw new ConnectionCreationException(String.format("Failed to login through %s: form POST failed.",
					resourceProfile.getLoginPage()), e);
		}
	}

	private void checkCookiePresent(HttpClient client) throws ConnectionCreationException {
		List<Cookie> cookies = ((DefaultHttpClient) client).getCookieStore().getCookies();
		for (Cookie cookie : cookies) {
			if (cookie.getName().equalsIgnoreCase(resourceProfile.getUserCookieName())) {
				return;
			}
		}
		throw new ConnectionCreationException(String.format("Failed to login through %s: cookie not found.",
				resourceProfile.getLoginPage()));
	}
}
