package ru.ispras.crawler.core.connection.vkontakte;

import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteGroupMembersRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.SortOrder;

/**
 * Connection for https://vk.com/dev/groups.getMembers method
 * 
 * @author andrey
 *
 */
public class VkontakteGroupMembersConnection 
	implements IConnection<VkontakteGroupMembersRequest, BasicResponse<List<Long>>>{

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	public VkontakteGroupMembersConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}

	private String getMethod(VkontakteGroupMembersRequest request) {
		return "groups.getMembers?group_id="+request.getGroupId()+
				"&sort="+toString(request.getSort());
		
	}

	private String toString(SortOrder sort) {
		return (sort == SortOrder.ASC) ? "id_asc" : "id_desc";
	}

	@Override
	public BasicResponse<List<Long>> getResponse(
			VkontakteGroupMembersRequest request)
			throws UnavailableConnectionException {
		String url = getMethod(request);
		List<Long> response = VkontakteConnectionUtils.getPagedResponse(url, 1000, request.getLimit(), httpConnection, Long.class);
		return new BasicResponse<List<Long>>(response);
	}
}
