package ru.ispras.crawler.core.connection.twitter;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFavoritedTweetsRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.Status;

/**
 * {@link IConnection} for downloading a list of given user's favorite Tweets
 *
 * See https://dev.twitter.com/rest/reference/get/favorites/list
 */
public class TwitterUserFavoritedTweetsConnection extends
		AbstractTwitterConnection<TwitterUserFavoritedTweetsRequest, BasicResponse<List<Status>>> {
	private static final int MAX_TWEETS_PER_QUERY = 200;
	private static final String METHOD_BASE = "favorites/list.json?count="
			+ MAX_TWEETS_PER_QUERY + "&include_entities=false&%s%s";

	public TwitterUserFavoritedTweetsConnection(IConnection<HttpRequest, HttpResponse> twitterHttpConnection) {
		super(twitterHttpConnection);
	}

	@Override
	public BasicResponse<List<Status>> getResponse(TwitterUserFavoritedTweetsRequest request)
			throws UnavailableConnectionException {
		Long maxId = null;
		List<Status> tweets = new ArrayList<Status>();
		while (true) {
			String method = String.format(METHOD_BASE,
					getIdParameter(request),
					getMaxIdParameter(maxId));
			
			List<Status> response = getHttpResponse(method, new TypeReference<List<Status>>() {});

			tweets.addAll(response);

			if (tweets.size() >= request.getLimit() || response.size() < MAX_TWEETS_PER_QUERY) {
				break;
			}
			Status oldestTweet = response.get(response.size() - 1);
			maxId = oldestTweet.getId() - 1;
		}
		return new BasicResponse<List<Status>>(tweets);
	}
}
