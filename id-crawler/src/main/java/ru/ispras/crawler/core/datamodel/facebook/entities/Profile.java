package ru.ispras.crawler.core.datamodel.facebook.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Profile {
	private final long id;
	private final String name;
	private final int friendsCount;
	private final String politicalViews;
	private final String religionViews;
	private final Gender gender;
	private final String birthday;
	private final String currentCity;
	private final String hometown;
	private final String twitter;
	private final String website;
	private final List<List<String>> education;
	private final List<List<String>> work;
	
	public static enum Gender{
		MALE, FEMALE, UNKNOWN
	}
	
	@JsonCreator
	public Profile(
			@JsonProperty("id") long id,
			@JsonProperty("name") String name,
			@JsonProperty("number of friends") int friendsCount,
			@JsonProperty("political views") String politicalViews,
			@JsonProperty("religion views") String religionViews,
			@JsonProperty("gender") String gender,
			@JsonProperty("birthday") String birthday,
			@JsonProperty("current city") String currentCity,
			@JsonProperty("hometown") String hometown,
			@JsonProperty("twitter") String twitter,
			@JsonProperty("website") String website,
			@JsonProperty("education") List<List<String>> education,
			@JsonProperty("work") List<List<String>> work) {
		this.id = id;
		this.name = name;
		this.friendsCount = friendsCount;
		this.politicalViews = politicalViews;
		this.religionViews = religionViews;
		this.gender = parseGender(gender);
		this.birthday = birthday;
		this.currentCity = currentCity;
		this.hometown = hometown;
		this.twitter = twitter;
		this.website = website;
		this.education = education;
		this.work = work;
	}

	@JsonProperty("id")
	public long getId() {
		return id;
	}

	@JsonProperty("name") 
	public String getName() {
		return name;
	}

	@JsonProperty("number of friends")
	public int getFriendsCount() {
		return friendsCount;
	}

	@JsonProperty("political views")
	public String getPoliticalViews() {
		return politicalViews;
	}

	@JsonProperty("religion views")
	public String getReligionViews() {
		return religionViews;
	}

	@JsonIgnore
	public Gender getGender() {
		return gender;
	}
	
	@JsonProperty("gender")
	public String getGenderString() {
		if (gender == Gender.MALE) return "Male";
		if (gender == Gender.FEMALE) return "Female";
		return null;
	}
	
	@JsonProperty("birthday")
	public String getBirthday() {
		return birthday;
	}

	@JsonProperty("current city")
	public String getCurrentCity() {
		return currentCity;
	}

	@JsonProperty("hometown")
	public String getHometown() {
		return hometown;
	}

	@JsonProperty("twitter")
	public String getTwitter() {
		return twitter;
	}

	@JsonProperty("website")
	public String getWebsite() {
		return website;
	}

	@JsonProperty("education")
	public List<List<String>> getEducation() {
		return education;
	}

	@JsonProperty("work")
	public List<List<String>> getWork() {
		return work;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profile other = (Profile) obj;
		if (id != other.id)
			return false;
		return true;
	}

	private Gender parseGender(String gender) {
		if ("Male".equals(gender)) return Gender.MALE;
		if ("Female".equals(gender)) return Gender.FEMALE;
		return Gender.UNKNOWN;
	}

}
