/*
 * Copyright 2012 Ryuji Yamashita
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.facebook.api.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ryuji Yamashita - roundrop at gmail.com, andrey g
 */
public class Targeting {
	private final List<String> countries;
	private final List<String> cities;
	private final List<String> regions;
	private final List<String> locales;

	@JsonCreator
	public Targeting(
			@JsonProperty("countries") List<String> countries,
			@JsonProperty("cities") List<String> cities,
			@JsonProperty("regions") List<String> regions,
			@JsonProperty("locales") List<String> locales) {
		this.countries = countries;
		this.cities = cities;
		this.regions = regions;
		this.locales = locales;
	}

	@JsonProperty("countries")
	public List<String> getCountries() {
		return countries;
	}

	@JsonProperty("cities")
	public List<String> getCities() {
		return cities;
	}

	@JsonProperty("regions")
	public List<String> getRegions() {
		return regions;
	}

	@JsonProperty("locales")
	public List<String> getLocales() {
		return locales;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cities == null) ? 0 : cities.hashCode());
		result = prime * result
				+ ((countries == null) ? 0 : countries.hashCode());
		result = prime * result + ((locales == null) ? 0 : locales.hashCode());
		result = prime * result + ((regions == null) ? 0 : regions.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Targeting other = (Targeting) obj;
		if (cities == null) {
			if (other.cities != null)
				return false;
		} else if (!cities.equals(other.cities))
			return false;
		if (countries == null) {
			if (other.countries != null)
				return false;
		} else if (!countries.equals(other.countries))
			return false;
		if (locales == null) {
			if (other.locales != null)
				return false;
		} else if (!locales.equals(other.locales))
			return false;
		if (regions == null) {
			if (other.regions != null)
				return false;
		} else if (!regions.equals(other.regions))
			return false;
		return true;
	}
	
	
	
}
