package ru.ispras.crawler.core.saver.responsechunker;

import java.util.List;

import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * Splits a {@link IResponse} to a non-empty list of {@link IResponse}s and
 * merges them back.
 * <p>
 * Implementations are expected to be thread-safe.
 * 
 * @author Ivan Andrianov
 */
public interface IResponseChunker<Resp extends IResponse> {
	List<Resp> split(Resp response);

	Resp merge(List<Resp> responseChunks);
}
