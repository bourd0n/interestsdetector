package ru.ispras.crawler.core.datamodel.vkontakte.entities.users;

/**
 * Created by padre on 14.07.14
 */
public enum Hidden {
    HIDDEN, NONHIDDEN;

    public static Hidden intToHidden(Integer number) {
        if (number != null && number == 1) return HIDDEN;
        return NONHIDDEN;
    }

    public static Integer hiddenToInt(Hidden status) {
        if (status == HIDDEN) return 1;
        return 0;
    }
}
