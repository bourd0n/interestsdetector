package ru.ispras.crawler.core.connection.facebook;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * Abstract {@link IConnection} which contains common FacebookHTML-related utils for downloading (https://m.facebook.com/...)
 *
 * @author andrey g
 *
 */
public abstract class AbstractFacebookConnection<Req extends IRequest, Resp extends IResponse> implements
		IConnection<Req, Resp> {
	private static final Logger logger = Logger
			.getLogger(AbstractFacebookConnection.class);

	public final static String BASE_URL = "https://m.facebook.com/";

	private final IConnection<HttpRequest, HttpResponse> facebookHttpConnection;

	public AbstractFacebookConnection(IConnection<HttpRequest, HttpResponse> facebookHttpConnection) {
		this.facebookHttpConnection = facebookHttpConnection;
	}

	protected String getHttpResponse(String method) throws UnavailableConnectionException {
		try {
			String url = BASE_URL + method;
			HttpRequest facebookHttpRequest = new HttpRequest(new URL(url));
			logger.info("Facebook request: " + url);

			HttpResponse httpResponse = facebookHttpConnection.getResponse(facebookHttpRequest);
			return httpResponse.getString();
		} catch (MalformedURLException e) {
			throw new IllegalArgumentException(e);
		}
	}
}
