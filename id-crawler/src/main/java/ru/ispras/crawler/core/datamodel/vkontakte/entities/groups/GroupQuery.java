package ru.ispras.crawler.core.datamodel.vkontakte.entities.groups;

import org.apache.commons.lang.Validate;

public class GroupQuery {
	private String query;
	private Type type;
	private SortType sort = SortType.NUM_USERS;
	private Long countryId;
	private Long cityId;
	private Boolean future;	
	
	public static enum Type{
		GROUP("group"), PAGE("page"), EVENT("event");
		
		private final String value;
		
		private Type(String value){
			this.value = value;
		}
		
		public String getValue(){
			return value;
		}
	}
	
	public static enum SortType{
		NUM_USERS("0"), GROWTH("1"), DAILY_ATT_TONUM_USERS("2"),
		LIKES_TO_NUM_USERS("3"), COMMENTS_TO_NUM_USERS("4"),
		RECORDS_TO_NUM_USERS("5");
		
		private final String value;
		
		private SortType(String value){
			this.value = value;
		}
		
		public String getValue(){
			return value;
		}
	}

	public GroupQuery(GroupQuery other) {
		this.query = other.query;
		this.type = other.type;
		this.sort = other.sort;
		this.countryId = other.countryId;
		this.cityId = other.cityId;
		this.future = other.future;
	}

	public GroupQuery(String query){
		Validate.notNull(query);
		this.query = query;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public SortType getSort() {
		return sort;
	}

	public void setSort(SortType sort) {
		Validate.notNull(sort);
		this.sort = sort;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Boolean getFuture() {
		return future;
	}

	public void setFuture(Boolean future) {
		this.future = future;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cityId == null) ? 0 : cityId.hashCode());
		result = prime * result
				+ ((countryId == null) ? 0 : countryId.hashCode());
		result = prime * result + ((future == null) ? 0 : future.hashCode());
		result = prime * result + ((query == null) ? 0 : query.hashCode());
		result = prime * result + ((sort == null) ? 0 : sort.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroupQuery other = (GroupQuery) obj;
		if (cityId == null) {
			if (other.cityId != null)
				return false;
		} else if (!cityId.equals(other.cityId))
			return false;
		if (countryId == null) {
			if (other.countryId != null)
				return false;
		} else if (!countryId.equals(other.countryId))
			return false;
		if (future == null) {
			if (other.future != null)
				return false;
		} else if (!future.equals(other.future))
			return false;
		if (query == null) {
			if (other.query != null)
				return false;
		} else if (!query.equals(other.query))
			return false;
		if (sort != other.sort)
			return false;
		if (type != other.type)
			return false;
		return true;
	}
}
