package ru.ispras.crawler.core.connection.twitter;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFollowersRequest;

/**
 * {@link AbstractTwitterUserNeighboursConnection} for
 * {@link TwitterUserFollowersRequest}.
 *
 * See https://dev.twitter.com/rest/reference/get/followers/ids
 *
 * @author Ivan Andrianov
 * 
 */
public class TwitterUserFollowersConnection extends
		AbstractTwitterUserNeighboursConnection<TwitterUserFollowersRequest> {
	public TwitterUserFollowersConnection(IConnection<HttpRequest, HttpResponse> twitterHttpConnection) {
		super(twitterHttpConnection);
	}

	@Override
	protected String getUserNeighbourMethod() {
		return "followers";
	}
}
