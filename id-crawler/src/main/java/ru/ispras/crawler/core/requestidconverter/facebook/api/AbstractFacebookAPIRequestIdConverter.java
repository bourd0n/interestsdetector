package ru.ispras.crawler.core.requestidconverter.facebook.api;

import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public abstract class AbstractFacebookAPIRequestIdConverter<Req extends FacebookAPIRequest> implements
		IRequestIdConverter<Req, Long> {
	@Override
	public Long getId(Req request) {
		return request.getId();
	}
}
