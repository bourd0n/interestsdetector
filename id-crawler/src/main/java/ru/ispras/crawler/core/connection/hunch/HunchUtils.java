package ru.ispras.crawler.core.connection.hunch;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.TargetUnreachableException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * Utility class containing some useful methods for parsing Hunch pages 
 *
 */
public class HunchUtils {

	public static final String BASE_URL = "https://hunch.com/";
	
	private static final String PAGE_LINKS_SELECTOR = "div.paginator > a:not(.next)";
	private static final String PAGE_NUMBER = "?p=%d";
	
	protected static final Logger logger = Logger.getLogger(HunchUtils.class);
	
	/**
	 * Returns URL to nth page of paginated list
	 * @param baseMethod base URL of page
	 * @param pageNumber number of page
	 * @return URL to nth page of paginated list
	 */
	public static String getPagedMethod(String baseMethod, int pageNumber) {
		return baseMethod + String.format(PAGE_NUMBER, pageNumber);
	}
	
	/**
	 * Returns number of pages in pagination list
	 * @param doc 
	 * @return number of the last page. If list is not paginated, it returns 1
	 */
	public static int getPagesCount(Document doc) {
		Element lastPageLink = doc.select(PAGE_LINKS_SELECTOR).last();
		if (lastPageLink != null) {
			int lastPageNumber = Integer.parseInt(lastPageLink.text());
			if (lastPageNumber > 0) {
				return lastPageNumber;
			}
		}
		return 1;
	}
	
	public interface IHunchPagedListPageProcessor<T> {		
		/**
		 * Parses page and returns all its items as JSONArray 
		 * @param page page to parse
		 * @return all items extracted from page
		 */
		List<T> processPage(Document page);
	}
	
	/**
	 * Downloads Hunch lists scattered across several pages
	 * @param method base URL of the list
	 * @param processor processor which extracts item from the page
	 * @param httpManager httpManager which will perform requests
	 * @return
	 * @throws TargetUnreachableException
	 * @throws NoAvailableConnectionException
	 */
	public static <T> List<T> getPagedItems(String method, IHunchPagedListPageProcessor<T> processor,
			IConnection<HttpRequest, HttpResponse> hunchHttpConnection) throws UnavailableConnectionException {
		Document firstPage = getDocument(method, hunchHttpConnection);
		List<T> itemsList = processor.processPage(firstPage);
		final int pagesCount = getPagesCount(firstPage);

		for (int currentPageNumber = 2; currentPageNumber <= pagesCount; currentPageNumber++) {
			final String currentPageMethod = HunchUtils.getPagedMethod(method, currentPageNumber);
			itemsList.addAll(processor.processPage(getDocument(currentPageMethod, hunchHttpConnection)));
		}
		return itemsList;
	}
	
	/**
	 * Performs Http request to Hunch.com and returns response as string
	 * @param method relative url
	 * @param authorizedHttpConnection
	 * @return
	 * @throws TargetUnreachableException
	 * @throws NoAvailableConnectionException
	 */
	public static String getHttpResponse(String method, IConnection<HttpRequest, HttpResponse> hunchHttpConnection)
			throws UnavailableConnectionException {
		try {
			String url = BASE_URL + method;
			HttpRequest hunchHttpRequest = new HttpRequest(new URL(url));
			logger.info("Hunch request: " + url);
			HttpResponse httpResponse = hunchHttpConnection.getResponse(hunchHttpRequest);
			return httpResponse.getString();
		} catch (MalformedURLException e) {
			throw new TargetUnreachableException(e);
		}
	}
	
	/**
	 * Performs Http request to Hunch.com and returns response as Document
	 * @param method
	 * @param authorizedHttpConnection
	 * @return
	 * @throws TargetUnreachableException
	 * @throws NoAvailableConnectionException
	 */
	public static Document getDocument(String method, IConnection<HttpRequest, HttpResponse> hunchHttpConnection)
			throws UnavailableConnectionException {
		final String response = getHttpResponse(method, hunchHttpConnection);
		return Jsoup.parse(response);
	}
}
