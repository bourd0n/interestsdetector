package ru.ispras.crawler.core.requestidconverter.twitter;

import ru.ispras.crawler.core.datamodel.twitter.TwitterUserProfileRequest;

public class TwitterUserProfileRequestIdConverter extends
		AbstractTwitterUserRequestIdConverter<TwitterUserProfileRequest> {
	@Override
	public TwitterUserProfileRequest getRequest(Long id) {
		return new TwitterUserProfileRequest(id);
	}
}
