package ru.ispras.crawler.core.datamodel.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 22.05.14
 * Time: 16:00
 *
 * Request for https://vk.com/dev/database.getFaculties
 */
public class VkontakteFacultiesRequest implements IRequest{
    private final long universityId;

    public VkontakteFacultiesRequest(long universityId) {
        this.universityId = universityId;
    }

    public long getUniversityId() {
        return universityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VkontakteFacultiesRequest)) return false;

        VkontakteFacultiesRequest that = (VkontakteFacultiesRequest) o;

        if (universityId != that.universityId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (universityId ^ (universityId >>> 32));
    }

    @Override
	public String toString() {
		return "VkontakteFacultiesRequest [universityId=" + universityId + "]";
	}
}
