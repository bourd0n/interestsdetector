package ru.ispras.crawler.core.connection.http.ext;

import org.apache.http.client.methods.HttpUriRequest;

import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;

/**
 * Converts given {@link HttpRequest} in HTTP Client's {@link HttpUriRequest}.
 * 
 * @author Ivan Andrianov
 * 
 */
public interface IHttpRequestConverter {
	HttpUriRequest convert(HttpRequest request) throws UnavailableConnectionException;
}
