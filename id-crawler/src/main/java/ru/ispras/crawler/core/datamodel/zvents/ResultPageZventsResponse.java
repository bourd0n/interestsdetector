package ru.ispras.crawler.core.datamodel.zvents;

import java.net.URL;
import java.util.Collection;

/**
 * Response, representing information from Zvents search results page. This response contains
 * <ul>
 * <li> link to next page with search results (if present); </li>
 * <li> links to venues from this page;</li>
 * <li> links to events from this page. </li>
 * </ul>
 * 
 * @author sysoev
 */
public class ResultPageZventsResponse implements IZventsResponse {

	private final URL nextPageUrl;
	
	private final Collection<EventUrlId> events;
	private final Collection<VenueUrlId> venues;
	
	public ResultPageZventsResponse(
			URL nextPageUrl,
			Collection<EventUrlId> events,
			Collection<VenueUrlId> venues) {
		this.nextPageUrl = nextPageUrl;
		this.events = events;
		this.venues = venues;
	}

	public URL getNextPageUrl() {
		return nextPageUrl;
	}
	
	public boolean hasNextPageUrl() {
		return nextPageUrl != null;
	}

	public Collection<EventUrlId> getEvents() {
		return events;
	}

	public Collection<VenueUrlId> getVenues() {
		return venues;
	}
	
	// ****************************************************************

	public static class VenueUrlId {
		private final URL url;
		private final String id;
		
		public VenueUrlId(URL url, String id) {
			super();
			this.url = url;
			this.id = id;
		}

		public URL getUrl() {
			return url;
		}

		public String getId() {
			return id;
		}
	}
	
	public static class EventUrlId {
		private final URL url;
		private final String eventId;
		private final String venueId;
		
		public EventUrlId(URL url, String eventId, String venueId) {
			super();
			this.url = url;
			this.eventId = eventId;
			this.venueId = venueId;
		}

		public URL getUrl() {
			return url;
		}

		public String getEventId() {
			return eventId;
		}

		public String getVenueId() {
			return venueId;
		}
	}
}
