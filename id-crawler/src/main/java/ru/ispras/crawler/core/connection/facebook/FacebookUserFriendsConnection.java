package ru.ispras.crawler.core.connection.facebook;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.facebook.FacebookUserFriendsRequest;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * {@link IConnection} for downloading friends list of given Facebook user
 * Example: https://m.facebook.com/friends/?id=100001698473676&f=0
 *
 * @author andrey g
 *
 */
public class FacebookUserFriendsConnection extends
		AbstractFacebookConnection<FacebookUserFriendsRequest, BasicResponse<List<Long>>> {
	private static final String BASE_METHOD = "friends/?id=%d&f=%d";
	private static final int FRIENDS_PAGE_SIZE = 10;

	private static Pattern addLinkPattern = Pattern.compile("id=([0-9]+)&");

	public FacebookUserFriendsConnection(IConnection<HttpRequest, HttpResponse> facebookHttpConnection) {
		super(facebookHttpConnection);
	}

	@Override
	public BasicResponse<List<Long>> getResponse(FacebookUserFriendsRequest request) throws UnavailableConnectionException {
		long userId = request.getId();
		int pageNumber = 0;
		List<Long> friends = new ArrayList<Long>();
		while (true) {
			String htmlPage = getHttpResponse(String.format(BASE_METHOD, userId, pageNumber
					* FRIENDS_PAGE_SIZE));
			Document doc = Jsoup.parse(htmlPage);
			addFriendsFromDocument(friends, doc);
			Element moreItemsElement = doc.getElementById("m_more_item");
			if (moreItemsElement == null) {
				break;
			}
			pageNumber++;
		}
		return new BasicResponse<List<Long>>(friends);
	}

	private void addFriendsFromDocument(List<Long> friends, Document doc) {
		Elements friendDivs = doc.getElementsByClass("c");
		for (Element friendDiv : friendDivs) {
			Element addFriendLink = friendDiv.getElementsByTag("a").last();
			String href = addFriendLink.attr("href");
			Matcher addLinkMatcher = addLinkPattern.matcher(href);
			addLinkMatcher.find();
			friends.add(Long.parseLong(addLinkMatcher.group(1)));
		}
	}
}
