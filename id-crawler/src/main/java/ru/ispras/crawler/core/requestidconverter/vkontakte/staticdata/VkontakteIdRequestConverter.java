package ru.ispras.crawler.core.requestidconverter.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteIdRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 10.06.14
 * Time: 18:49
 */
public class VkontakteIdRequestConverter implements IRequestIdConverter<VkontakteIdRequest, Long> {
    @Override
    public Long getId(VkontakteIdRequest request) {
        return request.getId();
    }

    @Override
    public VkontakteIdRequest getRequest(Long aLong) {
        return new VkontakteIdRequest(aLong);
    }
}
