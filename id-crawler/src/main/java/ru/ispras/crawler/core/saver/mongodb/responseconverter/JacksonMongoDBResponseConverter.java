package ru.ispras.crawler.core.saver.mongodb.responseconverter;

import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jsoup.helper.Validate;

import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.jackson.DefaultObjectMapperFactory;
import ru.ispras.crawler.core.saver.mongodb.JacksonMongoDBUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mongodb.DBObject;

/**
 * Jackson-based {@link IMongoDBResponseConverter} generic implementation.
 * <p>
 * Corresponding {@link IResponse} should be annotated with Jackson annotations
 * for this class to do its work.
 * <p>
 * This class works with one element lists of {@link DBObject}s only.
 * 
 * @author Ivan Andrianov
 */
public class JacksonMongoDBResponseConverter<Resp extends IResponse> implements IMongoDBResponseConverter<Resp> {
	private final TypeReference<Resp> type;
    private final JacksonMongoDBUtils jacksonUtils;

    public JacksonMongoDBResponseConverter(TypeReference<Resp> type) {
        this.type = type;
        this.jacksonUtils = new JacksonMongoDBUtils(new DefaultObjectMapperFactory().create());
    }

	public JacksonMongoDBResponseConverter(TypeReference<Resp> type, ObjectMapper mapper) {
		this.type = type;
        this.jacksonUtils = new JacksonMongoDBUtils(mapper);
	}

	@Override
	public List<DBObject> getValues(Resp response) {
		// As Resp is guaranteed not to be Number or String we are guaranteed to
		// get DBObject in toMongoDB.
		return Collections.<DBObject> singletonList((DBObject) jacksonUtils.toMongoDB(response));
	}

	@Override
	public Resp getResponse(List<DBObject> values) {
		Validate.isTrue(values.size() == 1);
		DBObject value = values.get(0);
		return jacksonUtils.fromMongoDB(value, type);
	}
}
