package ru.ispras.crawler.core.datamodel.vkontakte.entities;

import ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Member {
	private final Long userId;
	private final Boolean member;
	private final Boolean request;
	private final Boolean invitation;
	
	@JsonCreator
	public Member(
			@JsonProperty("user_id") Long userId, 
			@JsonProperty("member") Integer member, 
			@JsonProperty("request") Integer request,
			@JsonProperty("invitation") Integer invitation) {
		this.userId = userId;
		this.member = VkontakteUtils.flagToBoolean(member);
		this.request = VkontakteUtils.flagToBoolean(request);
		this.invitation = VkontakteUtils.flagToBoolean(invitation);
	}

	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	@JsonIgnore
	public Boolean getMember() {
		return member;
	}
	
	@JsonProperty("member")
	public Integer getMemberInt() {
		return VkontakteUtils.booleanToFlag(member);
	}

	@JsonIgnore
	public Boolean getRequest() {
		return request;
	}
	
	@JsonProperty("request")
	public Integer getRequestInt() {
		return VkontakteUtils.booleanToFlag(request);
	}

	@JsonIgnore
	public Boolean getInvitation() {
		return invitation;
	}
	
	@JsonProperty("invitation")
	public Integer getInvitationInt() {
		return VkontakteUtils.booleanToFlag(invitation);
	}
	
}
