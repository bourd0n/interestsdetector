package ru.ispras.crawler.core.connection.facebook.api;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.Video;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * {@link IConnection} for downloading video object via Facebook API
 * Example: https://graph.facebook.com/941268445887422
 *
 * @author andrey g
 *
 */
public class FacebookAPIVideoConnection extends AbstractFacebookAPIObjectConnection<Video> {

	public FacebookAPIVideoConnection(IConnection<HttpRequest, HttpResponse> facebookAPIHttpConnection) {
		super(facebookAPIHttpConnection, Video.class);
	}

}
