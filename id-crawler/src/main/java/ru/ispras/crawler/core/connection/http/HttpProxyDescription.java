package ru.ispras.crawler.core.connection.http;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "http-proxy")
public class HttpProxyDescription {
	private String ip;
	private int port;
	private String login;
	private String password;

	public HttpProxyDescription() {
	}

	public HttpProxyDescription(String ip, int port) {
		this.ip = ip;
		this.port = port;
	}

	public HttpProxyDescription(String ip, int port, String login, String password) {
		this.ip = ip;
		this.port = port;
		this.login = login;
		this.password = password;
	}

	@XmlElement(required = true)
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@XmlElement(required = true)
	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	@XmlElement
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@XmlElement
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
