package ru.ispras.crawler.core.connection.vkontakte.staticdata;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteSchoolClassesRequest;

/**
 * see description in https://vk.com/dev/database.getSchoolClasses
 *  
 * @author Ivan Andrianov
 */
public class VkontakteSchoolClassesConnection extends AbstractVkontakteIdTitleConnection<VkontakteSchoolClassesRequest> {
	public VkontakteSchoolClassesConnection(IConnection<HttpRequest, HttpResponse> httpConnection) {
		super(httpConnection);
	}

	@Override
	protected String getMethod(VkontakteSchoolClassesRequest request) {
		return "database.getSchoolClasses?country_id=" + request.getCountryId();
	}
}
