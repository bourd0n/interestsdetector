package ru.ispras.crawler.core.requestidconverter.twitter;

import ru.ispras.crawler.core.datamodel.twitter.TwitterUserMembershipsListsRequest;

public class TwitterUserMembershipsListsRequestIdConverter extends
		AbstractTwitterUserRequestIdConverter<TwitterUserMembershipsListsRequest> {
	@Override
	public TwitterUserMembershipsListsRequest getRequest(Long id) {
		return new TwitterUserMembershipsListsRequest(id);
	}
}
