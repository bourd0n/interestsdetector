package ru.ispras.crawler.core.datamodel.vkontakte.entities.users;

/**
 * Created by padre on 14.07.14
 */
public enum Deactivated {
    DEACTIVATED, NORMAL, BANNED;

    public static Deactivated stringToDeactevated(String deactivated) {
        if (deactivated == "deleted")
            return Deactivated.DEACTIVATED;
        else if (deactivated == "banned")
            return Deactivated.BANNED;
        else
            return Deactivated.NORMAL;
    }

    public static String deactevatedToString(Deactivated deactivated) {
        if (deactivated == Deactivated.DEACTIVATED)
            return "deleted";
        else if (deactivated == Deactivated.BANNED)
            return "banned";
        else
            return "normal";
    }
}
