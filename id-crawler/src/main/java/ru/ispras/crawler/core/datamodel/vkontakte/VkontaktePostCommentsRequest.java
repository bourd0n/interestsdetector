package ru.ispras.crawler.core.datamodel.vkontakte;

import org.apache.commons.lang.Validate;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.SortOrder;

/**
 * Request for https://vk.com/dev/wall.getComments method
 * 
 * @author andrey
 *
 */
public class VkontaktePostCommentsRequest extends AbstractVkontakteLimitedRequest
		implements IRequest {

	private final long ownerId;
	private final long postId;
	private final SortOrder sort;
		
	public VkontaktePostCommentsRequest(long ownerId, long postId, SortOrder sort, int limit) {
		super(limit);
		Validate.notNull(sort);
		this.ownerId = ownerId;
		this.postId = postId;
		this.sort = sort;
	}
	
	public VkontaktePostCommentsRequest(long ownerId, long postId, SortOrder sort) {
		super();
		Validate.notNull(sort);
		this.ownerId = ownerId;
		this.postId = postId;
		this.sort = sort;
	}
	
	public VkontaktePostCommentsRequest(long ownerId, long postId) {
		this(ownerId, postId, SortOrder.ASC);
	}

	public VkontaktePostCommentsRequest(long ownerId, long postId, int limit) {
		this(ownerId, postId, SortOrder.ASC, limit);
	}

	public long getOwnerId() {
		return ownerId;
	}

	public long getPostId() {
		return postId;
	}

	public SortOrder getSort() {
		return sort;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (ownerId ^ (ownerId >>> 32));
		result = prime * result + (int) (postId ^ (postId >>> 32));
		result = prime * result + ((sort == null) ? 0 : sort.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VkontaktePostCommentsRequest other = (VkontaktePostCommentsRequest) obj;
		if (ownerId != other.ownerId)
			return false;
		if (postId != other.postId)
			return false;
		if (sort != other.sort)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VkontaktePostCommentsRequest [ownerId=" + ownerId + ", postId="
				+ postId + ", sort=" + sort + "]";
	}
}
