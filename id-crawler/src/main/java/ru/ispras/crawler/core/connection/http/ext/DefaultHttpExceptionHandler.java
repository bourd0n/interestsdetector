package ru.ispras.crawler.core.connection.http.ext;

import java.io.IOException;

import org.apache.http.client.methods.HttpUriRequest;

import ru.ispras.crawler.core.connection.exception.TargetUnreachableException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;

/**
 * Default exception handler always throws {@link TargetUnreachableException}
 * @author andrey
 *
 */
public class DefaultHttpExceptionHandler implements IHttpExceptionHandler {

	@Override
	public void handle(HttpUriRequest uriRequest, IOException e) throws UnavailableConnectionException{
		throw new TargetUnreachableException(String.format("Failed to process %s.", uriRequest.getURI()), e);
	}

}
