package ru.ispras.crawler.core.connection.twitter;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.TwitterUserFriendsRequest;

/**
 * {@link AbstractTwitterUserNeighboursConnection} for
 * {@link TwitterUserFriendsRequest}.
 *
 * See https://dev.twitter.com/rest/reference/get/friends/ids
 *
 * @author Ivan Andrianov
 * 
 */
public class TwitterUserFriendsConnection extends
		AbstractTwitterUserNeighboursConnection<TwitterUserFriendsRequest> {
	public TwitterUserFriendsConnection(IConnection<HttpRequest, HttpResponse> twitterHttpConnection) {
		super(twitterHttpConnection);
	}

	@Override
	protected String getUserNeighbourMethod() {
		return "friends";
	}
}
