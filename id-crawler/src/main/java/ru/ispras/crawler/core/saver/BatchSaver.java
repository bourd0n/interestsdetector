package ru.ispras.crawler.core.saver;

import java.util.Map.Entry;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.datamodel.batch.IBatchRequest;
import ru.ispras.crawler.core.datamodel.batch.IBatchResponse;

/**
 * Batch saver is used for saving {@link IBatchRequest}s and {@link IBatchResponse}s.
 * The saver executes save method of the wrapped saver for each of request/response pair from {@link IBatchResponse}   
 * 
 * @author andrey
 *
 * @param <Req>
 * @param <Resp>
 */
public class BatchSaver<Req extends IRequest, Resp extends IResponse> implements ISaver<IBatchRequest<Req>, IBatchResponse<Req, Resp>> {

	private final ISaver<Req, Resp> wrappedSaver;
	
	public BatchSaver(ISaver<Req, Resp> wrappedSaver) {
		this.wrappedSaver = wrappedSaver;
	}

	@Override
	public void save(IBatchRequest<Req> request,	IBatchResponse<Req, Resp> response) {
		for (Entry<Req, Resp> entry: response.getRequestToResponseMap().entrySet()){
			wrappedSaver.save(entry.getKey(), entry.getValue());
		}
	}
}
