package ru.ispras.crawler.core.requestidconverter.vkontakte;

import ru.ispras.crawler.core.datamodel.vkontakte.VkontaktePostCommentsRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class VkontaktePostCommentsRequestIdConverter implements
		IRequestIdConverter<VkontaktePostCommentsRequest, String> {

	@Override
	public String getId(VkontaktePostCommentsRequest request) {
		return request.getOwnerId()+"_"+request.getPostId();
	}

	@Override
	public VkontaktePostCommentsRequest getRequest(String id) {
		String[] elems = id.split("_");
		return new VkontaktePostCommentsRequest(Long.parseLong(elems[0]), Long.parseLong(elems[1]));
	}

}
