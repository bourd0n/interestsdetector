package ru.ispras.crawler.core.connection.facebook.api;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.Photo;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * {@link IConnection} for downloading photo object via Facebook API
 * Example: https://graph.facebook.com/182443505155597
 *
 * @author andrey g
 *
 */
public class FacebookAPIPhotoConnection extends AbstractFacebookAPIObjectConnection<Photo> {

	public FacebookAPIPhotoConnection(IConnection<HttpRequest, HttpResponse> facebookAPIHttpConnection) {
		super(facebookAPIHttpConnection, Photo.class);
	}

}
