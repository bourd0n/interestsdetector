package ru.ispras.crawler.core.connection.twitter.http;

import ru.ispras.crawler.core.connection.IConfigurableConnectionFactory;
import ru.ispras.crawler.core.connection.exception.ConnectionCreationException;
import ru.ispras.crawler.core.connection.http.HttpConnection;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpClientFactory;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * Twitter API {@link IConfigurableConnectionFactory} implementation.
 * 
 * @author Ivan Andrianov
 * 
 */
public class TwitterHttpConnectionFactory implements
		IConfigurableConnectionFactory<HttpRequest, HttpResponse, TwitterHttpConnectionConfiguration> {
	@Override
	public HttpConnection<TwitterHttpConnectionConfiguration> create(TwitterHttpConnectionConfiguration configuration)
			throws ConnectionCreationException {
		TwitterHttpRequestResponseConverter converter = new TwitterHttpRequestResponseConverter(configuration);
		return new HttpConnection<>(converter, new DefaultHttpClientFactory(), configuration, converter);
	}
}
