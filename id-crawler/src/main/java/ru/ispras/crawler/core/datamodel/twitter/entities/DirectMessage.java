/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.twitter.entities;


import java.util.Date;

import ru.ispras.crawler.core.datamodel.twitter.utils.TwitterDateConverter;
import ru.ispras.crawler.core.datamodel.utils.DateConverter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * A data class representing sent/received direct message.
 *
 * @author Yusuke Yamamoto - yusuke at mac.com, andrey g
 */
public class DirectMessage {
	private final long id;
	private final String text;
	private final long senderId;
	private final long recipientId;
	private final Date createdAt;
    private final String senderScreenName;
    private final String recipientScreenName;
    private final StatusEntities entities;
    private final User sender;
    private final User recipient;
    
    private final static DateConverter dateConverter = new TwitterDateConverter();
    
    @JsonCreator
	public DirectMessage(	@JsonProperty("id") long id, 
							@JsonProperty("text") String text,
							@JsonProperty("sender_id") long senderId, 
							@JsonProperty("recipient_id") long recipientId,
							@JsonProperty("created_at") String createdAt, 
							@JsonProperty("sender_screen_name") String senderScreenName,
							@JsonProperty("recipient_screen_name") String recipientScreenName,
							@JsonProperty("entities") StatusEntities entities,
							@JsonProperty("sender") User sender,
							@JsonProperty("recipient") User recipient) {
		this.id = id;
		this.text = text;
		this.senderId = senderId;
		this.recipientId = recipientId;
		this.createdAt = dateConverter.getDate(createdAt);
		this.senderScreenName = senderScreenName;
		this.recipientScreenName = recipientScreenName;
		this.entities = entities;
		this.sender = sender;
		this.recipient = recipient;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("sender_id")
    public long getSenderId() {
        return senderId;
    }

    @JsonProperty("recipient_id")
    public long getRecipientId() {
        return recipientId;
    }

    @JsonIgnore
    public Date getCreatedAt() {
        return createdAt;
    }
    
    @JsonProperty("created_at")
    public String getCreatedAtString() {
        return dateConverter.getString(createdAt);
    }

    @JsonProperty("sender_screen_name") 
    public String getSenderScreenName() {
        return senderScreenName;
    }

    @JsonProperty("recipient_screen_name")
    public String getRecipientScreenName() {
        return recipientScreenName;
    }

    @JsonProperty("sender")
    public User getSender() {
        return sender;
    }
    
    @JsonProperty("recipient")
    public User getRecipient() {
        return recipient;
    }
    
    @JsonProperty("entities")
	public StatusEntities getEntities() {
		return entities;
	}
    
    @Override
	public int hashCode() {
        return (int) id;
    }
    
	@Override
    public boolean equals(Object obj) {
        if (null == obj) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return obj instanceof DirectMessage && ((DirectMessage) obj).getId() == this.id;
    }
}
