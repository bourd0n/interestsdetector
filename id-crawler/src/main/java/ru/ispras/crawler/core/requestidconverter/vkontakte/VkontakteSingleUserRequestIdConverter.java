package ru.ispras.crawler.core.requestidconverter.vkontakte;

import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteUserRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class VkontakteSingleUserRequestIdConverter implements
		IRequestIdConverter<VkontakteUserRequest, Long> {

	@Override
	public Long getId(VkontakteUserRequest request) {
		return request.getId();
	}

	@Override
	public VkontakteUserRequest getRequest(Long id) {
		return new VkontakteUserRequest(id);
	}
}
