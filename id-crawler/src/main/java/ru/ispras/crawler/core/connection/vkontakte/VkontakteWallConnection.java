package ru.ispras.crawler.core.connection.vkontakte;

import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteWallRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.entities.walls.Post;

/**
 * Connection for https://vk.com/dev/wall.get method
 * 
 * @author andrey
 *
 */
public class VkontakteWallConnection implements IConnection<VkontakteWallRequest, BasicResponse<List<Post>>> {

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	public VkontakteWallConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}

	private String getMethod(VkontakteWallRequest request) {
		return "wall.get?owner_id="+request.getUserId()+"&extended=0";
	}
	
	@Override
	public BasicResponse<List<Post>> getResponse(VkontakteWallRequest request)
			throws UnavailableConnectionException {
		String url = getMethod(request);
		List<Post> response = VkontakteConnectionUtils.getPagedResponse(url, 100, request.getLimit(), httpConnection, Post.class);
		return new BasicResponse<List<Post>>(response);
	}
}
