package ru.ispras.crawler.core.datamodel.vkontakte.entities.users;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 15.05.14
 * Time: 15:06
 */
public class Date {
    private final int day;
    private final int month;
    private final int year;

    @JsonCreator
    public Date(
            @JsonProperty("bdate")  String dataString) {
        String[] splitString = dataString.split("\\.");
        this.day = Integer.parseInt(splitString[0]);
        this.month = Integer.parseInt(splitString[1]);
        if(splitString.length == 3)
            this.year = Integer.parseInt(splitString[2]);
        else
            this.year = -1;
    }

    @JsonIgnore
    public int getDay() {
        return day;
    }

    @JsonIgnore
    public int getMonth() {
        return month;
    }

    @JsonIgnore
    public int getYear() {
        return year;
    }

    @JsonProperty("bdate")
    public String getString() {
    	String dayMonth = day + "." + month;
		if (year == -1){
    		return dayMonth;
    	}else{
    		return dayMonth + "." + year;
    	}
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + day;
		result = prime * result + month;
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Date other = (Date) obj;
		if (day != other.day)
			return false;
		if (month != other.month)
			return false;
		if (year != other.year)
			return false;
		return true;
	}  
}
