package ru.ispras.crawler.core.requestidconverter.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteRegionsRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 03.06.14
 * Time: 16:54
 */
public class VkontakteRegionRequestConverter implements IRequestIdConverter<VkontakteRegionsRequest, Long> {
    @Override
    public Long getId(VkontakteRegionsRequest request) {
        return request.getCountryId();
    }

    @Override
    public VkontakteRegionsRequest getRequest(Long aLong) {
        return new VkontakteRegionsRequest(aLong);
    }
}
