package ru.ispras.crawler.core.datamodel.vkontakte.entities.stats;

/**
 * Created by padre on 14.07.14
 */
public class Day {
    private final Integer day;
    private final Integer mounth;
    private final Integer year;

    public Day(int day, int mounth, int year) {
        this.day = day;
        this.mounth = mounth;
        this.year = year;
    }

    public Day(String string) {
        String[] split = string.split("-");
        this.year = Integer.parseInt(split[0]);
        this.mounth = Integer.parseInt(split[1]);
        this.day = Integer.parseInt(split[2]);
    }

    public Integer getDay() {
        return day;
    }

    public Integer getMounth() {
        return mounth;
    }

    public Integer getYear() {
        return year;
    }

    @Override
    public String toString() {
        return year.toString() + "-" + mounth.toString() + "-" + day.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Day)) return false;

        Day day1 = (Day) o;

        if (day != null ? !day.equals(day1.day) : day1.day != null) return false;
        if (mounth != null ? !mounth.equals(day1.mounth) : day1.mounth != null) return false;
        if (year != null ? !year.equals(day1.year) : day1.year != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = day != null ? day.hashCode() : 0;
        result = 31 * result + (mounth != null ? mounth.hashCode() : 0);
        result = 31 * result + (year != null ? year.hashCode() : 0);
        return result;
    }
}
