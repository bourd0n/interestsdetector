package ru.ispras.crawler.core.datamodel.twitter;

import org.apache.commons.lang.Validate;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} for downloading Twitter user profile.
 *
 * See https://dev.twitter.com/rest/reference/get/users/show
 *
 * @author Ivan Andrianov
 * 
 */
public class TwitterUserProfileRequest extends AbstractTwitterUserRequest {
	
	private String screenName = null;
	
	public TwitterUserProfileRequest(long id) {
		super(id);
	}
	
	public TwitterUserProfileRequest(String screenName) {
		super(1);
		Validate.isTrue(screenName != null, "screen name should not be null");
		this.screenName = screenName;
	}

	public String getScreenName() {
		 return screenName;
	}

	public boolean isScreenNameRequest() {
		return screenName != null;
	}

	@Override
	public int hashCode() {
		long id = getId();
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((screenName == null) ? 0 : screenName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TwitterUserProfileRequest other = (TwitterUserProfileRequest) obj;

		long id = getId();
		if (id != other.getId())
			return false;
		if (screenName == null) {
			if (other.screenName != null)
				return false;
		} else if (!screenName.equals(other.screenName))
			return false;
		return true;
	}
}
