package ru.ispras.crawler.core.datamodel.facebook.api;

import ru.ispras.crawler.core.datamodel.IRequest;
/**
 * {@link IRequest} for downloading given user's wall.
 * Example: https://graph.facebook.com/100001698473676
 *
 * @author andrey g
 */
public class FacebookAPIWallRequest extends FacebookAPIPaggedRequest {

	public FacebookAPIWallRequest(long id) {
		this(id, "");
	}
	
	public FacebookAPIWallRequest(long id, String parameters) {
		super(id, "feed", parameters);
	}
	
	public FacebookAPIWallRequest(long id, String parameters, int limit) {
		super(id, "feed", parameters, limit);
	}

}
