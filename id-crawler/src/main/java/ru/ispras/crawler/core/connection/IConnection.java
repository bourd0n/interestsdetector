package ru.ispras.crawler.core.connection;

import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * Interface for receiving {@link IResponse} from {@link IRequest}.
 * <p>
 * Implementations are expected to be thread-safe.
 * 
 * @author Alexander
 */
public interface IConnection<Req extends IRequest, Resp extends IResponse> {
	/**
	 * /** Try to get {@link IResponse} for given {@link IRequest}.
	 * 
	 * @param request
	 */
	public Resp getResponse(Req request) throws UnavailableConnectionException;
}
