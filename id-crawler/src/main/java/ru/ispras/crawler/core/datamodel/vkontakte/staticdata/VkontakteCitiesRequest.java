package ru.ispras.crawler.core.datamodel.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 22.05.14
 * Time: 15:40
 *
 * Request for https://vk.com/dev/database.getCities
 */
public class VkontakteCitiesRequest implements IRequest {
    private final long countryId;

    public VkontakteCitiesRequest(long countryId) {
        this.countryId = countryId;
    }

    public long getCountryId() {
        return countryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VkontakteCitiesRequest that = (VkontakteCitiesRequest) o;

        if (countryId != that.countryId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (countryId ^ (countryId >>> 32));
    }

    @Override
	public String toString() {
		return "VkontakteCitiesRequest [countryId=" + countryId + "]";
	}

}
