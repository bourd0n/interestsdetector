package ru.ispras.crawler.core.connection.facebook.api;

import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.facebook.api.utils.UserToLongConverter;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIMutualFriendsRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.User;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * {@link IConnection} for downloading list of mutual with account owner friends for given Facebook user via Facebook API.
 * Example: https://graph.facebook.com/100001698473676/mutualfriends
 *
 * @author andrey g
 *
 */
public class FacebookAPIMutualFriendsConnection extends
		AbstractFacebookAPIConnection<FacebookAPIMutualFriendsRequest, BasicResponse<List<Long>>> {

	public FacebookAPIMutualFriendsConnection(IConnection<HttpRequest, HttpResponse> facebookAPIHttpConnection) {
		super(facebookAPIHttpConnection);
	}

	@Override
	public BasicResponse<List<Long>> getResponse(FacebookAPIMutualFriendsRequest request) throws UnavailableConnectionException {
		return new BasicResponse<List<Long>>(UserToLongConverter.getLongList(getPagedResponse(request, User.class)));
	}
}
