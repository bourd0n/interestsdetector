package ru.ispras.crawler.core.connection.vkontakte;

import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.vkontakte.utils.VkontakteConnectionUtils;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteLikesRequest;

/**
 * Connection for https://vk.com/dev/likes.getList method
 * 
 * @author andrey
 *
 */
public class VkontakteLikesConnection implements IConnection<VkontakteLikesRequest, BasicResponse<List<Long>>> {

	private final IConnection<HttpRequest, HttpResponse> httpConnection;
	
	public VkontakteLikesConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		this.httpConnection = httpConnection;
	}

	private String getMethod(VkontakteLikesRequest request) {
		return "likes.getList?owner_id="+request.getOwnerId()+
				"&item_id="+request.getItemId()+
				"&type="+request.getType().getValue()+
				"&friends_only=0&extended=0";
	}

	@Override
	public BasicResponse<List<Long>> getResponse(VkontakteLikesRequest request)
			throws UnavailableConnectionException {
		String url = getMethod(request);
		List<Long> response = VkontakteConnectionUtils.getPagedResponse(url, 1000, request.getLimit(), httpConnection, Long.class);
		return new BasicResponse<List<Long>>(response);
	}
}
