package ru.ispras.crawler.core.connection.facebook.api.http;

import javax.xml.bind.annotation.XmlRootElement;

import ru.ispras.crawler.core.connection.http.HttpProxyDescription;
import ru.ispras.crawler.core.connection.http.auth.accesstoken.AccessTokenConfiguration;

/**
 * 
 * @author andrey
 * 
 */
@XmlRootElement(name = "facebookAPI")
public class FacebookAPIHttpConnectionConfiguration extends AccessTokenConfiguration {

	public FacebookAPIHttpConnectionConfiguration() {
		super();
	}

	public FacebookAPIHttpConnectionConfiguration(HttpProxyDescription proxy,
			Integer connectionTimeout, Integer socketTimeout, String token) {
		super(proxy, connectionTimeout, socketTimeout, token);
	}

	public FacebookAPIHttpConnectionConfiguration(HttpProxyDescription proxy,
			String token) {
		super(proxy, token);
	}

	public FacebookAPIHttpConnectionConfiguration(String token) {
		super(token);
	}

	
}
