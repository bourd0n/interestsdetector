package ru.ispras.crawler.core.datamodel.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Tiny wrapper around {@link SimpleDateFormat} which wraps
 * {@link ParseException} into {@link IllegalArgumentException} and provides
 * synchronization.
 * 
 * @author andrey g
 * 
 */
public class DateConverter {
	private final SimpleDateFormat currentFormat;

	public DateConverter(SimpleDateFormat format) {
		this.currentFormat = format;
	}

	public synchronized Date getDate(String string) {
		if (string == null){
			return null;
		}
		try {
			return currentFormat.parse(string);
		} catch (ParseException e) {
			throw new IllegalArgumentException(e);
		}
	}

	public synchronized String getString(Date date) {
		if (date == null){
			return null;
		}
		return currentFormat.format(date);
	}
}
