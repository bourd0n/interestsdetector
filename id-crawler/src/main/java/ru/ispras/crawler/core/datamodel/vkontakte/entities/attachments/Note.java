package ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 16.05.14
 * Time: 17:47
 */
public class Note  extends Attachment {
    private final Long id;
    private final Long userId;
    private final String title;
    private final String text;
    private final Long date;
    private final String comments;
    private final Long readComments;
    private final String viewUrl;

    @JsonCreator
    public Note(
            @JsonProperty("id") Long id,
            @JsonProperty("user_id") Long userId,
            @JsonProperty("title") String title,
            @JsonProperty("text") String text,
            @JsonProperty("date") Long date,
            @JsonProperty("comments") String comments,
            @JsonProperty("read_comments") Long readComments,
            @JsonProperty("view_url") String viewUrl) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.text = text;
        this.date = date;
        this.comments = comments;
        this.readComments = readComments;
        this.viewUrl = viewUrl;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("user_id")
    public Long getUserId() {
        return userId;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("date")
    public Long getDate() {
        return date;
    }

    @JsonProperty("comments")
    public String getComments() {
        return comments;
    }

    @JsonProperty("read_comments")
    public Long getReadComments() {
        return readComments;
    }

    @JsonProperty("view_url")
    public String getViewUrl() {
        return viewUrl;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Note other = (Note) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}
}
