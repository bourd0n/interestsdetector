package ru.ispras.crawler.core.datamodel.vkontakte.entities.users.personal;

/**
 * Created by padre on 14.07.14
 */
public enum Political {
    COMMUNIST, SOCIALIST, MODERATE, LIBERAL, CONSERVATIVE, MONARCHIST, ULTRACONSERVATIVE, INDIFFERENT, LIBERTARIAN, UNKNOWN;

    public static Political intToPolitical(Integer number) {
        if (number == null) return UNKNOWN;
        switch (number) {
            case 1: return COMMUNIST;
            case 2: return SOCIALIST;
            case 3: return MODERATE;
            case 4: return LIBERAL;
            case 5: return CONSERVATIVE;
            case 6: return MONARCHIST;
            case 7: return ULTRACONSERVATIVE;
            case 8: return INDIFFERENT;
            case 9: return LIBERTARIAN;
            default: return UNKNOWN;
        }
    }

    public static Integer politicalToInt(Political type) {
        switch (type) {
            case COMMUNIST: return 1;
            case SOCIALIST: return 2;
            case MODERATE: return 3;
            case LIBERAL: return 4;
            case CONSERVATIVE: return 5;
            case MONARCHIST: return 6;
            case ULTRACONSERVATIVE: return 7;
            case INDIFFERENT: return 8;
            case LIBERTARIAN: return 9;
            default: return 0;
        }
    }
}
