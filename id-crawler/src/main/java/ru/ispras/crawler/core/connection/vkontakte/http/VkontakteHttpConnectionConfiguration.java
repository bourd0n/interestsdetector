package ru.ispras.crawler.core.connection.vkontakte.http;

import javax.xml.bind.annotation.XmlRootElement;

import ru.ispras.crawler.core.connection.http.HttpProxyDescription;
import ru.ispras.crawler.core.connection.http.auth.accesstoken.AccessTokenConfiguration;

/**
 * 
 * @author andrey g
 *
 */
@XmlRootElement(name = "vkontakte")
public class VkontakteHttpConnectionConfiguration extends AccessTokenConfiguration {

	public VkontakteHttpConnectionConfiguration() {
		super();
	}

	public VkontakteHttpConnectionConfiguration(HttpProxyDescription proxy,
			Integer connectionTimeout, Integer socketTimeout, String token) {
		super(proxy, connectionTimeout, socketTimeout, token);
	}

	public VkontakteHttpConnectionConfiguration(HttpProxyDescription proxy,
			String token) {
		super(proxy, token);
	}

	public VkontakteHttpConnectionConfiguration(String token) {
		super(token);
	}
	
}
