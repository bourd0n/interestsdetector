package ru.ispras.crawler.core.requestidconverter.vkontakte;

import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteWallRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class VkontakteWallRequestIdConverter implements
		IRequestIdConverter<VkontakteWallRequest, Long> {

	@Override
	public Long getId(VkontakteWallRequest request) {
		return request.getUserId();
	}

	@Override
	public VkontakteWallRequest getRequest(Long id) {
		return new VkontakteWallRequest(id);
	}

}
