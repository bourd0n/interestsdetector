package ru.ispras.crawler.core.requestidconverter.vkontakte;

import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteLikesRequest;
import ru.ispras.crawler.core.datamodel.vkontakte.VkontakteLikesRequest.Type;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public class VkontakteLikesRequestIdConverter implements
		IRequestIdConverter<VkontakteLikesRequest, String> {

	@Override
	public String getId(VkontakteLikesRequest request) {
		return request.getType()+"_"+request.getOwnerId()+"_"+request.getItemId();
	}

	@Override
	public VkontakteLikesRequest getRequest(String id) {
		String[] elems = id.split("_");
		return new VkontakteLikesRequest(Type.valueOf(elems[0]), Long.parseLong(elems[1]), Long.parseLong(elems[2]));
	}

}
