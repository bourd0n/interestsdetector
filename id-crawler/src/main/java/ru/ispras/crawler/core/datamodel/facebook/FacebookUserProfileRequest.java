package ru.ispras.crawler.core.datamodel.facebook;

import org.apache.commons.lang.Validate;
import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} for downloading profile for given Facebook user.
 * May be specified by user id or screenName.
 *
 * Examples:
 * https://m.facebook.com/profile.php&id=100001698473676&v=info
 * https://m.facebook.com/ndreyg?v=info
 *
 * @author andrey g
 *
 */
public class FacebookUserProfileRequest extends AbstractFacebookUserRequest {
	private final String screenName;
	
	public FacebookUserProfileRequest(long id) {
		super(id);
		screenName = null;
	}
	
	public FacebookUserProfileRequest(String screenName) {
		super();
		Validate.isTrue(screenName != null, "screen name should not be null");
		this.screenName = screenName;
	}
	
	public String getScreenName(){
		return screenName;
	}
	
	public boolean isScreenNameRequest(){
		return screenName != null;
	}

	@Override
	public int hashCode() {
		if (isScreenNameRequest()){
			return ((screenName == null) ? 0 : screenName.hashCode());
		} else{
			return super.hashCode();
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FacebookUserProfileRequest other = (FacebookUserProfileRequest) obj;
		if (screenName == null) {
			if (other.screenName != null)
				return false;
		} else if (!screenName.equals(other.screenName))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		if (isScreenNameRequest()){
			return getClass().getName() + ":[userScreenName=" + screenName + "]";
		}else{
			return super.toString();
		}
	}
	
}
