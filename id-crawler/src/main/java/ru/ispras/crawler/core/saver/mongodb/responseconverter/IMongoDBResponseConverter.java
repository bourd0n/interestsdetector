package ru.ispras.crawler.core.saver.mongodb.responseconverter;

import java.util.List;

import ru.ispras.crawler.core.datamodel.IResponse;

import com.mongodb.DBObject;

/**
 * Converts a {@link IResponse} into a non-empty list of {@link DBObject}s and
 * vice versa.
 * <p>
 * Implementations are expected to be thread-safe.
 * 
 * @author Ivan Andrianov
 */
public interface IMongoDBResponseConverter<Resp extends IResponse> {

	List<DBObject> getValues(Resp response);

	Resp getResponse(List<DBObject> values);
}
