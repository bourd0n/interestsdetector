package ru.ispras.crawler.core.datamodel.http;

import java.net.URL;

import ru.ispras.crawler.core.datamodel.IRequest;

public class HttpRequest implements IRequest {
	private final URL url;

	public HttpRequest(URL url) {
		this.url = url;
	}

	public URL getURL() {
		return url;
	}

	@Override
	public String toString() {
		return getClass().getName() + ":[" + url + "]";
	}

	@Override
	public int hashCode() {
		return url.hashCode();
	}

	@Override
	public boolean equals(Object object) {
		if (object instanceof HttpRequest) {
			return ((HttpRequest) object).getURL().equals(getURL());
		} else {
			return false;
		}

	}
}