package ru.ispras.crawler.core.datamodel.vkontakte.entities.walls;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by padre on 14.07.14
 * see description in https://vk.com/dev/post_source
 */

public class Source {
    private final String type;
    private final String platform;
    private final String data;

    @JsonCreator
    public Source(
            @JsonProperty("type") String type,
            @JsonProperty("platform") String platform,
            @JsonProperty("data") String data) {
        this.type = type;
        this.platform = platform;
        this.data = data;
    }

    @JsonProperty("platform")
    public String getPlatform() {
        return platform;
    }

    @JsonProperty("data")
    public String getData() {
        return data;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((data == null) ? 0 : data.hashCode());
        result = prime * result
                + ((platform == null) ? 0 : platform.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Source other = (Source) obj;
        if (data == null) {
            if (other.data != null)
                return false;
        } else if (!data.equals(other.data))
            return false;
        if (platform == null) {
            if (other.platform != null)
                return false;
        } else if (!platform.equals(other.platform))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }
}
