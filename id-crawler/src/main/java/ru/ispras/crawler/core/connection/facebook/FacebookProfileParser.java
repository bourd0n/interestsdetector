package ru.ispras.crawler.core.connection.facebook;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ru.ispras.crawler.core.datamodel.facebook.entities.Profile;

/**
 * This class creates a Facebook {@link Profile} object form HTML profile page (ex. https://m.facebook.com/profile.php&id=100001698473676&v=info)
 * @author andrey g
 *
 */
class FacebookProfileParser {

	private final static Pattern friendsNumberPattern = Pattern
			.compile("All\\sFriends\\s\\((\\d+)\\)");
	
	private final static Set<String> facebookFields = getFacebookFields();

	/**
	 * Parse raw HTML. User id will be extracted from HTML page  
	 * @param htmlPage
	 * @return
	 */
	public Profile parseProfile(String htmlPage) {
		return parseProfile(htmlPage, null);
	}
	

	/**
	 * Parse raw HTML. User id is extracted from HTML page if userId parameter is null
	 * @param htmlPage
	 * @param userId
	 * @return
	 */
	public Profile parseProfile(String htmlPage, Long userId) {
		final Document doc = Jsoup.parse(htmlPage);

		String name = extractName(doc);
		if (name == null) {
			throw new IllegalArgumentException("Profile must have a name");
		}

		// extract id if unknown (0)
		if (userId == null) {
			userId = extractId(doc);

			if (userId == null) {
				throw new IllegalArgumentException("Profile must have an id");
			}
		}

		int friendsCount = extractNumberOfFriends(htmlPage);

		Map<String, String> singleFields = extractSingleFields(doc);
		
		String politicalViews = singleFields.get("Political Views");
		String religionViews = singleFields.get("Religious Views");
		String gender = singleFields.get("Gender");
		String birthday = singleFields.get("Birthday");
		String currentCity = singleFields.get("Current City");
		String hometown = singleFields.get("Hometown");
		String twitter = singleFields.get("Twitter");
		String website = singleFields.get("Website");
		
		List<List<String>> education = parseMulipleField(doc, "Work");
		List<List<String>> work = parseMulipleField(doc, "Education");
		
		return new Profile(userId, name, friendsCount, 
				politicalViews, religionViews, gender,
				birthday, currentCity, hometown,
				twitter, website, education, work);
	}

	Map<String, String> extractSingleFields(Document doc) {
		Map<String, String> map = new TreeMap<String, String>();

		// perform single fields with using ALL elements' texts
		List<String> mfsmTexts = extractElementsTexts(doc);
		for (int i = 0; i < mfsmTexts.size() - 1; i++) {
			String fieldName = mfsmTexts.get(i);
			// map should contain only Facebook fields as keys
			if (fieldName != null && facebookFields.contains(fieldName)) {
				map.put(fieldName, mfsmTexts.get(i + 1));
			}
		}
		return map;
	}

	private List<String> extractElementsTexts(Document doc) {
		List<String> result = new ArrayList<String>();

		Elements elements = doc.getAllElements();

		for (Element elem : elements){
			result.add(elem.text());
		}

		return result;
	}

	String extractName(Document doc) {
		Elements elems = doc.getElementsByClass("profileName");
		if (elems.size() > 0) {
			Element curElem = elems.get(0);
			return curElem.ownText();
		}
		return null;
	}

	Long extractId(Document doc) {
		Elements buttons = doc.getElementsByClass("btnD");

		for (Element elem : buttons) {
			String text = elem.text();
			String href = elem.attr("href");

			// extract from "Add Friend" button
			if ("Add Friend".equals(text)) {
				return parseIdFromURL(href, "subjectid=");
			}

			// extract from "Like" button
			if ("Like".equals(text)) {
				return parseIdFromURL(href, "id=");
			}
		}
		return null;
	}

	private Long parseIdFromURL(String url, String pattern) {
		int beginIndex = url.indexOf(pattern);
		beginIndex += pattern.length();
		int endIndex = beginIndex;
		while (Character.isDigit(url.charAt(endIndex))) {
			endIndex++;
		}

		return Long.parseLong(url.substring(beginIndex, endIndex));
	}

	Integer extractNumberOfFriends(String htmlPage) {
		final Matcher matcher = friendsNumberPattern.matcher(htmlPage);
		if (matcher.find()) {
			return Integer.parseInt(matcher.group(1));
		}
		return 0;
	}

	private List<String> getMultipleItemValues(Element itemArray) {
		List<String> array = new ArrayList<String>();
		
		Elements itemArrayChilds = itemArray.children();
		//try to parse from EVERY child
		for (Element child : itemArrayChilds) {
			if (child.childNodeSize()>0){
				for (Element childOfChild : child.children()) {
					String text = childOfChild.text();
					if (!"".equals(text)){
						array.add(text);
					}
				}
			}
		}
		return array;
	}

	private List<List<String>> parseMultipleFieldElement(Element arrayOfItems) {
		List<List<String>> collection = new ArrayList<List<String>>();
		
		Elements arrayOfItemsChilds = arrayOfItems.children();
		
		for (Element child : arrayOfItemsChilds) {
			// parse item
			Element itemArray = child.child(0);
			collection.add(getMultipleItemValues(itemArray));
		}
		return collection;
	}

	List<List<String>> parseMulipleField(Document doc, String sectionName) {
		Elements elements = doc.getElementsByClass("_3o-d");
		for (Element elem : elements) {
			String caption = elem.child(0).text();

			if (sectionName.equals(caption)) {
				return parseMultipleFieldElement(elem.child(1));
			}
		}
		return new ArrayList<List<String>>();
	}

	private static Set<String> getFacebookFields() {
		Set<String> result = new HashSet<String>();
		result.add("Political Views");
		result.add("Religious Views");
		result.add("Gender");
		result.add("Birthday");
		result.add("Current City");
		result.add("Hometown");
		result.add("Twitter");
		result.add("Website");
		return result;
	}
	
}
