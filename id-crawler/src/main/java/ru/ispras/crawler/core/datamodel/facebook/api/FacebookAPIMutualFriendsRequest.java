package ru.ispras.crawler.core.datamodel.facebook.api;

/**
 * {@link  FacebookAPIPaggedRequest} for downloading list of mutual with account owner friends for given Facebook user via Facebook API
 * Example: https://graph.facebook.com/100001698473676/mutualfriends
 *
 * @author andrey g
 *
 */
public class FacebookAPIMutualFriendsRequest extends FacebookAPIPaggedRequest{

	public FacebookAPIMutualFriendsRequest(long id) {
		this(id, "");
	}
	
	public FacebookAPIMutualFriendsRequest(long id, String parameters) {
		super(id, "mutualfriends", parameters);
	}
	
	public FacebookAPIMutualFriendsRequest(long id, String parameters, int limit) {
		super(id, "mutualfriends", parameters, limit);
	}

}
