package ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 16.05.14
 * Time: 17:46
 */
public class Doc extends Attachment {
    private final Long id;
    private final Long ownerId;
    private final String title;
    private final int size;
    private final String ext;
    private final String url;
    private final String photo100;
    private final String photo130;

    @JsonCreator
    public Doc(
            @JsonProperty("id") Long id,
            @JsonProperty("owner_id") Long ownerId,
            @JsonProperty("title") String title,
            @JsonProperty("size") int size,
            @JsonProperty("ext") String ext,
            @JsonProperty("url") String url,
            @JsonProperty("photo_100") String photo100,
            @JsonProperty("photo_130") String photo130) {
        this.id = id;
        this.ownerId = ownerId;
        this.title = title;
        this.size = size;
        this.ext = ext;
        this.url = url;
        this.photo100 = photo100;
        this.photo130 = photo130;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("owner_id")
    public Long getOwnerId() {
        return ownerId;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("size")
    public int getSize() {
        return size;
    }

    @JsonProperty("ext")
    public String getExt() {
        return ext;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("photo_100")
    public String getPhoto100() {
        return photo100;
    }

    @JsonProperty("photo_130")
    public String getPhoto130() {
        return photo130;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ownerId == null) ? 0 : ownerId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Doc other = (Doc) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ownerId == null) {
			if (other.ownerId != null)
				return false;
		} else if (!ownerId.equals(other.ownerId))
			return false;
		return true;
	}
   
}

