package ru.ispras.crawler.core.connection.twitter;

import java.util.ArrayList;
import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.AbstractTwitterUserNeigboursRequest;
import ru.ispras.crawler.core.datamodel.twitter.entities.IDs;

/**
 * {@link IConnection} which returns Twitter user id list for every given {@link AbstractTwitterUserNeigboursRequest}.
 * 
 * @author Ivan Andrianov
 * 
 */
public abstract class AbstractTwitterUserNeighboursConnection<Req extends AbstractTwitterUserNeigboursRequest>
		extends AbstractTwitterConnection<Req, BasicResponse<List<Long>>> {
	private static final String METHOD_BASE = "%s/ids.json?%s&%s";

	public AbstractTwitterUserNeighboursConnection(IConnection<HttpRequest, HttpResponse> twitterHttpConnection) {
		super(twitterHttpConnection);
	}

	@Override
	public BasicResponse<List<Long>> getResponse(Req request) throws UnavailableConnectionException {
		Long cursor = null;
		List<Long> ids = new ArrayList<Long>();
		while (true) {
			String method = String.format(METHOD_BASE,
					getUserNeighbourMethod(),
					getIdParameter(request), getCursorParameter(cursor));
			
			IDs response = getHttpResponse(method, IDs.class);
			ids.addAll(response.getIds());

			long nextCursor = response.getNextCursor();
			if (nextCursor == 0 || request.getLimit() <= ids.size()) {
				break;
			}
			cursor = nextCursor;
		}
		return new BasicResponse<List<Long>>(ids);
	}

	/**
	 * @return method name for corresponding neighbour type.
	 */
	protected abstract String getUserNeighbourMethod();
}
