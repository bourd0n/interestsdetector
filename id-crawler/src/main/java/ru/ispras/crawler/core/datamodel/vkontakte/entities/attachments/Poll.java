package ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments;

import static ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils.flagToBoolean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 16.05.14
 * Time: 17:47
 */
public class Poll extends Attachment {
	public static class Answer {
	    private final Long id;
	    private final String text;
	    private final int votes;
	    private final double rate;

	    @JsonCreator
	    public Answer(
	            @JsonProperty("id") Long id,
	            @JsonProperty("text") String text,
	            @JsonProperty("votes") int votes,
	            @JsonProperty("rate") double rate) {
	        this.id = id;
	        this.text = text;
	        this.votes = votes;
	        this.rate = rate;
	    }

	    @JsonProperty("id")
	    public Long getId() {
	        return id;
	    }

	    @JsonProperty("text")
	    public String getText() {
	        return text;
	    }

	    @JsonProperty("votes")
	    public int getVotes() {
	        return votes;
	    }

	    @JsonProperty("rate")
	    public double getRate() {
	        return rate;
	    }

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Answer other = (Answer) obj;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			return true;
		}
	}

    private final Long ownerId;
    private final Long id;
    private final Long created;
    private final String question;
    private final int votes;
    private final Long answerId;
    private final List<Answer> answers;
    private final String text;
    private final double rate;
    private final Boolean anonymous;

    @JsonCreator
    public Poll(
            @JsonProperty("ownerId") Long ownerId,
            @JsonProperty("id") Long id,
            @JsonProperty("created") Long created,
            @JsonProperty("question") String question,
            @JsonProperty("answer_id") Long answerId,
            @JsonProperty("answers") List<Answer> answers,
            @JsonProperty("text") String text,
            @JsonProperty("votes") int votes,
            @JsonProperty("rate") double rate,
            @JsonProperty("anonymous") int anonymous) {
        this.ownerId = ownerId;
        this.id = id;
        this.created = created;
        this.question = question;
        this.votes = votes;
        this.answerId = answerId;
        this.answers = answers;
        this.text = text;
        this.rate = rate;
        this.anonymous = flagToBoolean(anonymous);
    }

    @JsonProperty("ownerId")
    public Long getOwnerId() {
        return ownerId;
    }
    @JsonProperty("id")
    public Long getId() {
        return id;
    }
    @JsonProperty("created")
    public Long getCreated() {
        return created;
    }
    @JsonProperty("question")
    public String getQuestion() {
        return question;
    }
    @JsonProperty("votes")
    public int getVotes() {
        return votes;
    }
    @JsonProperty("answer_id")
    public Long getAnswerId() {
        return answerId;
    }
    @JsonProperty("answers")
    public List<Answer> getAnswers() {
        return answers;
    }
    @JsonProperty("text")
    public String getText() {
        return text;
    }
    @JsonProperty("rate")
    public double getRate() {
        return rate;
    }
    @JsonProperty("anonymous")
    public Boolean getAnonymous() {
        return anonymous;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ownerId == null) ? 0 : ownerId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Poll other = (Poll) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ownerId == null) {
			if (other.ownerId != null)
				return false;
		} else if (!ownerId.equals(other.ownerId))
			return false;
		return true;
	}
}
