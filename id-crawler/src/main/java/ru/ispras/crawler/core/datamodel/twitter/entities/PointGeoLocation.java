package ru.ispras.crawler.core.datamodel.twitter.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PointGeoLocation extends AbstractGeoLocation {
	private final List<Double> coordinates;

	@JsonCreator
	public PointGeoLocation(
			@JsonProperty("type") String type, 
			@JsonProperty("coordinates") List<Double> coordinates) {
		super(type);
		this.coordinates = coordinates;
	}

	@JsonProperty("coordinates")
	public List<Double> getCoordinates() {
		return coordinates;
	}
}
