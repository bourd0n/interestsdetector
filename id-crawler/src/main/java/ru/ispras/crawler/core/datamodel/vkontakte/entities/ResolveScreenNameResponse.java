package ru.ispras.crawler.core.datamodel.vkontakte.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.ispras.crawler.core.connection.vkontakte.VkontakteResolveScreenNameConnection;

/**
 * Created with IntelliJ IDEA.
 * User: andrey
 * Date: 9/1/14
 * Time: 5:11 PM
 * Entity that contains response from {@link VkontakteResolveScreenNameConnection}
 */
public class ResolveScreenNameResponse {
    private final String type;
    private final long objectId;

    @JsonCreator
    public ResolveScreenNameResponse(
            @JsonProperty("type") String type,
            @JsonProperty("object_id") long objectId) {
        this.type = type;
        this.objectId = objectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ResolveScreenNameResponse that = (ResolveScreenNameResponse) o;

        if (objectId != that.objectId) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (int) (objectId ^ (objectId >>> 32));
        return result;
    }

    @JsonProperty("type")
    public String getType() {

        return type;
    }

    @JsonProperty("object_id")
    public long getObjectId() {
        return objectId;
    }
}
