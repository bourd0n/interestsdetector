package ru.ispras.crawler.core.visitor;

import java.util.Collection;
import java.util.Collections;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * {@link IVisitor} implementation which always produces no continuation
 * {@link IRequest}s on failure.
 * 
 * @author Ivan Andrianov
 */
public abstract class AbstractNonFailingVisitor<Req extends IRequest, Resp extends IResponse> implements
		IVisitor<Req, Resp> {

	@Override
	public Collection<? extends IRequest> visit(Req request, Throwable e) {
		return Collections.emptySet();
	}
}
