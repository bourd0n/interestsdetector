package ru.ispras.crawler.core.connection.facebook.http;

import javax.xml.bind.annotation.XmlRootElement;

import ru.ispras.crawler.core.connection.http.auth.cookie.CookieAuthHttpConnectionConfiguration;

@XmlRootElement(name = "facebook")
public class FacebookHttpConnectionConfiguration extends CookieAuthHttpConnectionConfiguration {
	public FacebookHttpConnectionConfiguration() {
	}

	public FacebookHttpConnectionConfiguration(String login, String password) {
		super(login, password);
	}
}
