package ru.ispras.crawler.core.datamodel.facebook.api;

import ru.ispras.crawler.core.datamodel.IRequest;
/**
 * {@link IRequest} for downloading photo object via Facebook API
 * Example: https://graph.facebook.com/941268445887422
 *
 * @author andrey g
 *
 */
public class FacebookAPIVideoRequest extends AbstractFacebookAPIObjectRequest {

	public FacebookAPIVideoRequest(long id, String parameters) {
		super(id, parameters);
	}

	public FacebookAPIVideoRequest(long id) {
		super(id);
	}

}
