package ru.ispras.crawler.core.connection.http.auth.oauth;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import ru.ispras.crawler.core.connection.IConnectionConfiguration;
import ru.ispras.crawler.core.connection.http.HttpConnectionConfiguration;

/**
 * OAuth-based {@link IConnectionConfiguration} implementation.
 * 
 * @author Ivan Andrianov
 * 
 */
@XmlRootElement(name = "oauth-http")
public class OAuthHttpConnectionConfiguration extends HttpConnectionConfiguration {
	private String consumerKey;
	private String consumerSecret;
	private String accessToken;
	private String tokenSecret;

	public OAuthHttpConnectionConfiguration() {
	}

	public OAuthHttpConnectionConfiguration(String consumerKey, String consumerSecret,
			String accessToken, String tokenSecret) {
		this.consumerKey = consumerKey;
		this.consumerSecret = consumerSecret;
		this.accessToken = accessToken;
		this.tokenSecret = tokenSecret;
	}
	
	@XmlElement(required = true)
	public String getConsumerKey() {
		return consumerKey;
	}

	public void setConsumerKey(String consumerKey) {
		this.consumerKey = consumerKey;
	}

	@XmlElement(required = true)
	public String getConsumerSecret() {
		return consumerSecret;
	}

	public void setConsumerSecret(String consumerSecret) {
		this.consumerSecret = consumerSecret;
	}

	@XmlElement(required = true)
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	@XmlElement(required = true)
	public String getTokenSecret() {
		return tokenSecret;
	}

	public void setTokenSecret(String tokenSecret) {
		this.tokenSecret = tokenSecret;
	}
}
