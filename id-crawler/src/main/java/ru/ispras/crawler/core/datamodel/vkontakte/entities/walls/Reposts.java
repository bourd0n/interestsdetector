package ru.ispras.crawler.core.datamodel.vkontakte.entities.walls;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import static ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils.flagToBoolean;

/**
 * Created by padre on 14.07.14
 */
public class Reposts {
    private final int count;
    private final Boolean userReposted;

    @JsonCreator
    public Reposts(
            @JsonProperty("count") int count,
            @JsonProperty("user_reposted") int userReposted) {
        this.count = count;
        this.userReposted = flagToBoolean(userReposted);
    }

    @JsonProperty("count")
    public int getCount() {
        return count;
    }

    @JsonProperty("user_reposted")
    public Boolean getUserReposted() {
        return userReposted;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + count;
        result = prime * result
                + ((userReposted == null) ? 0 : userReposted.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Reposts other = (Reposts) obj;
        if (count != other.count)
            return false;
        if (userReposted == null) {
            if (other.userReposted != null)
                return false;
        } else if (!userReposted.equals(other.userReposted))
            return false;
        return true;
    }
}
