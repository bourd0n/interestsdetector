package ru.ispras.crawler.core.datamodel.zvents;

import java.net.URL;

/**
 * Request for Zvents seed page (page with city events).
 * <p>
 * Example: http://zvents.com/san-francisco-ca/events
 * 
 * @author sysoev
 */
public class SeedZventsRequest extends ZventsRequest {
	public SeedZventsRequest(URL url) {
		super(url);
	}
}
