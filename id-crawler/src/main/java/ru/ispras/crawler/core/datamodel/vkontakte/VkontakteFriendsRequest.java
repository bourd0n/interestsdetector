package ru.ispras.crawler.core.datamodel.vkontakte;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Request for https://vk.com/dev/friends.get method
 * 
 * @author andrey
 *
 */
public class VkontakteFriendsRequest implements IRequest {
	private final long userId;

	public VkontakteFriendsRequest(long userId) {
		this.userId = userId;
	}

	public long getUserId() {
		return userId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (userId ^ (userId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VkontakteFriendsRequest other = (VkontakteFriendsRequest) obj;
		if (userId != other.userId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VkontakteFriendsRequest [userId=" + userId + "]";
	}
}
