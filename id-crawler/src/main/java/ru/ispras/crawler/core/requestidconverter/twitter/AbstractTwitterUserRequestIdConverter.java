package ru.ispras.crawler.core.requestidconverter.twitter;

import ru.ispras.crawler.core.datamodel.twitter.AbstractTwitterUserRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

public abstract class AbstractTwitterUserRequestIdConverter<Req extends AbstractTwitterUserRequest> implements
		IRequestIdConverter<Req, Long> {
	@Override
	public Long getId(Req request) {
		return request.getId();
	}
}
