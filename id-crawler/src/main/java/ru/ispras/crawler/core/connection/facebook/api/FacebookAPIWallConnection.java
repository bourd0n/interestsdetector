package ru.ispras.crawler.core.connection.facebook.api;

import java.util.List;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.facebook.api.FacebookAPIWallRequest;
import ru.ispras.crawler.core.datamodel.facebook.api.entities.Post;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 *
 * {@link IConnection} for downloading given user's wall via Facebook API.
 * Wall is a list of posts.
 * Example: https://graph.facebook.com/100001698473676
 *
 * @author andrey g
 *
 */
public class FacebookAPIWallConnection extends AbstractFacebookAPIConnection<FacebookAPIWallRequest, BasicResponse<List<Post>>> {

	public FacebookAPIWallConnection(IConnection<HttpRequest, HttpResponse> facebookAPIHttpConnection) {
		super(facebookAPIHttpConnection);
	}

	@Override
	public BasicResponse<List<Post>> getResponse(FacebookAPIWallRequest request) throws UnavailableConnectionException {
		return new BasicResponse<List<Post>>(getPagedResponse(request, Post.class));
	}
}
