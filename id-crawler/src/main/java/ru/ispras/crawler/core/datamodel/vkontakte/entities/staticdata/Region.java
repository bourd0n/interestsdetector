package ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 10.06.14
 * Time: 20:05
 */
public class Region {
    private final long id;
    private final String title;
    private final Long countryId;

    @JsonCreator
    public Region(
            @JsonProperty("id") long id,
            @JsonProperty("title") String title,
            @JsonProperty("country_id") Long countryId) {
        this.id = id;
        this.title = title;
        this.countryId = countryId;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("country_id")
    public Long getCountryId() {
        return countryId;
    }
}
