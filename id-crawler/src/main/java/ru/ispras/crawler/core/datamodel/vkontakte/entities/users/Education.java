package ru.ispras.crawler.core.datamodel.vkontakte.entities.users;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 15.05.14
 * Time: 15:05
 */
public class Education {
    private final int university;
    private final String universityName;
    private final int faculty;
    private final String facultyName;
    private final int graduation;

    @JsonCreator
    public Education(
            @JsonProperty("university") int university,
            @JsonProperty("university_name") String universityName,
            @JsonProperty("faculty") int faculty,
            @JsonProperty("faculty_name") String facultyName,
            @JsonProperty("graduation") int graduation) {
        this.university = university;
        this.universityName = universityName;
        this.faculty = faculty;
        this.facultyName = facultyName;
        this.graduation = graduation;
    }

    @JsonProperty("university")
	public int getUniversity() {
		return university;
	}

    @JsonProperty("university_name")
	public String getUniversityName() {
		return universityName;
	}

    @JsonProperty("faculty")
	public int getFaculty() {
		return faculty;
	}

    @JsonProperty("faculty_name")
	public String getFacultyName() {
		return facultyName;
	}

	@JsonProperty("graduation")
	public int getGraduation() {
		return graduation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + faculty;
		result = prime * result + graduation;
		result = prime * result + university;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Education other = (Education) obj;
		if (faculty != other.faculty)
			return false;
		if (graduation != other.graduation)
			return false;
		if (university != other.university)
			return false;
		return true;
	}
}