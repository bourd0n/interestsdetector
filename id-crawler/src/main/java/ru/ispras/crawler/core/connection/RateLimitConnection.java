package ru.ispras.crawler.core.connection;

import java.util.Date;

import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;

/**
 * {@link IConnection} wrapper implementation, which guarantees to access
 * network resources not more often than given interval. Default interval is 5s.
 * 
 * @author Ivan Andrianov
 * 
 */
public class RateLimitConnection<Req extends IRequest, Resp extends IResponse> implements IConnection<Req, Resp> {

	private final IConnection<Req, Resp> wrapped;
	private final long interval;

	private long lastRequestTime = 0;

	public RateLimitConnection(IConnection<Req, Resp> wrapped) {
		this(wrapped, 5000);
	}

	public RateLimitConnection(IConnection<Req, Resp> wrapped, long interval) {
		this.wrapped = wrapped;
		this.interval = interval;
	}

	@Override
	public Resp getResponse(Req request) throws UnavailableConnectionException {
		synchronized (this) {
			long timeout = lastRequestTime + interval;
			long time = System.currentTimeMillis();
			if (timeout > time) {
				throw new UnavailableConnectionException(String.format("Failed to process %s: unvailable until %s",
						request, new Date(timeout)), timeout);
			}
			lastRequestTime = time;
		}
		return wrapped.getResponse(request);
	}
}
