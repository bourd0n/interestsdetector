package ru.ispras.crawler.core.connection.hunch;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.hunch.HunchUserFollowersRequest;

/**
 * {@link IConnection} for downloading list of followers for given user
 * Example: https://hunch.com/ndrey/followers
 *
 */
public class HunchUserFollowersConnection extends AbstractHunchUserListConnection<HunchUserFollowersRequest> {

	private final String METHOD = "%s/followers/";
	
	public HunchUserFollowersConnection(IConnection<HttpRequest, HttpResponse> hunchHttpConnection) {
		super(hunchHttpConnection);
	}

	@Override
	protected String getMethod(HunchUserFollowersRequest request) {
		final String screenName = request.getScreenName();
		return String.format(METHOD, screenName);
	}
}
