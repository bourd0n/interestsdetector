package ru.ispras.crawler.core.datamodel.vkontakte;

import ru.ispras.crawler.core.datamodel.IRequest;


/**
 * Abstract Vkontakte request for multi-page responses.
 * Contains limit field
 * 
 * @author andrey
 *
 */
public abstract class AbstractVkontakteLimitedRequest implements IRequest {
	private final int limit;
	
	public AbstractVkontakteLimitedRequest() {
		this(Integer.MAX_VALUE);
	}

	public AbstractVkontakteLimitedRequest(int limit) {
		this.limit = limit;
	}

	public int getLimit() {
		return limit;
	}
}
