package ru.ispras.crawler.core.connection.twitter.http;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.apache.http.Header;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpUriRequest;

import ru.ispras.crawler.core.connection.exception.PermanentlyUnavailableConnectionException;
import ru.ispras.crawler.core.connection.exception.TargetUnreachableException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.http.auth.oauth.OAuthHttpRequestConverter;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpResponseConverter;
import ru.ispras.crawler.core.connection.http.ext.IHttpRequestConverter;
import ru.ispras.crawler.core.connection.http.ext.IHttpResponseConverter;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * {@link IHttpRequestConverter} and {@link IHttpResponseConverter}
 * implementation for Twitter API.
 * <p>
 * It stores rate limits and timeouts for all types of requests it makes in
 * {@link #convert(HttpUriRequest, org.apache.http.HttpResponse)} and uses them
 * not to be banned ({@link #convert(HttpRequest)} fails with
 * {@link UnavailableConnectionException} without actual request to Twitter
 * API).
 * 
 * @author Ivan Andrianov
 */
public class TwitterHttpRequestResponseConverter implements IHttpRequestConverter, IHttpResponseConverter {
	private static final String RATE_LIMIT_HEADER = "X-Rate-Limit-Remaining";
	private static final String RATE_TIMEOUT_HEADER = "X-Rate-Limit-Reset";

	private static final String AUTHENTICATION_MARKER = "www-authenticate";
	private static final int UNAUTHORIZED = 401;
	private static final int FORBIDDEN = 403;

	private final IHttpRequestConverter requestConverter;
	private final IHttpResponseConverter responseConverter;

	private final Map<String, Integer> rateLimits = new HashMap<>();
	private final Map<String, Long> rateTimeouts = new HashMap<>();

	public TwitterHttpRequestResponseConverter(TwitterHttpConnectionConfiguration configuration) {
		this(new OAuthHttpRequestConverter(configuration), new DefaultHttpResponseConverter());
	}

	public TwitterHttpRequestResponseConverter(IHttpRequestConverter requestConverter,
			IHttpResponseConverter responseConverter) {
		this.requestConverter = requestConverter;
		this.responseConverter = responseConverter;
	}

	@Override
	public HttpUriRequest convert(HttpRequest request) throws UnavailableConnectionException {
		checkRateLimitTimeout(request);
		return requestConverter.convert(request);
	}

	private void checkRateLimitTimeout(HttpRequest request) throws UnavailableConnectionException {
		String path = request.getURL().getPath();
		Long rateTimeout = rateTimeouts.get(path);
		if (isRateLimitExceeded(path) && isRateTimeoutInFuture(rateTimeout)) {
			String errorMessage = String.format("Exceeded rate limit for %s requests. Reset will happen at %s.", path,
					new Date(rateTimeout));
			throw new UnavailableConnectionException(errorMessage, rateTimeout);
		}
	}

	private boolean isRateLimitExceeded(String path) {
		Integer rateLimit = rateLimits.get(path);
		return rateLimit != null && rateLimit == 0;
	}

	private boolean isRateTimeoutInFuture(Long rateTimeout) {
		return rateTimeout != null && rateTimeout > System.currentTimeMillis();
	}

	@Override
	public HttpResponse convert(HttpUriRequest request, org.apache.http.HttpResponse response)
			throws UnavailableConnectionException {
        checkFailedStatus(request, response);
		HttpResponse result = responseConverter.convert(request, response);
        saveRateLimitTimeout(request, response);
		return result;
	}

	private void saveRateLimitTimeout(HttpUriRequest request, org.apache.http.HttpResponse response) {
		int rateLimit = extractRateLimit(response);
		long rateTimeout = extractRateTimeout(response);
		String path = request.getURI().getPath();
		rateLimits.put(path, rateLimit);
		rateTimeouts.put(path, rateTimeout);
	}

	private int extractRateLimit(org.apache.http.HttpResponse response) {
		String rateLimitString = findOrThrowUnreachableException(response.getAllHeaders(), RATE_LIMIT_HEADER);
		try {
			return Integer.parseInt(rateLimitString);
		} catch (NumberFormatException e) {
			throw new TargetUnreachableException(String.format("Header %s should contain a number, contains: %s",
					RATE_LIMIT_HEADER, rateLimitString));
		}
	}

	private long extractRateTimeout(org.apache.http.HttpResponse response) {
		String rateTimeoutString = findOrThrowUnreachableException(response.getAllHeaders(), RATE_TIMEOUT_HEADER);
		try {
			return Long.parseLong(rateTimeoutString) * DateUtils.MILLIS_PER_SECOND;
		} catch (NumberFormatException e) {
			throw new TargetUnreachableException(String.format("Header %s should contain a timestamp, contains: %s",
					RATE_TIMEOUT_HEADER, rateTimeoutString));
		}
	}

	private void checkFailedStatus(HttpUriRequest request, org.apache.http.HttpResponse response)
			throws UnavailableConnectionException {
		StatusLine statusLine = response.getStatusLine();
		int statusCode = statusLine.getStatusCode();
		String errorMessage = String.format("Failed to process %s: %d %s.", request.getURI(), statusCode,
				statusLine.getReasonPhrase());

		if (statusCode == UNAUTHORIZED && !areValidCredentials(response)) {
			throw new PermanentlyUnavailableConnectionException(errorMessage);
		}

		if (statusCode == FORBIDDEN) {
			throw new TargetUnreachableException(errorMessage);
		}
	}

	private boolean areValidCredentials(org.apache.http.HttpResponse response) {
		return find(response.getAllHeaders(), AUTHENTICATION_MARKER) != null;
	}

	private String findOrThrowUnreachableException(Header[] headers, String headerName) {
		final String header = find(headers, headerName);
		if (header == null) {
			throw new TargetUnreachableException(String.format("Header %s must exist!", headerName));
		}
		return header;
	}

	private String find(Header[] headers, String headerName) {
		for (Header header : headers) {
			if (headerName.equalsIgnoreCase(header.getName())) {
				return header.getValue();
			}
		}
		return null;
	}
}
