package ru.ispras.crawler.core.connection.hunch;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.BasicResponse;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * Abstract {@link IConnection} which contains common Hunch-related utils for downloading.
 *
 * Examples:
 * https://hunch.com/ndrey/following
 * https://hunch.com/ndrey/followers
 *
 * @param <Req>
 */
public abstract class AbstractHunchUserListConnection<Req extends IRequest> implements
		IConnection<Req, BasicResponse<List<String>>> {
	
	private final IConnection<HttpRequest, HttpResponse> hunchHttpConnection;
	
	private static final String USER_LIST_SELECTOR = "div.who > h3 > a";
	
	public AbstractHunchUserListConnection(IConnection<HttpRequest, HttpResponse> hunchHttpConnection) {
		this.hunchHttpConnection = hunchHttpConnection;
	}

	@Override
	public BasicResponse<List<String>> getResponse(Req request) throws UnavailableConnectionException {
		final String method = getMethod(request);
		
		return new BasicResponse<List<String>>(HunchUtils.getPagedItems(method, new HunchUtils.IHunchPagedListPageProcessor<String>() {
			@Override
			public List<String> processPage(Document page) {
				return parseUserListPage(page);
			}
		}, hunchHttpConnection));
	}
	
	private List<String> parseUserListPage(Document page) {
		final List<String> userList = new ArrayList<String>();
		final Elements userElements = page.select(USER_LIST_SELECTOR);
		for (Element userElement : userElements) {
			final String screenName = userElement.text();
			userList.add(screenName);
		}
		return userList;
	}
	
	protected abstract String getMethod(Req request);
}
