package ru.ispras.crawler.core.requestidconverter.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.vkontakte.staticdata.VkontakteSchoolsRequest;
import ru.ispras.crawler.core.requestidconverter.IRequestIdConverter;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 03.06.14
 * Time: 16:58
 */
public class VkontakteSchoolRequestConverter implements IRequestIdConverter<VkontakteSchoolsRequest, Long> {
    @Override
    public Long getId(VkontakteSchoolsRequest request) {
        return request.getCityId();
    }

    @Override
    public VkontakteSchoolsRequest getRequest(Long aLong) {
        return new VkontakteSchoolsRequest(aLong);
    }
}
