package ru.ispras.crawler.core.connection.facebook.http;

import ru.ispras.crawler.core.connection.IConfigurableConnectionFactory;
import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.RateLimitConnection;
import ru.ispras.crawler.core.connection.exception.ConnectionCreationException;
import ru.ispras.crawler.core.connection.facebook.AbstractFacebookConnection;
import ru.ispras.crawler.core.connection.http.HttpConnection;
import ru.ispras.crawler.core.connection.http.auth.cookie.CookieAuthHttpClientFactory;
import ru.ispras.crawler.core.connection.http.auth.cookie.CookieAuthHttpResourceProfile;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpRequestConverter;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpResponseConverter;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;

/**
 * Facebook {@link IConfigurableConnectionFactory} implementation.
 * 
 * @author Ivan Andrianov
 * 
 */
public class FacebookHttpConnectionFactory implements
		IConfigurableConnectionFactory<HttpRequest, HttpResponse, FacebookHttpConnectionConfiguration> {
	private final static CookieAuthHttpResourceProfile resourceProfile = new CookieAuthHttpResourceProfile(
			AbstractFacebookConnection.BASE_URL + "login.php", "email", "pass", "c_user");

	@Override
	public IConnection<HttpRequest, HttpResponse> create(FacebookHttpConnectionConfiguration configuration)
			throws ConnectionCreationException {
		return new RateLimitConnection<>(new HttpConnection<>(new DefaultHttpRequestConverter(),
				new CookieAuthHttpClientFactory(resourceProfile), configuration, new DefaultHttpResponseConverter()));
	}
}
