package ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 16.05.14
 * Time: 17:47
 */
public class Application  extends Attachment {
    private final Long id;
    private final String name;
    private final String photo130;
    private final String photo604;

    @JsonCreator
    public Application(
            @JsonProperty("id") Long id,
            @JsonProperty("name") String name,
            @JsonProperty("photo_130") String photo130,
            @JsonProperty("photo_604") String photo604) {
        this.id = id;
        this.name = name;
        this.photo130 = photo130;
        this.photo604 = photo604;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("photo_130")
    public String getPhoto130() {
        return photo130;
    }

    @JsonProperty("photo_604")
    public String getPhoto604() {
        return photo604;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Application other = (Application) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
