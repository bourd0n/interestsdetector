package ru.ispras.crawler.core.datamodel.vkontakte.entities.walls;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Place {
    private final Long id;
    private final String title;
    private final double latitude;
    private final double longitude;
    private final Long created;
    private final String icon;
    private final String country;
    private final String city;
    private final String type;
    private final Long groupId;
    private final String groupPhoto;
    private final int checkins;
    private final Long updated;
    private final String address;

    @JsonCreator
    public Place(
            @JsonProperty("id") Long id,
            @JsonProperty("title") String title,
            @JsonProperty("latitude") double latitude,
            @JsonProperty("longitude") double longitude,
            @JsonProperty("created") Long created,
            @JsonProperty("icon") String icon,
            @JsonProperty("country") String country,
            @JsonProperty("city") String city,
            @JsonProperty("type") String type,
            @JsonProperty("group_id") Long groupId,
            @JsonProperty("group_photo") String groupPhoto,
            @JsonProperty("checkins") int checkins,
            @JsonProperty("updated") Long updated,
            @JsonProperty("address") String address) {
        this.id = id;
        this.title = title;
        this.latitude = latitude;
        this.longitude = longitude;
        this.created = created;
        this.icon = icon;
        this.country = country;
        this.city = city;
        this.type = type;
        this.groupId = groupId;
        this.groupPhoto = groupPhoto;
        this.checkins = checkins;
        this.updated = updated;
        this.address = address;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("latitude")
    public double getLatitude() {
        return latitude;
    }

    @JsonProperty("longitude")
    public double getLongitude() {
        return longitude;
    }

    @JsonProperty("created")
    public long getCreated() {
        return created;
    }

    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("group_id")
    public Long getGroupId() {
        return groupId;
    }

    @JsonProperty("group_photo")
    public String getGroupPhoto() {
        return groupPhoto;
    }

    @JsonProperty("checkins")
    public int getCheckins() {
        return checkins;
    }

    @JsonProperty("updated")
    public Long getUpdated() {
        return updated;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Place other = (Place) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}