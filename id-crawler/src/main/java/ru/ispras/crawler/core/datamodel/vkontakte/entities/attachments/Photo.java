package ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 16.05.14
 * Time: 17:44
 */
public class Photo extends Attachment{
    private final Long id;
    private final Long albumId;
    private final Long ownerId;
    private final Long userId;
    private final String photo75;
    private final String photo130;
    private final String photo604;
    private final String photo807;
    private final String photo1280;
    private final String photo2560;
    private final int width;
    private final int height;
    private final String text;
    private final Long date;

    @JsonCreator
    public Photo(
            @JsonProperty("id") Long id,
            @JsonProperty("album_id") Long albumId,
            @JsonProperty("owner_id") Long ownerId,
            @JsonProperty("user_id") Long userId,
            @JsonProperty("photo_75") String photo75,
            @JsonProperty("photo_130") String photo130,
            @JsonProperty("photo_604") String photo604,
            @JsonProperty("photo_807") String photo807,
            @JsonProperty("photo_1280") String photo1280,
            @JsonProperty("photo_2560") String photo2560,
            @JsonProperty("width") int width,
            @JsonProperty("height") int height,
            @JsonProperty("text") String text,
            @JsonProperty("date") Long date) {
        this.id = id;
        this.albumId = albumId;
        this.ownerId = ownerId;
        this.userId = userId;
        this.photo75 = photo75;
        this.photo130 = photo130;
        this.photo604 = photo604;
        this.photo807 = photo807;
        this.photo1280 = photo1280;
        this.photo2560 = photo2560;
        this.width = width;
        this.height = height;
        this.text = text;
        this.date = date;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("album_id")
    public Long getAlbumId() {
        return albumId;
    }

    @JsonProperty("owner_id")
    public Long getOwnerId() {
        return ownerId;
    }

    @JsonProperty("user_id")
    public Long getUserId() {
        return userId;
    }

    @JsonProperty("photo_75")
    public String getPhoto75() {
        return photo75;
    }

    @JsonProperty("photo_130")
    public String getPhoto130() {
        return photo130;
    }

    @JsonProperty("photo_604")
    public String getPhoto604() {
        return photo604;
    }

    @JsonProperty("photo_807")
    public String getPhoto807() {
        return photo807;
    }

    @JsonProperty("photo_1280")
    public String getPhoto1280() {
        return photo1280;
    }

    @JsonProperty("photo_2560")
    public String getPhoto2560() {
        return photo2560;
    }

    @JsonProperty("width")
    public int getWidth() {
        return width;
    }

    @JsonProperty("height")
    public int getHeight() {
        return height;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("date")
    public Long getDate() {
        return date;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((albumId == null) ? 0 : albumId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ownerId == null) ? 0 : ownerId.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Photo other = (Photo) obj;
		if (albumId == null) {
			if (other.albumId != null)
				return false;
		} else if (!albumId.equals(other.albumId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ownerId == null) {
			if (other.ownerId != null)
				return false;
		} else if (!ownerId.equals(other.ownerId))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

}
