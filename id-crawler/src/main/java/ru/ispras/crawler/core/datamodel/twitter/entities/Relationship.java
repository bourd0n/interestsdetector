/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.twitter.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * A data class that has detailed information about a relationship between two users
 *
 * @author Perry Sakkaris - psakkaris at gmail.com, andrey g
 * @see <a href="https://dev.twitter.com/docs/api/1.1/get/friendships/show">GET friendships/show | Twitter Developers</a>
 * @since Twitter4J 2.1.0
 */
public class Relationship {
	private final Target target;
	private final Source source;
    
	@JsonCreator
    public Relationship(
    			@JsonProperty("target") Target target,
    			@JsonProperty("source") Source source) {
		this.target = target;
		this.source = source;
	}
	
	@JsonProperty("target")
	public Target getTarget() {
		return target;
	}

	@JsonProperty("source")
	public Source getSource() {
		return source;
	}

	/**
	 *  This class represents target user part of Relationship
	 * @author andrey g
	 *
	 */
	public static class Target{
		private final long id;
        private final String screenName;
        private final boolean following;
        private final boolean followedBy;
        
        @JsonCreator
        public Target(
        			@JsonProperty("id") long id,
        			@JsonProperty("screen_name") String screenName,
        			@JsonProperty("following") boolean following,
        			@JsonProperty("followed_by") boolean followedBy) {
			this.id = id;
			this.screenName = screenName;
			this.following = following;
			this.followedBy = followedBy;
		}

        @JsonProperty("id")
		public long getId() {
			return id;
		}

        @JsonProperty("screen_name")
		public String getScreenName() {
			return screenName;
		}

        @JsonProperty("following")
		public boolean isFollowing() {
			return following;
		}

        @JsonProperty("followed_by")
		public boolean isFollowedBy() {
			return followedBy;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (id ^ (id >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Target other = (Target) obj;
			if (id != other.id)
				return false;
			return true;
		}
    }

	/**
	 *  This class represents source user part of Relationship
	 * @author andrey g
	 *
	 */
    public static class Source{
        private final long id;
        private final String screenName;
        private final boolean blocking;
        private final boolean markedSpam;
		private final boolean notificationsEnabled;
        private final boolean following;
        private final boolean followedBy;
        private final boolean canDm;
        private final boolean wantRetweets;
        
        public Source(
        			@JsonProperty("id") long id,
        			@JsonProperty("screen_name") String screenName,
        			@JsonProperty("blocking") boolean blocking,
        			@JsonProperty("marked_spam") boolean markedSpam,
        			@JsonProperty("notifications_enabled") boolean notificationsEnabled,
        			@JsonProperty("following") boolean following,
        			@JsonProperty("followed_by") boolean followedBy,
        			@JsonProperty("can_dm") boolean canDm,
        			@JsonProperty("want_retweets") boolean wantRetweets) {
			this.id = id;
			this.screenName = screenName;
			this.blocking = blocking;
			this.markedSpam = markedSpam;
			this.notificationsEnabled = notificationsEnabled;
			this.following = following;
			this.followedBy = followedBy;
			this.canDm = canDm;
			this.wantRetweets = wantRetweets;
		}
        
        @JsonProperty("id")
		public long getId() {
			return id;
		}

        @JsonProperty("screen_name")
		public String getScreenName() {
			return screenName;
		}

        @JsonProperty("blocking")
		public boolean isBlocking() {
			return blocking;
		}

        @JsonProperty("marked_spam")
		public boolean getMarkedSpam() {
			return markedSpam;
		}

        @JsonProperty("notifications_enabled") 
		public boolean isNotificationsEnabled() {
			return notificationsEnabled;
		}

        @JsonProperty("following")
		public boolean isFollowing() {
			return following;
		}
        
        @JsonProperty("followed_by")
		public boolean isFollowedBy() {
			return followedBy;
		}

        @JsonProperty("can_dm")
		public boolean isCanDm() {
			return canDm;
		}

        @JsonProperty("want_retweets")
		public boolean isWantRetweets() {
			return wantRetweets;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (id ^ (id >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Source other = (Source) obj;
			if (id != other.id)
				return false;
			return true;
		}
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Relationship other = (Relationship) obj;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}   
}