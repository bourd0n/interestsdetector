/*
 * Copyright 2007 Yusuke Yamamoto
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.twitter.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Yusuke Yamamoto - yusuke at mac.com, andrey g
 * @since Twitter4J 2.1.1
 */
public class Category {

	private final String name;
	private final String slug;
	private final int size;

	@JsonCreator
	public Category(
			@JsonProperty("name") String name,
			@JsonProperty("slug") String slug,
			@JsonProperty("size") int size) {
		this.name = name;
		this.slug = slug;
		this.size = size;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("slug")
	public String getSlug() {
		return slug;
	}

	@JsonProperty("size")
	public int getSize() {
		return size;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Category that = (Category) o;

		if (size != that.size)
			return false;
		if (name != null ? !name.equals(that.name) : that.name != null)
			return false;
		if (slug != null ? !slug.equals(that.slug) : that.slug != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (slug != null ? slug.hashCode() : 0);
		result = 31 * result + size;
		return result;
	}
	
}
