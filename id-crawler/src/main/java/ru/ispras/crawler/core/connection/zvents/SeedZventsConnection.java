package ru.ispras.crawler.core.connection.zvents;

import java.net.URL;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.zvents.SeedZventsRequest;
import ru.ispras.crawler.core.datamodel.zvents.SeedZventsResponse;

/**
 * Connection to download and process Zvents seed page.
 * 
 * @author sysoev
 */
public class SeedZventsConnection extends ZventsConnection<SeedZventsRequest, SeedZventsResponse> {

	private static final Logger logger = Logger.getLogger(SeedZventsConnection.class);
	
	public SeedZventsConnection(
			IConnection<HttpRequest, HttpResponse> httpConnection) {
		super(httpConnection);
	}

	@Override
	protected SeedZventsResponse convertToResponse(Document doc, String html, SeedZventsRequest request) {
		URL moreEventsUrl = null;
		try {
			final Element div = doc.getElementsByClass("link_to_more").first();
			if (div != null) {
				final String url = div.getElementsByTag("a").attr("abs:href");
				moreEventsUrl = new URL(url);
			}
		}
		catch (Exception e) {
			logger.warn("", e);
		}
		return new SeedZventsResponse(moreEventsUrl);
	}
}
