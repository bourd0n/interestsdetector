package ru.ispras.crawler.core.requestidconverter.facebook;

import ru.ispras.crawler.core.datamodel.facebook.FacebookUserFriendsRequest;

public class FacebookUserFriendsRequestIdConverter extends
		AbstractFacebookUserRequestIdConverter<FacebookUserFriendsRequest> {
	@Override
	public FacebookUserFriendsRequest getRequest(Long id) {
		return new FacebookUserFriendsRequest(id);
	}
}
