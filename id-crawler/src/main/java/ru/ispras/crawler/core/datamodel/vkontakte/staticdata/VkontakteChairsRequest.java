package ru.ispras.crawler.core.datamodel.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 22.05.14
 * Time: 16:04
 *
 * Request for https://vk.com/dev/database.getChairs
 */
public class VkontakteChairsRequest implements IRequest{
    private final long facultyId;

    public VkontakteChairsRequest(long facultyId) {
        this.facultyId = facultyId;
    }

    public long getFacultyId() {
        return facultyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VkontakteChairsRequest)) return false;

        VkontakteChairsRequest that = (VkontakteChairsRequest) o;

        if (facultyId != that.facultyId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (int) (facultyId ^ (facultyId >>> 32));
    }

    @Override
	public String toString() {
		return "VkontakteChairsRequest [facultyId=" + facultyId + "]";
	}
}
