package ru.ispras.crawler.core.requestidconverter.hunch;

import ru.ispras.crawler.core.datamodel.hunch.HunchUserItemRatingsRequest;

public class HunchUserItemRatingsRequestIdConverter extends
		AbstractHunchUserRequestIdConverter<HunchUserItemRatingsRequest> {
	@Override
	public HunchUserItemRatingsRequest getRequest(String screenName) {
		return new HunchUserItemRatingsRequest(screenName);
	}
}
