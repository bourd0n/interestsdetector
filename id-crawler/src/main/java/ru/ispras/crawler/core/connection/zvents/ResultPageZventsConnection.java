package ru.ispras.crawler.core.connection.zvents;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.zvents.ResultPageZventsRequest;
import ru.ispras.crawler.core.datamodel.zvents.ResultPageZventsResponse;
import ru.ispras.crawler.core.datamodel.zvents.ResultPageZventsResponse.EventUrlId;
import ru.ispras.crawler.core.datamodel.zvents.ResultPageZventsResponse.VenueUrlId;

/**
 * Connection to download and process Zvents search result page.
 * 
 * @author sysoev
 */
public class ResultPageZventsConnection extends ZventsConnection<ResultPageZventsRequest, ResultPageZventsResponse> {

	private static final Logger logger = Logger.getLogger(ResultPageZventsConnection.class);
	
	public ResultPageZventsConnection(IConnection<? super HttpRequest, ? extends HttpResponse> httpConnection) {
		super(httpConnection);
	}

	@Override
	protected ResultPageZventsResponse convertToResponse(Document doc, String html, ResultPageZventsRequest request) {
		final Collection<EventUrlId> events = new ArrayList<>();
		final Collection<VenueUrlId> venues = new ArrayList<>();
		collectEventsAndVenues(doc, events, venues);
		final URL nextPageUrl = getNextPageURL(doc);
		return new ResultPageZventsResponse(nextPageUrl, events, venues);
	}

	// ***************************************************************************
	
	private void collectEventsAndVenues(
			Document doc,
			Collection<EventUrlId> events, 
			Collection<VenueUrlId> venues) {
		final Elements resultItems = doc.getElementsByClass("resultitem");
		for (Element resultItem : resultItems) {
			final VenueUrlId venue = getVenue(resultItem);
			if (venue != null) {
				venues.add(venue);
				final EventUrlId event = getEvent(resultItem, venue.getId());
				if (event != null) {
					events.add(event);
				}
			}
		}
	}
	
	private VenueUrlId getVenue(Element resultItem) {
		try {
			final Element ahref = resultItem.select("a[data-z-destination-venue-id]").first();
			final String id = ahref.attr("data-z-destination-venue-id");
			final String url = ahref.attr("abs:href");
			return new VenueUrlId(new URL(url), id);
		}
		catch (Exception e) {
			logger.warn("Could not add venue request.", e);
		}
		return null;
	}

	private EventUrlId getEvent(Element resultItem, String venueId) {
		try {
			final Element ahref = resultItem.getElementsByClass("search_result_content").first()
					.getElementsByClass("event_detail_link").first();
			final String id = ahref.attr("data-z-event-id");
			final String url = ahref.attr("abs:href");
			return new EventUrlId(new URL(url), id, venueId);
		}
		catch (Exception e) {
			logger.warn("Could not add event request.", e);
		}
		return null;
	}
	
	// ***************************************************************************
	
	private URL getNextPageURL(Document doc) {
		for (Element pager : doc.getElementsByClass("pager")) {
			final Element ahref = pager.getElementsByTag("a").first();
			if (ahref.text().toLowerCase().contains("next")) {
				final String url = ahref.attr("abs:href");
				try {
					return new URL(url);
				}
				catch (MalformedURLException e) {
					return null;
				}
			}
		}
		return null;
	}
}
