package ru.ispras.crawler.core.datamodel.vkontakte.entities.groups;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 19.05.14
 * Time: 16:08
 */

import static ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils.booleanToFlag;
import static ru.ispras.crawler.core.datamodel.vkontakte.utils.VkontakteUtils.flagToBoolean;

import java.util.List;

import ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata.IdTitle;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * see description in https://vk.com/dev/fields_groups
 */
public class Group {
	
	public static class Link {
		private final String url;
		private final String name;
		private final String desc;
		
		@JsonCreator
		public Link(
				@JsonProperty("url") String url, 
				@JsonProperty("name") String name, 
				@JsonProperty("desc") String desc) {
			this.url = url;
			this.name = name;
			this.desc = desc;
		}

		@JsonProperty("url")
		public String getUrl() {
			return url;
		}

		@JsonProperty("name")
		public String getName() {
			return name;
		}

		@JsonProperty("desc")
		public String getDesc() {
			return desc;
		}
	}
	
	
	public static class Contact {
		private final long userId;
		private final String desc;
		private final String phone;
		private final String email;
		
		@JsonCreator
		public Contact(
				@JsonProperty("user_id") long userId, 
				@JsonProperty("desc") String desc, 
				@JsonProperty("phone") String phone, 
				@JsonProperty("email") String email) {
			this.userId = userId;
			this.desc = desc;
			this.phone = phone;
			this.email = email;
		}
		
		@JsonProperty("user_id")
		public long getUserId() {
			return userId;
		}
		
		@JsonProperty("desc")
		public String getDesc() {
			return desc;
		}
		
		@JsonProperty("phone")
		public String getPhone() {
			return phone;
		}
		
		@JsonProperty("email")
		public String getEmail() {
			return email;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (userId ^ (userId >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Contact other = (Contact) obj;
			if (userId != other.userId)
				return false;
			return true;
		}
	}
	
	public static class Counters {
	    private final int photos;
	    private final int albums;
	    private final int audios;
	    private final int videos;
	    private final int topics;
	    private final int docs;

	    @JsonCreator
	    public Counters(
	            @JsonProperty("photos") int photos,
	            @JsonProperty("albums") int albums,
	            @JsonProperty("audios") int audios,
	            @JsonProperty("videos") int videos,
	            @JsonProperty("topics") int topics,
	            @JsonProperty("docs") int docs) {
	        this.photos = photos;
	        this.albums = albums;
	        this.audios = audios;
	        this.videos = videos;
	        this.topics = topics;
	        this.docs = docs;
	    }

	    @JsonProperty("photos")
	    public int getPhotos() {
	        return photos;
	    }

	    @JsonProperty("albums")
	    public int getAlbums() {
	        return albums;
	    }

	    @JsonProperty("audios")
	    public int getAudios() {
	        return audios;
	    }

	    @JsonProperty("videos")
	    public int getVideos() {
	        return videos;
	    }

	    @JsonProperty("topics")
	    public int getTopics() {
	        return topics;
	    }

	    @JsonProperty("docs")
	    public int getDocs() {
	        return docs;
	    }

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + albums;
			result = prime * result + audios;
			result = prime * result + docs;
			result = prime * result + photos;
			result = prime * result + topics;
			result = prime * result + videos;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Counters other = (Counters) obj;
			if (albums != other.albums)
				return false;
			if (audios != other.audios)
				return false;
			if (docs != other.docs)
				return false;
			if (photos != other.photos)
				return false;
			if (topics != other.topics)
				return false;
			if (videos != other.videos)
				return false;
			return true;
		}
	}

	public static class Place {
	    private final long id;
	    private final String title;
	    private final int latitude;
	    private final int longitude;
	    private final String type;
	    private final int country;
	    private final int city;
	    private final String address;

	    @JsonCreator
	    public Place(
	            @JsonProperty("id") long id,
	            @JsonProperty("title") String title,
	            @JsonProperty("latitude") int latitude,
	            @JsonProperty("longitude") int longitude,
	            @JsonProperty("type") String type,
	            @JsonProperty("country") int country,
	            @JsonProperty("city") int city,
	            @JsonProperty("address") String address) {
	        this.id = id;
	        this.title = title;
	        this.latitude = latitude;
	        this.longitude = longitude;
	        this.type = type;
	        this.country = country;
	        this.city = city;
	        this.address = address;
	    }
	    @JsonProperty("id")
	    public long getId() {
	        return id;
	    }

	    @JsonProperty("title")
	    public String getTitle() {
	        return title;
	    }

	    @JsonProperty("latitude")
	    public int getLatitude() {
	        return latitude;
	    }

	    @JsonProperty("longitude")
	    public int getLongitude() {
	        return longitude;
	    }

	    @JsonProperty("type")
	    public String getType() {
	        return type;
	    }

	    @JsonProperty("country")
	    public int getCountry() {
	        return country;
	    }

	    @JsonProperty("city")
	    public int getCity() {
	        return city;
	    }

	    @JsonProperty("address")
	    public String getAddress() {
	        return address;
	    }
	    
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (id ^ (id >>> 32));
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Place other = (Place) obj;
			if (id != other.id)
				return false;
			return true;
		}
	}
	
    private final long id;
    private final String name;
    private final String screenName;
    private final int isClosed;
    private final String deactivated;
    private final int isAdmin;
    private final int adminLevel;
    private final boolean isMember;
    private final GroupType type;
    private final String photo50;
    private final String photo100;
    private final String photo200;
    private final IdTitle city;
    private final IdTitle country;
    private final Place place;
    private final String description;
    private final String wikiPage;
    private final int membersCount;
    private final Counters counters;
    private final long startDate;
    private final long endDate;
    private final boolean canPost;
    private final boolean canSeeAllPosts;
    private final boolean canUploadDoc;
    private final boolean canCreateTopic;
    private final String activity;
    private final String status;
    private final List<Contact> contacts;
    private final List<Link> links;
    private final long fixedPost;
    private final boolean verified;
    private final String site;

    @JsonCreator
    public Group(
            @JsonProperty("id") long id,
            @JsonProperty("name") String name,
            @JsonProperty("screen_name") String screenName,
            @JsonProperty("is_closed") int closed,
            @JsonProperty("deactivated") String deactivated,
            @JsonProperty("is_admin") int admin,
            @JsonProperty("admin_level") int adminLevel,
            @JsonProperty("is_member") int member,
            @JsonProperty("type") String type,
            @JsonProperty("photo_50") String photo50,
            @JsonProperty("photo_100") String photo100,
            @JsonProperty("photo_200") String photo200,
            @JsonProperty("city") IdTitle city,
            @JsonProperty("country") IdTitle country,
            @JsonProperty("place") Place place,
            @JsonProperty("description") String description,
            @JsonProperty("wiki_page") String wikiPage,
            @JsonProperty("members_count") int membersCount,
            @JsonProperty("counters") Counters counters,
            @JsonProperty("start_date") long startDate,
            @JsonProperty("end_date") long endDate,
            @JsonProperty("can_post") int canPost,
            @JsonProperty("can_see_all_posts") int canSeeAllPosts,
            @JsonProperty("can_upload_doc") int canUploadDoc,
            @JsonProperty("can_create_topic") int canCreateTopic,
            @JsonProperty("activity") String activity,
            @JsonProperty("status") String status,
            @JsonProperty("contacts") List<Contact> contacts,
            @JsonProperty("links") List<Link> links,
            @JsonProperty("fixed_post") long fixedPost,
            @JsonProperty("verified") int verified,
            @JsonProperty("site") String site) {
        this.id = id;
        this.name = name;
        this.screenName = screenName;
        isClosed = closed;
        this.deactivated = deactivated;
        isAdmin = admin;
        this.adminLevel = adminLevel;
        isMember = flagToBoolean(member);
        this.type = GroupType.stringToType(type);
        this.photo50 = photo50;
        this.photo100 = photo100;
        this.photo200 = photo200;
        this.city = city;
        this.country = country;
        this.place = place;
        this.description = description;
        this.wikiPage = wikiPage;
        this.membersCount = membersCount;
        this.counters = counters;
        this.startDate = startDate;
        this.endDate = endDate;
        this.canPost = flagToBoolean(canPost);
        this.canSeeAllPosts = flagToBoolean(canSeeAllPosts);
        this.canUploadDoc = flagToBoolean(canUploadDoc);
        this.canCreateTopic = flagToBoolean(canCreateTopic);
        this.activity = activity;
        this.status = status;
        this.contacts = contacts;
        this.links = links;
        this.fixedPost = fixedPost;
        this.verified = flagToBoolean(verified);
        this.site = site;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("screen_name")
    public String getScreenName() {
        return screenName;
    }

    @JsonProperty("is_closed")
    public int getClosed() {
        return isClosed;
    }

    @JsonProperty("deactivated")
    public String getDeactivated() {
        return deactivated;
    }

    @JsonProperty("is_admin")
    public int getAdmin() {
        return isAdmin;
    }

    @JsonProperty("admin_level")
    public int getAdminLevel() {
        return adminLevel;
    }

    @JsonProperty("is_member")
    public int getMember() {
        return booleanToFlag(isMember);
    }

    @JsonIgnore
    public boolean isMember() {
        return isMember;
    }

    @JsonProperty("type")
    public String getTypeString() {
        return GroupType.typeToString(type);
    }

    @JsonIgnore
    public GroupType getType() {
        return type;
    }

    @JsonProperty("photo_50")
    public String getPhoto50() {
        return photo50;
    }

    @JsonProperty("photo_100")
    public String getPhoto100() {
        return photo100;
    }

    @JsonProperty("photo_200")
    public String getPhoto200() {
        return photo200;
    }

    @JsonProperty("city")
    public IdTitle getCity() {
        return city;
    }

    @JsonProperty("country")
    public IdTitle getCountry() {
        return country;
    }

    @JsonProperty("place")
    public Place getPlace() {
        return place;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("wiki_page")
    public String getWikiPage() {
        return wikiPage;
    }

    @JsonProperty("members_count")
    public int getMembersCount() {
        return membersCount;
    }

    @JsonProperty("counters")
    public Counters getCounters() {
        return counters;
    }

    @JsonProperty("start_date")
    public long getStartDate() {
        return startDate;
    }

    @JsonProperty("end_date")
    public long getEndDate() {
        return endDate;
    }

    @JsonProperty("can_post")
    public int getCanPost() {
        return booleanToFlag(canPost);
    }

    @JsonIgnore
    public boolean isCanPost() {
        return canPost;
    }

    @JsonProperty("can_see_all_posts")
    public int getCanSeeAllPosts() {
        return booleanToFlag(canSeeAllPosts);
    }

    @JsonIgnore
    public boolean isCanSeeAllPosts() {
        return canSeeAllPosts;
    }

    @JsonProperty("can_upload_doc")
    public int getCanUploadDoc() {
        return booleanToFlag(canUploadDoc);
    }

    @JsonIgnore
    public boolean isCanUploadDoc() {
        return canUploadDoc;
    }

    @JsonProperty("can_create_topic")
    public int getCanCreateTopic() {
        return booleanToFlag(canCreateTopic);
    }

    @JsonIgnore
    public boolean isCanCreateTopic() {
        return canCreateTopic;
    }

    @JsonProperty("activity")
    public String getActivity() {
        return activity;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("contacts")
    public List<Contact> getContacts() {
        return contacts;
    }

    @JsonProperty("links")
    public List<Link> getLinks() {
        return links;
    }

    @JsonProperty("fixed_post")
    public long getFixedPost() {
        return fixedPost;
    }

    @JsonProperty("verified")
    public int getVerified() {
        return booleanToFlag(verified);
    }

    @JsonIgnore
    public boolean isVerified() {
        return verified;
    }

    @JsonProperty("site")
    public String getSite() {
        return site;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
