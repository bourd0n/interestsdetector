package ru.ispras.crawler.core.datamodel.vkontakte.entities.staticdata;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 28.05.14
 * Time: 16:05
 */
public class City {
    private final long id;
    private final String title;
    private final String area;
    private final String region;
    private final Long countryId;

    @JsonCreator
    public City(
            @JsonProperty("id") long id,
            @JsonProperty("title") String title,
            @JsonProperty("region") String region,
            @JsonProperty("area") String area,
            @JsonProperty("important") Integer important,
            @JsonProperty("country_id") Long countryId) {
        this.id = id;
        this.title = title;
        if(important != null && important.equals(1) && area == null) {this.area = title;} else {this.area = area;}
        if(important != null && important.equals(1) && region == null) {this.region = title;} else {this.region = region;}
        this.countryId = countryId;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("region")
    public String getArea() {
        return area;
    }

    @JsonProperty("area")
    public String getRegion() {
        return region;
    }

    @JsonProperty("country_id")
    public Long getCountryId() {
        return countryId;
    }

    public City setCountry(long countryId) {
        return new City(id, title, region, area, 0, countryId);
    }
}
