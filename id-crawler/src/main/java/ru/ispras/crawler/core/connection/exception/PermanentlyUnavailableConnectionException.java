package ru.ispras.crawler.core.connection.exception;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link UnavailableConnectionException} subclass which signals that
 * {@link IConnection} is unavailable for unlimited amount of time for
 * processing of any {@link IRequest} (e.g., because of invalid credentials).
 * 
 * @author Ivan Andrianov
 */
public class PermanentlyUnavailableConnectionException extends UnavailableConnectionException {
	private static final long serialVersionUID = 5229774038216494028L;

	public PermanentlyUnavailableConnectionException() {
		super(Long.MAX_VALUE);
	}

	public PermanentlyUnavailableConnectionException(String message, Throwable cause) {
		super(message, cause, Long.MAX_VALUE);
	}

	public PermanentlyUnavailableConnectionException(String message) {
		super(message, Long.MAX_VALUE);
	}

	public PermanentlyUnavailableConnectionException(Throwable cause) {
		super(cause, Long.MAX_VALUE);
	}
}
