package ru.ispras.crawler.core.datamodel.vkontakte;

import ru.ispras.crawler.core.datamodel.batch.BasicBatchRequest;
import ru.ispras.crawler.core.datamodel.batch.IBatchRequest;

import java.util.Arrays;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: andrey
 * Date: 9/1/14
 * Time: 6:12 PM
 * {@link IBatchRequest} for {@link VkontakteUserRequest}
 */
public class VkontakteBatchUserRequest extends BasicBatchRequest<VkontakteUserRequest> {
    public VkontakteBatchUserRequest(VkontakteUserRequest... requests) {
        super(Arrays.asList(requests));
    }

    public VkontakteBatchUserRequest(List<VkontakteUserRequest> requests) {
        super(requests);
    }
}