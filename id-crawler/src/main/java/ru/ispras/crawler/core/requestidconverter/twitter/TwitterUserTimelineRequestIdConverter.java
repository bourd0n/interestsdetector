package ru.ispras.crawler.core.requestidconverter.twitter;

import ru.ispras.crawler.core.datamodel.twitter.TwitterUserTimelineRequest;

public class TwitterUserTimelineRequestIdConverter extends
		AbstractTwitterUserRequestIdConverter<TwitterUserTimelineRequest> {
	@Override
	public TwitterUserTimelineRequest getRequest(Long id) {
		return new TwitterUserTimelineRequest(id);
	}
}
