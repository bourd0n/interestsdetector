package ru.ispras.crawler.core.datamodel.vkontakte;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.Validate;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Request for https://vk.com/dev/groups.getMembers method
 * 
 * @author andrey
 *
 */
public class VkontakteIsMemberRequest implements IRequest {
	private final long groupId;
	private final List<Long> userIds;
	
	public VkontakteIsMemberRequest(long groupId, List<Long> userIds) {
		Validate.notNull(userIds);
		this.groupId = groupId;
		this.userIds = new ArrayList<Long>(userIds);
	}

	public long getGroupId() {
		return groupId;
	}

	public List<Long> getUserIds() {
		return Collections.unmodifiableList(userIds);
	}

	@Override
	public String toString() {
		return "VkontakteIsMemberRequest [groupId=" + groupId + ", userIds="
				+ userIds + "]";
	}	
}
