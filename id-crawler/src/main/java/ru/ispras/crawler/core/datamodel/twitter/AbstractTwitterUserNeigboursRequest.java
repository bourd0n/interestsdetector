package ru.ispras.crawler.core.datamodel.twitter;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} for downloading of Twitter user friends / followers.
 *
 * See:
 * https://dev.twitter.com/rest/reference/get/followers/ids
 * https://dev.twitter.com/rest/reference/get/friends/ids
 *
 * @author Ivan Andrianov
 * 
 */
public abstract class AbstractTwitterUserNeigboursRequest extends AbstractTwitterUserRequest {
	
	private final int limit;
	
	public AbstractTwitterUserNeigboursRequest(long id) {
		this(id, Integer.MAX_VALUE);
	}

	public AbstractTwitterUserNeigboursRequest(long id, int limit) {
		super(id);
		this.limit = limit;
	}

	public int getLimit() {
		return limit;
	}
}
