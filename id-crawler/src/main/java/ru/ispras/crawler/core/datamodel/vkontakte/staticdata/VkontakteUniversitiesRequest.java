package ru.ispras.crawler.core.datamodel.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 22.05.14
 * Time: 15:53
 *
 *  Request for https://vk.com/dev/database.getUniversities
 */
public class VkontakteUniversitiesRequest implements IRequest{
    private final long cityId;

    public VkontakteUniversitiesRequest(long cityId) {
        this.cityId = cityId;
    }

    public long getCityId() {
        return cityId;
    }

    @Override
    public int hashCode() {
        return (int) (cityId ^ (cityId >>> 32));
    }
    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VkontakteUniversitiesRequest other = (VkontakteUniversitiesRequest) obj;
		if (cityId != other.cityId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "VkontakteUniversitiesRequest [cityId=" + cityId + "]";
	}
}
