/*
 * Copyright 2012 Ryuji Yamashita
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.ispras.crawler.core.datamodel.facebook.api.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Ryuji Yamashita - roundrop at gmail.com, andrey g
 */
public class Place {
	private final String id;
	private final String name;
	private final Place.Location location;

	@JsonCreator
	public Place(
			@JsonProperty("id") String id,
			@JsonProperty("name") String name,
			@JsonProperty("location") Location location) {
		this.id = id;
		this.name = name;
		this.location = location;
	}
	
	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("location")
	public Location getLocation() {
		return location;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Place other = (Place) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public static class Location {
		private final String street;
		private final String city;
		private final String state;
		private final String country;
		private final String zip;
		private final Double latitude;
		private final Double longitude;
		private final String text;

		
		@JsonCreator
		public Location(
				@JsonProperty("street") String street,
				@JsonProperty("city") String city,
				@JsonProperty("state") String state,
				@JsonProperty("country") String country,
				@JsonProperty("zip") String zip,
				@JsonProperty("latitude") Double latitude,
				@JsonProperty("longitude") Double longitude,
				@JsonProperty("text") String text) {
			this.street = street;
			this.city = city;
			this.state = state;
			this.country = country;
			this.zip = zip;
			this.latitude = latitude;
			this.longitude = longitude;
			this.text = text;
		}

		@JsonProperty("street")
		public String getStreet() {
			return street;
		}

		@JsonProperty("city")
		public String getCity() {
			return city;
		}

		@JsonProperty("state")
		public String getState() {
			return state;
		}

		@JsonProperty("country")
		public String getCountry() {
			return country;
		}

		@JsonProperty("zip")
		public String getZip() {
			return zip;
		}

		@JsonProperty("latitude")
		public Double getLatitude() {
			return latitude;
		}

		@JsonProperty("longitude")
		public Double getLongitude() {
			return longitude;
		}
		@JsonProperty("text")
		public String getText() {
			return text;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((city == null) ? 0 : city.hashCode());
			result = prime * result
					+ ((country == null) ? 0 : country.hashCode());
			result = prime * result
					+ ((latitude == null) ? 0 : latitude.hashCode());
			result = prime * result
					+ ((longitude == null) ? 0 : longitude.hashCode());
			result = prime * result + ((state == null) ? 0 : state.hashCode());
			result = prime * result
					+ ((street == null) ? 0 : street.hashCode());
			result = prime * result + ((text == null) ? 0 : text.hashCode());
			result = prime * result + ((zip == null) ? 0 : zip.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Location other = (Location) obj;
			if (city == null) {
				if (other.city != null)
					return false;
			} else if (!city.equals(other.city))
				return false;
			if (country == null) {
				if (other.country != null)
					return false;
			} else if (!country.equals(other.country))
				return false;
			if (latitude == null) {
				if (other.latitude != null)
					return false;
			} else if (!latitude.equals(other.latitude))
				return false;
			if (longitude == null) {
				if (other.longitude != null)
					return false;
			} else if (!longitude.equals(other.longitude))
				return false;
			if (state == null) {
				if (other.state != null)
					return false;
			} else if (!state.equals(other.state))
				return false;
			if (street == null) {
				if (other.street != null)
					return false;
			} else if (!street.equals(other.street))
				return false;
			if (text == null) {
				if (other.text != null)
					return false;
			} else if (!text.equals(other.text))
				return false;
			if (zip == null) {
				if (other.zip != null)
					return false;
			} else if (!zip.equals(other.zip))
				return false;
			return true;
		}
		
		
	}
}
