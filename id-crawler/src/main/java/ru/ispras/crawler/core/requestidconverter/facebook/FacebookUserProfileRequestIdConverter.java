package ru.ispras.crawler.core.requestidconverter.facebook;

import ru.ispras.crawler.core.datamodel.facebook.FacebookUserProfileRequest;

public class FacebookUserProfileRequestIdConverter extends
		AbstractFacebookUserRequestIdConverter<FacebookUserProfileRequest> {
	@Override
	public FacebookUserProfileRequest getRequest(Long id) {
		return new FacebookUserProfileRequest(id);
	}
}
