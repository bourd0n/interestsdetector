package ru.ispras.crawler.core.datamodel.vkontakte.entities.users;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 15.05.14
 * Time: 15:05
 */
public class University {
    private final int id;
    private final int country;
    private final int city;
    private final String name;
    private final int faculty;
    private final String facultyName;
    private final int chair;
    private final String chairName;
    private final int graduation;

    @JsonCreator
    public University(
            @JsonProperty("id") int id,
            @JsonProperty("country") int country,
            @JsonProperty("city") int city,
            @JsonProperty("name") String name,
            @JsonProperty("faculty") int faculty,
            @JsonProperty("faculty_name") String facultyName,
            @JsonProperty("chair") int chair,
            @JsonProperty("chair_name") String chairName,
            @JsonProperty("graduation") int graduation) {
        this.id = id;
        this.country = country;
        this.city = city;
        this.name = name;
        this.faculty = faculty;
        this.facultyName = facultyName;
        this.chair = chair;
        this.chairName = chairName;
        this.graduation = graduation;
    }

	@JsonProperty("id")
	public int getId() {
		return id;
	}

	@JsonProperty("country")
	public int getCountry() {
		return country;
	}

	@JsonProperty("city")
	public int getCity() {
		return city;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("faculty")
	public int getFaculty() {
		return faculty;
	}

	@JsonProperty("faculty_name")
	public String getFacultyName() {
		return facultyName;
	}

	@JsonProperty("chair")
	public int getChair() {
		return chair;
	}

	@JsonProperty("chair_name")
	public String getChairName() {
		return chairName;
	}

	@JsonProperty("graduation")
	public int getGraduation() {
		return graduation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + chair;
		result = prime * result + city;
		result = prime * result + country;
		result = prime * result + faculty;
		result = prime * result + graduation;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		University other = (University) obj;
		if (chair != other.chair)
			return false;
		if (city != other.city)
			return false;
		if (country != other.country)
			return false;
		if (faculty != other.faculty)
			return false;
		if (graduation != other.graduation)
			return false;
		if (id != other.id)
			return false;
		return true;
	}
}
