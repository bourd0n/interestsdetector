package ru.ispras.crawler.core.requestidconverter.hunch;

import ru.ispras.crawler.core.datamodel.hunch.HunchUserFollowingRequest;

public class HunchUserFollowingRequestIdConverter extends
		AbstractHunchUserRequestIdConverter<HunchUserFollowingRequest> {
	@Override
	public HunchUserFollowingRequest getRequest(String screenName) {
		return new HunchUserFollowingRequest(screenName);
	}
}
