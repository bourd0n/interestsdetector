package ru.ispras.crawler.core.datamodel.vkontakte.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 20.05.14
 * Time: 12:41
 * 
 * Entity class that maps paged responses form vk-API
 */
public class ResponseList<T> {
    private final int count;
    private final List<T> items;

    @JsonCreator
    public ResponseList(
            @JsonProperty("count") int count,
            @JsonProperty("items") List<T> items) {
        this.count = count;
        this.items = items;
    }

    @JsonProperty("count")
    public int getCount() {
        return count;
    }

    @JsonProperty("items")
    public List<T> getItems() {
        return items;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + count;
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		return result;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponseList other = (ResponseList) obj;
		if (count != other.count)
			return false;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		return true;
	}
}
