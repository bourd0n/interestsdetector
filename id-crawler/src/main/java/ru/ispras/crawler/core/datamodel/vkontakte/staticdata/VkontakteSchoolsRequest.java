package ru.ispras.crawler.core.datamodel.vkontakte.staticdata;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 22.05.14
 * Time: 15:50
 *
 * Request for https://vk.com/dev/database.getSchools
 */
public class VkontakteSchoolsRequest implements IRequest{
    private final long cityId;

    public VkontakteSchoolsRequest(long cityId) {
        this.cityId = cityId;
    }

    public long getCityId() {
        return cityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VkontakteSchoolsRequest)) return false;

        VkontakteSchoolsRequest that = (VkontakteSchoolsRequest) o;

        return cityId != that.cityId;
    }


    @Override
    public int hashCode() {
        return (int) (cityId ^ (cityId >>> 32));
    }

    @Override
	public String toString() {
		return "VkontakteSchoolsRequest [cityId=" + cityId + "]";
	}
}
