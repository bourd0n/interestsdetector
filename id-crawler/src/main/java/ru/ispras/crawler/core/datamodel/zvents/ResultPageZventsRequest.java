package ru.ispras.crawler.core.datamodel.zvents;

import java.net.URL;

/**
 * {@link ZventsRequest} for downloading Zvents search result pages.
 * <p>
 * Example: http://www.zvents.com/search?st=event&swhere=San+Francisco%2C+CA
 * 
 * @author sysoev
 */
public class ResultPageZventsRequest extends ZventsRequest {

	public ResultPageZventsRequest(URL url) {
		super(url);
	}
}
