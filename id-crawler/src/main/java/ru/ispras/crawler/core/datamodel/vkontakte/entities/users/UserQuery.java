package ru.ispras.crawler.core.datamodel.vkontakte.entities.users;

import org.apache.commons.lang.Validate;


public class UserQuery {
	private String query;
	private SortType sort = SortType.POPULARITY;
	private Long countryId;
	private Long cityId;
	private Long hometownId;
	private Long universityCountryId;
	private Long universityId;
	private Integer universityYear;
	private Long universityFacultyId;
	private Long universityChairId;
	private Gender gender;
	private RelationshipStatus status;
	private Integer ageFrom;
	private Integer ageTo;
	private Integer birthDay;
	private Integer birthMonth;
	private Integer birthYear;
	private Long schoolCountryId;
	private Long schoolCityId;
	private Integer schoolClass;
	private Long schoolId;
	private Integer schoolYear;
	private String religion;
	private String interests;
	private String company;
	private String position;
	private Long groupId;
	
	public static enum SortType{
		DATE("1"), POPULARITY("0");
		private final String value;
		
		private SortType(String value){
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}
	
	public static enum RelationshipStatus{
		SINGLE("1"), MEETS("2"), ENGAGED("3"), MARRIED("4"), IN_LOVE("5"), COMPLICATED("6"), ACTTIVELY_SEARCHING("7");
		
		private final String value;
		
		private RelationshipStatus(String value){
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	public UserQuery(){
	}
	
	public UserQuery(UserQuery other) {
		super();
		this.query = other.query;
		this.sort = other.sort;
		this.countryId = other.countryId;
		this.cityId = other.cityId;
		this.hometownId = other.hometownId;
		this.universityCountryId = other.universityCountryId;
		this.universityId = other.universityId;
		this.universityYear = other.universityYear;
		this.universityFacultyId = other.universityFacultyId;
		this.universityChairId = other.universityChairId;
		this.gender = other.gender;
		this.status = other.status;
		this.ageFrom = other.ageFrom;
		this.ageTo = other.ageTo;
		this.birthDay = other.birthDay;
		this.birthMonth = other.birthMonth;
		this.birthYear = other.birthYear;
		this.schoolCountryId = other.schoolCountryId;
		this.schoolCityId = other.schoolCityId;
		this.schoolClass = other.schoolClass;
		this.schoolId = other.schoolId;
		this.schoolYear = other.schoolYear;
		this.religion = other.religion;
		this.interests = other.interests;
		this.company = other.company;
		this.position = other.position;
		this.groupId = other.groupId;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public SortType getSort() {
		return sort;
	}

	public void setSort(SortType sort) {
		Validate.notNull(sort);
		this.sort = sort;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Long getCityId() {
		return cityId;
	}

	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}

	public Long getHometownId() {
		return hometownId;
	}

	public void setHometownId(Long hometownId) {
		this.hometownId = hometownId;
	}

	public Long getUniversityCountryId() {
		return universityCountryId;
	}

	public void setUniversityCountryId(Long universityCountryId) {
		this.universityCountryId = universityCountryId;
	}

	public Long getUniversityId() {
		return universityId;
	}

	public void setUniversityId(Long universityId) {
		this.universityId = universityId;
	}

	public Integer getUniversityYear() {
		return universityYear;
	}

	public void setUniversityYear(Integer universityYear) {
		this.universityYear = universityYear;
	}

	public Long getUniversityFacultyId() {
		return universityFacultyId;
	}

	public void setUniversityFacultyId(Long universityFacultyId) {
		this.universityFacultyId = universityFacultyId;
	}

	public Long getUniversityChairId() {
		return universityChairId;
	}

	public void setUniversityChairId(Long universityChairId) {
		this.universityChairId = universityChairId;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public RelationshipStatus getStatus() {
		return status;
	}

	public void setStatus(RelationshipStatus status) {
		this.status = status;
	}

	public Integer getAgeFrom() {
		return ageFrom;
	}

	public void setAgeFrom(Integer ageFrom) {
		this.ageFrom = ageFrom;
	}

	public Integer getAgeTo() {
		return ageTo;
	}

	public void setAgeTo(Integer ageTo) {
		this.ageTo = ageTo;
	}

	public Integer getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Integer birthDay) {
		this.birthDay = birthDay;
	}

	public Integer getBirthMonth() {
		return birthMonth;
	}

	public void setBirthMonth(Integer birthMonth) {
		this.birthMonth = birthMonth;
	}

	public Integer getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(Integer birthYear) {
		this.birthYear = birthYear;
	}

	public Long getSchoolCountryId() {
		return schoolCountryId;
	}

	public void setSchoolCountryId(Long schoolCountryId) {
		this.schoolCountryId = schoolCountryId;
	}

	public Long getSchoolCityId() {
		return schoolCityId;
	}

	public void setSchoolCityId(Long schoolCityId) {
		this.schoolCityId = schoolCityId;
	}

	public Integer getSchoolClass() {
		return schoolClass;
	}

	public void setSchoolClass(Integer schoolClass) {
		this.schoolClass = schoolClass;
	}

	public Long getSchoolId() {
		return schoolId;
	}

	public void setSchoolId(Long schoolId) {
		this.schoolId = schoolId;
	}

	public Integer getSchoolYear() {
		return schoolYear;
	}

	public void setSchoolYear(Integer schoolYear) {
		this.schoolYear = schoolYear;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getInterests() {
		return interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ageFrom == null) ? 0 : ageFrom.hashCode());
		result = prime * result + ((ageTo == null) ? 0 : ageTo.hashCode());
		result = prime * result
				+ ((birthDay == null) ? 0 : birthDay.hashCode());
		result = prime * result
				+ ((birthMonth == null) ? 0 : birthMonth.hashCode());
		result = prime * result
				+ ((birthYear == null) ? 0 : birthYear.hashCode());
		result = prime * result + ((cityId == null) ? 0 : cityId.hashCode());
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime * result
				+ ((countryId == null) ? 0 : countryId.hashCode());
		result = prime * result + ((groupId == null) ? 0 : groupId.hashCode());
		result = prime * result
				+ ((hometownId == null) ? 0 : hometownId.hashCode());
		result = prime * result
				+ ((interests == null) ? 0 : interests.hashCode());
		result = prime * result
				+ ((position == null) ? 0 : position.hashCode());
		result = prime * result + ((query == null) ? 0 : query.hashCode());
		result = prime * result
				+ ((religion == null) ? 0 : religion.hashCode());
		result = prime * result
				+ ((schoolCityId == null) ? 0 : schoolCityId.hashCode());
		result = prime * result
				+ ((schoolClass == null) ? 0 : schoolClass.hashCode());
		result = prime * result
				+ ((schoolCountryId == null) ? 0 : schoolCountryId.hashCode());
		result = prime * result
				+ ((schoolId == null) ? 0 : schoolId.hashCode());
		result = prime * result
				+ ((schoolYear == null) ? 0 : schoolYear.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((sort == null) ? 0 : sort.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime
				* result
				+ ((universityChairId == null) ? 0 : universityChairId
						.hashCode());
		result = prime
				* result
				+ ((universityCountryId == null) ? 0 : universityCountryId
						.hashCode());
		result = prime
				* result
				+ ((universityFacultyId == null) ? 0 : universityFacultyId
						.hashCode());
		result = prime * result
				+ ((universityId == null) ? 0 : universityId.hashCode());
		result = prime * result
				+ ((universityYear == null) ? 0 : universityYear.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserQuery other = (UserQuery) obj;
		if (ageFrom == null) {
			if (other.ageFrom != null)
				return false;
		} else if (!ageFrom.equals(other.ageFrom))
			return false;
		if (ageTo == null) {
			if (other.ageTo != null)
				return false;
		} else if (!ageTo.equals(other.ageTo))
			return false;
		if (birthDay == null) {
			if (other.birthDay != null)
				return false;
		} else if (!birthDay.equals(other.birthDay))
			return false;
		if (birthMonth == null) {
			if (other.birthMonth != null)
				return false;
		} else if (!birthMonth.equals(other.birthMonth))
			return false;
		if (birthYear == null) {
			if (other.birthYear != null)
				return false;
		} else if (!birthYear.equals(other.birthYear))
			return false;
		if (cityId == null) {
			if (other.cityId != null)
				return false;
		} else if (!cityId.equals(other.cityId))
			return false;
		if (company == null) {
			if (other.company != null)
				return false;
		} else if (!company.equals(other.company))
			return false;
		if (countryId == null) {
			if (other.countryId != null)
				return false;
		} else if (!countryId.equals(other.countryId))
			return false;
		if (groupId == null) {
			if (other.groupId != null)
				return false;
		} else if (!groupId.equals(other.groupId))
			return false;
		if (hometownId == null) {
			if (other.hometownId != null)
				return false;
		} else if (!hometownId.equals(other.hometownId))
			return false;
		if (interests == null) {
			if (other.interests != null)
				return false;
		} else if (!interests.equals(other.interests))
			return false;
		if (position == null) {
			if (other.position != null)
				return false;
		} else if (!position.equals(other.position))
			return false;
		if (query == null) {
			if (other.query != null)
				return false;
		} else if (!query.equals(other.query))
			return false;
		if (religion == null) {
			if (other.religion != null)
				return false;
		} else if (!religion.equals(other.religion))
			return false;
		if (schoolCityId == null) {
			if (other.schoolCityId != null)
				return false;
		} else if (!schoolCityId.equals(other.schoolCityId))
			return false;
		if (schoolClass == null) {
			if (other.schoolClass != null)
				return false;
		} else if (!schoolClass.equals(other.schoolClass))
			return false;
		if (schoolCountryId == null) {
			if (other.schoolCountryId != null)
				return false;
		} else if (!schoolCountryId.equals(other.schoolCountryId))
			return false;
		if (schoolId == null) {
			if (other.schoolId != null)
				return false;
		} else if (!schoolId.equals(other.schoolId))
			return false;
		if (schoolYear == null) {
			if (other.schoolYear != null)
				return false;
		} else if (!schoolYear.equals(other.schoolYear))
			return false;
		if (gender != other.gender)
			return false;
		if (sort != other.sort)
			return false;
		if (status != other.status)
			return false;
		if (universityChairId == null) {
			if (other.universityChairId != null)
				return false;
		} else if (!universityChairId.equals(other.universityChairId))
			return false;
		if (universityCountryId == null) {
			if (other.universityCountryId != null)
				return false;
		} else if (!universityCountryId.equals(other.universityCountryId))
			return false;
		if (universityFacultyId == null) {
			if (other.universityFacultyId != null)
				return false;
		} else if (!universityFacultyId.equals(other.universityFacultyId))
			return false;
		if (universityId == null) {
			if (other.universityId != null)
				return false;
		} else if (!universityId.equals(other.universityId))
			return false;
		if (universityYear == null) {
			if (other.universityYear != null)
				return false;
		} else if (!universityYear.equals(other.universityYear))
			return false;
		return true;
	}
}
