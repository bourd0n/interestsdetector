package ru.ispras.crawler.core.connection.twitter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;

import ru.ispras.crawler.core.connection.IConnection;
import ru.ispras.crawler.core.connection.exception.TargetUnreachableException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;
import ru.ispras.crawler.core.datamodel.http.HttpResponse;
import ru.ispras.crawler.core.datamodel.twitter.AbstractTwitterUserRequest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.ispras.crawler.core.jackson.DefaultObjectMapperFactory;

/**
 * Abstract {@link IConnection} which contains common Twitter-related
 * utils for downloading (https://api.twitter.com/1.1/...)
 * 
 * @author Ivan Andrianov
 * 
 */
public abstract class AbstractTwitterConnection<Req extends IRequest, Resp extends IResponse> implements
		IConnection<Req, Resp> {
	private static final Logger logger = Logger.getLogger(AbstractTwitterConnection.class);

	private static final String API_URL = "https://api.twitter.com/1.1/";

	private final IConnection<HttpRequest, HttpResponse> twitterHttpConnection;

	private static final ObjectMapper objectMapper = new DefaultObjectMapperFactory().create();
	
	public AbstractTwitterConnection(IConnection<HttpRequest, HttpResponse> twitterHttpConnection) {
		this.twitterHttpConnection = twitterHttpConnection;
	}

	private String getHttpResponse(String method) throws UnavailableConnectionException {
		try {
			String url = API_URL + method;
			HttpRequest oauthRequest = new HttpRequest(new URL(url));
			logger.info("Twitter request: " + url);
			HttpResponse httpResponse = twitterHttpConnection.getResponse(oauthRequest);
			return httpResponse.getString();
		} catch (MalformedURLException e) {
			throw new TargetUnreachableException(e);
		}
	}
	
	protected <T> T getHttpResponse(String method, Class<T> clazz) throws UnavailableConnectionException {
		String httpResponse = getHttpResponse(method);
		T result = null;
		try {
			result = objectMapper.readValue(httpResponse, clazz);
		} catch (JsonParseException e) {
			throw new TargetUnreachableException(e);
		} catch (JsonMappingException e) {
			throw new IllegalStateException(e);
		} catch (IOException e) {
			throw new TargetUnreachableException(e);
		}
		return result;
	}
	
	protected <T> T getHttpResponse(String method, TypeReference<T> typeRef) throws UnavailableConnectionException {
		String httpResponse = getHttpResponse(method);
		try {
			return objectMapper.readValue(httpResponse, typeRef);
		} catch (JsonParseException e) {
			throw new TargetUnreachableException(e);
		} catch (JsonMappingException e) {
			throw new IllegalStateException(e);
		} catch (IOException e) {
			throw new TargetUnreachableException(e);
		}
	}	

	protected String getIdParameter(AbstractTwitterUserRequest userRequest) {
		return String.format("user_id=%d", userRequest.getId());
	}

	protected String getCursorParameter(Long cursor) {
		return String.format("cursor=%d", cursor != null ? cursor : -1);
	}

	protected String getMaxIdParameter(Long maxId) {
		return maxId != null ? String.format("&max_id=%d", maxId) : "";
	}
}
