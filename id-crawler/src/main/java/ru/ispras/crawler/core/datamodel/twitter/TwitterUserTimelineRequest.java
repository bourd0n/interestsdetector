package ru.ispras.crawler.core.datamodel.twitter;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * {@link IRequest} for downloading Twitter user timeline (recent tweets).
 *
 * See https://dev.twitter.com/rest/reference/get/statuses/user_timeline
 *
 * @author Ivan Andrianov
 * 
 */
public class TwitterUserTimelineRequest extends AbstractTwitterUserRequest {
	private final int limit;

	public TwitterUserTimelineRequest(long id) {
		this(id, Integer.MAX_VALUE);
	}

	public TwitterUserTimelineRequest(long id, int limit) {
		super(id);
		this.limit = limit;
	}
	
	public int getLimit() {
		return limit;
	}
}
