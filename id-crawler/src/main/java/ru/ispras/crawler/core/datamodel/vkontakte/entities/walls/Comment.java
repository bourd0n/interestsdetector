package ru.ispras.crawler.core.datamodel.vkontakte.entities.walls;

/**
 * Created with IntelliJ IDEA.
 * User: padre
 * Date: 19.05.14
 * Time: 15:57
 */

import java.util.List;

import ru.ispras.crawler.core.datamodel.vkontakte.entities.attachments.Attachment;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * see description in https://vk.com/dev/comment_object
 */
public class Comment {
    private final long id;
    private final Long fromId;
    private final Long date;
    private final String text;
    private final Long replyToUser;
    private final Long replyToComment;
    private final List<Attachment> attachments;
	private final Likes likes;

    @JsonCreator
    public Comment(
            @JsonProperty("id") long id,
            @JsonProperty("from_id") Long fromId,
            @JsonProperty("date") Long date,
            @JsonProperty("text") String text,
            @JsonProperty("reply_to_user") Long replyToUser,
            @JsonProperty("reply_to_comment") Long replyToComment,
            @JsonProperty("attachments") List<Attachment> attachments,
            @JsonProperty("likes") Likes likes) {
        this.id = id;
        this.fromId = fromId;
        this.date = date;
        this.text = text;
        this.replyToUser = replyToUser;
        this.replyToComment = replyToComment;
        this.attachments = attachments;
        this.likes = likes;
    }

    @JsonProperty("id")
    public long getId() {
        return id;
    }

    @JsonProperty("from_id")
    public Long getFromId() {
        return fromId;
    }

    @JsonProperty("date")
    public Long getDate() {
        return date;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("reply_to_user")
    public Long getReplyToUser() {
        return replyToUser;
    }

    @JsonProperty("reply_to_comment")
    public Long getReplyToComment() {
        return replyToComment;
    }

    @JsonProperty("attachments")
    public List<Attachment> getAttachments() {
        return attachments;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (date ^ (date >>> 32));
		result = prime * result + (int) (fromId ^ (fromId >>> 32));
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (date != other.date)
			return false;
		if (fromId != other.fromId)
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@JsonProperty("likes")
	public Likes getLikes() {
		return likes;
	}
}
