package ru.ispras.crawler.core.datamodel.batch;

import java.util.List;

import ru.ispras.crawler.core.datamodel.IRequest;

/**
 * Abstraction for entity classes which stores information about some collection
 * of requests to be performed at once
 * <p>
 * Implementations are expected to be immutable.
 * 
 * @author andrey g
 */
public interface IBatchRequest<Req extends IRequest> extends IRequest {
	public List<Req> getRequests();
}
