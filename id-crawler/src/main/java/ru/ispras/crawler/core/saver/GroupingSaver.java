package ru.ispras.crawler.core.saver;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import ru.ispras.crawler.core.datamodel.IRequest;
import ru.ispras.crawler.core.datamodel.IResponse;
import ru.ispras.crawler.core.datamodel.grouping.GroupingRequest;
import ru.ispras.crawler.core.datamodel.grouping.GroupingResponse;

/**
 * {@link ISaver} implementation which encapsulates a collection of
 * {@link ISaver}s per {@link IRequest} type.
 * <p>
 * For every {@link IRequest} type specified in {@link GroupingRequest} it finds
 * appropriate {@link ISaver} and feeds it with corresponding {@link IRequest}
 * and {@link IResponse}.
 * 
 * @author Ivan Andrianov
 * 
 */
public class GroupingSaver implements ISaver<GroupingRequest, GroupingResponse> {
	/**
	 * Configuration of {@link GroupingSaver} instances.
	 * <p>
	 * Provides a type-safe way to setup needed {@link ISaver} instances.
	 * 
	 * @author Ivan Andrianov
	 * 
	 */
	public static class Configuration {
		private final Map<Class<? extends IRequest>, ISaver<?, ?>> savers = new HashMap<>();

		public <Req extends IRequest> void put(Class<Req> requestType, ISaver<? super Req, ?> saver) {
			savers.put(requestType, saver);
		}
	}

	private final Map<Class<? extends IRequest>, ISaver<?, ?>> savers;

	public GroupingSaver(Configuration configuration) {
		savers = new HashMap<>(configuration.savers);
	}

	@Override
	public void save(GroupingRequest request, GroupingResponse response) {
		Set<Class<? extends IRequest>> requestTypes = response.getResponses().keySet();
		for (Class<? extends IRequest> requestType : requestTypes) {
			ISaver<IRequest, IResponse> saver = getSaver(requestType);
			saver.save(request.getRequest(requestType), response.getResponse(requestType));
		}
	}

	@SuppressWarnings("unchecked")
	private ISaver<IRequest, IResponse> getSaver(Class<? extends IRequest> requestType) {
		ISaver<IRequest, IResponse> saver = (ISaver<IRequest, IResponse>) savers.get(requestType);
		if (saver == null) {
			throw new IllegalStateException("No saver specified for request type: " + requestType.getName());
		}
		return saver;
	}
}
