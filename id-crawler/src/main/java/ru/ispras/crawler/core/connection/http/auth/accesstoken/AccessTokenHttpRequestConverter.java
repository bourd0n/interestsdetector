package ru.ispras.crawler.core.connection.http.auth.accesstoken;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.client.methods.HttpUriRequest;

import ru.ispras.crawler.core.connection.exception.TargetUnreachableException;
import ru.ispras.crawler.core.connection.exception.UnavailableConnectionException;
import ru.ispras.crawler.core.connection.http.ext.DefaultHttpRequestConverter;
import ru.ispras.crawler.core.connection.http.ext.IHttpRequestConverter;
import ru.ispras.crawler.core.datamodel.http.HttpRequest;

/**
 * {@link IHttpRequestConverter} for API methods that require signing with access token.
 * <p>
 * Adds access token at the end of URL and calls wrapped
 * {@link IHttpRequestConverter}.
 * 
 * @author Ivan Andrianov, andrey g
 * 
 */
public class AccessTokenHttpRequestConverter implements IHttpRequestConverter {
	private final IHttpRequestConverter wrapped;
	private final AccessTokenConfiguration configuration;
	private final String accessTokenFieldName;
	
	public AccessTokenHttpRequestConverter(AccessTokenConfiguration configuration, String accessTokenFieldName) {
		this(new DefaultHttpRequestConverter(), configuration, accessTokenFieldName);
	}

	public AccessTokenHttpRequestConverter(IHttpRequestConverter wrapped,
			AccessTokenConfiguration configuration, String acessTokenFieldName) {
		this.wrapped = wrapped;
		this.configuration = configuration;
		this.accessTokenFieldName = acessTokenFieldName; 
	}

	@Override
	public HttpUriRequest convert(HttpRequest request) throws UnavailableConnectionException {
		return wrapped.convert(addToken(request));
	}

	private HttpRequest addToken(HttpRequest request) {
		try {
			String uri = request.getURL().toExternalForm();
			String token = configuration.getToken();
			if (uri.indexOf('?') < 0) {
				uri = uri + "?" + accessTokenFieldName + "=" + token;
			} else {
				uri = uri + "&" + accessTokenFieldName + "=" + token;
			}
			return new HttpRequest(new URL(uri));
		} catch (MalformedURLException e) {
			throw new TargetUnreachableException(e);
		}
	}
}
